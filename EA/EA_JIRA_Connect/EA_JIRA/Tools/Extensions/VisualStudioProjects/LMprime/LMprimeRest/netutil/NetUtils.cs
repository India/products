﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: NetUtils.cs

   Description: This File contains utility functions related to network.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.constants;

namespace LMprimeRest.netutil
{
    class NetUtils
    {
        /*  Function for getting property type, if the type of property is string then it returns type as string
         *  If it is class type then it returns type as object.
         **/
        public int getPropertyType(Object obj, String strPropName)
        {
            int iPropertyType = -1;
            
            try
            {
                if (obj != null && strPropName != null && strPropName.Length > 0)
                {
                    Type type = obj.GetType();

                    System.Reflection.PropertyInfo propertyInfo = type.GetProperty(strPropName);

                    if (propertyInfo != null)
                    {
                        if (propertyInfo.PropertyType == typeof(string))
                        {
                            iPropertyType = Constants.TYPE_STRING;
                        }
                        else if (propertyInfo.PropertyType.IsClass)
                        {
                            iPropertyType = Constants.TYPE_OBJECT;
                        }
                    }
                }

                return iPropertyType;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



