﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: RestGeneralReqAndRespMain.cs

   Description: This File is specific for dealing general requests and responces.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeRest.restgeneraReqAndResp
{
    public class RestGeneralReqAndRespMain
    {
        RestGetCompReqandResp restGetComp = new RestGetCompReqandResp();

        /*  This function is starting point for getting components list */
        public List<string> getComponentsList(string strProjectKey)
        {
            List<string> lCompList = new List<string>();
            
            try
            {
                if (strProjectKey != null && strProjectKey.Length > 0)
                {
                    lCompList = restGetComp.getComponentsListReq(strProjectKey);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return lCompList;
        }
    }
}



