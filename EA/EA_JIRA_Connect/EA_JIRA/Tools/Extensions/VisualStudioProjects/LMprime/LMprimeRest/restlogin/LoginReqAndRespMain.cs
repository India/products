﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoginReqAndRespMain.cs

   Description: This file is entry point for login requests and responces.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.constants;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using Newtonsoft.Json;
using LMprimeRest.parsers;
using System.Runtime.Serialization.Json;

namespace LMprimeRest.restlogin
{
    public class LoginReqAndRespMain
    {
        NetworkMain netObj                          = NetworkMain.getInstance();
        LoginProjListReqAndResp lprjReqAndResp      = new LoginProjListReqAndResp();
        LoginIssueTypeReqAndResp issueTypReqAndResp = new LoginIssueTypeReqAndResp();

        /* This function is starting point for entire Login process, On Success completion it returns a 
         * Dicitionary containing project key as project name and corresponding list of issue types as its value.
         * <key,value> = <Project name, <Bug,Story,Epic>
         * 
         * */
        public Dictionary<string, List<string>> performLoginOperation(List<string> ltPrjList)
        {
            Dictionary<string, List<string>> lProjectAndIssueType = new Dictionary<string, List<string>>();

            try
            {
                List<string> ltPrj = null;

                /*  If input project list is provided then not requesting for project list,
                 * the only source from where the input project list has been genrated is mapping file */
                if(ltPrjList != null && ltPrjList.Count > 0)
                {
                    ltPrj = ltPrjList;
                }
                else
                {
                    ltPrj = lprjReqAndResp.sendProjectListRequest();
                }

                if (ltPrj == null || ltPrj.Count == 0)
                {
                    throw new Exception(Constants.NO_PROJECT_AVAILABLE);
                }

                for (int iDx = 0; iDx < ltPrj.Count; iDx++)
                {
                    List<string> lIssue = new List<string>();

                    lIssue = issueTypReqAndResp.sendIssueListRequest(ltPrj[iDx]);
                    /*   Not if User is not having configured Issue type for a project then that project is not added to list */
                    if (lIssue != null && lIssue.Count > 0)
                    {
                        lProjectAndIssueType.Add(ltPrj[iDx], lIssue);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            
            return lProjectAndIssueType;
        }

        /*  This function is starting point for getting user list corresponnding to project */
        public List<string> getUsersList(string strProjectKey)
        {
            List<string> temp = new List<string>();

            try
            {
                LoginProjectUsersReqAndResp lprUserMain = new LoginProjectUsersReqAndResp();

                temp = lprUserMain.sendProjUserReq(strProjectKey);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return temp;
        }

        /*  This function is starting point for getting user list corresponding to project,
         *  Priority are not binded with project.*/
        public List<string> getPriorityList()
        {
            List<string> temp = new List<string>();

            try
            {
                LoginPriorityReqAndResp lPriorityReqAndResp = new LoginPriorityReqAndResp();

                temp = lPriorityReqAndResp.sendPriorityReq();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        
            return temp;
        }
        
    }
}







