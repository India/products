﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: RestGetCompReqandResp.cs

   Description: This File is specific for Coponents rest requests and responces.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using LMprimeRest.constants;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using LMprimeRest.parsers;

namespace LMprimeRest.restgeneraReqAndResp
{
    public class RestGetCompReqandResp
    {
        /*  Function that initiates Components List request */
        public List<string> getComponentsListReq(string strProj)
        {
            List<string> temp                    = null;
            CreateAndSendHttpReq httpRequest     = new CreateAndSendHttpReq();
            ParseJson pjson                      = new ParseJson();
            List<ProjComponents> lProjComponents = new List<ProjComponents>();

            try
            {
                /*  Sending Request to get Components 
                 *  It returns the response in string.
                 *  This string needs to be parse and Components are extracted from it and a list is formed from them
                 */
                string strResponse = httpRequest.sendHttpRequest(Constants.COMP_REQUEST + strProj + Constants.COMPONENTS, null, null, Constants.GET);

                lProjComponents = pjson.JsonDeserialize<List<ProjComponents>>(strResponse);

                temp = processComponentsResponse(lProjComponents);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return temp;
        }

        /*  This function processs Component Response */
        public List<string> processComponentsResponse(List<ProjComponents> lProjComponents)
        {
            List<string> lComponents = new List<string>();
            
            try
            {
                if (lProjComponents != null && lProjComponents.Count > 0)
                {
                    for (int iDx = 0; iDx < lProjComponents.Count; iDx++)
                    {
                        lComponents.Add(lProjComponents[iDx].name);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return lComponents;
        }
    }
}





