﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: NetworkMain.cs

   Description: This file is contains API related to Network.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using LMprimeRest.netutil;
using LMprimeRest.constants;
using System.Text.RegularExpressions;


namespace LMprimeRest
{
    public class NetworkMain
    {
        /*  This class static instance of itself   */
        static NetworkMain netMain = null;
        public string strUserName = null;
        public string strPassword = null;
        public string strJIRAUrl = null;
        public string strSDKUrl = null;
        public Atlassian.Jira.Jira objJira = null;

        /*  Function to get static instance, Reason for having static instance is that because it will have static data 
         *  This function is static because it can be directly invoked by class name.
         */
        public static NetworkMain getInstance()
        {
            if (netMain == null)
            {
                netMain = new NetworkMain();
            }

            return netMain;
        }

        /*  Function to initialize network variable . This function takes JIRA Username, JiraPassword, and Jira base Url and initializes it to network static object*/
        public void initializeNetworkData(string strUserName, string strPassword, string strJiraBaseUrl)
        {
            if (netMain == null)
            {
                netMain = getInstance();
            }

            /*  Storing static information at network   */
            netMain.strUserName = strUserName;

            netMain.strPassword = strPassword;

            strJiraBaseUrl = correctURLending(strJiraBaseUrl);

            netMain.strJIRAUrl = strJiraBaseUrl;
            
            createJiraRestClient();
        }

        /*  Function to create Jira rest client.This client will be used for handling OTB atlassian sdk requests */
        public void createJiraRestClient()
        {
            try
            {
                netMain.strSDKUrl = correctURLending(netMain.strJIRAUrl);

                netMain.objJira = Atlassian.Jira.Jira.CreateRestClient(netMain.strJIRAUrl, netMain.strUserName, netMain.strPassword);
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public string correctURLending(string strUrl)
        {
            string strOutUrl = "";

            if (strUrl != null && strUrl.Count() > 0)
            {
                int iSize = strUrl.Length;

                if (strUrl[iSize - 1].CompareTo(Constants.FWD_SLASH) == 0)
                {
                    strOutUrl = strUrl;
                }
                else
                {
                    strOutUrl = strUrl + Constants.FWD_SLASH;
                }
            }
            return strOutUrl;
        }

        /*  For removing any unwanted special characters form URL */
        public string removeHTMLFormating(string strInpString)
        {
            string strOutString = "";

            if (strInpString != null)
            {
                strOutString = Regex.Replace(strInpString, "<.*?>", String.Empty);
            }
            return strOutString;
        }

        /*  To cretae JIRA Issue    */

        public Issue createJiraIssue
        (
            string strIssueType,
            string strProject,
            string strAssignee,
            string strPriority,
            string strComponents,
            Dictionary<string, string> issueDict,
            List<String> lLinkIssueList,
            string strParentKey,
            string strOperation
        )
        {
            String issueId     = "";        /*  Output parameter    */
            Issue issue        = null;
            NetUtils netUtils  = new NetUtils();
            List<string> label = new List<string>();

            try
            {

                if (strOperation != null && strOperation.Length > 0 && strOperation.CompareTo(Constants.CREATE_IN_CONTEXT) == 0 && strParentKey != null && strParentKey.Length > 0)
                {
                    issue = netMain.objJira.GetIssue(strParentKey);
                }
                else
                {

                    if (strIssueType == null || strIssueType.Length <= 0 || strProject == null || strProject.Length <= 0 || issueDict == null)
                    {
                        throw new Exception("Invalid Parameters for Request");
                    }

                    if (strIssueType.CompareTo(Constants.SUB_TASK) == 0)
                    {
                        issue = netMain.objJira.CreateIssue(strProject, strParentKey);
                    }
                    else
                    {
                        issue = netMain.objJira.CreateIssue(strProject);
                    }

                    if (issue != null)
                    {
                        foreach (string key in issueDict.Keys)
                        {
                            string value = "";

                            issueDict.TryGetValue(key, out value);

                            Type type = issue.GetType();

                            System.Reflection.PropertyInfo propertyInfo = type.GetProperty(key);

                            if (propertyInfo != null)
                            {
                                int iPtype = netUtils.getPropertyType(issue, key);

                                if (iPtype == Constants.TYPE_STRING)
                                {
                                    if (propertyInfo.CanWrite && propertyInfo != null)
                                    {
                                        /*  To check whether it is possible to set Jira Property or not */
                                        value = removeHTMLFormating(value);

                                        propertyInfo.SetValue(issue, value, null);
                                    }
                                    else
                                    {
                                        throw new Exception("Write access not available on " + key + "Jira Property");
                                    }
                                }
                                else if (iPtype == Constants.TYPE_OBJECT)
                                {
                                    SetPropertyObject sPropObj = new SetPropertyObject();

                                    String[] splitedString = value.Split(Constants.VAL_SEP);

                                    for (int iFx = 0; iFx < splitedString.Count(); iFx++)
                                    {
                                        if (key.CompareTo(Constants.LABELS) == 0)
                                        {
                                            if (!label.Contains(splitedString[iFx]))
                                            {
                                                if (splitedString[iFx].Contains(" ")) ;
                                                {
                                                    splitedString[iFx] = Regex.Replace(splitedString[iFx], @"\s", "");

                                                    splitedString[iFx] = removeHTMLFormating(splitedString[iFx]);
                                                }
                                                label.Add(splitedString[iFx]);
                                            }
                                        }
                                        else
                                        {
                                            splitedString[iFx] = removeHTMLFormating(splitedString[iFx]);

                                            sPropObj.setPropertyObject(key, splitedString[iFx], ref issue);
                                        }
                                    }

                                }
                            }
                        }

                        issue.Type = strIssueType;

                        if (strAssignee != null && strAssignee.Length > 0)
                        {
                            issue.Assignee = strAssignee;
                        }

                        if (strPriority != null && strPriority.Length > 0)
                        {
                            issue.Priority = strPriority;
                        }

                        if (strComponents != null && strComponents.Length > 0)
                        {
                            issue.Components.Add(strComponents);
                        }

                        issue.SaveChanges();


                        if (label.Count() > 0)
                        {
                            string[] labelArray = label.ToArray();

                            issue.SetLabelsAsync(labelArray);

                            labelArray = null;

                            label.Clear();
                        }
                    }
                 
                } // Operation type condition ends

                if (lLinkIssueList != null && lLinkIssueList.Count > 0)
                {
                    for (int iDx = 0; iDx < lLinkIssueList.Count; iDx++)
                    {
                        issue.LinkToIssue(lLinkIssueList[iDx], Constants.RELATES);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return issue;
        }
    }
}
