﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateAndSendHttpReq.cs

   Description: This File contains API's for creating and sending http request.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using LMprimeRest.constants;
using LMprimeRest.credentials;

namespace LMprimeRest.httpreq
{
    class CreateAndSendHttpReq
    {
        /**
         * Description : This function is responsible for sending HTTP Requests. It creates http request,
         *               sets content type, set request method (Get/Post), Addes header to request, Passes
         *               Encoded credentials and receives responce.
         *              
         * @param strReqType   :   Request type(for project,creeating issues etc)
         * @param strArgument  :   Query Paramenters if any
         * @param strData      :   Json data for post Requests
         * @param strMethod    :   Method for requests GET/POST etc
         */
        public string sendHttpRequest
         (
             string strReqType,
             string strArgument,
             string strData,
             string strMethod
         )
        {
            HttpWebRequest request      = null;
            HttpWebResponse response    = null;
            NetworkMain netObj          = NetworkMain.getInstance();
            EncodeCredentials encodCred = new EncodeCredentials();
            string strResponceString    = null;
            string strBase64Credentials = null;
            string strUrl               = null;
           
            try
            {
                /*  Creating url for requests which is followed by query parameters   */
                strUrl = createUrlWithQueryParameters(strReqType, strArgument);

                HttpReqUtil hreqUtil = new HttpReqUtil(strUrl);

                /*  Creating Requests   */
                request = hreqUtil.createHttpRequest(strReqType);

                /*  Setting Content Type    */
                hreqUtil.setContentType(ref request, Constants.APPLICATIONJSON);

                /*  Setting Method type of Request  */
                hreqUtil.setMethod(ref request, strMethod);

                /*  Getting Encoded coded Credentials */
                strBase64Credentials = encodCred.getEncodedCredentials();

                /*  Adding Header Node  */
                hreqUtil.addHeader(ref request, Constants.AUTHORIZATION, Constants.BASIC + " " + strBase64Credentials);

                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                
                /*  Converting request into streams */
                if (strData != null)
                {
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                    {
                        writer.Write(strData);
                    }
                }

                /*  Sending request and waiting for responce */
                response = request.GetResponse() as HttpWebResponse;

                /*  Genrates responce string and from receive responce object */
                strResponceString = generateResponceString(response);

                return strResponceString;
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }

        /*  This Function generates String from responce stream .
         *  This string will be further parsed to fil Objects.
         */
        private string generateResponceString(HttpWebResponse response)
        {
            string strResponceString = null;

            try
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responceStream = response.GetResponseStream();
                    
                    StreamReader strReader = new StreamReader(responceStream);
                    
                    strResponceString = strReader.ReadToEnd();
                }

                return strResponceString;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function creates URL with query parameters, This function Query Parameters to request,
         *  For every request to JIRA "rest/api/2" this should be appended. This function takes  
         *  care of appending it.It takes object from network and extracts URL from it.
         */
        private string createUrlWithQueryParameters(string strReqType, string strArgument)
        {
            NetworkMain netObj = NetworkMain.getInstance();
            string sstrUrl     = netObj.strJIRAUrl;
            string strUrl      = null;

            try
            {
                sstrUrl = sstrUrl + Constants.JIRA_ENDING_URL;

                strUrl = string.Format("{0}/{1}", sstrUrl, strReqType);

                if (strArgument != null)
                {
                    strUrl = string.Format("{0}{1}/", strUrl, strArgument);
                    
                    strUrl = strUrl.Remove(strUrl.Length - 1);
                }

                return strUrl;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
     }
}






