﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: HttpReqUtil.cs

   Description: This File contains API's for creating http request.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace LMprimeRest.httpreq
{
    class HttpReqUtil
    {
        String strUrl = null;

        /*  Defaut Consstructor */
        public HttpReqUtil(String strUrl)
        {
            this.strUrl = strUrl;
        }

        /*  This Methods creates Http Web requests  */
        public HttpWebRequest createHttpRequest(string strRequestType)
        {
            HttpWebRequest request = null;

            try
            {
                if (strUrl != null && strUrl.Length > 0 && strRequestType != null && strRequestType.Length > 0)
                {
                    request = WebRequest.Create(strUrl) as HttpWebRequest;
                }

                return request;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This Methods sets Content Type  */
        public void setContentType(ref HttpWebRequest request, string strContentType)
        {
            try
            {
                if (request != null && strContentType != null && strContentType.Length > 0)
                {
                    request.ContentType = strContentType;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This Method sets method for Request */
        public void setMethod(ref HttpWebRequest request, string strMethodType)
        {
            try
            {
                if (request != null && strMethodType != null && strMethodType.Length > 0)
                {
                    request.Method = strMethodType;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This method adds headers    */
        public void addHeader(ref HttpWebRequest request, string strHeaderKey, string strHeaderValue)
        {
            try
            {
                if (request != null && strHeaderKey != null && strHeaderKey.Length > 0 && strHeaderValue != null && strHeaderValue.Length > 0)
                {
                    request.Headers.Add(strHeaderKey, strHeaderValue);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



