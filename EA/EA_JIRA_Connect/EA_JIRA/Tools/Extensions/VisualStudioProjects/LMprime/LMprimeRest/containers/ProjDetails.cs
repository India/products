﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ProjDetails.cs

   Description: This File is container for complete project details.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeRest.containers
{
       
    public class Lead
    {
        public string self { get; set; }
        public string key { get; set; }
        public string accountId { get; set; }
        public string name { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
    }

    public class IssueType
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public bool subtask { get; set; }
        public int avatarId { get; set; }
    }

    public class Roles
    {
        public string __invalid_name__atlassian_addons_project_access { get; set; }
        public string __invalid_name__Service_Desk_Team { get; set; }
        public string __invalid_name__Service_Desk_Customers { get; set; }
        public string Administrators { get; set; }
        public string __invalid_name__Tempo_Project_Managers { get; set; }
    }

    public class AvatarUrls
    {
        public string __invalid_name__48x48 { get; set; }
        public string __invalid_name__24x24 { get; set; }
        public string __invalid_name__16x16 { get; set; }
        public string __invalid_name__32x32 { get; set; }
    }

    public class ProjDetails
    {
        public string expand { get; set; }
        public string self { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string description { get; set; }
        public Lead lead { get; set; }
        public List<object> components { get; set; }
        public List<IssueType> issueTypes { get; set; }
        public string assigneeType { get; set; }
        public List<object> versions { get; set; }
        public string name { get; set; }
        public Roles roles { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public string projectTypeKey { get; set; }
    }
}

