﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ProjComponents.cs

   Description: This File is container for Project Components.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeRest.containers
{
    public class ProjComponents
    {
        public string self { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string assigneeType { get; set; }
        public string realAssigneeType { get; set; }
        public bool isAssigneeTypeValid { get; set; }
        public string project { get; set; }
        public int projectId { get; set; }
        public string description { get; set; }
        
    }
}



