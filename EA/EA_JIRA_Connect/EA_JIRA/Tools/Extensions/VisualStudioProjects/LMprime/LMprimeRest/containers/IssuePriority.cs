﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: IssuePriority.cs

   Description: This File is container for Priorities related to issue.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeRest.containers
{
    public class IssuePriority
    {
        public string self { get; set; }
        public string statusColor { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }
}
