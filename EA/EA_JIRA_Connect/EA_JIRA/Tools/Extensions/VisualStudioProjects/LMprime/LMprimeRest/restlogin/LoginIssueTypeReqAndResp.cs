﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoginIssueTypeReqAndResp.cs

   Description: This file is contains API for issue type request.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using LMprimeRest.parsers;
using LMprimeRest.constants;

namespace LMprimeRest.restlogin
{
    class LoginIssueTypeReqAndResp
    {
        /*  Function that initiates project list request    */
        public List<string> sendIssueListRequest(string strProjKey)
        {
            List<string> ltTemp              = null;
            CreateAndSendHttpReq httpRequest = new CreateAndSendHttpReq();
            ParseJson pjson                  = new ParseJson();
            ProjDetails projDetails          = new ProjDetails();

            try
            {
                /*  Sending Request to get project list 
                 *  It returns the response in string.
                 *  This string needs to be parse and project keys should be taken from it and is to be placed in list.
                 */
                string strResponse = httpRequest.sendHttpRequest(Constants.PROJECT_REQUEST + "/"+ strProjKey, null, null, Constants.GET);

                projDetails = pjson.JsonDeserialize<ProjDetails>(strResponse);

                ltTemp = processIssueTypeListResponse(projDetails);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltTemp;
        }

        /*  This creates a list of issue tyes from Project detail Object and returns it */
        public List<string> processIssueTypeListResponse(ProjDetails projDetails)
        {
            List<string> ltIssueTyp = new List<string>();

            try
            {
                if (projDetails != null && projDetails.issueTypes != null && projDetails.issueTypes.Count > 0)
                {
                    for (int iDx = 0; iDx < projDetails.issueTypes.Count; iDx++)
                    {
                        if (projDetails.issueTypes[iDx] != null)
                        {
                            ltIssueTyp.Add(projDetails.issueTypes[iDx].name);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltIssueTyp;
        }
    }
}



