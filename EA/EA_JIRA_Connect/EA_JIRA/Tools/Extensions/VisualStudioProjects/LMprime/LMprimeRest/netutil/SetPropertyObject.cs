﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: HttpReqUtil.cs

   Description: This File contains API's for creating http request.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.constants;

namespace LMprimeRest.netutil
{
    class SetPropertyObject
    {
        /*  This function sets property to object of JIRA Issue. Certain objects in JIRA ar not updated as 
         *  string to property of Issue object, they are to be updated as object. This function is specific
         *  to updation of those objects.
         * */

        public void setPropertyObject(String szPropName, String szPropValue, ref Atlassian.Jira.Issue obj)
        {
            try
            {
                if (szPropName.CompareTo(Constants.COMPONENTS_PROP) == 0)
                {
                    obj.Components.Add(szPropValue);
                }
                if (szPropName.CompareTo(Constants.FIX_VERSION) == 0)
                {
                    obj.FixVersions.Add(szPropValue);
                }
                if (szPropName.CompareTo(Constants.AFFECTS_VERSION) == 0)
                {
                    obj.AffectsVersions.Add(szPropValue);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }
    }
}



