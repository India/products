﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: IssueDetailReqAndRespMain.cs

   Description: This file is contains API for getting issue details.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.httpreq;
using LMprimeRest.parsers;
using LMprimeRest.constants;
using Newtonsoft.Json;
using Atlassian.Jira;

namespace LMprimeRest.restissuedetail
{
    public class IssueDetailReqAndRespMain
    {

        NetworkMain netObj = NetworkMain.getInstance();

        /*  To get Issue details form JIRA, It uses Atlassian SDK, It takes input JIRA ID as argument */

        public Issue getIssueDetailsFromJira(string szIssueId)
        {
            Issue issue = null;

            try
            {
                netObj.createJiraRestClient();

                issue = netObj.objJira.GetIssue(szIssueId);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return issue;
        }
    }
}

