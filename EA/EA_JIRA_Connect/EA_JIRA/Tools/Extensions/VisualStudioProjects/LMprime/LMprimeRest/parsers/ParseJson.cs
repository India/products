﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseJson.cs

   Description: This File contains functions for parsing response JSON.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using System.IO;

namespace LMprimeRest.parsers
{
    class ParseJson
    {
    
        /*   Defaullt Constructor    */
        public ParseJson()
        {
        }

        /* This method is used for deserializing JSON into class object type,
         * It takes json string as argument and and serializing object type
         * On successful seerialization it returns the required Object.
         */
        public inpObjType JsonDeserialize<inpObjType>(string strJsonString)
        {
            try
            {
                DataContractJsonSerializer serialize = new DataContractJsonSerializer(typeof(inpObjType));

                MemoryStream mStream = new MemoryStream(Encoding.UTF8.GetBytes(strJsonString));

                inpObjType outObj = (inpObjType)serialize.ReadObject(mStream);

                return outObj;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



