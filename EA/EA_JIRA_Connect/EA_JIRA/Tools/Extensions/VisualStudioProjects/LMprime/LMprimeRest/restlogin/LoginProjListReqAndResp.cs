﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoginProjListReqAndResp.cs

   Description: This file is contains API For project list request.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.parsers;
using LMprimeRest.constants;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;

namespace LMprimeRest.restlogin
{
    class LoginProjListReqAndResp
    {
        /*  Function that initiates project list request    */
        public List<string> sendProjectListRequest()
        {
            List<string> ltTemp              = null;
            CreateAndSendHttpReq httpRequest = new CreateAndSendHttpReq();
            List<ProjectInfo> ltProjectInfo  = new List<ProjectInfo>();
            ParseJson pjson                  = new ParseJson();

            try
            {
                /*  Sending Request to get project list 
                 *  It returns the response in string.
                 *  This string needs to be parse and project keys should be taken from it and is to be placed in list.
                 */
                string strResponse = httpRequest.sendHttpRequest(Constants.PROJECT_REQUEST, null, null,Constants.GET);

                ltProjectInfo = pjson.JsonDeserialize<List<ProjectInfo>>(strResponse);

                ltTemp = processProjectListResponse(ltProjectInfo);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltTemp;
        }

        /*  This function creates a list of project keys */
        public List<string> processProjectListResponse(List<ProjectInfo> ltPinfo)
        {
            List<string> ltPrjKeys = new List<string>();

            try
            {
                if (ltPinfo != null && ltPinfo.Count > 0)
                {
                    for (int iDx = 0; iDx < ltPinfo.Count; iDx++)
                    {
                        if (ltPinfo[iDx] != null)
                        {
                            ltPrjKeys.Add(ltPinfo[iDx].key);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltPrjKeys;
        }
    }
}



