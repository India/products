﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EncodeCredentials.cs

   Description: This File is specific to encoding of credentials.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest;

namespace LMprimeRest.credentials
{
    class EncodeCredentials
    {
        /*  This function returns encoded crendentials, It takes object from network, takes credentials from it,
         *  It encodes it ans returns it as string.
         */
        public string getEncodedCredentials()
        {
            string strEncodedCreden = null;

            try
            {
                NetworkMain netObj = NetworkMain.getInstance();

                /*  Merged Username and Password for encoding */
                string mergedCredentials = string.Format("{0}:{1}", netObj.strUserName, netObj.strPassword);

                /*  Encoding them in UTF - 8 */
                byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);

                strEncodedCreden = Convert.ToBase64String(byteCredentials);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return strEncodedCreden;
        }
    }
}



