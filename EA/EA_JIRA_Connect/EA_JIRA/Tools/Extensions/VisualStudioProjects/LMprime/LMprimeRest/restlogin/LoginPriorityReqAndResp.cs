﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoginPriorityReqAndResp.cs

   Description: This file is contains API For priority request.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using LMprimeRest.parsers;
using LMprimeRest.constants;

namespace LMprimeRest.restlogin
{
    class LoginPriorityReqAndResp
    {
        /*  Function that initiates project list request    */
        public List<string> sendPriorityReq()
        {
            List<string> ltemp               = null;
            CreateAndSendHttpReq httpRequest = new CreateAndSendHttpReq();
            ParseJson pjson                  = new ParseJson();
            List<IssuePriority> ltPriority   = new List<IssuePriority>();

            try
            {
                /*  Sending Request to get priority list 
                 *  It returns the response in string.
                 *  This string needs to be parse and prioriy List should be taken from it.
                 */
                string strResponse = httpRequest.sendHttpRequest(Constants.PRIORITY_REQUEST + Constants.FWD_SLASH, null, null, Constants.GET);

                ltPriority = pjson.JsonDeserialize<List<IssuePriority>>(strResponse);

                ltemp = processPriorityResponse(ltPriority);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltemp;
        }

        /*  This function extracts priority name from issue priority object and creates its list and returns */

        public List<string> processPriorityResponse(List<IssuePriority> lpriority)
        {
            List<string> lPriorityList = new List<string>();

            try
            {
                if (lpriority != null && lpriority.Count > 0)
                {
                    for (int iDx = 0; iDx < lpriority.Count; iDx++)
                    {
                        if (lpriority[iDx] != null)
                        {
                            lPriorityList.Add(lpriority[iDx].name);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return lPriorityList;
        }
    }
}



