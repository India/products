﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoginProjectUsersReqAndResp.cs

   Description: This file is contains API For project users request.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeRest.httpreq;
using LMprimeRest.containers;
using LMprimeRest.parsers;
using LMprimeRest.constants;

namespace LMprimeRest.restlogin
{
    class LoginProjectUsersReqAndResp
    {
        public List<string> sendProjUserReq(string strProjKey)
        {
                List<string> ltTemp               = null;
                CreateAndSendHttpReq httpRequest  = new CreateAndSendHttpReq();
                ParseJson pjson                   = new ParseJson();
                List<ProjUsers> ltProjUsers       = new List<ProjUsers>();

                try
                {
                    /*
                     * Sends the request to get users in project
                     * */
                    string strResponse = httpRequest.sendHttpRequest(Constants.USER_REQUEST + strProjKey, null, null, Constants.GET);

                    ltProjUsers = pjson.JsonDeserialize<List<ProjUsers>>(strResponse);

                    ltTemp = processUserList(ltProjUsers);

                    return ltTemp;
                }
                catch (Exception exception)
                {
                    throw exception;
                }
         }

        /*  This function finds out project user names from Project User object, and creates a list of names and returns it */
        public List<string> processUserList(List<ProjUsers> lProjUsers)
        {
            List<string> lPrjUsers = new List<string>();

            try
            {
                if (lProjUsers != null && lProjUsers.Count > 0)
                {
                    for (int iDx = 0; iDx < lProjUsers.Count; iDx++)
                    {
                        if (lProjUsers[iDx] != null)
                        {
                            lPrjUsers.Add(lProjUsers[iDx].name);
                        }
                    }
                }

                return lPrjUsers;
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }
    }
}
