﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Constants.cs

   Description: This File contains constants specific to this DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-08-2017    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeRest.constants
{
    class Constants
    {
        /*  Request and Responce Constants */
        public static string GET                  = "GET";
        public static string POST                 = "POST";
        public static string APPLICATIONJSON      = "application/json";
        public static string AUTHORIZATION        = "Authorization";
        public static string BASIC                = "Basic";

        /*  Error Constants     */
        public static string NO_PROJECT_AVAILABLE = "No project available in JIRA for current user.Either Configure projects for the current user or login with other user";
        public static string NO_ISSUE_TYPE_EXISTS = "No Issue type is availaible for logged in user.Either Configure Issue Types for the current user or login with other user";

        /*   JIRA Request Constants */
        public static string PROJECT_REQUEST      = "project";
        public static string ISSUE_REQUEST        = "issue";
        public static string PRIORITY_REQUEST     = "priority";
        public static string ISSUE_TYPE_REQUEST   = "issuetype";
        public static string JIRA_ENDING_URL      = "rest/api/2";
        public static string USER_REQUEST         = "user/assignable/search?project=";
        public static string COMP_REQUEST         = "project/";

        /*  Property Type Macros    */
        public static int TYPE_STRING             = 0;
        public static int TYPE_OBJECT             = 1;

        /*  JIRA issue link name    */
        public static string RELATES              = "Relates";
        public static string CREATE_IN_CONTEXT    = "Set in Context";

        /*Issue types */
        public static string SUB_TASK             = "Sub-task";

        /*  General Components  */
        public static string COMPONENTS           = "/components";

        /*  Separators */
        public static char FWD_SLASH              = '/';
        public static char VAL_SEP                = '§';

        /*  JIRA Properties */
        public static string COMPONENTS_PROP      = "Components";
        public static string FIX_VERSION          = "Fix Version";
        public static string AFFECTS_VERSION      = "Affects Version";
        public static string LABELS               = "Labels";
    }
}



