﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Constants.cs

   Description: This File contains constants related to JIRA Dll.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeJIRA.constants
{
    class Constants
    {
        public static string  INDEPENDENT_TICKETS                    = "Independent Tickets : ";
        public static string  MAIN_TICKET                            = "Main Ticket : ";
        public static string INDEPENDENT_TICKETS_LINKED_WITH_PARENT  = "Tickets Linked with main ticket : ";
        public static string SUBTASK                                 = "Sub Task : ";
        public static string TAGGED_VALUE                            = "Tagged Value";
        public static string CREATE_IN_CONTEXT                       = "Set in Context";
    }
}



