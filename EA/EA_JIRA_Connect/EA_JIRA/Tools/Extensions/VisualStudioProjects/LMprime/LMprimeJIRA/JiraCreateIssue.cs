﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                    Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: JiraCreateIssue.cs

   Description: This File contains functions specific to JIRA issue creation.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.commonbeans;
using LMprimeRest;
using Atlassian.Jira;
using LMprimeJIRA.constants;
using LMprimeJIRA.reversemapping;

namespace LMprimeJIRA
{
    public class JiraCreateIssue
    {

        NetworkMain         netMain        = new NetworkMain();
        ReverseMappingMain  revMappingMain = new ReverseMappingMain();

        public string createJIRAIssue(string strProject, EA.Repository inpRepository, List<LMprimeIssueBean> parentElem, List<LMprimeIssueBean> independentElem, List<LMprimeIssueBean> independentLinkedElem, List<string> strMergedElemList)
        {
            string strIssueId       = "";
            
            List<string> LinkedIssueId      = new List<string>();
            List<string> IndependentTickets = new List<string>();
            List<string> SubTask            = new List<string>();

            try
            {

                netMain.createJiraRestClient();

                /*  Creating issues that needs to be linked with parent */
                createIssueandLinkWithParent(strProject, inpRepository, independentLinkedElem, ref LinkedIssueId);

                /*  Creating parent issue, and linking those issues which are created independently and neeeds to be linked with parent */
                strIssueId = createParentIssue(strProject, inpRepository, parentElem, LinkedIssueId, strMergedElemList);

                /*  Creating independent issues including Sub Task also */
                createIndependentIssue(strProject, inpRepository, independentElem, strIssueId, ref IndependentTickets, ref SubTask);

                string strOutString = genrateOutString(LinkedIssueId, strIssueId, IndependentTickets, SubTask);

                return strOutString;
            }
            catch (Exception exception)
            {
                throw exception;
            }


        }

        public string createParentIssue(string strProject, EA.Repository inpRepository, List<LMprimeIssueBean> parentElem, List<string> LinkedIssueId, List<string> strMergedElemList)
        {
            Issue  issue       = null;
            String strIssueID  = null;

            try
            {
                if (strProject != null && strProject.Length > 0 && inpRepository != null && parentElem != null && parentElem.Count > 0)
                {
                    string strIssueType = parentElem[0].strIssueType;

                    issue = netMain.createJiraIssue(strIssueType, strProject, parentElem[0].strAssignee, parentElem[0].strPriority, parentElem[0].strComponents, parentElem[0].issue, LinkedIssueId, parentElem[0].strExistingTicketId, parentElem[0].strOperation);

                    revMappingMain.updateReverseMaping(inpRepository, parentElem[0].strElementGUID, issue);

                    if (strMergedElemList != null && strMergedElemList.Count > 0)
                    {
                        for (int iDx = 0; iDx < strMergedElemList.Count; iDx++)
                        {
                            revMappingMain.updateReverseMaping(inpRepository, strMergedElemList[iDx], issue);
                        }
                    }
                    if (issue != null && issue.Key != null && issue.Key.ToString().Length > 0)
                    {
                        strIssueID = issue.Key.ToString();
                    }
                }
                return strIssueID;
            }
            catch (Exception exception)
            {
                throw exception;
            }
           
        }

        /*  For Creating Independent Isssues */
        public void createIndependentIssue(string strProject, EA.Repository inpRepository, List<LMprimeIssueBean> independentElem, string strParentIssueId, ref List<string> createIndependentIssue, ref List<string> subTaskList)
        {
            try
            {
                Issue issue;
                if (strProject != null && strProject.Length > 0 && inpRepository != null && independentElem != null && independentElem.Count > 0 && strParentIssueId != null && strParentIssueId.Length > 0)
                {
                    for (int iDx = 0; iDx < independentElem.Count; iDx++)
                    {
                        issue = null;

                        issue = netMain.createJiraIssue(independentElem[iDx].strIssueType, strProject, independentElem[iDx].strAssignee, independentElem[iDx].strPriority, independentElem[iDx].strComponents, independentElem[iDx].issue, null, strParentIssueId, null);

                        revMappingMain.updateReverseMaping(inpRepository, independentElem[iDx].strElementGUID, issue);

                        if (independentElem[iDx].strIssueType.CompareTo("Sub-task") == 0)
                        {

                            subTaskList.Add(issue.Key.ToString());
                        }
                        else
                        {
                            createIndependentIssue.Add(issue.Key.ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function creates those issues that needs to be linked with parent, it returns their id */
        public void createIssueandLinkWithParent(string strProject, EA.Repository inpRepository, List<LMprimeIssueBean> lIndependentLinkedElem, ref List<string> LinkedIssueId)
        {
            try
            {
                Issue issue;

                if (strProject != null && strProject.Length > 0 && inpRepository != null && lIndependentLinkedElem != null && lIndependentLinkedElem.Count > 0)
                {
                    for (int iDx = 0; iDx < lIndependentLinkedElem.Count; iDx++)
                    {
                        issue = null;

                        issue = netMain.createJiraIssue(lIndependentLinkedElem[iDx].strIssueType, strProject, lIndependentLinkedElem[iDx].strAssignee, lIndependentLinkedElem[iDx].strPriority, lIndependentLinkedElem[iDx].strComponents, lIndependentLinkedElem[iDx].issue, null, null, null);

                        if (issue != null)
                        {
                            revMappingMain.updateReverseMaping(inpRepository, lIndependentLinkedElem[iDx].strElementGUID, issue);

                            LinkedIssueId.Add(issue.Key.ToString());
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string genrateOutString(List<string> LinkedIssueId, string strIssueId, List<string> IndependentTickets, List<string> Subtask)
        {

            string strOutString = "";

            strOutString = Constants.MAIN_TICKET + strIssueId + "\n";
            
            if (IndependentTickets != null && IndependentTickets.Count > 0)
            {
                if (IndependentTickets.Count == 1)
                {
                    strOutString = strOutString + Constants.INDEPENDENT_TICKETS + IndependentTickets[0] + "\n";
                }
                else
                {
                    for (int iDx = 0; iDx < IndependentTickets.Count; iDx++)
                    {
                        if (iDx == 0)
                        {
                            strOutString = strOutString + Constants.INDEPENDENT_TICKETS+ IndependentTickets[iDx];
                        }
                        else
                        {
                            strOutString = strOutString + "," + IndependentTickets[iDx];
                        }
                        strOutString = strOutString + "\n";
                    }
                }
                
            }

            if (Subtask != null && Subtask.Count > 0)
            {
                if (Subtask.Count == 1)
                {
                    strOutString = strOutString + Constants.SUBTASK + Subtask[0] + "\n";
                }
                else
                {
                    for (int iDx = 0; iDx < Subtask.Count; iDx++)
                    {
                        if (iDx == 0)
                        {
                            strOutString = strOutString + Constants.SUBTASK + Subtask[iDx];
                        }
                        else
                        {
                            strOutString = strOutString + "," + Subtask[iDx];
                        }
                        strOutString = strOutString + "\n";
                    }
                }

            }

            if (LinkedIssueId != null && LinkedIssueId.Count > 0)
            {
                if (LinkedIssueId.Count == 1)
                {
                    strOutString = strOutString + Constants.INDEPENDENT_TICKETS_LINKED_WITH_PARENT + LinkedIssueId[0] + "\n";
                }
                else
                {
                    for (int iDx = 0; iDx < LinkedIssueId.Count; iDx++)
                    {
                        if (iDx == 0)
                        {
                            strOutString = strOutString + Constants.INDEPENDENT_TICKETS_LINKED_WITH_PARENT + LinkedIssueId[iDx];
                        }
                        else
                        {
                            strOutString = strOutString + "," + LinkedIssueId[iDx];
                        }
                        strOutString = strOutString + "\n";
                    }
                }

            }
            return strOutString;
        }
    }
}


