﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReverseMappingMain.cs

   Description: This file is contains API related to Reverse Mapping.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using Atlassian.Jira;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;
using LMprimeJIRA.constants;
using LMprimeEA.Utilities;
using LMprimeCommon.Utilities;

namespace LMprimeJIRA.reversemapping
{
    class ReverseMappingMain
    {
        /*  This function is starting point for updating reverese mapping */
        public void updateReverseMaping(EA.Repository Repository, string strElemGuid, Atlassian.Jira.Issue inpIssue)
        {
            JIRA2EAColl lmpJiraToEA      = new JIRA2EAColl();
            EAMappingUtilities eMapUtils = new EAMappingUtilities();

            try
            {
                if (Repository != null && strElemGuid != null && inpIssue != null)
                {
                    EA.Element elem = Repository.GetElementByGuid(strElemGuid);

                    lmpJiraToEA = eMapUtils.getJIRAToEAObj(elem);

                    updateElement(ref elem, lmpJiraToEA, inpIssue);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void updateElement(ref EA.Element elem, JIRA2EAColl lmpJiraToEA, Atlassian.Jira.Issue Issue)
        {
            string strValue = "";
            PropertyInfoUtil pInfo = new PropertyInfoUtil();

            try
            {
                if (lmpJiraToEA != null && lmpJiraToEA.ltJira2EASgl != null && lmpJiraToEA.ltJira2EASgl.Count > 0)
                {
                    for (int iDx = 0; iDx < lmpJiraToEA.ltJira2EASgl.Count; iDx++)
                    {
                        strValue = pInfo.GetPropertyValue(Issue, lmpJiraToEA.ltJira2EASgl[iDx].strSourceAttrName);

                        /*  For tagged Value */
                        if (lmpJiraToEA.ltJira2EASgl[iDx].strDesAttrTyp != null
                            && lmpJiraToEA.ltJira2EASgl[iDx].strDesAttrTyp.CompareTo(Constants.TAGGED_VALUE) == 0)
                        {
                            EATaggedValuesHelper eTagValue = new EATaggedValuesHelper();

                            eTagValue.setTaggedValueByName(lmpJiraToEA.ltJira2EASgl[iDx].strDesAttrName, strValue.ToString(), ref elem, lmpJiraToEA.ltJira2EASgl[iDx].strSourceAttrName);
                        }
                        else
                        {
                            /*  For non tagged value */
                            Object obj = (Object)elem;

                            pInfo.SetObjectProperty(lmpJiraToEA.ltJira2EASgl[iDx].strDesAttrName, strValue.ToString(), ref obj, lmpJiraToEA.ltJira2EASgl[iDx].strSourceAttrName);

                            elem = (EA.Element)obj;

                            elem.Update();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



