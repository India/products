﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMICommonConst.cs

   Description: This File contains constants specific to this DLL.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-03-2018    Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeCommon.constants
{
    class LMICommonConst
    {
        /*JIRA Specific */
        public static string KEY              = "Key";
        
        /*  Separators */
        public static string COMMA_STRING     = ",";
        public static char COMMA_CHAR         = ',';
        public static string BLANK            = "";
        public static char FWD_SLASH_CHAR     = '/';
        public static string FWD_SLASH_STRING = "/";
    }
}



