﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: PropertyInfoGet.cs

   Description: This File contains API's Property get operations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-July-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using LMprimeCommon.constants;

namespace LMprimeCommon.Utilities
{
    public class PropertyInfoUtil
    {
        /*  This function is used to getting class properties dynamically,
         *  It receives input Object and property name, It finds out the value of the input property from object
         *  It the propery is not found, the null is returns as output value from this function.
         *  In case of any exception blank string is returne. @ No exception is thrown from here so calling function has
         *  to take care of it.
         */
        public string GetPropertyValue(object instance, string szPropertyName)
        {
            try
            {
                Type type = instance.GetType();
                string strPropertyInfo = "";
                var propertyInfo = "";

                try
                {
                    /*  Getting property value */
                    propertyInfo = type.InvokeMember(szPropertyName, System.Reflection.BindingFlags.GetProperty, null, instance, null).ToString();
                }
                catch (Exception exception)
                {
                    return LMICommonConst.BLANK;
                }

                /*  If property is not found */
                if (propertyInfo == null)
                {
                    return null;
                }
                else
                {
                    strPropertyInfo = propertyInfo.ToString();

                    return propertyInfo;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        
        /*This function is specific to JIRA Reverse mapping updation, it takes input object passed by reference, 
         *property name, property value, src attribute name.
         * The logic is that when we reverse update the information in ea element, if it is key then we first takes
         * out the its value if exists in ea element, adds the new value of key separated by comma.
         * For all the other properties for reverse mapping updation, the existing value is replaced with new value
         * This function sets the new value of property in the input object which is passed by reference.
         * @typically in our case the input object will be EA.Element.
         */
        public void SetObjectProperty(string propertyName, string value, ref object obj, string strSrcAttr)
        {
            try
            {
                 PropertyInfo propertyInfo = obj.GetType().GetProperty(propertyName);
                
                /* Make sure object has the property we are after */
                if (propertyInfo != null)
                {
                    /*  Checking whether source attribute is key or not */
                    if (strSrcAttr != null && strSrcAttr.Length > 0 && strSrcAttr.CompareTo(LMICommonConst.KEY) == 0)
                    {
                        /*  Getting out the value of property */
                        string strTemp = GetPropertyValue(obj, propertyName);

                        if (strTemp != null && strTemp.Length > 0)
                        {
                            string[] tempArr = strTemp.Split(LMICommonConst.COMMA_CHAR);

                            /*  Running a loop on the complete array and checking that if the input value already exists in it
                             *  or not. If it already exists then just returning from it.
                             */
                            for (int iDx = 0; iDx < tempArr.Length; iDx++)
                            {
                                if (tempArr[iDx].CompareTo(value) == 0)
                                {
                                    return;
                                }
                            }

                            if (strTemp != null && strTemp.Length > 0)
                            {
                                value = strTemp + LMICommonConst.COMMA_STRING + value;
                            }
                        }
                    }

                    propertyInfo.SetValue(obj, value, null);
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
    }
}



