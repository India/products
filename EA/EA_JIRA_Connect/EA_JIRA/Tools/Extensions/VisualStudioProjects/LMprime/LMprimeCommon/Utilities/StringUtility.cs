﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: StringUtility.cs

   Description: This file contains utility functions related to strings.

========================================================================================================================================================================
Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using LMprimeCommon.constants;

namespace LMprimeCommon.Utilities
{
    public class StringUtility
    {
        /*  This function removes any special character from input string, and replaces it with empty string
         **/
        public string removeHTMLFormating(string strInpString)
        {
            string strOutString = "";

            try
            {
                if (strInpString != null)
                {
                    strOutString = Regex.Replace(strInpString, "<.*?>", String.Empty);
                }
                return strOutString;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function corrects Url ending, it checks whether input URL consists forward slash at end or not,
         *  it it does not containd forward slash at end, then it adds it at the end and returns it
         **/
        public string correctURLending(string strUrl)
        {
            string strOutUrl = "";

            try
            {
                if (strUrl != null && strUrl.Length > 0)
                {
                    int iSize = strUrl.Length;

                    if (strUrl[iSize - 1].CompareTo(LMICommonConst.FWD_SLASH_CHAR) == 0)
                    {
                        strOutUrl = strUrl;
                    }
                    else
                    {
                        strOutUrl = strUrl + LMICommonConst.FWD_SLASH_STRING;
                    }
                }
                return strOutUrl;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



