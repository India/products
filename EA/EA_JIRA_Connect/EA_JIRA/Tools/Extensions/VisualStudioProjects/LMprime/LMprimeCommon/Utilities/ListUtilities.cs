﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ListUtilities.cs

   Description: This File contains API's specific to List Utilities.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
28-July-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.constants;

namespace LMprimeCommon.Utilities
{
    public class ListUtilities
    {
        /*
         * This function is to segeragate list of specific types from a collection of lists.
         * As the tagged values specific to a element is mapping file is stored in the below format
         * List<List<string>> = {tagvalv1Name, tagvalv1Value} List1
         *                      {tagvalv2Name, tagvalv2Value} List2
         *                      {tagvalv3Name, tagvalv4Value} List3
         *  This function traverses on all the sub lists of main input list and segragate the list of specific type from mapping
         *  for e.g 
         *  The mapping for elements contain all values EA2JIRA, JIRA2EA, etc
         *  This function is capable of seprating list of specific type base on input key i.e EA2JIRA or JIRA2EA
         *  Currently this function is not used in storing or dealing the tagged values related to mapping
         *  getSeparatedTVListForMapping defined in Tagged values helper in LMprimeEA dll is used for mapping related operations.
         * */
        public List<List<string>> getSeparatedTVList(string strKey, List<List<string>> ltInpTVList)
         {
            List<List<string>> ltOutString = new List<List<string>>();
            try
            {
                /*  Validations on input list and key */
                if (ltInpTVList != null && ltInpTVList.Count > 0 && strKey != null && strKey.Length > 0)
                {
                    /*  Running a loop on all sub lists */
                    for (int iDx = 0; iDx < ltInpTVList.Count; iDx++)
                    {
                        List<string> ltTemp = new List<string>();

                        /*  Extracting a sub list */
                        ltTemp = ltInpTVList[iDx];

                        if (ltTemp != null && ltTemp.Count > 0)
                        {
                            /*  Comparing the list 1st elemet i.e tagname with input key */
                            if (ltTemp[0] != null && ltTemp[0].Length > 0 && ltTemp[0].CompareTo(strKey) == 0)
                            {
                                ltOutString.Add(ltTemp);
                            }
                        }
                    }
                }
                return ltOutString;
            }         
            catch(Exception exException)
            {
                throw exException;
            }
        }

        /*  This function converts the input array to list, the final list created is passed as reference to this
         *  function. Note It inserts each elenent of array into list but it first checks whether it already exists in
         *  list or not, if it already exists in list then it skips that element.It trims the array element before inserting it into list.
         * */
        public void addElementsFromArrayToList(string[] inpArray, ref List<string> ltFinalList)
        {
            try
            {
                if (inpArray.Length > 0)
                {
                    /* Running a loop on all the elements of input array, if its size is greater then zero */
                    for (int iDx = 0; iDx < inpArray.Length; iDx++)
                    {
                        string temp = inpArray[iDx];

                        /* Trimming the array element for removing spaces */
                        temp = temp.Trim();

                        /* Checking that element already exists in list if yes then skip else add it to list */
                        if (ltFinalList.Contains(temp))
                        {
                            continue;
                        }
                        else
                        {
                            ltFinalList.Add(temp);
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}















