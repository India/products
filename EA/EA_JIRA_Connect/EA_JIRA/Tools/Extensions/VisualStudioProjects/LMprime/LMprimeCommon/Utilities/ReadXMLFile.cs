﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReadXMLFile.cs

   Description: This file is utility file for reading xml file.

========================================================================================================================================================================
Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeCommon.Utilities
{
    public class ReadXMLFile
    {
        /*  This function reads a xml file from particular location and returns a string */
        public string readXmlFile(string strFilepath)
        {
            string strXmlFile = null;

            try
            {
                if (strFilepath != null && strFilepath.Length > 0)
                {
                    strXmlFile = System.IO.File.ReadAllText(strFilepath);
                }
                return strXmlFile;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



