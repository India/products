﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeIssueBean.cs

   Description: This File is container for issue creation.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Mar-2018   Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeCommon.commonbeans
{
    public class LMprimeIssueBean
    {
        public string strElementGUID = "";
        public string strAssignee = "";
        public string strIssueType = "";
        public string strPriority = "";
        public string strOperation = "";
        public string strExistingTicketId = "";
        public string strComponents = "";
        public Dictionary<string, string> issue = new Dictionary<string, string>();
        public List<string> lMergedElem = new List<string>();
    }
}
