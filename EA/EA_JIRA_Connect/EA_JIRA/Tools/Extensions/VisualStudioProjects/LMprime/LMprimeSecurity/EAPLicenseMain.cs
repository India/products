﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeSecurity.constants;


namespace LMprimeSecurity
{
    public class EAPLicenseMain
    {
        public Boolean checkValidLicense()
        {
            try
            {
                
                long unixTime = UnixTimeNow();

                long unixTimeinMarch2020 = 1585585990;

                if (unixTime > unixTimeinMarch2020)
                    return false;
                else
                    return true;
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public long UnixTimeNow()
        {
            try
            {
                var timeSpan = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0));

                return (long)timeSpan.TotalSeconds;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
