﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: GetMappingRelObjects.cs

   Description: This File contains API related to performing operations on related eement in Tree view.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprimeEA.eadataengine;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;

namespace LMprime.lmprimecreateissue
{
    class GetMappingRelObjects
    {
        /*
         * This function returns a list of element related with input element, If isBiDir is Set to true then related elements in both directions are displayed
         * else related elements in only one directions are displayed.
         **/
        public List<EA.Element> getRelElemForTree(EA.Element inpElem, Boolean isBiDir)
        {
            EAPConnectionColl lmpConnection = null;
            EAMappingUtilities eMapUtil     = new EAMappingUtilities();
            EARelatedElements eaRelElem     = new EARelatedElements();
            List<EA.Element> tempElem       = new List<EA.Element>();
            
            try
            {
                if (inpElem != null)
                {
                    lmpConnection = eMapUtil.getSelConnectionObj(inpElem);

                    tempElem = eaRelElem.getChildElementsForTree(AppDelegate.statSel_Repository, lmpConnection, inpElem, isBiDir);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return tempElem;
        }
    }
}



