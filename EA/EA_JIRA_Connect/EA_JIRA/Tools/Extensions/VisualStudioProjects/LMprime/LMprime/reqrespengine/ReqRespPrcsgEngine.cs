﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReqTespPrcsgEngine.cs

   Description: This File is to process request and responses.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Mar-2018   Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.constants;
using LMprime.userinterface.login;
using LMprime.lmprimelogin;
using LMprime.userinterface.createIssue;
using LMprime.lmprimecreateissue;
using LMprime.lmprimecreateissue.createissuefinalimpl;
using LMprimeRest.restlogin;


namespace LMprime.reqrespengine
{
    class ReqRespPrcsgEngine
    {
        /*  Adding event, These events are added on Background Worker thread, Do not perform any operations related to user interface here, the system will crash, processing is going not on UI Thread */
        public void addEvent(Object objInp, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case LMprimeConst.REQ_LOGIN_REQUEST:
                         
                         req_login_request(objInp);
                         
                         break;
                    
                    case LMprimeConst.REQ_CREATE_ISSUE:

                         req_create_jira_issue(objInp);

                         break;

                    case LMprimeConst.REQ_GET_ISSUE_DETAIL:

                         req_get_issue_details(objInp);
                         
                         break;

                    case LMprimeConst.REQ_GET_COMPONENTS:

                         req_get_components();

                         break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  For handling event in Callback */
        public void handleEvent(Object outObj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case LMprimeConst.REQ_LOGIN_REQUEST:
                         
                         Login.stLogin.handleEvent(outObj, iEventType);
                         
                         break;

                    case LMprimeConst.REQ_CREATE_ISSUE:

                         CreateIssue.stCreateIssue.handleEvent(outObj, iEventType);
                         
                         break;

                    case LMprimeConst.REQ_GET_ISSUE_DETAIL:

                         CreateIssue.stCreateIssue.handleEvent(outObj, iEventType);
                         
                         break;

                    case LMprimeConst.REQ_CANCELLED:
                         
                         break;

                    case LMprimeConst.REQ_GET_COMPONENTS:

                         CreateIssue.stCreateIssue.handleEvent(outObj, iEventType);

                         break;
                }
            }
            catch (Exception exception)
            {
                /*  No need to do anything here, because this has been called by Custom Progress bar form, and upto now that form has already been disposed,
                 *  so that exception will become unhandled, leaving it the way it is and it will remain the thing as it is. 
                 */
            }
        }

        /*  Initiating Login request */
        public void req_login_request(Object objInp)
        {
            try
            {
                LMILoginMain lmiLoginMain = new LMILoginMain();

                lmiLoginMain.performLoginOperation(objInp);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Initiating Components Request */
        public void req_get_components()
        {
            try
            { 
                LMILoginMain lmiLoginMain = new LMILoginMain();

                lmiLoginMain.getComponents();
            }
            catch (Exception exception)
            {
            }
        }

        /*  Initiating Issue Detail Request */
        public void req_get_issue_details(Object objInp)
        {
            try
            {
                CreateIssueEAJIRADGV cIssueEAJIRADGV = new CreateIssueEAJIRADGV();

                cIssueEAJIRADGV.getJIRAIssueObj(objInp);
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        /*  Initiating Create JIRA Issue Request */
        public void req_create_jira_issue(Object objInp)
        {
            try
            {
                CreateIssueMain cIssueMain = new CreateIssueMain();

                cIssueMain.createJIRAIssue(objInp);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}





