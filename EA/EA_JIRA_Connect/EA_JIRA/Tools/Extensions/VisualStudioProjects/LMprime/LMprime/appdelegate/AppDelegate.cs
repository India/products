﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Appdelegate.cs

   Description: This File contains constants specific to appdelegate.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.eabeans;
using LMprime.lmprimebeans;
using Atlassian.Jira;

namespace LMprime.appdelegate
{
    class AppDelegate
    {
        /*  Contains current repository */
        public static EA.Repository statSel_Repository                       = null;
        
        /*  Static variable that stores value whether is logged in or not   */
        public static Boolean bIS_USER_LOGGED_IN                             = false;

        /*  Static variable that stores current selected element  */
        public static EA.Element statSelectedElement                         = null;

        /*  Static variable for storing configuration file    */
        public static LMIConfig statLMIBeans                                 = null;

        /*  Statically Storing Dictionary to App delegate containing project names and corresponding issue types */
        public static Dictionary<string, List<string>> dtPROJ_ISSUE_TYPE     = null;

        /*  Statically storing Project Name along with Users Corresponding to it */
        public static Dictionary<string, List<string>> dtPROJ_USERS          = new Dictionary<string, List<string>>();

        /*  Statistically storing Components List   */
        public static List<string> ltPROJ_COMPONENTS                         = new List<string>();

        /*  Statically Storing Project List */
        public static List<string>ltPROJ_LIST                                = null;

        /*  For Maintaining Bi-directional Relationships */
        public static Boolean bIS_BI_DIR_ALLOWED                             = true;

        /*  For storing selected Project */
        public static string strSEL_JIRA_PROJECT                             = null;

        /*  Priority List   */
        public static List<string> ltPRIORITY_LIST                           = null;

        /*  Issue Object */
        public static Atlassian.Jira.Issue selElemIssue                      = null;
        
        /*  Mapping Constants   */
        public static string strJIRA_URL                                     = null;
        public static Dictionary<string, EAPMapping> dtFINAL_MAPPING         = null;
        public static Dictionary<string, EAPMapping> dtSELECTED_ELEM_MAPPING = null;

        /*  Temp Variable that stores ELEM GUID */
        public static string strELEMGUID                                     = null;

        /*  Create Issue Final String */
        public static string strOutputIDs                                    = null;

        public static Boolean IS_SET_IN_CONTEXT                              = false;

    }
}
