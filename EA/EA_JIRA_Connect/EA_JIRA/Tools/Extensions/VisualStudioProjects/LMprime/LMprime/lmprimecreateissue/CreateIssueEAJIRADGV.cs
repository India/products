﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueEAJIRADGV.cs

   Description: This File contains Creation Logics related to logics EA JIRA Data Grid View.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprime.lmprimebeans;
using LMprimeEA.utilities;
using LMprimeEA.eabeans;
using LMprimeCommon.Utilities;
using LMprime.constants;
using Atlassian.Jira;
using LMprimeEA.Utilities;
using LMprimeRest.restissuedetail;
using System.Windows.Forms;
using LMprime.requestbeans;


namespace LMprime.lmprimecreateissue
{
    public class CreateIssueEAJIRADGV
    {
        PropertyInfoUtil pInfoGet         = new PropertyInfoUtil();
        EATaggedValuesHelper eTagValues   = new EATaggedValuesHelper();
        EAReverseMappingUtil eaRevMapUtil = new EAReverseMappingUtil();

        /*  This function gets JIRA Ticket ID For Selected Element in Tree View
         *  It gets the the GUID of element corresponding to tree node,
         *  and then it traverses the entire Data Grid View and find the row of corresponding to that element.
         * */
        public string getIssueIdForTreeElem(string strElemGuid, DataGridView dgView)
        {
            string strTikId = "";

            try
            {
                if (strElemGuid != null && strElemGuid.Length > 0 && dgView != null)
                {
                    /*  Traversing the entire data grid view and finding a row corresponding to this element guid */
                    for (int iDx = 0; iDx < dgView.Rows.Count; iDx++)
                    {
                        if (dgView.Rows[iDx] != null && dgView.Rows[iDx].Tag != null && dgView.Rows[iDx].Tag.ToString().Length > 0)
                        {
                            /*  Comparing the input GUID with the GUID binded with the Data Grid View Row */
                            if (strElemGuid.Equals(dgView.Rows[iDx].Tag.ToString(), StringComparison.InvariantCultureIgnoreCase))
                            {
                                /*  If row is finded then checking whether JIRA ID exists in it, if yes then returning it */
                                if (dgView.Rows[iDx].Cells[2].Value != null && dgView.Rows[iDx].Cells[2].Value.ToString().Length > 0)
                                {
                                    strTikId = dgView.Rows[iDx].Cells[2].Value.ToString();
                                }
                            }
                        }
                    }
                }

                return strTikId;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function gets Issue Object for corresponing to input Ticket ID, If it successfully gets issue object then it sets it statically to app delegate */
        public void getJIRAIssueObj(Object inpObj)
        {
            try
            {
                /*  Making the selected element issue at appdelegate to be null, before request */
                AppDelegate.selElemIssue = null;

                if (inpObj != null)
                {
                    AppDelegate.selElemIssue                 = null;
                    Atlassian.Jira.Issue issue               = null;
                    IssueDetailReqAndRespMain issueDetailReq = new IssueDetailReqAndRespMain();
                    ReqIssueDetails issueDetails             = (ReqIssueDetails)inpObj;

                    if (issueDetails != null)
                    {
                        /*  Getting issue id from input container */
                        string strTikID = issueDetails.strJIRAID;

                        if (strTikID != null && strTikID.Length > 0)
                        {
                            /*  Getting Issue Object, if Successfully found then Setting it to AppDelegate */
                            issue = issueDetailReq.getIssueDetailsFromJira(strTikID);

                            if (issue != null)
                            {
                                AppDelegate.selElemIssue = issue;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
                    
        /* This function populates informantion in EA JIRA Data Grid View, This view is also called compare view,
         * This function is fired when user selectes the tree node or any row in operation view.
         * This function first unpopulates all the existing data from EA JIRA View.
         * This function takes input GUID, from it it finds the respective element from Repository and from that
         * element EA to JIRA mapping is found, and from that o
         */
        public void populateInfoInEAJIRADGV(string strElemGuid, ref DataGridView dgEAJIRADetails)
        {
            try
            {
                if (strElemGuid != null && strElemGuid.Length > 0 && dgEAJIRADetails != null)
                {
                    List<LMprimeEAJIRABean> ltEAJIRA = new List<LMprimeEAJIRABean>();

                    /*  Allowing User to add rows in Data Grid View */
                    dgEAJIRADetails.AllowUserToAddRows = true;

                    if (dgEAJIRADetails.Rows.Count > 0)
                    {
                        /*  First Unpopulating the information filled in EA JIRA Data Grid View */
                        unPopulateInfoInEAJIRADGV(ref dgEAJIRADetails);
                    }

                    /*  Getting List of LMprimeEAJIRABean type containing data to populate in Data Grid View */
                    ltEAJIRA = getInfoForEAJIRADGV(strElemGuid);
                    
                    if (ltEAJIRA != null && ltEAJIRA.Count > 0)
                    {

                        for (int iDx = 0; iDx < ltEAJIRA.Count; iDx++)
                        {

                            DataGridViewRow row = (DataGridViewRow)dgEAJIRADetails.Rows[dgEAJIRADetails.Rows.Count - 1].Clone();
                            
                            if (ltEAJIRA[iDx] != null)
                            {
                                ((DataGridViewTextBoxCell)(row.Cells[0])).ReadOnly = false;

                                ((DataGridViewTextBoxCell)(row.Cells[1])).ReadOnly = false;

                                ((DataGridViewTextBoxCell)(row.Cells[2])).ReadOnly = false;

                                ((DataGridViewTextBoxCell)(row.Cells[3])).ReadOnly = false;

                                ((DataGridViewTextBoxCell)(row.Cells[0])).Value = ltEAJIRA[iDx].strEAPropertyName;

                                ((DataGridViewTextBoxCell)(row.Cells[1])).Value = ltEAJIRA[iDx].strEAPropertyValue;

                                ((DataGridViewTextBoxCell)(row.Cells[2])).Value = ltEAJIRA[iDx].strJIRAPropertyName;

                                ((DataGridViewTextBoxCell)(row.Cells[3])).Value = ltEAJIRA[iDx].strJIRAPropertyValue;

                                ((DataGridViewTextBoxCell)(row.Cells[0])).ReadOnly = true;

                                ((DataGridViewTextBoxCell)(row.Cells[1])).ReadOnly = true;

                                ((DataGridViewTextBoxCell)(row.Cells[2])).ReadOnly = true;

                                ((DataGridViewTextBoxCell)(row.Cells[3])).ReadOnly = true;

                                dgEAJIRADetails.Rows.Add(row);
                            }
                        }
                    }
                    dgEAJIRADetails.ClearSelection();
                }

                dgEAJIRADetails.AllowUserToAddRows = false;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* This function unpopulates all information from EA JIRA data Grid View, It clears all the rows and then refresh the entire data grid view */
        public void unPopulateInfoInEAJIRADGV(ref DataGridView dgEAJIRADetails)
        {
            try
            {
                if (dgEAJIRADetails != null && dgEAJIRADetails.Rows.Count > 0)
                {
                    dgEAJIRADetails.Rows.Clear();

                    dgEAJIRADetails.Refresh();
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        /*  This function is starting point for EA and corresponding JIRA attributes value, It first find out the selected element from repository, then it
         *  finds out its mapping and then it invokes other function to get the data.
         * */
        public List<LMprimeEAJIRABean> getInfoForEAJIRADGV(string strElemGuid)
        {
            try
            {
                List<LMprimeEAJIRABean> lOutList = new List<LMprimeEAJIRABean>();
                EAMappingUtilities eMapUtil      = new EAMappingUtilities();
                EA2JIRAColl lmpEAToJIRA          = new EA2JIRAColl();

                if (strElemGuid != null && strElemGuid.Length > 0)
                {
                    /*  Getting EA Element corresponding to element GUID from repository */
                    EA.Element inpElem = AppDelegate.statSel_Repository.GetElementByGuid(strElemGuid);

                    if (inpElem != null)
                    {
                        /*  Getting EATOJIRA mapping object */
                        lmpEAToJIRA = eMapUtil.getEAToJiraObj(inpElem);

                        createFinalList(ref lOutList, lmpEAToJIRA, inpElem);
                    }
                }

                return lOutList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* This function traverses all elements in lmpEAToJIRA and create a List of type LMprimeEAJIRABean that contains
         * Ea jira attributes along with their values.
         * */
        public void createFinalList(ref List<LMprimeEAJIRABean> lOutList, EA2JIRAColl lmpEAToJIRA, EA.Element inpElem)
        {
            try
            {
                if (lmpEAToJIRA != null && lmpEAToJIRA.ltEAtoJiraFinal != null && lmpEAToJIRA.ltEAtoJiraFinal.Count > 0)
                {
                    Issue issue = AppDelegate.selElemIssue;

                    /*  Traversing the entire EA2JIRAColl object */
                    for (int iDx = 0; iDx < lmpEAToJIRA.ltEAtoJiraFinal.Count; iDx++)
                    {
                        LMprimeEAJIRABean temp = new LMprimeEAJIRABean();

                        if (lmpEAToJIRA.ltEAtoJiraFinal[iDx] != null)
                        {
                            /*  EA Property type */
                            temp.strEAPropertyType = lmpEAToJIRA.ltEAtoJiraFinal[iDx].strSourceAttrTyp;

                            /*  EA Property Value */
                            temp.strEAPropertyName = lmpEAToJIRA.ltEAtoJiraFinal[iDx].strSourceAttr;

                            if (temp.strEAPropertyName != null && temp.strEAPropertyName.Length > 0)
                            {
                                string strTemp = "";
                                /*  Checking if type is tagged value or not */
                                if (temp.strEAPropertyType != null && temp.strEAPropertyType.Equals(LMprimeConst.TAGGED_VALUE, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    strTemp = eTagValues.getFirstTaggedValue(inpElem, temp.strEAPropertyName);
                                }
                                else
                                {
                                    strTemp = pInfoGet.GetPropertyValue(inpElem, temp.strEAPropertyName);
                                }
                                if (strTemp != null && strTemp.Length > 0)
                                {
                                    temp.strEAPropertyValue = strTemp;
                                }
                            }

                            /*  Getting JIRA property name */
                            temp.strJIRAPropertyName = lmpEAToJIRA.ltEAtoJiraFinal[iDx].strDestAttr;

                            if (temp.strJIRAPropertyName != null && temp.strJIRAPropertyName.Length > 0)
                            {
                                if (issue != null)
                                {
                                    string strTemp = "";

                                    /*  Components and Labels are in thr form of objects, they cannot be received directly using GetPropertyValue Function */
                                    if (temp.strJIRAPropertyName.Equals(LMprimeConst.COMPONENTS, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        strTemp = getCompAndLabels(issue, LMprimeConst.COMPONENTS);
                                    }
                                    else if (temp.strJIRAPropertyName.Equals(LMprimeConst.LABELS, StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        strTemp = getCompAndLabels(issue, LMprimeConst.LABELS);
                                    }
                                    else
                                    {
                                        strTemp = pInfoGet.GetPropertyValue(issue, temp.strJIRAPropertyName);
                                    }
                                    if (strTemp != null && strTemp.Length > 0)
                                    {
                                            temp.strJIRAPropertyValue = strTemp;
                                    }
                                }
                            }

                            lOutList.Add(temp);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function gets components and Labels from the JIRA Object and returns them in Comma separated string */

        public string getCompAndLabels(Issue issue, string strPropName)
        {
            int    iCount       = 0;
            string strOutString = "";

            try
            {
                if (issue != null && strPropName != null && strPropName.Length > 0)
                {
                    /*  Check for components */
                    if (strPropName.Equals(LMprimeConst.COMPONENTS, StringComparison.InvariantCultureIgnoreCase))
                    {
                        iCount = issue.Components.Count;

                        for (int iDx = 0; iDx < iCount; iDx++)
                        {
                            if (issue.Components[iDx] != null && issue.Components[iDx].ToString() != null)
                            {
                                if (iDx == 0)
                                {
                                    strOutString = issue.Components[iDx].ToString();
                                }
                                else
                                {
                                    strOutString = strOutString + LMprimeConst.SEP_COMMA + issue.Components[iDx].ToString();
                                }
                            }
                        }
                    }

                    /*  Check for labels */
                    if (strPropName.Equals(LMprimeConst.LABELS, StringComparison.InvariantCultureIgnoreCase))
                    {
                        string[] labels = issue.Labels.Get();

                        for (int iDx = 0; iDx < labels.Length; iDx++)
                        {
                            if (labels[iDx] != null && labels[iDx].ToString() != null)
                            {
                                if (iDx == 0)
                                {
                                    strOutString = labels[iDx];
                                }
                                else
                                {
                                    strOutString = strOutString + LMprimeConst.SEP_COMMA + labels[iDx];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return strOutString;
        }
    }
}