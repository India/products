﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeLoginMain.cs

   Description: This File is specific to implementaional logic to login dialog.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprimeEA.eabeans;
using LMprimeCommon.Utilities;
using LMprime.constants;
using LMprime.lmprimeconfig;
using LMprimeRest.restlogin;
using System.Windows.Forms;
using LMprimeRest;
using LMprime.requestbeans;
using LMprimeRest.restgeneraReqAndResp;

namespace LMprime.lmprimelogin
{
    class LMILoginMain
    {
        LoginReqAndRespMain lReqAndResp = new LoginReqAndRespMain();
                
        /*  Entry point function to get JIRA URL */
        public string getJIRAUrl()
        {
            string strURL = null;

            try
            {
                strURL = getFinalJIRAUrl();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return strURL;
        }

        /*  Function to get JIRA Url and setting it staticaly to app delegate   */
        public string getFinalJIRAUrl()        
        {
            string strElemType       = "";
            string strElemStereoType = "";
            string strUrl            = "";

            try
            {
                /*  Seleceted element type  */
                strElemType = AppDelegate.statSelectedElement.Type;

                /*  Seleceted element Stereotype  */
                strElemStereoType = AppDelegate.statSelectedElement.Stereotype;

                EAPMapping eapMapping = new EAPMapping();

                if (AppDelegate.dtFINAL_MAPPING != null)
                {
                    /*  First checking element with its stereotype.This dual checking is performed because when in ea mapping template two 
                     *  elements can have same types but there stereotype will always be diffrent.So when we construct dictionary for storing
                     *  mapping file(i.e AppDelegate.Final Mapping) we checks any key exists with the element type name. If yes that means the 
                     *  element are having same type so we store second element with its stereotype name as key in dictionary. 
                     */
                    if (AppDelegate.dtFINAL_MAPPING.ContainsKey(strElemStereoType))
                    {
                        eapMapping = AppDelegate.dtFINAL_MAPPING[strElemStereoType];
                    }
                    else if (AppDelegate.dtFINAL_MAPPING.ContainsKey(strElemType))
                    {
                        /*  Secondly Checking for element type   */
                        eapMapping = AppDelegate.dtFINAL_MAPPING[strElemType];
                    }

                    if (eapMapping != null && eapMapping.lmpURL != null)
                    {
                        strUrl = eapMapping.lmpURL.strUrl;
                    }
                }

                /*  If URL is not present in mapping then checking it in configuration file */
                if (strUrl == null || strUrl.Length == 0)
                {
                    strUrl = AppDelegate.statLMIBeans.JIRAurl;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return strUrl;
         }

        /*  This function is entry point for entire login operation.It fist checks for the project list from the static
         *  Object which we have stored on App Delegate by parsing the configuration file.The entire logic is driven in 
         *  such a way that if one or multiple Projects are found in AppDelegate, then only projects that are stored in object
         *  at AppDelegate will be taken into consideration. If it does not have any project, then project list is fetched from JIRA
         *  corresponding to user. And all the other operations i.e Issue type , assignee etc will be in context with this project list.
         * 
         *  This function receives a generic object that contains Url, username, password.
         *  It detype casts them to corresponding ReqLogin Object.
         *  It initializes network Object (netObj) with that data.
         * 
         *  Below function creates a dictionary<Key, Value> = <Project Key, List of Issue type> to App Delegate.
         *  It further extracts out projects from it and creates a list and set this ist to app delegate also.
         *  It further requests for assignee list.
         *  It gets list of priority also.
         */
        public void performLoginOperation(Object obj)
        {
            try
            {
                List<string> ltPrjList = null;
                Dictionary<string, List<string>> ProjAndIssueTyp = new Dictionary<string, List<string>>();
                ReqLogin reqLogin = new ReqLogin();
                NetworkMain netObj = NetworkMain.getInstance();

                /*  Getting Project List from Appdelegate(Static Object of parsed Config file) */
                ltPrjList = AppDelegate.statLMIBeans.JIRAproject;

                /*  Typcasting the input Object for extrcting credentials */
                reqLogin = (ReqLogin)obj;
                    
                /*  Setting Url to app delegate */
                AppDelegate.strJIRA_URL = reqLogin.strUrl;

                /*  Initializing the network */
                netObj.initializeNetworkData(reqLogin.strUsername, reqLogin.strPassword, reqLogin.strUrl);

                /*  Requesting for dictionary of Project list and issue type */
                ProjAndIssueTyp = lReqAndResp.performLoginOperation(ltPrjList);

                /*Extratcting project List out of it and setting it to app delegate */
                createProjectList(ProjAndIssueTyp);

                if (AppDelegate.ltPROJ_LIST == null || AppDelegate.ltPROJ_LIST.Count <= 0)
                {
                    throw new Exception(LMprimeConst.NO_PRJ_EXISTS);
                }

                /*  Sending Request For Getting Users Corresponding to project */
                getAssigneeList();

                /*  Sending Request For Getting priority Corresponding to project*/
                getPriorityList();

                /*  Sending Request For Project Components */
                getComponents();

                /* Statically setting the dictionary to app delegate */
                AppDelegate.dtPROJ_ISSUE_TYPE = ProjAndIssueTyp;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* This function creates a project list from the Dictionary of project, IssueTypes. Key of the dictionay is project list */
        public void createProjectList(Dictionary<string, List<string>> inpDictionary)
        {
            List<string> lProjectList = new List<string>();

            try
            {
                if (inpDictionary != null && inpDictionary.Count > 0)
                {
                    lProjectList = inpDictionary.Keys.ToList<string>();

                    /*  Setting the Project List To App Delegate */
                    AppDelegate.ltPROJ_LIST = lProjectList;

                    if (AppDelegate.ltPROJ_LIST != null && AppDelegate.ltPROJ_LIST.Count > 0)
                    {
                        AppDelegate.strSEL_JIRA_PROJECT = AppDelegate.ltPROJ_LIST[0];
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }

        /* This function send requests for getting Assignee List. It first clears the existing assignee list which is present on App Delegate, 
         * and runs a loop on all the projects that is set on app delegate, One by one it receives the assignee for corresponding project, 
         * and adds it to dictionary with structure Dictionary <Key,Value> = <Project key, Assignee List>. After creating the complete dictionary
         * it sets it statically to app delegate.
         */
        public void getAssigneeList()
        {
            try
            {
                if (AppDelegate.dtPROJ_USERS != null && AppDelegate.dtPROJ_USERS.Count > 0)
                {
                    /*  Clearing existing assignee list */
                    AppDelegate.dtPROJ_USERS.Clear();
                }

                /*  Running a loop on all projects stored at app delegate, getting assignee correspoding to them */
                for (int iDx = 0; iDx < AppDelegate.ltPROJ_LIST.Count; iDx++)
                {
                    List<string> temp = new List<string>();

                    if(AppDelegate.ltPROJ_LIST[iDx] != null && AppDelegate.ltPROJ_LIST[iDx].Length > 0)
                    {
                        /*  Requesting for user corresponding to project */
                        temp = lReqAndResp.getUsersList(AppDelegate.ltPROJ_LIST[iDx]);

                        if(temp != null && temp.Count > 0)
                        {
                            AppDelegate.dtPROJ_USERS.Add(AppDelegate.ltPROJ_LIST[iDx], temp);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function is for getting Priority List, If existing priority list is set to app delegate then it, clears it and sends request to 
         *  for getting proprity list, After getting the priority list successfully it sets is statically to app delegate.
         */
        public void getPriorityList()
        {
            try
            {
                if (AppDelegate.ltPRIORITY_LIST != null && AppDelegate.ltPRIORITY_LIST.Count > 0)
                {
                    /*Clearing the existing priority list if exists */
                    AppDelegate.ltPRIORITY_LIST.Clear();
                }
                if (AppDelegate.ltPROJ_LIST != null && AppDelegate.ltPROJ_LIST.Count > 0)
                {
                    /*  Getting the priority, only if project exissts in App Delegate */
                    AppDelegate.ltPRIORITY_LIST = lReqAndResp.getPriorityList();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        /*  This function is for getting Components List, After receiveing components list its sets them statically to app delegate */
        public void getComponents()
        {
            try
            {
                RestGeneralReqAndRespMain restGenReqAndresp = new RestGeneralReqAndRespMain();

                if (AppDelegate.strSEL_JIRA_PROJECT != null && AppDelegate.strSEL_JIRA_PROJECT.Length > 0)
                {
                    List<string> ltComponentsTemp = new List<string>();

                    ltComponentsTemp = restGenReqAndresp.getComponentsList(AppDelegate.strSEL_JIRA_PROJECT);

                    if (ltComponentsTemp != null && ltComponentsTemp.Count > 0)
                    {
                        if (AppDelegate.ltPROJ_COMPONENTS != null)
                        {
                            AppDelegate.ltPROJ_COMPONENTS.Clear();
                        }
                        AppDelegate.ltPROJ_COMPONENTS = ltComponentsTemp;
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}









