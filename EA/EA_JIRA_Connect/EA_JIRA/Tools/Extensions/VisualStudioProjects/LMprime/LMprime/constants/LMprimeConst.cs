﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeConst.cs

   Description: This File contains constants specific to LMprime DLL.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.constants
{
    class LMprimeConst
    {
        /*  EA Delegate Function specific constants */
        public static string A_STRING                         = "a string";

        /*  Message Box error strings */
        public static string LIC_EXP_MSG_STRING               = "Your LMIEAPluggin trial period has expired. Please Contact LMtec India at lmtec@lmtec.in for renew update.";
        public static string LIC_EXP_MSG_HEADER               = "Trial Period Expired";
        public static string URL_NOT_EXISTS                   = "Jira url does not exists";
        public static string ERROR_IN_OPENING_DIALOG          = "Error in Launching Jira Create issue dialog, Check license details";
        public static string SEL_REP_IS_NULL                  = "Selected Repository is null.";
        public static string SEL_ELEM_IS_NULL                 = "Please select a element in diagram, Selected Element is null";
        public static string NO_PRJ_EXISTS                    = "Current logged in user does not have access to any project in JIRA. Please configure a project in JIRA.";

        /*  Create Issue Tabbed windows specific constants */
        public static string TAB_WINDOW_NAME                  = "Create Jira Issue";
        public static string TAB_WINDOW_LOCATION              = "LMprime.userinterface.createIssue.CreateIssue";

        /*  Exception strings */
        public static string EXCEPTION_STRING                 = "Exception occured";
        public static string ERROR                            = "Error";

        /*  Configuration file related contants */
        public static string FILE_NAME                        = "/LMIEAPlugin.xml";

        /*  Create Issue UI related Issues */
        public static string PROJECT_DOES_NOT_EXISTS          = "No project Exists for user in JIRA configure project for User in JIRA";
    
        /*  Data Grid View Operation List with Sub Task*/
        public static string[] OPERATION_LIST_COMPLETE = new string[] 
                                     { 
                                         "Independent Ticket", 
                                         "Create Independent and Link with Parent",
                                         "Merge with Parent",
                                     };

        public static string[] OPERATION_LIST_WITHOUT_MERGE_WITH_PARENT = new string[] 
                                     { 
                                         "Independent Ticket", 
                                         "Create Independent and Link with Parent",
                                     };

        public static string[] INDEPENDENT_TICKET = new string[]
                                     {
                                         "Independent Ticket"
                                     };
        public static string[] INDEPENDENT_TICKET_CREATE_IN_CONTEXT = new string[]
                                     {
                                         "Independent Ticket",
                                         "Set in Context"
                                     };

        public static string[] SUB_TASK                       = new string[] { "Sub Task" };

        /*  Separators */
        public static string SEP_FRWD_SLASH                   = "/";
        public static char   SEP_VAL_SEPARATOR                = '§';
        public static string SEP_COMMA                        = ",";
        
        /*  General Constants    */
        public static string KEY                              = "Key";
        public static string TAGGED_VALUE                     = "Tagged Value";
        public static string INDEPENDENT_TICKET_CMP_STR       = "Independent Ticket";
        public static string INDEPENDENT_TICKET_LINK_WITH_PAR = "Create Independent and Link with Parent";
        public static string MERGE_WITH_PARENT                = "Merge with Parent";
        public static string SUBTASK_OPERATION                = "Sub Task";
        public static string SUCCESS                          = "Success";
        public static string MEDIUM_PRIORITY                  = "Medium";
        public static string SUBTASK                          = "Sub-task";
        public static string SUBTASK_CAPS_OFF                 = "sub-task";
        public static string TASK                             = "Task";
        public static string SET_IN_CONTEXT                   = "Set in Context";
        public static string BROWSE                           = "browse/";
        public static string COMPONENTS                       = "Components";
        public static string LABELS                           = "Labels";

        /* Request types */
        public const int REQ_INVALID                          = -1;
        public const int REQ_CANCELLED                        = 100; 
        public const int REQ_LOGIN_REQUEST                    = 101;
        public const int REQ_CREATE_ISSUE                     = 102;
        public const int REQ_GET_ISSUE_DETAIL                 = 103;
        public const int REQ_GET_COMPONENTS                   = 104;

    }
}
