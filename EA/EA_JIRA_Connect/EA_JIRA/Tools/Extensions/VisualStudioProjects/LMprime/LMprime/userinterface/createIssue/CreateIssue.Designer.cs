﻿namespace LMprime.userinterface.createIssue
{
    public partial class CreateIssue
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateIssue));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.spContParent = new System.Windows.Forms.SplitContainer();
            this.spUpperStrip = new System.Windows.Forms.SplitContainer();
            this.btSubmit = new System.Windows.Forms.Button();
            this.cbSelectProject = new System.Windows.Forms.ComboBox();
            this.lbProject = new System.Windows.Forms.Label();
            this.cbOperations = new System.Windows.Forms.ComboBox();
            this.lbOperation = new System.Windows.Forms.Label();
            this.spElementBrowser = new System.Windows.Forms.SplitContainer();
            this.tvElemBrowser = new LMprime.userinterface.createIssue.TreeViewCustom();
            this.imglTreeImage = new System.Windows.Forms.ImageList(this.components);
            this.spDataGridView = new System.Windows.Forms.SplitContainer();
            this.dgElemDetails = new System.Windows.Forms.DataGridView();
            this.dgEAJIRADetails = new System.Windows.Forms.DataGridView();
            this.EAAttribute = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EAAttributeValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jiraProperty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jiraPropertyValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wbJIRABrowser = new System.Windows.Forms.WebBrowser();
            this.elemName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IssueType = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.ExistingJIRAIssueID = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Assignee = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Operation = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Priority = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.spContParent)).BeginInit();
            this.spContParent.Panel1.SuspendLayout();
            this.spContParent.Panel2.SuspendLayout();
            this.spContParent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spUpperStrip)).BeginInit();
            this.spUpperStrip.Panel1.SuspendLayout();
            this.spUpperStrip.Panel2.SuspendLayout();
            this.spUpperStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spElementBrowser)).BeginInit();
            this.spElementBrowser.Panel1.SuspendLayout();
            this.spElementBrowser.Panel2.SuspendLayout();
            this.spElementBrowser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spDataGridView)).BeginInit();
            this.spDataGridView.Panel1.SuspendLayout();
            this.spDataGridView.Panel2.SuspendLayout();
            this.spDataGridView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgElemDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEAJIRADetails)).BeginInit();
            this.SuspendLayout();
            // 
            // spContParent
            // 
            this.spContParent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spContParent.Location = new System.Drawing.Point(0, 0);
            this.spContParent.Name = "spContParent";
            this.spContParent.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spContParent.Panel1
            // 
            this.spContParent.Panel1.Controls.Add(this.spUpperStrip);
            // 
            // spContParent.Panel2
            // 
            this.spContParent.Panel2.Controls.Add(this.wbJIRABrowser);
            this.spContParent.Size = new System.Drawing.Size(1107, 820);
            this.spContParent.SplitterDistance = 544;
            this.spContParent.TabIndex = 0;
            // 
            // spUpperStrip
            // 
            this.spUpperStrip.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spUpperStrip.Location = new System.Drawing.Point(0, 0);
            this.spUpperStrip.Name = "spUpperStrip";
            this.spUpperStrip.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spUpperStrip.Panel1
            // 
            this.spUpperStrip.Panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.spUpperStrip.Panel1.Controls.Add(this.btSubmit);
            this.spUpperStrip.Panel1.Controls.Add(this.cbSelectProject);
            this.spUpperStrip.Panel1.Controls.Add(this.lbProject);
            this.spUpperStrip.Panel1.Controls.Add(this.cbOperations);
            this.spUpperStrip.Panel1.Controls.Add(this.lbOperation);
            this.spUpperStrip.Panel1.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // spUpperStrip.Panel2
            // 
            this.spUpperStrip.Panel2.Controls.Add(this.spElementBrowser);
            this.spUpperStrip.Size = new System.Drawing.Size(1107, 544);
            this.spUpperStrip.SplitterDistance = 32;
            this.spUpperStrip.TabIndex = 0;
            // 
            // btSubmit
            // 
            this.btSubmit.Dock = System.Windows.Forms.DockStyle.Right;
            this.btSubmit.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSubmit.Location = new System.Drawing.Point(994, 0);
            this.btSubmit.Name = "btSubmit";
            this.btSubmit.Size = new System.Drawing.Size(113, 32);
            this.btSubmit.TabIndex = 4;
            this.btSubmit.Text = "Submit";
            this.btSubmit.UseVisualStyleBackColor = true;
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            // 
            // cbSelectProject
            // 
            this.cbSelectProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSelectProject.FormattingEnabled = true;
            this.cbSelectProject.Location = new System.Drawing.Point(347, 0);
            this.cbSelectProject.Name = "cbSelectProject";
            this.cbSelectProject.Size = new System.Drawing.Size(121, 26);
            this.cbSelectProject.TabIndex = 3;
            this.cbSelectProject.SelectedIndexChanged += new System.EventHandler(this.cbSelectProject_SelectedIndexChanged);
            // 
            // lbProject
            // 
            this.lbProject.AutoSize = true;
            this.lbProject.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProject.Location = new System.Drawing.Point(241, 4);
            this.lbProject.Name = "lbProject";
            this.lbProject.Size = new System.Drawing.Size(98, 18);
            this.lbProject.TabIndex = 2;
            this.lbProject.Text = "Select Project-";
            // 
            // cbOperations
            // 
            this.cbOperations.AutoCompleteCustomSource.AddRange(new string[] {
            "\"\""});
            this.cbOperations.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbOperations.FormattingEnabled = true;
            this.cbOperations.Items.AddRange(new object[] {
            "Uni Directional",
            "Bi Directional",
            "Reset"});
            this.cbOperations.Location = new System.Drawing.Point(79, 0);
            this.cbOperations.Name = "cbOperations";
            this.cbOperations.Size = new System.Drawing.Size(121, 26);
            this.cbOperations.TabIndex = 1;
            this.cbOperations.SelectedIndexChanged += new System.EventHandler(this.cbOperations_SelectedIndexChanged);
            // 
            // lbOperation
            // 
            this.lbOperation.AutoSize = true;
            this.lbOperation.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbOperation.Location = new System.Drawing.Point(-3, 4);
            this.lbOperation.Name = "lbOperation";
            this.lbOperation.Size = new System.Drawing.Size(76, 18);
            this.lbOperation.TabIndex = 0;
            this.lbOperation.Text = "Operation-";
            // 
            // spElementBrowser
            // 
            this.spElementBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spElementBrowser.Location = new System.Drawing.Point(0, 0);
            this.spElementBrowser.Name = "spElementBrowser";
            // 
            // spElementBrowser.Panel1
            // 
            this.spElementBrowser.Panel1.Controls.Add(this.tvElemBrowser);
            // 
            // spElementBrowser.Panel2
            // 
            this.spElementBrowser.Panel2.Controls.Add(this.spDataGridView);
            this.spElementBrowser.Size = new System.Drawing.Size(1107, 508);
            this.spElementBrowser.SplitterDistance = 369;
            this.spElementBrowser.TabIndex = 0;
            // 
            // tvElemBrowser
            // 
            this.tvElemBrowser.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tvElemBrowser.CheckBoxes = true;
            this.tvElemBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvElemBrowser.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tvElemBrowser.HideSelection = false;
            this.tvElemBrowser.ImageIndex = 1;
            this.tvElemBrowser.ImageList = this.imglTreeImage;
            this.tvElemBrowser.Location = new System.Drawing.Point(0, 0);
            this.tvElemBrowser.Name = "tvElemBrowser";
            this.tvElemBrowser.SelectedImageIndex = 1;
            this.tvElemBrowser.ShowNodeToolTips = true;
            this.tvElemBrowser.Size = new System.Drawing.Size(369, 508);
            this.tvElemBrowser.TabIndex = 0;
            this.tvElemBrowser.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tvElemBrowser_AfterCheck);
            this.tvElemBrowser.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvElemBrowser_AfterSelect);
            // 
            // imglTreeImage
            // 
            this.imglTreeImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imglTreeImage.ImageStream")));
            this.imglTreeImage.TransparentColor = System.Drawing.Color.Transparent;
            this.imglTreeImage.Images.SetKeyName(0, "ea_jira_with_ticket_x24.png");
            this.imglTreeImage.Images.SetKeyName(1, "ea_jira_element_without_jira_id24_new.png");
            // 
            // spDataGridView
            // 
            this.spDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spDataGridView.Location = new System.Drawing.Point(0, 0);
            this.spDataGridView.Margin = new System.Windows.Forms.Padding(0);
            this.spDataGridView.Name = "spDataGridView";
            this.spDataGridView.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // spDataGridView.Panel1
            // 
            this.spDataGridView.Panel1.Controls.Add(this.dgElemDetails);
            // 
            // spDataGridView.Panel2
            // 
            this.spDataGridView.Panel2.Controls.Add(this.dgEAJIRADetails);
            this.spDataGridView.Size = new System.Drawing.Size(734, 508);
            this.spDataGridView.SplitterDistance = 238;
            this.spDataGridView.TabIndex = 0;
            // 
            // dgElemDetails
            // 
            this.dgElemDetails.AllowDrop = true;
            this.dgElemDetails.AllowUserToResizeRows = false;
            this.dgElemDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgElemDetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgElemDetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgElemDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgElemDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgElemDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.elemName,
            this.IssueType,
            this.ExistingJIRAIssueID,
            this.Assignee,
            this.Operation,
            this.Priority,
            this.Column1});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.ActiveBorder;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgElemDetails.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgElemDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgElemDetails.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgElemDetails.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgElemDetails.Location = new System.Drawing.Point(0, 0);
            this.dgElemDetails.Name = "dgElemDetails";
            this.dgElemDetails.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgElemDetails.RowHeadersVisible = false;
            this.dgElemDetails.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgElemDetails.RowTemplate.Height = 24;
            this.dgElemDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgElemDetails.Size = new System.Drawing.Size(734, 238);
            this.dgElemDetails.TabIndex = 0;
            this.dgElemDetails.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgElemDetails_CellContentClick);
            this.dgElemDetails.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgElemDetails_CellValueChanged);
            this.dgElemDetails.CurrentCellDirtyStateChanged += new System.EventHandler(this.dgElemDetails_CurrentCellDirtyStateChanged);
            this.dgElemDetails.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dgElemDetails_DataError);
            this.dgElemDetails.SelectionChanged += new System.EventHandler(this.dgElemDetails_SelectionChanged);
            this.dgElemDetails.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgElemDetails_DragDrop);
            this.dgElemDetails.DragOver += new System.Windows.Forms.DragEventHandler(this.dgElemDetails_DragOver);
            this.dgElemDetails.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgElemDetails_MouseDown);
            this.dgElemDetails.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgElemDetails_MouseMove);
            // 
            // dgEAJIRADetails
            // 
            this.dgEAJIRADetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgEAJIRADetails.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgEAJIRADetails.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dgEAJIRADetails.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgEAJIRADetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEAJIRADetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EAAttribute,
            this.EAAttributeValue,
            this.jiraProperty,
            this.jiraPropertyValue});
            this.dgEAJIRADetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgEAJIRADetails.GridColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgEAJIRADetails.Location = new System.Drawing.Point(0, 0);
            this.dgEAJIRADetails.Name = "dgEAJIRADetails";
            this.dgEAJIRADetails.RowHeadersVisible = false;
            this.dgEAJIRADetails.RowTemplate.Height = 24;
            this.dgEAJIRADetails.Size = new System.Drawing.Size(734, 266);
            this.dgEAJIRADetails.TabIndex = 0;
            // 
            // EAAttribute
            // 
            this.EAAttribute.HeaderText = "EAAttribute";
            this.EAAttribute.Name = "EAAttribute";
            // 
            // EAAttributeValue
            // 
            this.EAAttributeValue.HeaderText = "EAAttribute Value";
            this.EAAttributeValue.Name = "EAAttributeValue";
            // 
            // jiraProperty
            // 
            this.jiraProperty.HeaderText = "JIRA Property";
            this.jiraProperty.Name = "jiraProperty";
            // 
            // jiraPropertyValue
            // 
            this.jiraPropertyValue.HeaderText = "JIRA Property Value";
            this.jiraPropertyValue.Name = "jiraPropertyValue";
            // 
            // wbJIRABrowser
            // 
            this.wbJIRABrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wbJIRABrowser.Location = new System.Drawing.Point(0, 0);
            this.wbJIRABrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.wbJIRABrowser.Name = "wbJIRABrowser";
            this.wbJIRABrowser.ScriptErrorsSuppressed = true;
            this.wbJIRABrowser.Size = new System.Drawing.Size(1107, 272);
            this.wbJIRABrowser.TabIndex = 0;
            this.wbJIRABrowser.Url = new System.Uri("https://asingh.atlassian.net", System.UriKind.Absolute);
            // 
            // elemName
            // 
            this.elemName.HeaderText = "Element Name";
            this.elemName.Name = "elemName";
            this.elemName.ReadOnly = true;
            // 
            // IssueType
            // 
            this.IssueType.AutoComplete = false;
            this.IssueType.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.IssueType.DisplayStyleForCurrentCellOnly = true;
            this.IssueType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IssueType.HeaderText = "Issue Type";
            this.IssueType.Name = "IssueType";
            this.IssueType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.IssueType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // ExistingJIRAIssueID
            // 
            this.ExistingJIRAIssueID.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.ExistingJIRAIssueID.DisplayStyleForCurrentCellOnly = true;
            this.ExistingJIRAIssueID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ExistingJIRAIssueID.HeaderText = "Existing JIRA Issue Id";
            this.ExistingJIRAIssueID.Name = "ExistingJIRAIssueID";
            this.ExistingJIRAIssueID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.ExistingJIRAIssueID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // Assignee
            // 
            this.Assignee.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Assignee.DisplayStyleForCurrentCellOnly = true;
            this.Assignee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Assignee.HeaderText = "Assignee";
            this.Assignee.Name = "Assignee";
            // 
            // Operation
            // 
            this.Operation.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Operation.DisplayStyleForCurrentCellOnly = true;
            this.Operation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Operation.HeaderText = "Operation";
            this.Operation.Name = "Operation";
            // 
            // Priority
            // 
            this.Priority.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Priority.DisplayStyleForCurrentCellOnly = true;
            this.Priority.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Priority.HeaderText = "Priority";
            this.Priority.Name = "Priority";
            // 
            // Column1
            // 
            this.Column1.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.Column1.DisplayStyleForCurrentCellOnly = true;
            this.Column1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Column1.HeaderText = "Components";
            this.Column1.Name = "Column1";
            this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // CreateIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Controls.Add(this.spContParent);
            this.Name = "CreateIssue";
            this.Size = new System.Drawing.Size(1107, 820);
            this.Load += new System.EventHandler(this.CreateIssue_Load);
            this.spContParent.Panel1.ResumeLayout(false);
            this.spContParent.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spContParent)).EndInit();
            this.spContParent.ResumeLayout(false);
            this.spUpperStrip.Panel1.ResumeLayout(false);
            this.spUpperStrip.Panel1.PerformLayout();
            this.spUpperStrip.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spUpperStrip)).EndInit();
            this.spUpperStrip.ResumeLayout(false);
            this.spElementBrowser.Panel1.ResumeLayout(false);
            this.spElementBrowser.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spElementBrowser)).EndInit();
            this.spElementBrowser.ResumeLayout(false);
            this.spDataGridView.Panel1.ResumeLayout(false);
            this.spDataGridView.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spDataGridView)).EndInit();
            this.spDataGridView.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgElemDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgEAJIRADetails)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer spContParent;
        private System.Windows.Forms.SplitContainer spUpperStrip;
        private System.Windows.Forms.SplitContainer spElementBrowser;
        private System.Windows.Forms.SplitContainer spDataGridView;
        private System.Windows.Forms.Button btSubmit;
        private System.Windows.Forms.ComboBox cbSelectProject;
        private System.Windows.Forms.Label lbProject;
        private System.Windows.Forms.ComboBox cbOperations;
        private System.Windows.Forms.Label lbOperation;
        private System.Windows.Forms.DataGridView dgEAJIRADetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn EAAttribute;
        private System.Windows.Forms.DataGridViewTextBoxColumn EAAttributeValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn jiraProperty;
        private System.Windows.Forms.DataGridViewTextBoxColumn jiraPropertyValue;
        private System.Windows.Forms.ImageList imglTreeImage;
        private System.Windows.Forms.DataGridView dgElemDetails;
        private TreeViewCustom tvElemBrowser;
        private System.Windows.Forms.WebBrowser wbJIRABrowser;
        private System.Windows.Forms.DataGridViewTextBoxColumn elemName;
        private System.Windows.Forms.DataGridViewComboBoxColumn IssueType;
        private System.Windows.Forms.DataGridViewComboBoxColumn ExistingJIRAIssueID;
        private System.Windows.Forms.DataGridViewComboBoxColumn Assignee;
        private System.Windows.Forms.DataGridViewComboBoxColumn Operation;
        private System.Windows.Forms.DataGridViewComboBoxColumn Priority;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column1;
    }
}
