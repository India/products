﻿namespace LMprime.userinterface.about
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbNameKey = new System.Windows.Forms.Label();
            this.lbOrgKey = new System.Windows.Forms.Label();
            this.lbVersionKey = new System.Windows.Forms.Label();
            this.lbNameVal = new System.Windows.Forms.Label();
            this.lbOrgVal = new System.Windows.Forms.Label();
            this.lbVersionVal = new System.Windows.Forms.Label();
            this.lbExpiryDate = new System.Windows.Forms.Label();
            this.lbexpdatevalue = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbNameKey
            // 
            this.lbNameKey.AutoSize = true;
            this.lbNameKey.Location = new System.Drawing.Point(28, 20);
            this.lbNameKey.Name = "lbNameKey";
            this.lbNameKey.Size = new System.Drawing.Size(45, 17);
            this.lbNameKey.TabIndex = 0;
            this.lbNameKey.Text = "Name";
            // 
            // lbOrgKey
            // 
            this.lbOrgKey.AutoSize = true;
            this.lbOrgKey.Location = new System.Drawing.Point(28, 55);
            this.lbOrgKey.Name = "lbOrgKey";
            this.lbOrgKey.Size = new System.Drawing.Size(89, 17);
            this.lbOrgKey.TabIndex = 1;
            this.lbOrgKey.Text = "Organization";
            // 
            // lbVersionKey
            // 
            this.lbVersionKey.AutoSize = true;
            this.lbVersionKey.Location = new System.Drawing.Point(28, 95);
            this.lbVersionKey.Name = "lbVersionKey";
            this.lbVersionKey.Size = new System.Drawing.Size(56, 17);
            this.lbVersionKey.TabIndex = 2;
            this.lbVersionKey.Text = "Version";
            // 
            // lbNameVal
            // 
            this.lbNameVal.AutoSize = true;
            this.lbNameVal.Location = new System.Drawing.Point(211, 20);
            this.lbNameVal.Name = "lbNameVal";
            this.lbNameVal.Size = new System.Drawing.Size(86, 17);
            this.lbNameVal.TabIndex = 3;
            this.lbNameVal.Text = "LMIEAplugin";
            // 
            // lbOrgVal
            // 
            this.lbOrgVal.AutoSize = true;
            this.lbOrgVal.Location = new System.Drawing.Point(211, 55);
            this.lbOrgVal.Name = "lbOrgVal";
            this.lbOrgVal.Size = new System.Drawing.Size(264, 17);
            this.lbOrgVal.TabIndex = 4;
            this.lbOrgVal.Text = "LMtec India Consulting Services Pvt. Ltd.";
            // 
            // lbVersionVal
            // 
            this.lbVersionVal.AutoSize = true;
            this.lbVersionVal.Location = new System.Drawing.Point(211, 95);
            this.lbVersionVal.Name = "lbVersionVal";
            this.lbVersionVal.Size = new System.Drawing.Size(88, 17);
            this.lbVersionVal.TabIndex = 5;
            this.lbVersionVal.Text = "2.0.0.1_beta";
            // 
            // lbExpiryDate
            // 
            this.lbExpiryDate.AutoSize = true;
            this.lbExpiryDate.Location = new System.Drawing.Point(28, 134);
            this.lbExpiryDate.Name = "lbExpiryDate";
            this.lbExpiryDate.Size = new System.Drawing.Size(80, 17);
            this.lbExpiryDate.TabIndex = 6;
            this.lbExpiryDate.Text = "Expiry Date";
            // 
            // lbexpdatevalue
            // 
            this.lbexpdatevalue.AutoSize = true;
            this.lbexpdatevalue.Location = new System.Drawing.Point(211, 134);
            this.lbexpdatevalue.Name = "lbexpdatevalue";
            this.lbexpdatevalue.Size = new System.Drawing.Size(92, 17);
            this.lbexpdatevalue.TabIndex = 7;
            this.lbexpdatevalue.Text = "30-May-2018";
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 190);
            this.Controls.Add(this.lbexpdatevalue);
            this.Controls.Add(this.lbExpiryDate);
            this.Controls.Add(this.lbVersionVal);
            this.Controls.Add(this.lbOrgVal);
            this.Controls.Add(this.lbNameVal);
            this.Controls.Add(this.lbVersionKey);
            this.Controls.Add(this.lbOrgKey);
            this.Controls.Add(this.lbNameKey);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "About";
            this.Text = "About";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbNameKey;
        private System.Windows.Forms.Label lbOrgKey;
        private System.Windows.Forms.Label lbVersionKey;
        private System.Windows.Forms.Label lbNameVal;
        private System.Windows.Forms.Label lbOrgVal;
        private System.Windows.Forms.Label lbVersionVal;
        private System.Windows.Forms.Label lbExpiryDate;
        private System.Windows.Forms.Label lbexpdatevalue;
    }
}