﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueProjectUtil.cs

   Description: This File contains project utilities specific to create Issue Dialog.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprime.constants;

namespace LMprime.lmprimecreateissue
{
    class CreateIssueProjectUtil
    {

        /*  This function returns project list from App Delegate */
        public List<string> getProjectList()
        {
            try
            {
                List<string> ltProjectList = new List<string>();

                ltProjectList = AppDelegate.ltPROJ_LIST;

                return ltProjectList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



