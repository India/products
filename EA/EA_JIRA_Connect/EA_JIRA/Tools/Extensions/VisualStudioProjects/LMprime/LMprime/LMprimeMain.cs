﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMain.cs

   Description: This File is starting point for entire plugin.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/*
 * This file is entry point to whole plugin. Do not modify anything in this file.It contains EA Speicif API's that needs to be same and will not work if changed a litte bit.
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EA;
using LMprime.lmprimemenu;
using LMprime.appdelegate;
using LMprimeEA.Mapping;
using LMprime.lmprimebeans;
using LMprime.lmprimeexception;
using LMprime.lmprimeconfig;
using LMprime.userinterface.createIssue;
using LMprime.Test;
using LMprimeSecurity;
using LMprime.userinterface.about;
using LMprime.constants;

/*
 *  This class in entry point for whole plugin, It mostly contains the methods that are specific to EA Implementation.
 *  These are delegate Methods and should not be changed.
 **/
namespace LMprime
{
    public class LMprimeMain
    {
        /*  Establishing relation between Repository */
        public String EA_Connect(EA.Repository Repository)
        {
            /* No special processing required. */
            return LMprimeConst.A_STRING ;
        }

        /*  Constructing Main Menus and Sub menu Item  */
        public object EA_GetMenuItems(EA.Repository Repository, string strLocation, string strMenuName)
        {
            LMprimeConstructMenu lmConstructMenu = new LMprimeConstructMenu();
            
            string strMainMenu = lmConstructMenu.getMainMenu();

            switch (strMenuName)
            {
              /*  For top level Menu  */
                case LMPrimeMenuConstants.BLANK_SPACE:
                  
                    return strMainMenu;
               
              /* Defines the submenu options  */
              case LMPrimeMenuConstants.MENU_HEADER:
                   
                   string[] astrSubMenus = lmConstructMenu.getSubMenu();
                
                   return astrSubMenus;
            }

            return LMPrimeMenuConstants.BLANK_SPACE;
        }

        /* This function is delegate function and not to be invoked externally
         * This function returns true if project is opened and false if it is not opened  
         **/
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                EA.Collection c = Repository.Models;
        
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        /*  Function that gets called when user clicks on menu options  */
        public void EA_MenuClick(EA.Repository Repository, string strLocation, string strMenuName, string strItemName)
        {
            EAPLicenseMain eapLicense = new EAPLicenseMain();

            try
            {
                /*  Checking whether license is valid or not */
                if (eapLicense.checkValidLicense())
                {
                    LMprimeMenuEvent lmpEvent = new LMprimeMenuEvent();

                    /*  Entry point to menu click handler function  */
                    lmpEvent.peformMenuClickEvent(strItemName, Repository);
                }
                else
                {
                    /*  Displying error if Licence is Expired */
                    MessageBox.Show(LMprimeConst.LIC_EXP_MSG_STRING, LMprimeConst.LIC_EXP_MSG_HEADER);

                    return;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(LMprimeConst.ERROR_IN_OPENING_DIALOG, LMprimeConst.EXCEPTION_STRING);
            }
        }

        /*  Function that gets called when user clicks on create JIRA Issue button.   
         *  It checks whether user is logged in or not. If user is not logged in then Login dialog is shown else create JIRA Issue Dialog is shown. . 
         */
        public void checkAndProceed(EA.Repository Repository)
        {
            LMprimeJIRAMain        lmpJiraMain          = new LMprimeJIRAMain();
            LMprimeExceptionMain   lmprimeException     = new LMprimeExceptionMain();
            ParseLMprimeConfigFile parseLMPrimeConfig   = new ParseLMprimeConfigFile();
            EA.Collection          selObjects           = null;
            EA.DiagramObject       diagObj              = null;
            
            /* Below instance is for testing purpose only */
            TestApi                testApi              = new TestApi();
            
            try
            {
                if (Repository != null)
                {
                    /*  Setting repository to app delegate  */
                    AppDelegate.statSel_Repository = Repository;

                    EAJIRAMappingMain eaJiraMappingMain = new EAJIRAMappingMain();

                    /*  Checking the type of selected element from repository */
                    if (AppDelegate.statSel_Repository.GetContextItemType() == EA.ObjectType.otElement)
                    {
                        AppDelegate.statSelectedElement = (EA.Element)AppDelegate.statSel_Repository.GetContextObject();

                        if (AppDelegate.statSelectedElement == null)
                        {
                            MessageBox.Show(LMprimeConst.SEL_ELEM_IS_NULL);
                            
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show(LMprimeConst.SEL_ELEM_IS_NULL);
                        
                        return;
                    }

                    /*  Generating the consolidated mapping and setting it statically to app delegate   */
                    AppDelegate.dtFINAL_MAPPING = eaJiraMappingMain.getConsolidateMapping(AppDelegate.statSel_Repository);

                    /*  Parsing the configuration file */
                    parseLMPrimeConfig.parseConfigFile();

                    /*  Check if user is logged in or not this is entry point to show login or create issue dialog   */
                    lmpJiraMain.createJiraIssueMain();
                }
                else
                {
                    MessageBox.Show(LMprimeConst.SEL_REP_IS_NULL);

                    return;
                }
            }

            /* This catch block handles exceptions related to mapping file parsing, showing login as well as create issue dialog */
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, LMprimeConst.ERROR);
            }
         }
        
        /*  Code to show about Dialog  */
        public void showAboutDialog()
        {
            About about = new About();
            
            about.ShowDialog();
        }
    }
 }














