﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeBeans.cs

   Description: This File contains containers specific to LMprimelibrary.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMprime.lmprimebeans
{
    [XmlRoot(ElementName = "LMprimeConfig")]
    public class LMIConfig
    {
        [XmlElement(ElementName = "JIRAurl")]
        public string JIRAurl { get; set; }

        [XmlElement(ElementName = "JIRAproject")]
        public List<string> JIRAproject { get; set; }
    }

}
