﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMIIssueObj.cs

   Description: This File is container for issue creation.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimebeans
{
    public class LMIIssueObj
    {
        public string strElemGUID   = "";
        public string strIssueType  = "";
        public string strTikID      = "";
        public string strAssignee   = "";
        public string strOperation  = "";
        public string strPriority   = "";
        public string strComponents = "";
    }
}
