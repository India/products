﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueOperationDGV.cs

   Description: This File contains Logics related to Data Grid View Construction.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
15-March-18    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using LMprime.appdelegate;
using LMprime.constants;

namespace LMprime.lmprimecreateissue
{
    class CreateIssueOperationDGVConstruct
    {
            /* This function constructs Operation View/Elem Details Data Grid View. The structure of Collumms is given below:
            *  1st Collumn Contains    Element Name
            *  2nd Collumn Contains    Issue Type
            *  3rd Collumn Contains    Existing JIRA Issue Id
            *  4th Collumn Contains    Assignee
            *  5th Collumn Contains    Operation
            *  6th Collumn Contains    Priority
            *  7th Collumn Contains    Components
            ***/
            public void createMainRowDGV(ref DataGridView dgElemDetails)
            {
                CreateIssueOperationDGV cIssueDgv    = new CreateIssueOperationDGV();
                CreateIssueEAJIRADGV cIssueEAJIRADGV = new CreateIssueEAJIRADGV();
                List<string> ltExistingTicId         = null;

                try
                {
                    if(dgElemDetails != null)
                    {
                        /*  Making Data Grid View Editable */
                        dgElemDetails.AllowUserToAddRows = true;
                    
                        /*  For Disabling Collumns sorting, As user will not be able to change reordring  */
                        dgElemDetails.Columns.Cast<DataGridViewColumn>().ToList().ForEach(f => f.SortMode = DataGridViewColumnSortMode.NotSortable);

                        /*  Getting Issue Type List */
                        List<string> ltIssueTypList = cIssueDgv.getIssueTypListForProject(AppDelegate.statSelectedElement.ElementGUID, AppDelegate.strSEL_JIRA_PROJECT);

                        DataGridViewRow row = new DataGridViewRow();

                        row = (DataGridViewRow)dgElemDetails.Rows[0].Clone();

                        /*  Setting Selected Element name */
                        if (AppDelegate.statSelectedElement != null && AppDelegate.statSelectedElement.Name != null && AppDelegate.statSelectedElement.Name.Length > 0)
                        {
                            /*  Setting Selected Element Name in 1st Collumn */
                            row.Cells[0].Value = AppDelegate.statSelectedElement.Name;
                        }

                        /*  Setting Issue Type List */
                        if (ltIssueTypList != null && ltIssueTypList.Count > 0)
                        {
                            if (ltIssueTypList.Count > 0)
                            {
                                /*  Removing Sub task from top element if it exists */
                                if (ltIssueTypList.Contains(LMprimeConst.SUBTASK))
                                {
                                    ltIssueTypList.Remove(LMprimeConst.SUBTASK);
                                }

                                if (ltIssueTypList.Contains(LMprimeConst.SUBTASK_CAPS_OFF))
                                {
                                    ltIssueTypList.Remove(LMprimeConst.SUBTASK_CAPS_OFF);
                                }

                                ((DataGridViewComboBoxCell)(row.Cells[1])).DataSource = ltIssueTypList;

                                ((DataGridViewComboBoxCell)(row.Cells[1])).Value = ltIssueTypList[0];
                            }
                        }

                        /*  Setting Existing JIRA Issue ID's */
                        ltExistingTicId = cIssueDgv.getExistingJiraIssueId(AppDelegate.statSelectedElement.ElementGUID);

                        if (ltExistingTicId != null && ltExistingTicId.Count > 0)
                        {
                            //ltExistingTicId.Sort();

                            ltExistingTicId.Reverse();

                            ((DataGridViewComboBoxCell)(row.Cells[2])).DataSource = ltExistingTicId;

                            ((DataGridViewComboBoxCell)(row.Cells[2])).Value = ltExistingTicId[0];
                        }

                        /*  Setting Project Users For Assignee */
                        if (AppDelegate.strSEL_JIRA_PROJECT != null)
                        {
                            if (AppDelegate.dtPROJ_USERS != null && AppDelegate.dtPROJ_USERS.Count > 0 && AppDelegate.dtPROJ_USERS.ContainsKey(AppDelegate.strSEL_JIRA_PROJECT))
                            {
                                List<string> ltTempProjUsers = new List<string>();

                                ltTempProjUsers = AppDelegate.dtPROJ_USERS[AppDelegate.strSEL_JIRA_PROJECT];

                                if (ltTempProjUsers != null && ltTempProjUsers.Count > 0)
                                {
                                    
                                    ((DataGridViewComboBoxCell)(row.Cells[3])).DataSource = ltTempProjUsers;

                                    ((DataGridViewComboBoxCell)(row.Cells[3])).Value = ltTempProjUsers[0];
                                }
                            }
                        }

                        /*  Setting Operation List */
                        if (ltExistingTicId != null && ltExistingTicId.Count > 0)
                        {
                            /*  If existing issue id does exists the dislaying operation list with Set In Context */
                            ((DataGridViewComboBoxCell)(row.Cells[4])).DataSource = LMprimeConst.INDEPENDENT_TICKET_CREATE_IN_CONTEXT;

                            ((DataGridViewComboBoxCell)(row.Cells[4])).Value = LMprimeConst.INDEPENDENT_TICKET_CREATE_IN_CONTEXT[0];
                        }
                        else
                        {
                            /*  If existing issue id does not exists the dislaying operation list without Set In Context */
                            ((DataGridViewComboBoxCell)(row.Cells[4])).DataSource = LMprimeConst.INDEPENDENT_TICKET;

                            ((DataGridViewComboBoxCell)(row.Cells[4])).Value = LMprimeConst.INDEPENDENT_TICKET[0];
                        }

                        /*  Setting Priority List */
                        if (AppDelegate.ltPRIORITY_LIST != null && AppDelegate.ltPRIORITY_LIST.Count > 0)
                        {
                            ((DataGridViewComboBoxCell)(row.Cells[5])).DataSource = AppDelegate.ltPRIORITY_LIST;
                        }

                        /*  Setting Components List */
                        if (AppDelegate.ltPROJ_COMPONENTS != null && AppDelegate.ltPROJ_COMPONENTS.Count > 0)
                        {
                            ((DataGridViewComboBoxCell)(row.Cells[6])).DataSource = AppDelegate.ltPROJ_COMPONENTS;
                        }

                        /*  Assigning Unique GUID to row, This will help us to map a tree node with data grid view node, as tree node contains this unique GUID in its name */
                        row.Tag = AppDelegate.statSelectedElement.ElementGUID;

                        dgElemDetails.Rows.Add(row);

                        dgElemDetails.AllowUserToAddRows = false;

                        dgElemDetails.Update();
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            /*  This function adds Row To Data Grid View */
            public void addRowToDataGridView(string strElementGuid, ref DataGridView dgElemDetails)
            {
                CreateIssueOperationDGV cIssueDgv = new CreateIssueOperationDGV();
                List<string> ltExistingTicId      = new List<string>();

                try
                {
                    if (strElementGuid != null && strElementGuid.Length > 0 && dgElemDetails != null)
                    {
                        /*  This is extra check for the top line element, we have not to add it as it will already be there as top line. */
                        if (strElementGuid == AppDelegate.statSelectedElement.ElementGUID)
                            return;

                        /*  Checking whether row exists or not, if not then adding it to data grid view. */
                        if (!cIssueDgv.checkRowExists(dgElemDetails, strElementGuid))
                        {
                            DataGridViewRow row = new DataGridViewRow();

                            /*  Taking Data Grid View row, count will be total no. of rows */
                            row = (DataGridViewRow)dgElemDetails.Rows[dgElemDetails.Rows.Count - 1].Clone();

                            EA.Element tempElem = AppDelegate.statSel_Repository.GetElementByGuid(strElementGuid);

                            if (tempElem != null && tempElem.Name != null && tempElem.Name.Length > 0)
                            {
                                row.Cells[0].Value = tempElem.Name;
                            }

                            /*  Getting Issue Type List */
                            List<string> ltIssueTypList = cIssueDgv.getIssueTypListForProject(strElementGuid, AppDelegate.strSEL_JIRA_PROJECT);

                            if (ltIssueTypList != null && ltIssueTypList.Count > 0)
                            {
                                DataGridViewComboBoxCell issueType = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[1];

                                if (issueType != null && issueType.Value != null)
                                {
                                    if (!(issueType.Value.ToString().Equals(LMprimeConst.TASK, StringComparison.InvariantCultureIgnoreCase)))
                                    {
                                        /*  Removing Sub task from top element if it exists */
                                        if (ltIssueTypList.Contains(LMprimeConst.SUBTASK))
                                        {
                                            ltIssueTypList.Remove(LMprimeConst.SUBTASK);
                                        }

                                        if (ltIssueTypList.Contains(LMprimeConst.SUBTASK_CAPS_OFF))
                                        {
                                            ltIssueTypList.Remove(LMprimeConst.SUBTASK_CAPS_OFF);
                                        }
                                    }

                                    ((DataGridViewComboBoxCell)(row.Cells[1])).DataSource = ltIssueTypList;
                                }
                                
                            }

                            /*  Setting Existing JIRA Issue ID's */
                            ltExistingTicId = cIssueDgv.getExistingJiraIssueId(strElementGuid);

                            if (ltExistingTicId != null && ltExistingTicId.Count > 0)
                            {
                                //ltExistingTicId.Sort();

                                ltExistingTicId.Reverse();

                                ((DataGridViewComboBoxCell)(row.Cells[2])).DataSource = ltExistingTicId;

                                ((DataGridViewComboBoxCell)(row.Cells[2])).Value = ltExistingTicId[0];
                            }

                            /*  Setting Project Users For Assignee */
                            if (AppDelegate.strSEL_JIRA_PROJECT != null)
                            {
                                if (AppDelegate.dtPROJ_USERS != null && AppDelegate.dtPROJ_USERS.Count > 0 && AppDelegate.dtPROJ_USERS.ContainsKey(AppDelegate.strSEL_JIRA_PROJECT))
                                {
                                    List<string> ltTempProjUsers = new List<string>();

                                    ltTempProjUsers = AppDelegate.dtPROJ_USERS[AppDelegate.strSEL_JIRA_PROJECT];

                                    if (ltTempProjUsers != null && ltTempProjUsers.Count > 0)
                                    {
                                        ((DataGridViewComboBoxCell)(row.Cells[3])).DataSource = ltTempProjUsers;

                                        ((DataGridViewComboBoxCell)(row.Cells[3])).Value = ltTempProjUsers[0];
                                    }
                                }
                            }

                            /*  For setting Operation */
                            ((DataGridViewComboBoxCell)(row.Cells[4])).DataSource = LMprimeConst.OPERATION_LIST_COMPLETE;

                            ((DataGridViewComboBoxCell)(row.Cells[4])).Value = LMprimeConst.OPERATION_LIST_COMPLETE[0];

                            /*  Setting Priority List */
                            if (AppDelegate.ltPRIORITY_LIST != null && AppDelegate.ltPRIORITY_LIST.Count > 0)
                            {
                                ((DataGridViewComboBoxCell)(row.Cells[5])).DataSource = AppDelegate.ltPRIORITY_LIST;
                            }

                            /*  Setting Project Components */
                            if (AppDelegate.ltPROJ_COMPONENTS != null && AppDelegate.ltPROJ_COMPONENTS.Count > 0)
                            {
                                ((DataGridViewComboBoxCell)(row.Cells[6])).DataSource = AppDelegate.ltPROJ_COMPONENTS;
                            }

                            row.Tag = strElementGuid;

                            row.ReadOnly = false;

                            dgElemDetails.Rows.Add(row);

                            makeReadOnlyFalse(ref dgElemDetails);

                        }
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            /*  This function adds Row To Data Grid View */
            public void removeRowFromDataGridView(string strElementGuid, ref DataGridView dgElemDetails)
            {
                CreateIssueOperationDGV cIssueDgv = new CreateIssueOperationDGV();

                try
                {
                    if (strElementGuid != null && strElementGuid.Length > 0 && dgElemDetails != null)
                    {
                        if (strElementGuid == AppDelegate.statSelectedElement.ElementGUID)
                        {
                            return;
                        }

                        /*  Checking whether row exists in Data Grid View or not, if yes then removing that row */
                        if (cIssueDgv.checkRowExists(dgElemDetails, strElementGuid))
                        {
                            int position = cIssueDgv.findRowPosition(dgElemDetails, strElementGuid);

                            dgElemDetails.Rows.RemoveAt(position);
                        }
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            /*  This function modifies existing data Grid View, that it data Grid View in Sync with the Data Grid View*/
            public void modifyExistingDataGridView(TreeView tvElemBrowser,ref DataGridView dgElemDetails)
            {
                CreateIssueTreeView cIssueTreeView = new CreateIssueTreeView();
                List<TreeNode> lTNode              = new List<TreeNode>();

                try
                {
                
                    if(tvElemBrowser != null && dgElemDetails != null)
                    {
                        lTNode = cIssueTreeView.createFlatList(tvElemBrowser.Nodes);

                        if(lTNode != null && lTNode.Count > 0)
                        {
                            for (int iDx = 0; iDx < dgElemDetails.Rows.Count; iDx++)
                            {
                                DataGridViewRow row = (DataGridViewRow)dgElemDetails.Rows[iDx];

                                if (row != null)
                                {
                                    string elemGuid = row.Tag.ToString();

                                    if (elemGuid != null && elemGuid.Length > 0)
                                    {
                                        for (int iFx = 0; iFx < lTNode.Count; iFx++)
                                        {
                                            if (lTNode[iFx] != null)
                                            {
                                                if (lTNode[iFx].Name.CompareTo(elemGuid) == 0 && lTNode[iFx].Checked == true)
                                                    
                                                    break;
                                                if (iFx == lTNode.Count - 1)
                                                {
                                                    dgElemDetails.Rows.RemoveAt(iDx);
                                                    
                                                    iDx--;
                                                }
                                            }
                                        }
                                     }
                                }
                            }
                        }
                    }
                }
                catch(Exception exception)
                {
                    throw exception;
                }
            }

        /*  Initially making all the collumns as read only as false */
        public void makeReadOnlyFalse(ref DataGridView dgElemDetails)
        {
            try
            {
                if(dgElemDetails != null)
                {
                    int iRowCount = dgElemDetails.Rows.Count;

                    if(iRowCount > 1)
                    {
                        DataGridViewComboBoxCell issueType  = (DataGridViewComboBoxCell)dgElemDetails.Rows[iRowCount-1].Cells[1];
                        DataGridViewComboBoxCell assignee   = (DataGridViewComboBoxCell)dgElemDetails.Rows[iRowCount-1].Cells[3];
                        DataGridViewComboBoxCell priority   = (DataGridViewComboBoxCell)dgElemDetails.Rows[iRowCount-1].Cells[5];
                        DataGridViewComboBoxCell components = (DataGridViewComboBoxCell)dgElemDetails.Rows[iRowCount-1].Cells[6];

                        issueType.ReadOnly  = false;
                        priority.ReadOnly   = false;
                        assignee.ReadOnly   = false;
                        components.ReadOnly = false;
                    }
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
    }
}
