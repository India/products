﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeSubMenu.cs

   Description: This File is specific to sub menus.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file is specific to sub menus.
 * 
 * List<String> getSubMenu()-
 *  Function to return list of Sub menus.
 *  
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMprimeSubMenu
    {
        public List<String> getSubMenu()
        {
            List<string> ltTempList = new List<string>();

            ltTempList.Add(LMPrimeMenuConstants.MENU_CREATE_JIRA_ISSUE);

            ltTempList.Add(LMPrimeMenuConstants.MENU_ABOUT);

            return ltTempList;
        }
    }
}



