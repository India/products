﻿/*======================================================================================================================================================================
                                    Copyright 2017  LMtec
                                Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

Filename: LMprimeJIRAMain.cs

Description: This File is entry point for Creating JIRA Issue.

========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-May-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/* Class Description - This file is entry point for creating JIRA issue
* 
* void createJiraIssueMain()-
*  Entry Function for creating JIRA Issue.It first checks whether user is Logged In or not.
*  If user is Logged In then it displays Create Issue Dialog.
*  If user is not Logged In then it displays Login dialog.
*  
**/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprime.userinterface.login;
using LMprime.userinterface.createIssue;
using LMprime.constants;

namespace LMprime.lmprimemenu
{
    class LMprimeJIRAMain
    {
        public void createJiraIssueMain()
        {
            Login login = new Login();
            
            try
            {
                /*  If user is not logged in then displaying login dialog   */
                if (!AppDelegate.bIS_USER_LOGGED_IN)
                {
                    try
                    {
                        /*  Displaying the login dialog at central position */
                        login.StartPosition = FormStartPosition.CenterParent;
                        
                        login.ShowDialog();
                    }
                    catch (Exception exception)
                    {
                        /*  If any exception occures during login process then setting app delegate bIS_USER_LOGGED_IN variable to false. */ 
                        AppDelegate.bIS_USER_LOGGED_IN = false;

                        throw exception;
                    }
                }
                else
                {
                    /*  If User is Logged in showing Create Isssue tabbed Control   */
                    CreateIssue mControl;
                    
                    /*  Show create Issue Dialog    */
                    mControl = (CreateIssue)AppDelegate.statSel_Repository.AddTab(LMprimeConst.TAB_WINDOW_NAME, LMprimeConst.TAB_WINDOW_LOCATION);

                    mControl.Show();
                
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}















