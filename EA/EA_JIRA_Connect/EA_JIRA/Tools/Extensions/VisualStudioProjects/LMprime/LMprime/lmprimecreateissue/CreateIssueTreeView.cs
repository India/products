﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueTreeView.cs

   Description: This File contains Creation Logics related to Tree View.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprime.userinterface.createIssue;

namespace LMprime.lmprimecreateissue
{
    class CreateIssueTreeView
    {
        CreateIssueOperationDGV cIssueDgv = new CreateIssueOperationDGV();
        List<TreeNode> flatNodeList       = new List<TreeNode>();         
        
        /*
         * This function constructs element Browser, It takes whole TreeView by reference as Input Arguments,
         * It first gets App delegate Selected element as Set it as Top line. Note During the setting of Top line,
         * it checks whether a jira tickets exists for this element or not if yes then green image is shown n node,
         * otherwise blue image is shown on node.
         * After that it calls other function to add child nodes in parent node, and passes the parent node as Argument. 
         * The name of node in tree view consists of GUID of the lement for that line.
         * * */
        public void constructElementBrowser(ref TreeViewCustom tvElemBrowser)
        {
            TreeNode tNode               = new TreeNode();
            List<string> ltExistingTicId = null;

            try
            {
                ltExistingTicId = cIssueDgv.getExistingJiraIssueId(AppDelegate.statSelectedElement.ElementGUID);

                /*  If it consists existing ticket id then green icon should be displayed */
                if (ltExistingTicId != null && ltExistingTicId.Count > 0)
                {
                    tNode.ImageIndex = 0;
                 
                    tNode.SelectedImageIndex = 0;
                }
                else
                {
                    tNode.ImageIndex = 1;

                    tNode.SelectedImageIndex = 1;
                }

                /* Setting Top Node as App Delegate Selected Element as Selected Element will always be top Node*/
                tNode.Text = AppDelegate.statSelectedElement.Name;

                tNode.Name = AppDelegate.statSelectedElement.ElementGUID;

                tNode.Checked = true;

                tvElemBrowser.Nodes.Add(tNode);

                /*  Calling child nodes to add, Entry point for creating entire tree recursively */
                AddChildNodes(tNode, AppDelegate.statSelectedElement, AppDelegate.bIS_BI_DIR_ALLOWED, ref tvElemBrowser);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* This function add child nodes to the tree view recursively, It extratcs the related element list from GetMappingRelObjects class
         * based on the allowed operation(Bi Direction/Uni direction) etc. and the recursive add child elements.
         * It checks whether a jira tickets exists for this element or not if yes then green image is shown n node,
         * otherwise blue image is shown on node.
         * */
        public void AddChildNodes(TreeNode tempNode, EA.Element inpElem, Boolean isBiDir, ref TreeViewCustom tvElemBrowser)
        {
            GetMappingRelObjects gMapRelObj = new GetMappingRelObjects();

            try
            {
                /*  Getting Related Element List */
                List<EA.Element> ltChildElem = gMapRelObj.getRelElemForTree(inpElem, isBiDir);

                if (tempNode != null && ltChildElem != null && ltChildElem.Count > 0)
                {
                    /*  Checking if Node corresponding to any element exists in tree or not if yes then removing it from tree*/
                    checkForPreExistence(ref ltChildElem, ref tvElemBrowser);

                    /*  Adding all child nodes */
                    AddNodes(tempNode, ltChildElem);

                    for (int iDx = 0; iDx < ltChildElem.Count; iDx++)
                    {
                        /* Recursivey adding child nodes */
                        AddChildNodes(tempNode.Nodes[iDx], ltChildElem[iDx], isBiDir, ref tvElemBrowser);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function add child nodes to a node, It takes a parent node and List of elements which have to be added as child nodes.
         *  Element GUID is stored as internal name(Name) of node in tree. 
         * */
        public void AddNodes(TreeNode inpNode, List<EA.Element> inpList)
        {
            List<string> ltExistingTicId = null;
            
            try
            {
                if (inpNode != null && inpList != null && inpList.Count > 0)
                {
                    for (int iDx = 0; iDx < inpList.Count; iDx++)
                    {
                        TreeNode tempNode = new TreeNode();

                        if (inpList[iDx] != null)
                        {
                            tempNode.Text = inpList[iDx].Name;

                            tempNode.Name = inpList[iDx].ElementGUID;

                            inpNode.Nodes.Add(tempNode);

                            ltExistingTicId = cIssueDgv.getExistingJiraIssueId(tempNode.Name);

                            /*  Displaying image based on whether JIRA ID exists for selected node in context of current project */
                            if (ltExistingTicId != null && ltExistingTicId.Count > 0)
                            {
                                tempNode.ImageIndex = 0;

                                tempNode.SelectedImageIndex = 0;
                            }
                            else
                            {
                                tempNode.ImageIndex = 1;

                                tempNode.SelectedImageIndex = 1;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This checks whether that Element contained in List are already present in tree or not,
         *  if yes then it removes these nodes from input list.
         **/
        public void checkForPreExistence(ref List<EA.Element> ltInpElem, ref TreeViewCustom tvElemBrowser)
        {
            try
            {
                if(ltInpElem != null && ltInpElem.Count > 0 && tvElemBrowser != null)
                {
                    createFlatList(tvElemBrowser.Nodes);

                    if(flatNodeList != null && flatNodeList.Count > 0)
                    {
                        /*  Comparing all elements is List from That of Elements that exists in tree view */
                        for (int iDx = 0; iDx < ltInpElem.Count; iDx++)
                        {
                            for (int iFx = 0; iFx < flatNodeList.Count; iFx++)
                            {
                                if(ltInpElem[iDx] != null && flatNodeList[iFx] != null && flatNodeList[iFx].Name.Length > 0)
                                {
                                    /*  Checking whether element from input element list exists in tree view, if yes then removing it from input element list */
                                    if (ltInpElem[iDx].ElementGUID.CompareTo(flatNodeList[iFx].Name) == 0)
                                    {
                                        ltInpElem.RemoveAt(iDx);

                                        iDx--;

                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
       
        /* This function creates a flat lst of all nodes in a tree */
        public List<TreeNode> createFlatList(TreeNodeCollection tNodesCol)
        {
            try
            {
                foreach (TreeNode tNode in tNodesCol)
                {
                    if (tNode != null)
                    {
                        
                        flatNodeList.Add(tNode);

                        /*  Recursing creating a flat nodes */
                        if (tNode.Nodes.Count != 0)
                        {
                            createFlatList(tNode.Nodes);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return flatNodeList;
        }
    }
}









