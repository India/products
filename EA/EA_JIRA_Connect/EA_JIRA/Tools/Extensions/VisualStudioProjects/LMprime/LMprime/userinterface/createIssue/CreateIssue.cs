﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssue.cs

   Description: This File is specific to Create Issue UI element.
========================================================================================================================================================================
Date          Name                  Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.lmprimebeans;
using LMprime.appdelegate;
using LMprimeEA.eadataengine;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;
using LMprime.lmprimecreateissue;
using LMprime.lmprimecreateissue.createissuefinalimpl;
using LMprime.constants;
using LMprime.requestbeans;
using LMprimeRest.restgeneraReqAndResp;
using LMprime.userinterface.progressBar;
using LMprime.userinterface.createIssue;


namespace LMprime.userinterface.createIssue
{
    public partial class CreateIssue : UserControl
    {
        CreateIssueEAJIRADGV                cIssueEAJiraDGV     = new CreateIssueEAJIRADGV();
        CreateIssueOperationDGV             cIssueDgv           = new CreateIssueOperationDGV();
        CreateIssueProjectUtil              cIssueProjectUtil   = new CreateIssueProjectUtil();
        CreateIssueTreeView                 cIssueTV            = new CreateIssueTreeView();
        CreateIssueOperationDGVConstruct    cIssueOPDGV         = new CreateIssueOperationDGVConstruct();
        CreateIssueValidateOperationDGVData cIssueValidate      = new CreateIssueValidateOperationDGVData();
        Boolean                             bIsFirstTimeLoaded  = true;
        public static CreateIssue           stCreateIssue       = null;

        /*  Default Constructor */
        public CreateIssue()
        {
            /*  Initialize component */
            InitializeComponent();
        }

        /*  Function that is fired when user control is Initially Loaded */
        private void CreateIssue_Load(object sender, EventArgs e)
        {
            /*  Starting point for initialization of complete dialog    */
            initializeCompleteDialog();

            /*  This static variable will be used in callback responces */
            stCreateIssue = this;
        }

        /*  Entry Point Initializes Complete Dialog */
        public void initializeCompleteDialog()
        {
            string strTicketId = "";

            try
            {
                /*  Set Select Operation Default Value*/
                setOperationListDefaultValue();

                /*  Load Project List   */
                loadProjectList();

                /*  Load Tree View*/
                loadElementBrowser();

                /*  Load Opeartion View */
                loadOperationDataGV();

                /*  Getting Ticket ID for top line */
                strTicketId = cIssueEAJiraDGV.getIssueIdForTreeElem(dgElemDetails.Rows[0].Tag.ToString(), dgElemDetails);
                
                /*  Loading Web Browser */
                loadWebBrowser(strTicketId);

                ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                reqIssueDetail.strJIRAID = strTicketId;

                AppDelegate.strELEMGUID = AppDelegate.statSelectedElement.ElementGUID;

                /*  Object is passed as null, as no need to provide any input data for it */
                CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                cpBar.ShowDialog();

                bIsFirstTimeLoaded = false;
             }
            catch(Exception exception)
            {
                /*  No need to throw exception from here, it is entry point , exception will become unhandled */
            }
        }

        /*  Function to reset entire dialog */
        public void resetEntireDialog()
        {
            try
            {
                AppDelegate.bIS_BI_DIR_ALLOWED = true;

                tvElemBrowser.Nodes.Clear();

                loadElementBrowser();

                /*  Loading Operation Data Grid View */
                loadOperationDataGV();

                CreateIssueEAJIRADGV cIssueEAJiraDGV = new CreateIssueEAJIRADGV();

                string strTicketId = cIssueEAJiraDGV.getIssueIdForTreeElem(dgElemDetails.Rows[0].Tag.ToString(), dgElemDetails);

                if (strTicketId != null && strTicketId.Length > 0)
                {
                    loadWebBrowser(strTicketId);
                }

                ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                reqIssueDetail.strJIRAID = strTicketId;

                AppDelegate.strELEMGUID = AppDelegate.statSelectedElement.ElementGUID;

                /*  Object is passed as null, as no need to provide any input data for it */
                CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                cpBar.ShowDialog();
            }
            catch (Exception exception)
            {
                /* No need to throw exception, it will become unhandeled */
            }
        }
        
        /*****************************Select Project Section starts*******************************************/

        /* This function sets Project list in Select Project Dialog Initially */
        public void loadProjectList()
        {
            List<string> ltProjectList = new List<string>();
            
            try
            {
                ltProjectList = cIssueProjectUtil.getProjectList();

                if (ltProjectList != null && ltProjectList.Count > 0)
                {
                    cbSelectProject.DataSource = ltProjectList;

                    /*  Initial Setting of Selected Project to App Delegate */
                    AppDelegate.strSEL_JIRA_PROJECT = ltProjectList[0];
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handler that gets called when user changes project */
        private void cbSelectProject_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if(cbSelectProject.Text != null && cbSelectProject.Text.Length > 0)
                {
                    AppDelegate.strSEL_JIRA_PROJECT = cbSelectProject.Text;
                    /*  
                     *  Object is passed as null, as no need to provide any input data for it,
                     *  This component request is only send from this form when user has changed project,
                     *  It has not been sent from anywhere, so in the responce of this component request,
                     *  entire dialog is reset and created as new.
                     */
                    CustomProgressBar cpBar = new CustomProgressBar(null, LMprimeConst.REQ_GET_COMPONENTS);

                    cpBar.ShowDialog();
                }
            }
            catch (Exception exception)
            {
                /*  No need to throw exception it, it will become unhandeled */
            }
        }

        /*****************************Select Project Section ends*******************************************/

        /*****************************Select Operation starts**********************************************/
        
        /*  For Showing Bi Directional Operation Selected at Default, At Start */
        public void setOperationListDefaultValue()
        {
            try
            {
                cbOperations.SelectedIndex = 1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handler that fires when user changes its click in any Bi directional Operation */
        private void cbOperations_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                /*  Initially when the dialog gets constructed, this event gets fired but as dialog is getting constructed and all the components
                 *  on the dialog are already in creation phase, then we do not need to handle this event.
                 **/
                if (bIsFirstTimeLoaded != true)
                {
                    /*  Uni directional */
                    if (cbOperations.SelectedIndex == 0)
                    {
                        /*  If no node is selected then cosider 1st node as selected node */
                        TreeNode tNode = null;
                        cIssueTV       = new CreateIssueTreeView();
                        cIssueOPDGV    = new CreateIssueOperationDGVConstruct();

                        /*  If user has not selected any element on tree view and selected this option, then top node will be taken in context */
                        if (tvElemBrowser.SelectedNode == null)
                        {
                            tNode = tvElemBrowser.Nodes[0];
                        }
                        else
                        {
                            tNode = tvElemBrowser.SelectedNode;
                        }

                        if (tNode != null)
                        {
                            tNode.Nodes.Clear();

                            /*  Bi directional required flag is true */
                            cIssueTV.AddChildNodes(tNode, AppDelegate.statSel_Repository.GetElementByGuid(tNode.Name), false, ref tvElemBrowser);

                            /*  Adjusting the data grid view in context with the tree view */
                            cIssueOPDGV.modifyExistingDataGridView(tvElemBrowser, ref dgElemDetails);
                        }
                    }
                    else if (cbOperations.SelectedIndex == 1)
                    {
                        TreeNode tNode  = null;
                        cIssueTV        = new CreateIssueTreeView();
                        cIssueOPDGV     = new CreateIssueOperationDGVConstruct();

                        /*  If user has not selected any element on tree view and selected this option, then top node will be taken in context */
                        if (tvElemBrowser.SelectedNode == null)
                        {
                            tNode = tvElemBrowser.Nodes[0];
                        }
                        else
                        {
                            tNode = tvElemBrowser.SelectedNode;
                        }

                        if (tNode != null)
                        {

                            tNode.Nodes.Clear();

                            /*  Bi directional required flag is true */
                            cIssueTV.AddChildNodes(tNode, AppDelegate.statSel_Repository.GetElementByGuid(tNode.Name), true, ref tvElemBrowser);

                            /*  Adjusting the data grid view in context with the tree view */
                            cIssueOPDGV.modifyExistingDataGridView(tvElemBrowser, ref dgElemDetails);
                        }
                    }
                    else
                    {
                        resetEntireDialog();
                    }
                }
            }
            catch (Exception exception)
            {
                // No need to throw exception from here , it is call back function, exception will become unhandeled
            }
        }
        /*****************************Select Operation ends***************************************************/

        /*****************************Element Browser section starts******************************************/

        /*  This function is starting point for loading element browser It creates for Top Node of tree and then
         *  calls another function which is responsible for creating the entire Tree View
         * */
        public void loadElementBrowser()
        {
            try
            {
                cIssueTV = new CreateIssueTreeView();

                cIssueTV.constructElementBrowser(ref tvElemBrowser);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handler that gets called when user checked/Unchecked any row in data grid view */
        private void tvElemBrowser_AfterCheck(object sender, TreeViewEventArgs e)
        {
            TreeNode trNode                                    = e.Node;
            CreateIssueOperationDGVConstruct cIssueOPConstruct = new CreateIssueOperationDGVConstruct();

            try
            {
                if (e.Node != null)
                {
                    if (e.Node.Checked)
                    {
                        cIssueOPConstruct.addRowToDataGridView(trNode.Name, ref dgElemDetails);

                        tvElemBrowser.SelectedNode = trNode;

                        /*  Now tvElemBrowser_AfterSelect will be called and it will take care of rest of the things */
                    }
                    else
                    {
                        /*  If first element is checked then returning */
                        if (e.Node.Name.Equals(AppDelegate.statSelectedElement.ElementGUID, StringComparison.InvariantCultureIgnoreCase))
                        {
                            e.Node.Checked = true;

                            return;
                        }

                        cIssueOPConstruct.removeRowFromDataGridView(trNode.Name, ref dgElemDetails);
                    }
                }
            }
            catch (Exception exception)
            {
                /*  Do not throw exception, otherwise it will become unhandeled */
            }
        }

        /*   This function is for handling the after select operation on Tree View, Note After Check and After Selected both are diffrent events
         *   When an node is selected then in after select event we perform 2 operations, we load web browser, we 
         *   populate the information in EA-JIRA Data Grid View
         * */
        private void tvElemBrowser_AfterSelect(object sender, TreeViewEventArgs e)
        {
            TreeNode trNode                      = e.Node;
            CreateIssueEAJIRADGV cIssueEAJiraDGV = new CreateIssueEAJIRADGV();

            try
            {
                if (trNode != null)
                {
                    string strTempTikId = cIssueEAJiraDGV.getIssueIdForTreeElem(trNode.Name, dgElemDetails);

                    if (strTempTikId != null && strTempTikId.Length > 0)
                    {
                        loadWebBrowser(strTempTikId);
                    }

                    /*  Even if ticket ID does not exists, then also we have to add this event as in EA JIRA view empty mapping is shown. */
                    ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                    reqIssueDetail.strJIRAID = strTempTikId;

                    AppDelegate.strELEMGUID = trNode.Name;

                    /*  Object is passed as null, as no need to provide any input data for it */
                    CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                    cpBar.ShowDialog();
                }
            }
            catch (Exception exception)
            {
                /*  Do not throw exception, otherwise it will become unhandeled */
            }
        }
        /*****************************Element Browser section ends********************************************/


        /*****************************Operation Data Grid View Starts ******************************/

        /*  Handling data error event for comoonents display */
        private void dgElemDetails_DataError(object sender, DataGridViewDataErrorEventArgs anError)
        {
            try
            {
            }
            catch (Exception exception)
            {
                /*  Do not throw exception, otherwise it will become unhandeled */
            }

        }

        /*  Starting point of loading of data grid view */
        public void loadOperationDataGV()
        {
            try
            {
                cIssueOPDGV = new CreateIssueOperationDGVConstruct();


                do
                {
                    foreach (DataGridViewRow row in dgElemDetails.Rows)
                    {
                        try
                        {
                            dgElemDetails.Rows.Remove(row);
                        }
                        catch (Exception) { }
                    }
                } while (dgElemDetails.Rows.Count > 1);
                
                
                //dgElemDetails.DataSource = null;
                
                //dgElemDetails.Rows.Clear();
                
                dgElemDetails.Update();
                
                dgElemDetails.Refresh();

                cIssueOPDGV.createMainRowDGV(ref dgElemDetails);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Data Grid View Drag and Drop Code starts, This code cannot be shifted to other class, this needs to be in same file */
        private Rectangle dragBoxFromMouseDown;
        private int rowIndexFromMouseDown;
        private int rowIndexOfItemUnderMouseToDrop;
        private void dgElemDetails_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                /* If the mouse moves outside the rectangle, start the drag. */
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {

                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgElemDetails.DoDragDrop(
                    dgElemDetails.Rows[rowIndexFromMouseDown],
                    DragDropEffects.Move);
                }
            }
        }

        private void dgElemDetails_MouseDown(object sender, MouseEventArgs e)
        {
            /* Get the index of the item the mouse is below. */
            rowIndexFromMouseDown = dgElemDetails.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                /* Remember the point where the mouse down occurred. 
                 * The DragSize indicates the size that the mouse can move 
                 * before a drag event should be started.
                 **/
                Size dragSize = SystemInformation.DragSize;

                /* Create a rectangle using the DragSize, with the mouse position being
                 * at the center of the rectangle.
                 **/
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                    dragSize);
            }
            else
                /* Reset the rectangle if the mouse is not over an item in the ListBox. */
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgElemDetails_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void dgElemDetails_DragDrop(object sender, DragEventArgs e)
        {
            /* The mouse locations are relative to the screen, so they must be 
             * converted to client coordinates.
             * */
            Point clientPoint = dgElemDetails.PointToClient(new Point(e.X, e.Y));

            /* Get the row index of the item the mouse is below. */
            rowIndexOfItemUnderMouseToDrop =
                dgElemDetails.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            /* If the drag operation was a move then remove and insert the row. */
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                    typeof(DataGridViewRow)) as DataGridViewRow;
                if (rowIndexOfItemUnderMouseToDrop != -1)
                {
                    if (rowIndexOfItemUnderMouseToDrop != 0 && rowToMove.Index != 0)
                    {
                        dgElemDetails.Rows.RemoveAt(rowIndexFromMouseDown);
                        dgElemDetails.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);
                    }

                }

            }
        }

        private void dgElemDetails_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                this.dgElemDetails.ClearSelection();
            }
            catch (Exception exception)
            {
                /*  Do not throw exception, otherwise it will become unhandeled */
            }
        }
        /*  Data Grid View Drag and Drop Listener code ends */

        /*  Event handlers */

        void dgElemDetails_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (this.dgElemDetails.IsCurrentCellDirty)
            {
                /* This fires the cell value changed handler below */
                dgElemDetails.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgElemDetails_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    cIssueDgv = new CreateIssueOperationDGV();

                    /*  For adjusting changes in data grid view when user changes operation type of parent */
                    if (e.RowIndex == 0 && e.ColumnIndex == 4)
                    {
                        cIssueDgv.adjustDGSetInContext(ref dgElemDetails);

                        DataGridViewComboBoxCell operationTyp = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[4];
                        DataGridViewComboBoxCell tikID        = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[2];

                        if (operationTyp != null && operationTyp.Value != null && operationTyp.Value.ToString().CompareTo(LMprimeConst.SET_IN_CONTEXT) == 0)
                        {
                            if (tikID != null && tikID.Value != null && tikID.Value.ToString().Length > 0)
                            {
                                AppDelegate.IS_SET_IN_CONTEXT = true;

                                ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                                reqIssueDetail.strJIRAID = tikID.Value.ToString();

                                /*  Object is passed as null, as no need to provide any input data for it */
                                CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                                cpBar.ShowDialog();
                            }
                        }
                    }

                    /*  When User Changes Operation type, in data grid view, this will not fire for parent */
                    if (e.RowIndex != 0 && e.ColumnIndex == 4)
                    {
                        cIssueDgv.adjustDGOperation(ref dgElemDetails, e);
                    }

                    /*  When User changes issue type of parent */
                    if (e.RowIndex == 0 && e.ColumnIndex == 1)
                    {
                        cIssueDgv.adjustIssueTypeOfParent(ref dgElemDetails);
                    }

                    /*  When user changes Issue type of child */
                    if(e.RowIndex != 0 && e.ColumnIndex == 1)
                    {
                        cIssueDgv.adjustIssueTypeOfChild(ref dgElemDetails, e);
                    }

                    /*  When user changes Existing JIRA issue id */
                    if (e.ColumnIndex == 2)
                    {
                        DataGridViewComboBoxCell tikID        = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[2];
                        DataGridViewComboBoxCell operationTyp = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[4];

                        if (tikID != null && tikID.Value != null)
                        {
                            /*  Even if ticket ID does not exists, then also we have to add this event as in EA JIRA view empty mapping is shown. */
                            ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                            reqIssueDetail.strJIRAID = tikID.Value.ToString();

                            if (tikID.Value.ToString().Length > 0)
                            {
                                loadWebBrowser(tikID.Value.ToString());
                            }
                            if (operationTyp != null && operationTyp.Value != null && operationTyp.Value.ToString().CompareTo(LMprimeConst.SET_IN_CONTEXT) == 0)
                            {
                                AppDelegate.IS_SET_IN_CONTEXT = true;
                            }

                            /*  Object is passed as null, as no need to provide any input data for it */
                            CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                            cpBar.ShowDialog();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                /*  No need to throw exception else it will be unhandeled */
            }
        }

        /* Event Fired when Cell content is clicked */
        private void dgElemDetails_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dgElemDetails.ClearSelection();

            if (e.RowIndex != -1 && e.ColumnIndex != -1 && e.ColumnIndex == 0)
            {
                DataGridViewRow row = dgElemDetails.Rows[e.RowIndex];

                if (row != null)
                {
                    cIssueEAJiraDGV = new CreateIssueEAJIRADGV();

                    string strValue = cIssueEAJiraDGV.getIssueIdForTreeElem(dgElemDetails.Rows[e.RowIndex].Tag.ToString(), dgElemDetails);

                    if (strValue != null && strValue.Length > 0)
                    {
                        loadWebBrowser(strValue);
                    }
                    
                    ReqIssueDetails reqIssueDetail = new ReqIssueDetails();

                    reqIssueDetail.strJIRAID = strValue;

                    AppDelegate.strELEMGUID = dgElemDetails.Rows[e.RowIndex].Tag.ToString();
                    
                    /*  Object is passed as null, as no need to provide any input data for it */
                    CustomProgressBar cpBar = new CustomProgressBar(reqIssueDetail, LMprimeConst.REQ_GET_ISSUE_DETAIL);

                    cpBar.ShowDialog();
                }
            }
        }
        /*****************************Element information Data Grid View Stops *******************************/
        
        /************************  Web Browser Function starts *********************************************/

        /*  If ticket ID exists the loading the exact ticket otherwise loading the home page of the Project */
        public void loadWebBrowser(string strTikId)
        {
            try
            {
                if (strTikId != null && strTikId.Length > 0 && strTikId.CompareTo("") != 0)
                {
                    strTikId = AppDelegate.strJIRA_URL + LMprimeConst.SEP_FRWD_SLASH + LMprimeConst.BROWSE + strTikId;

                    wbJIRABrowser.Navigate(strTikId);
                }
                else
                {
                    wbJIRABrowser.Navigate(AppDelegate.strJIRA_URL + LMprimeConst.SEP_FRWD_SLASH);
                }
            }
            catch (Exception exception)
            {
                /*  Exception will not be throwm from here, otheriwise ot will result to unhandled exception */
            }
        }
        /***********************************Web Browser Function ends ****************************************/

        /************************Submit Button and common Functions Starts*******************************************************/
        
        private void btSubmit_Click(object sender, EventArgs e)
        {
            Boolean isDGValidated = true;
            String strErrorString = "";
            cIssueValidate      = new CreateIssueValidateOperationDGVData();

            isDGValidated = cIssueValidate.vaildateOperationDGV(ref dgElemDetails, ref strErrorString);

            if (!isDGValidated && strErrorString != null && strErrorString.Length > 0)
            {
                MessageBox.Show(strErrorString, LMprimeConst.ERROR);
            }
            else
            {

                ReqCreateIssue reqCreateIssue = new ReqCreateIssue();

                reqCreateIssue.dgElemDetails = dgElemDetails;
                
                CustomProgressBar cpBar = new CustomProgressBar(reqCreateIssue, LMprimeConst.REQ_CREATE_ISSUE);

                cpBar.ShowDialog();
            }
            return;
        }
        
        /************************Submit Button Ends*******************************************************/
        
        /*  Call back function starts */
        /*  For handling all event, in Callback */
        public void handleEvent(Object obj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case LMprimeConst.REQ_CREATE_ISSUE:

                        handleIssueCreationRequest();

                        break;
                    
                    case LMprimeConst.REQ_GET_ISSUE_DETAIL:
                        
                        handleIssueDetailRequest();
                        
                        break;

                    case LMprimeConst.REQ_GET_COMPONENTS:
                        
                        handleComponentsRequest();
                        
                        break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Call back Responce functions */

        /*  This callback function we receive when user has change project, because on project change we fire request for components
         *  On the responce of this request we resets entire dialog.
         */
        public void handleComponentsRequest()
        {
            try
            {
                resetEntireDialog();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This callback function we receive when user has either selected an element from Opertaion view or tree view.,
         *  On the responce of this function we populates data in EA JIRA view
         */
        public void handleIssueDetailRequest()
        {
            cIssueEAJiraDGV = new CreateIssueEAJIRADGV();
            cIssueDgv       = new CreateIssueOperationDGV();

            try
            {
                if (AppDelegate.IS_SET_IN_CONTEXT)
                {
                    AppDelegate.IS_SET_IN_CONTEXT = false;

                    cIssueDgv.adjustChildIssueTypeForSetInContext(ref dgElemDetails);
                }
               
                cIssueEAJiraDGV.populateInfoInEAJIRADGV(AppDelegate.strELEMGUID, ref dgEAJIRADetails);
            
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function is for handling issue creation responce, after success full creation it reset dialog and displays message */

        public void handleIssueCreationRequest()
        {
            try
            {
                resetEntireDialog();

                if (AppDelegate.strOutputIDs != null && AppDelegate.strOutputIDs.Length > 0)
                {
                    MessageBox.Show(AppDelegate.strOutputIDs, LMprimeConst.SUCCESS);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}









            
  
        
    
        