﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueOperationDGV.cs

   Description: This File contains Backend Logics related to Element Info Data Grid View, also called operation view
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;
using LMprimeRest.restissuedetail;
using Atlassian.Jira;
using LMprime.constants;

namespace LMprime.lmprimecreateissue
{
    class CreateIssueOperationDGV
    {
        /*  This function checks whether existing issue exits for the element whose GUID is the input argument of function.
         *  It returns the list of ID's in context of current selected Project.
         *
         *  Logic : It finds the corresponding element from Repository based on input element GUID.
         *  It then finds out corresponding Reverse Mapping JIRA2EA object, and check for which property key is mapped.
         *  If more then one property is mapped for Key then it merges the distinct values from both EA Attributes.
         *  After that it takes selected element from App delegate and separates the issue Id's of current selected Project and returns that list.
         */
        public List<string> getExistingJiraIssueId(string strElemGuid)
        {
            List<string> lOutList = new List<string>();

            try
            {
                JIRA2EAColl jira2EA               = new JIRA2EAColl();
                EAMappingUtilities eMapUtil       = new EAMappingUtilities();
                EAReverseMappingUtil eaRevMapUtil = new EAReverseMappingUtil();

                /*  Getting element from repository based on GUID */
                EA.Element tempElem = AppDelegate.statSel_Repository.GetElementByGuid(strElemGuid);

                /*  Getting Current Reverse Mapping Object  */
                jira2EA = eMapUtil.getJIRAToEAObj(tempElem);

                if (jira2EA != null && jira2EA.ltJira2EASgl != null && jira2EA.ltJira2EASgl.Count > 0)
                {
                    lOutList = eaRevMapUtil.getJIRAID(jira2EA, tempElem, AppDelegate.strSEL_JIRA_PROJECT);
                }

                return lOutList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function check whether a Row exists in Data Grid View or not,
         *  If it exists then this function return true, else it returns false
         * */
        public Boolean checkRowExists(DataGridView dgView, string strRowTag)
        {
            try
            {
                if (dgView != null && strRowTag != null && strRowTag.Length > 0)
                {
                    foreach (DataGridViewRow dgRow in dgView.Rows)
                    {
                        if (dgRow != null)
                        {
                            if (dgRow.Tag.ToString().CompareTo(strRowTag) == 0)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function returns issue type list for selected project/Element,
         *  It first finds out mapping corresponding to element, if find then it checks whether issue type list exists for the
         *  selected element, if yes then it returns the issue type list from Mapping else it returns issue type list corresponding to project.
         **/
        public List<string> getIssueTypListForProject(string strElemGUID, string strInpPrj)
        {
            List<string> IssueTypList = new List<string>();

            try
            {
                if (strElemGUID != null && strElemGUID.Length > 0)
                {
                    if (AppDelegate.statSel_Repository != null)
                    {
                        EA.Element tempElem;

                        tempElem = AppDelegate.statSel_Repository.GetElementByGuid(strElemGUID);

                        if (tempElem != null)
                        {

                            EAPMapping eapMapping = new EAPMapping();
                            EAMappingUtilities eMapUtil = new EAMappingUtilities();

                            eapMapping = eMapUtil.getSelectedObjMapping(tempElem);

                            if (eapMapping != null && eapMapping.lmiEAP != null)
                            {
                                List<string> ltIssueTyp = eapMapping.lmiEAP.ltIssueTypeList;

                                if (ltIssueTyp != null && ltIssueTyp.Count > 0)
                                {
                                    /*  Making the copy of list as Try get Value gives reference to object */
                                    List<string> ltIssueTemp = new List<string>(ltIssueTyp.Count);

                                    ltIssueTyp.ForEach((item) =>
                                    {
                                        ltIssueTemp.Add(item);
                                    });

                                    return ltIssueTemp;
                                }
                            }
                        }
                    }
                }
                
                /*  Case when Issue type list is not found in Mapping */
                if (strInpPrj != null && strInpPrj.Length > 0)
                {
                    AppDelegate.dtPROJ_ISSUE_TYPE.TryGetValue(strInpPrj, out IssueTypList);
                }
                
                /*  Making the copy of list as Try get Value gives reference to object */
                List<string> lTemp = new List<string>(IssueTypList.Count);

                IssueTypList.ForEach((item) =>
                {
                    lTemp.Add(item);
                });
                return lTemp;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function modifies the entire Data Grid View on the basis of Change in Opertaion type in 1st Row */
        public void adjustDGSetInContext(ref DataGridView dgElemDetails)
        {
            try
            {
                if (dgElemDetails.CurrentCell != null && dgElemDetails.CurrentCell.Value != null)
                {
                    string strValue = dgElemDetails.CurrentCell.Value.ToString();

                    if (strValue != null && strValue.Length > 0)
                    {
                        DataGridViewComboBoxCell issueType  = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[1];
                        DataGridViewComboBoxCell assignee   = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[3];
                        DataGridViewComboBoxCell priority   = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[5];
                        DataGridViewComboBoxCell components = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[6];

                        if (strValue.Equals(LMprimeConst.SET_IN_CONTEXT, StringComparison.InvariantCultureIgnoreCase))
                        {
                            issueType.Value  = "";
                           
                            priority.Value   = "";
                            
                            assignee.Value   = "";
                            
                            components.Value = "";
                            
                            issueType.ReadOnly  = true;
                            
                            priority.ReadOnly   = true;
                            
                            assignee.ReadOnly   = true;
                            
                            components.ReadOnly = true;

                            adjustDGSetInContext(ref dgElemDetails, true);
                        
                        }
                        else
                        {
                            issueType.ReadOnly  = false;
                            
                            priority.ReadOnly   = false;
                            
                            assignee.ReadOnly   = false;
                            
                            components.ReadOnly = false;

                            adjustDGSetInContext(ref dgElemDetails, false);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function adjusts Operation list in Data Grid View when Operation type for parent node ois changed */
        public void adjustDGSetInContext(ref DataGridView dgElemDetails, Boolean isSetInContext)
        {
            try
            {
                /*  This function will not disturb the operation type for parent element */
                if (dgElemDetails != null && dgElemDetails.Rows  != null && dgElemDetails.Rows.Count > 1)
                {
                    /*  Excluding the Parent row    */
                    for (int iDx = 1; iDx < dgElemDetails.Rows.Count; iDx++)
                    {
                        if (dgElemDetails.Rows[iDx] != null)
                        {
                            DataGridViewComboBoxCell operation = (DataGridViewComboBoxCell)dgElemDetails.Rows[iDx].Cells[4];

                            /*  If all the rows needs to be adjusted when parent is SET In Context then isSetInContext Boolean Variable is set to true */
                            if (isSetInContext)
                            {
                                operation.DataSource = LMprimeConst.OPERATION_LIST_WITHOUT_MERGE_WITH_PARENT;

                                operation.Value = "";

                            }
                            else
                            {
                                operation.DataSource = LMprimeConst.OPERATION_LIST_COMPLETE;

                                operation.Value = "";
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void adjustChildIssueTypeForSetInContext(ref DataGridView dgElemDetails)
        {
            try
            {
                if (AppDelegate.selElemIssue != null && AppDelegate.selElemIssue.Type != null
                    && AppDelegate.selElemIssue.Type.ToString().Equals(LMprimeConst.TASK, StringComparison.InvariantCultureIgnoreCase))
                {
                    for (int iDx = 1; iDx <= dgElemDetails.Rows.Count; iDx++)
                    {
                        DataGridViewComboBoxCell issueType = (DataGridViewComboBoxCell)dgElemDetails.Rows[iDx].Cells[1];

                        List<string> ltIssueType = new List<string>();

                        ltIssueType = getIssueTypListForProject(dgElemDetails.Rows[iDx].Tag.ToString(), AppDelegate.strSEL_JIRA_PROJECT);

                        issueType.DataSource = ltIssueType;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function adjusts Data grid when operation type is changed */
        public void adjustDGOperation(ref DataGridView dgElemDetails, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgElemDetails != null && e != null)
                {
                    DataGridViewComboBoxCell issueType  = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[1];
                    DataGridViewComboBoxCell assignee   = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[3];
                    DataGridViewComboBoxCell operation  = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[4];
                    DataGridViewComboBoxCell priority   = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[5];
                    DataGridViewComboBoxCell components = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[6];

                    if (operation != null && operation.Value != null && operation.Value.ToString() != null)
                    {
                        string value = operation.Value.ToString();
                        
                        if (value != null && value.CompareTo(LMprimeConst.MERGE_WITH_PARENT) == 0)
                        {
                            issueType.Value  = "";
                            priority.Value   = "";
                            assignee.Value   = "";
                            components.Value = "";
                           
                            issueType.ReadOnly  = true;
                            priority.ReadOnly   = true;
                            assignee.ReadOnly   = true;
                            components.ReadOnly = true;
                        }
                        else
                        {
                            issueType.ReadOnly  = false;
                            priority.ReadOnly   = false;
                            assignee.ReadOnly   = false;
                            components.ReadOnly = false;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        
        /*  This function adjusts Issue Type, of data grid view */
        public void adjustIssueTypeOfParent(ref DataGridView dgElemDetails)
        {
            try
            {
                if(dgElemDetails != null && dgElemDetails.Rows.Count > 1)
                {
                    DataGridViewComboBoxCell issueType  = (DataGridViewComboBoxCell)dgElemDetails.Rows[0].Cells[1];

                    if(issueType != null && issueType.Value != null && issueType.Value.ToString() != null)
                    {
                        String strTemp           = issueType.Value.ToString();
                        List<string> ltIssueType = new List<string>();

                        if (strTemp.Equals(LMprimeConst.TASK, StringComparison.InvariantCultureIgnoreCase))
                        {
                            /*  Excluding the Parent row    */
                            for (int iDx = 1; iDx < dgElemDetails.Rows.Count; iDx++)
                            {
                                DataGridViewComboBoxCell issueTypeCol = (DataGridViewComboBoxCell)dgElemDetails.Rows[iDx].Cells[1];

                                ltIssueType = new List<string>(); 

                                ltIssueType = getIssueTypListForProject(dgElemDetails.Rows[iDx].Tag.ToString(), AppDelegate.strSEL_JIRA_PROJECT);

                                if (ltIssueType != null && ltIssueType.Count > 0)
                                {
                                    if (issueTypeCol != null && issueTypeCol.Value != null && issueTypeCol.Value.ToString().Length > 0)
                                    {
                                        string strTempItype = issueTypeCol.Value.ToString();

                                        issueTypeCol.DataSource = ltIssueType;

                                        issueTypeCol.Value = strTempItype;
                                    }
                                    else
                                    {
                                        issueTypeCol.DataSource = ltIssueType;
                                    }
                                }
                            }
                        }
                        else
                        {
                            for (int iDx = 1; iDx < dgElemDetails.Rows.Count; iDx++)
                            {
                                DataGridViewComboBoxCell issueTypeCol = (DataGridViewComboBoxCell)dgElemDetails.Rows[iDx].Cells[1];

                                ltIssueType = new List<string>(); 

                                ltIssueType = getIssueTypListForProject(dgElemDetails.Rows[iDx].Tag.ToString(), AppDelegate.strSEL_JIRA_PROJECT);

                                if (ltIssueType != null && ltIssueType.Count > 0)
                                {

                                    /*  Removing Sub task if It exists in List */
                                    if (ltIssueType.Contains(LMprimeConst.SUBTASK))
                                    {
                                        ltIssueType.Remove(LMprimeConst.SUBTASK);
                                    }

                                    if (ltIssueType.Contains(LMprimeConst.SUBTASK_CAPS_OFF))
                                    {
                                        ltIssueType.Remove(LMprimeConst.SUBTASK_CAPS_OFF);
                                    }

                                    if (issueTypeCol != null && issueTypeCol.Value != null && issueTypeCol.Value.ToString().Length > 0)
                                    {
                                        string strTempItype = issueTypeCol.Value.ToString();

                                        issueTypeCol.DataSource = ltIssueType;

                                        issueTypeCol.Value = strTempItype;
                                    }
                                    else
                                    {
                                        issueTypeCol.DataSource = ltIssueType;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* Adjust Issue type of Child */
        public void adjustIssueTypeOfChild(ref DataGridView dgElemDetails, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dgElemDetails != null && e != null)
                {
                    DataGridViewComboBoxCell issueType = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[1];

                    if (issueType != null && issueType.Value != null && issueType.Value.ToString().Length > 0)
                    {
                        string strTemp = issueType.Value.ToString();

                        DataGridViewComboBoxCell operation = (DataGridViewComboBoxCell)dgElemDetails.Rows[e.RowIndex].Cells[4];

                        if (strTemp.Equals(LMprimeConst.SUBTASK, StringComparison.InvariantCultureIgnoreCase) ||
                            strTemp.Equals(LMprimeConst.SUBTASK_CAPS_OFF, StringComparison.InvariantCultureIgnoreCase))
                        {

                            operation.DataSource = LMprimeConst.SUB_TASK;

                            operation.ReadOnly = true;
                        }
                        else
                        {
                            if (operation != null && operation.Value != null)
                            {
                                operation.DataSource = LMprimeConst.OPERATION_LIST_COMPLETE;
                                
                                operation.ReadOnly = false;
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        /*  This functions finds the position of row based on input elemeguid (tag) */
        public int findRowPosition(DataGridView dgView, string strRowTag)
        {
            try
            {
                if (dgView != null && strRowTag != null)
                {
                    for (int iDx = 0; iDx < dgView.Rows.Count; iDx++)
                    {
                        DataGridViewRow dgRow = dgView.Rows[iDx];

                        if (dgRow != null && dgRow.Tag != null && dgRow.Tag.ToString().CompareTo(strRowTag) == 0)
                        {
                            return iDx;
                        }
                    }
                }
                return -1;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}




