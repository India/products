﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReqIssueDetails.cs

   Description: This file is conatiner for holding the data when request is added on background worker thread..
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Mar-2018   Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.requestbeans
{
    class ReqIssueDetails
    {
        public string strJIRAID = "";
    }
}
