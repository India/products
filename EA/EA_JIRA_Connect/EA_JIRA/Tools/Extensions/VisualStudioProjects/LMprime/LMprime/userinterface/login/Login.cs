﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Login.cs

   Description: This File is specific to Login Dialog.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*
 * This file is specific to login dialog and this file is responsible for all login related functionality.
 * */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprime.lmprimelogin;
using LMprime.userinterface.createIssue;
using LMprime.userinterface.progressBar;
using LMprime.requestbeans;
using LMprime.constants;

namespace LMprime.userinterface.login
{
    /* Inheriting the Abstract class only when Produt is in release mode because the designer will not work when form inheritance is used  */

//#if DEBUG
    public partial class Login : Form
    {
        //#else 
        //  public partial class Login : BaseForm
        //#endif

        LMILoginMain lmiLogin       = new LMILoginMain();
        public static Login stLogin = null;
        string strUrl               = null;
        string strUsername          = null;
        string strPassword          = null;

        public Login()
        {
            InitializeComponent();
        }

        /*  Function that is called when the form is initially loaded */
        private void Login_Load(object sender, EventArgs e)
        {
            try
            {
                /*  Function to initialize data */
                initializaData();

                /*  Setting static object of login */
                stLogin = this;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /*  Initialize Data */
        public void initializaData()
        {
            try
            {
                /*  Getting JIRA URL */
                strUrl = lmiLogin.getJIRAUrl();

                /*  Displaying JIRA URL in text box and making it non editable   */
                tbJIRAUrl.Text = strUrl;

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handler that fires when user clicks on login button */
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                /*  Perform validation */
                performValidation();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, LMprimeConst.ERROR);
                return;
            }
        }

        /*  Handler that fires when user clicks on cancel button */
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        /*  Function that checks whether user has entered username or password  */
        public void performValidation()
        {

            ReqLogin reqLogin = new ReqLogin();

            strUsername = tbUsername.Text;

            strPassword = tbPassword.Text;

            strUrl = tbJIRAUrl.Text;

            try
            {
                if (tbJIRAUrl.Text == null || tbJIRAUrl.Text.Length <= 0)
                {
                    MessageBox.Show("Please Enter JIRA URL.");
                }
                if ((strUsername == null || strUsername.Length <= 0) && (strPassword == null || strPassword.Length <= 0))
                {
                    MessageBox.Show("Please enter Username and Password.");
                }
                else if (strUsername == null || strUsername.Length <= 0)
                {
                    MessageBox.Show("Username cannot be empty.");
                }
                else if (strPassword == null || strPassword.Length <= 0)
                {
                    MessageBox.Show("Password cannot be empty.");
                }
                else
                {
                    reqLogin.strUrl = strUrl;

                    reqLogin.strUsername = strUsername;

                    reqLogin.strPassword = strPassword;

                    CustomProgressBar cpBar = new CustomProgressBar(reqLogin, LMprimeConst.REQ_LOGIN_REQUEST);

                    cpBar.ShowDialog();

                }
            }
            catch (Exception exception)
            {
                /*  Setting this variable to false for considering that user has not successfully logged in */
                AppDelegate.bIS_USER_LOGGED_IN = false;

                throw exception;
            }
        }

        /*  For handling all event, in Callback */
        public void handleEvent(Object obj, int iEventType)
        {
            try
            {
                switch (iEventType)
                {
                    case LMprimeConst.REQ_LOGIN_REQUEST:
                            handleLoginRequest(obj);
                         break;

                    case LMprimeConst.REQ_CANCELLED:
                         break;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Handle login request */
        public void handleLoginRequest(Object obj)
        {
            try
            {
                UserControl uControl = new UserControl();

                AppDelegate.bIS_USER_LOGGED_IN = true;

                this.Dispose();

                /*  Show create Issue Dialog    */
                uControl = (CreateIssue)AppDelegate.statSel_Repository.AddTab(LMprimeConst.TAB_WINDOW_NAME, LMprimeConst.TAB_WINDOW_LOCATION);

                uControl.Show();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}

