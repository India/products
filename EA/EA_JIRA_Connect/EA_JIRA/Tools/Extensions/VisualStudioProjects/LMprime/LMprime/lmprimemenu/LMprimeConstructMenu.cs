﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMain.cs

   Description: This File is starting point for entire plugin.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This File is entry point for Menu definations. It defines Main Menu and Sub Menus.
 *                     This class is entry point to two specific classes that defines Main and Sub Menus.
 * getMainMenu()-
 *  Reutrns Main Menu
 * 
 * string[] getSubMenu()-
 *  Return arrays of strings which contains Sub Menus.
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMprimeConstructMenu
    {
            LMprimeMainMenu lmMainMenu = new LMprimeMainMenu();
            LMprimeSubMenu lmSubMenu   = new LMprimeSubMenu();

            /* For main menus   */
            public string getMainMenu()
            {
                string strMainMenu = lmMainMenu.getMainMenu();
            
                return strMainMenu;
            }

            /*  For sub menus   */
            public string[] getSubMenu()
            {
                List<string> ltTempList = lmSubMenu.getSubMenu();

                string[] subMenu = ltTempList.ToArray();
            
                return subMenu;
            }
      }
}





