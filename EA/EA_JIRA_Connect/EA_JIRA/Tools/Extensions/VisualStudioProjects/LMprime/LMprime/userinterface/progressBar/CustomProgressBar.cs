﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CustomProgressBar.cs

   Description: This File is related to Progress Bar Form.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Mar-2018   Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.constants;
using LMprime.reqrespengine;
using LMprime.lmprimeexception;


namespace LMprime.userinterface.progressBar
{
 //   #if DEBUG
    public partial class CustomProgressBar : Form
    {
        //   #else 
        //   public partial class CustomProgressBar : BaseForm
        //   #endif

        Object objReq                         = new Object();
        static CustomProgressBar objCPBar     = new CustomProgressBar();
        ReqRespPrcsgEngine objReqAndResp      = new ReqRespPrcsgEngine();
        LMprimeExceptionMain lmprimeException = new LMprimeExceptionMain();
        int iEventType                        = LMprimeConst.REQ_INVALID;
        
        /*  Default Constructor */
        public CustomProgressBar()
        {
            InitializeComponent();
        }

        /*  Paramaterized Constructor, to be used by each request */
        public CustomProgressBar(Object objReq, int iEventType)
        {
            InitializeComponent();
            
            this.objReq = objReq;
         
            this.iEventType = iEventType;

            objCPBar = this;

        }

        /*  This function is called when progress bar is loaded */
        private void CustomProgressBar_Load(object sender, EventArgs e)
        {
            try
            {
                startProgress();

                /*  If thread is not busy and thread is not cancelled, then starting background worker thread */
                if (!bgwProcessing.IsBusy && !bgwProcessing.CancellationPending)
                {
                    bgwProcessing.RunWorkerAsync();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Called when Process is changed */
        private void bgwProcessing_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            
        }

        /*  Called when processing it completed */
        private void bgwProcessing_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                endProgress();
                
                /* This is the case if any Exception has Occured during the background operation */
                if (e.Error != null)
                {
                    string strErrorString = null;

                    if (e.Error.InnerException != null && e.Error.InnerException.ToString().Length > 0)
                    {
                        strErrorString = e.Error.InnerException.ToString() + "\n";
                    }

                    if (e.Error.Message != null && e.Error.Message.ToString().Length > 0)
                    {
                        strErrorString = strErrorString + e.Error.Message;
                    }

                    throw new Exception(strErrorString);
                }
                else if (e.Cancelled == true)
                {
                    objReqAndResp.handleEvent(sender, LMprimeConst.REQ_CANCELLED);
                }
                else
                {
                    objReqAndResp.handleEvent(sender, iEventType);
                }
            }
            catch (Exception exException)
            {
                /*  Any Exceptions that have occurred in background thread, are caught here, so no need to further throw exception from here,
                 *  Just displaying them in dialog.
                 **/
                
                endProgress();

                /*  Handle  all exception here   */
                string strExceptionstring = null;

                strExceptionstring = lmprimeException.processExceptionString(exException.Message, null);
                
                MessageBox.Show(strExceptionstring , LMprimeConst.ERROR);
            }
        }

        /*  Called when processing is started */
        private void bgwProcessing_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                objReqAndResp.addEvent(objReq, iEventType);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
   
        /*  Progress Bar start */
        public void startProgress()
        {
            progressBar1.Show();
        }

        /*  Progress Bar end */
        public void endProgress()
        {
            this.Dispose();
        }
    }
}









