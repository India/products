﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: About.cs

   Description: This Form displays about dialog.
========================================================================================================================================================================
Date          Name                 Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LMprime.userinterface.about
{
    
    /*  This check is paced as the designer cannot be placed on Abstract classes */
    #if DEBUG
    public partial class About : Form
    #else 
    public partial class About : BaseForm
    #endif
    {
        public About()
        {
            InitializeComponent();
        }
    }
}
