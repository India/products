﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMenuEvent.cs

   Description: This File is is sepcific for handling click event of context menus.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/* Class Description - This File is is sepcific for handling click event of context menus.
 * 
 *      peformMenuClickEvent(string strEventName, EA.Repository repository)
 *      Function which defines individual cases for handling context menus click.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;

namespace LMprime.lmprimemenu
{
    class LMprimeMenuEvent
    {
        LMprimeMain lmpMain = new LMprimeMain();
        
        public void peformMenuClickEvent(string strEventName, EA.Repository repository)
        {
            /*  strEventName contains the name of menu or sub menu */
            switch (strEventName)
            {
                case LMPrimeMenuConstants.MENU_CREATE_JIRA_ISSUE:
                    
                    lmpMain.checkAndProceed(repository);
                    
                    break;

                case LMPrimeMenuConstants.MENU_ABOUT:
                    
                    lmpMain.showAboutDialog();
                    
                    break;
            }
        }
    }
}









