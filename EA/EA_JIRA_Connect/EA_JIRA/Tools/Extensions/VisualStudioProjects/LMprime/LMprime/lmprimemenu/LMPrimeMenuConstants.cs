﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMenuConstants.cs

   Description: This File stores constants related to menus and sub menus.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file contains constants specific to Menu Options
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMPrimeMenuConstants
    {
        /*  Main Menu Name */
        public const string MENU_HEADER            = "-&LMI Plugin";
        public const string BLANK_SPACE            = "";

        /*  Sub Menu Names */
        public const string MENU_CREATE_JIRA_ISSUE = "&Create Jira Issue";
        public const string MENU_ABOUT             = "&About";
     }
}






