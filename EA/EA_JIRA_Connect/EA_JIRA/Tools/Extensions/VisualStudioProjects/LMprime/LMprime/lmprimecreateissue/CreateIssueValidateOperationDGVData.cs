﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueValidateOperationDGVData.cs

   Description: This File contains the logics for the validation of data grid view data after submit button click.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.constants;

namespace LMprime.lmprimecreateissue
{
    class CreateIssueValidateOperationDGVData
    {
        public Boolean vaildateOperationDGV(ref DataGridView dgElemDetails, ref String strErrorString)
        {
            Boolean isValidated = true;

            try
            {
                if (dgElemDetails != null && dgElemDetails.Rows.Count > 0)
                {
                    if (dgElemDetails.Rows[0] != null && dgElemDetails.Rows[0].Cells[4] != null && dgElemDetails.Rows[0].Cells[4].Value != null)
                    {
                        if (dgElemDetails.Rows[0].Cells[4].Value.ToString().CompareTo(LMprimeConst.INDEPENDENT_TICKET_CMP_STR) == 0)
                        {
                            if ((dgElemDetails.Rows[0].Cells[1].Value == null) ||
                                (dgElemDetails.Rows[0].Cells[1].Value.ToString() == null) ||
                                (dgElemDetails.Rows[0].Cells[1].Value.ToString().CompareTo("") == 0))
                            {
                                isValidated = false;
                                strErrorString = "Please select Issue Type for main Ticket";
                                return isValidated;

                            }
                            else if ((dgElemDetails.Rows[0].Cells[3].Value == null) ||
                                (dgElemDetails.Rows[0].Cells[3].Value.ToString() == null) ||
                                (dgElemDetails.Rows[0].Cells[3].Value.ToString().CompareTo("") == 0))
                            {
                                isValidated = false;
                                strErrorString = "Please select Assignee for main Ticket";
                                return isValidated;
                            }
                            else
                            {
                                if ((dgElemDetails.Rows[0].Cells[5].Value == null) ||
                                    (dgElemDetails.Rows[0].Cells[5].Value.ToString() == null) ||
                                    (dgElemDetails.Rows[0].Cells[5].Value.ToString().CompareTo("") == 0))
                                {
                                    isValidated = false;
                                    strErrorString = "Please select Priority for main Ticket";
                                    return isValidated;
                                }
                            }

                        }
                    }

                    for (int iDx = 1; iDx < dgElemDetails.Rows.Count; iDx++)
                    {
                        string strElemName = "";

                        if (dgElemDetails.Rows[iDx] != null)
                        {
                            if (dgElemDetails.Rows[iDx].Cells[0].Value != null
                                && dgElemDetails.Rows[iDx].Cells[0].Value.ToString() != null)
                            {
                                strElemName = dgElemDetails.Rows[iDx].Cells[0].Value.ToString();
                            }

                            if (dgElemDetails.Rows[iDx].Cells[4].Value != null
                                && dgElemDetails.Rows[iDx].Cells[4].Value.ToString() != null
                                && dgElemDetails.Rows[iDx].Cells[4].Value.ToString().CompareTo("") != 0)
                            {

                                if (dgElemDetails.Rows[iDx].Cells[4].Value.ToString().CompareTo(LMprimeConst.INDEPENDENT_TICKET_CMP_STR) == 0
                                    || dgElemDetails.Rows[iDx].Cells[4].Value.ToString().CompareTo(LMprimeConst.INDEPENDENT_TICKET_LINK_WITH_PAR) == 0)
                                {
                                    if ((dgElemDetails.Rows[iDx].Cells[1].Value == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[1].Value.ToString() == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[1].Value.ToString().CompareTo("") == 0))
                                    {
                                        isValidated = false;
                                        strErrorString = "Please select Issue Type for  " + strElemName;
                                        return isValidated;

                                    }
                                    else if ((dgElemDetails.Rows[iDx].Cells[3].Value == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[3].Value.ToString() == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[3].Value.ToString().CompareTo("") == 0))
                                    {
                                        isValidated = false;
                                        strErrorString = "Please select Assignee for   " + strElemName;
                                        return isValidated;
                                    }
                                    else
                                    {
                                        if ((dgElemDetails.Rows[iDx].Cells[5].Value == null) ||
                                            (dgElemDetails.Rows[iDx].Cells[5].Value.ToString() == null) ||
                                            (dgElemDetails.Rows[iDx].Cells[5].Value.ToString().CompareTo("") == 0))
                                        {
                                            isValidated = false;
                                            strErrorString = "Please select Priority for  " + strElemName;
                                            return isValidated;
                                        }
                                    }
                                }
                            }
                            else if (dgElemDetails.Rows[iDx].Cells[1].Value != null &&
                                     dgElemDetails.Rows[iDx].Cells[1].Value.ToString().CompareTo(LMprimeConst.SUBTASK) == 0)
                            {
                                if ((dgElemDetails.Rows[iDx].Cells[3].Value == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[3].Value.ToString() == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[3].Value.ToString().CompareTo("") == 0))
                                {
                                    isValidated = false;
                                    strErrorString = "Please select Assignee for   " + strElemName;
                                    return isValidated;
                                }
                                else
                                {
                                    if ((dgElemDetails.Rows[iDx].Cells[5].Value == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[5].Value.ToString() == null) ||
                                        (dgElemDetails.Rows[iDx].Cells[5].Value.ToString().CompareTo("") == 0))
                                    {
                                        isValidated = false;
                                        strErrorString = "Please select Priority for  " + strElemName;
                                        return isValidated;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return isValidated;
        }
    }
}
