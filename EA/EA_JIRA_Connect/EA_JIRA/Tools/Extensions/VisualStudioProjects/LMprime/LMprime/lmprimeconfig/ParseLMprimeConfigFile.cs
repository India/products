﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseLMprimeConfigFile.cs

   Description: This File is specifc for parsing congiguration file.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  This file parses the configuration file and stores it statistically to App Delegate 
 *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using LMprime.appdelegate;
using LMprime.lmprimebeans;
using LMprime.constants;
using LMprimeCommon.Utilities;

namespace LMprime.lmprimeconfig
{
    class ParseLMprimeConfigFile
    {
        public void parseConfigFile()
        {
            string      strFilePath   = "";
            string      strConfigFile = "";
            ReadXMLFile readFile      = new ReadXMLFile();

            try
            {
                /*  Assuming that configuration file will be at same place where DLL is present, Gettting path of Dll   */
             //   strFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;

             //   strFilePath = Path.GetDirectoryName(strFilePath);

                /*  Appending file name to path of Directory. File name cannot be other then "LMprimeConfig.xml" */
            //    strFilePath = strFilePath + LMprimeConst.FILE_NAME;

            //    strFilePath = strFilePath.Replace(@"\", @"/");

           //     strConfigFile = readFile.readXmlFile(strFilePath);

                /*  Read xml file from harcoded purpose only for testing purpose */
                strConfigFile = readFile.readXmlFile("C:/LMIEAPlugin.xml");

                if (strConfigFile != null)
                {
                    /*  Creating the object from file content which is in string    */
                    convertFileToObject(strConfigFile);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function to convert string to xml object    */
        public void convertFileToObject(string strConfigFile)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(LMIConfig));

                StringReader szrConfigString = new StringReader(strConfigFile);

                AppDelegate.statLMIBeans = (LMIConfig)serializer.Deserialize(szrConfigString);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}










