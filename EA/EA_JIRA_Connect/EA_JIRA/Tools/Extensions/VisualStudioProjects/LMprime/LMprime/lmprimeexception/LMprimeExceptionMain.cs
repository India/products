﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeExceptionMain.cs

   Description: This file os for handling excptions related to plugin.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.constants;

namespace LMprime.lmprimeexception
{
    class LMprimeExceptionMain
    {
        public string processExceptionString(string strException, string strInnerException)
        {
            string strTemp = null;
            
            if (strException != null)
            {
                strTemp = LMprimeConst.EXCEPTION_STRING+ " : " + strException;
            }

            if (strInnerException != null)
            {
                strTemp = strTemp + "\n" + strInnerException;
            }
            
            return strTemp;
        }
    }
}









