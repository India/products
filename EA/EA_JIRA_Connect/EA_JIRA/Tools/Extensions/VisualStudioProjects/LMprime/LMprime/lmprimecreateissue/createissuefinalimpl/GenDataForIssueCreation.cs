﻿/*======================================================================================================================================================================
                                      Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: GenDataForIssueCreation.cs

   Description: This file contains functions that are relate to data creation for issue
 
========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-March-2018       M.Singh                Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.issuemanager;
using LMprime.appdelegate;
using LMprimeCommon.commonbeans;
using LMprimeJIRA;
using System.Windows.Forms;
using LMprime.constants;
using LMprime.lmprimebeans;

namespace LMprime.lmprimecreateissue.createissuefinalimpl
{
    public class GenDataForIssueCreation
    {
        /* This function will receive separated list of elements and will be entry point for data generation */
        
        public string genDataForIssueCreationAndProceed
        (
            string strProject,
            LMIIssueObj objParentTik,
            List<string > lMergedElemList,
            List<LMIIssueObj> objIndependentNotLinkWithParent,
            List<LMIIssueObj> objIndependentLinkWithParent
        )
        {
            try
            {
                List<LMprimeIssueBean> parentElem = new List<LMprimeIssueBean>();
                List<LMprimeIssueBean> independentElem = new List<LMprimeIssueBean>();
                List<LMprimeIssueBean> independentLinkedElem = new List<LMprimeIssueBean>();
                JiraCreateIssue jCreateIssue = new JiraCreateIssue();

                string outString = null;

                generateParentAndMergedElemDict(objParentTik, lMergedElemList, ref parentElem);

                generateDataForIndependentTickets(objIndependentNotLinkWithParent, ref independentElem);

                generateDataForIndependentTickets(objIndependentLinkWithParent, ref independentLinkedElem);

                outString = jCreateIssue.createJIRAIssue(strProject, AppDelegate.statSel_Repository, parentElem, independentElem, independentLinkedElem, lMergedElemList);

                return outString;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function will genrate a object for main ticket creation and as well as merged ticket creation */
        public void generateParentAndMergedElemDict(LMIIssueObj objParentTik, List<string> lMergedElemList, ref List<LMprimeIssueBean> parentElem)
        {
            GenerateIssueDataMain gIssueDataMain = new GenerateIssueDataMain();
            Dictionary <string,string> parentElemDict = new Dictionary<string,string>();
            LMprimeIssueBean lmpIssueBean = new LMprimeIssueBean();

            try
            {
                /*  App Delegate selected element will be our main element for ticket creation,
                 *  This function will take Current EA repository which is statically set at app delegate,
                 *  In this case our parent element and merged element will be same as we are not going to merge any information for parent ticket
                 *  false boolean variable is to represent that this is parent ticket and its information needs not to be merged.
                 * */
               
                gIssueDataMain.getIssueDictionaryForSingleElem(AppDelegate.statSel_Repository, AppDelegate.statSelectedElement,AppDelegate.statSelectedElement,ref parentElemDict,false);
              
                if(lMergedElemList != null && lMergedElemList.Count > 0)
                {
                    for(int iDx = 0; iDx < lMergedElemList.Count; iDx ++)
                    {
                        EA.Element tempElem = AppDelegate.statSel_Repository.GetElementByGuid(lMergedElemList[iDx]);

                        lmpIssueBean.lMergedElem.Add(lMergedElemList[iDx]);
                       
                        gIssueDataMain.getIssueDictionaryForSingleElem(AppDelegate.statSel_Repository, AppDelegate.statSelectedElement, tempElem, ref parentElemDict, true);
                    }
                }
               
                if(objParentTik.strElemGUID != null && objParentTik.strElemGUID.Length > 0)
                {
                    lmpIssueBean.strElementGUID = objParentTik.strElemGUID;
                }

                if (objParentTik.strIssueType != null && objParentTik.strIssueType.Length > 0)
                {
                    lmpIssueBean.strIssueType = objParentTik.strIssueType;
                }

                if(objParentTik.strAssignee != null && objParentTik.strAssignee.Length > 0)
                {
                    lmpIssueBean.strAssignee = objParentTik.strAssignee;
                }

                if(objParentTik.strPriority != null && objParentTik.strPriority.Length > 0)
                {
                    lmpIssueBean.strPriority = objParentTik.strPriority;
                }
                
                if (objParentTik.strTikID != null && objParentTik.strTikID.Length > 0)
                {
                    lmpIssueBean.strExistingTicketId = objParentTik.strTikID;
                }

                if (objParentTik.strOperation != null && objParentTik.strOperation.Length > 0)
                {
                    lmpIssueBean.strOperation = objParentTik.strOperation;
                }

                if(objParentTik.strComponents != null && objParentTik.strOperation.Length > 0)
                {
                    lmpIssueBean.strComponents = objParentTik.strComponents;
                }

                lmpIssueBean.issue = parentElemDict;

                lmpIssueBean.lMergedElem = lMergedElemList;

                parentElem.Add(lmpIssueBean);

            }
            catch(Exception exception)
            {
                throw exception;
            }
       }
        public void generateDataForIndependentTickets(List<LMIIssueObj> ltInpList, ref List<LMprimeIssueBean> indLinkedAndNonLinked)
        {
            Dictionary<string, string> temp = new Dictionary<string, string>();
            GenerateIssueDataMain gIssueDataMain = new GenerateIssueDataMain();
            

            try
            {
                if (ltInpList != null && ltInpList.Count > 0)
                {
                    for (int iDx = 0; iDx < ltInpList.Count; iDx++)
                    {
                        LMIIssueObj tempOBJ= new LMIIssueObj();

                        if (ltInpList[iDx] != null)
                        {

                            tempOBJ = ltInpList[iDx];

                            LMprimeIssueBean lmpIssueBean = new LMprimeIssueBean();

                            Dictionary<string, string> tempOutDict = new Dictionary<string, string>();

                            EA.Element tempElem = AppDelegate.statSel_Repository.GetElementByGuid(tempOBJ.strElemGUID);

                            lmpIssueBean.strIssueType = tempOBJ.strIssueType;

                            lmpIssueBean.strAssignee = tempOBJ.strAssignee;

                            lmpIssueBean.strPriority = tempOBJ.strPriority;

                            lmpIssueBean.strElementGUID = tempOBJ.strElemGUID;

                            lmpIssueBean.strComponents = tempOBJ.strComponents;

                            gIssueDataMain.getIssueDictionaryForSingleElem(AppDelegate.statSel_Repository, AppDelegate.statSelectedElement, tempElem, ref tempOutDict, false);

                            lmpIssueBean.issue = tempOutDict;

                            indLinkedAndNonLinked.Add(lmpIssueBean);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
