﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeEAJIRABean.cs

   Description: This File is container for holding the data when it is displayed in EA JIRA Data grid view.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Mar-2018   Singh                 Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimebeans
{
    public class LMprimeEAJIRABean
    {
        public string strEAPropertyName    { get; set; }
        public string strEAPropertyType    { get; set; }
        public string strEAPropertyValue   { get; set; }
        public string strJIRAPropertyName  { get; set; }
        public string strJIRAPropertyValue { get; set; }
    }
}



