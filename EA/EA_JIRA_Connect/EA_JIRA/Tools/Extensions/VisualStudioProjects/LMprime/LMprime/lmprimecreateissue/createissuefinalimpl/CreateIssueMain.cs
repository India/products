﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssue.cs

   Description: This File is entry point to isssue creation.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprime.constants;
using LMprime.requestbeans;
using LMprime.lmprimebeans;

namespace LMprime.lmprimecreateissue.createissuefinalimpl
{
    class CreateIssueMain
    {
        /*  For holding parent ticket information */
        LMIIssueObj objParentTik = new LMIIssueObj();

        /* This will hold the GUID's of this elements whose information needs to be merged */
        List<string> lMergedElemList = new List<string>();

        /*   Independent issue not linked with parent */
        List<LMIIssueObj> objIndependentNotLinkWithParent = new List<LMIIssueObj>();

        /*  Independent ticket, Linked With Parent */
        List<LMIIssueObj> objIndependentLinkWithParent = new List<LMIIssueObj>();

        GenDataForIssueCreation genData = new GenDataForIssueCreation();

        /*  Main Function to create Issue in JIRA, The input data grid view is container of entire information in consolidated form   */
        public void createJIRAIssue(Object inpObj)
        {
            try
            {
                string strOutString = null;

                AppDelegate.strOutputIDs = null;
                
                if (inpObj != null)
                {
                    ReqCreateIssue reqCreateIssue = (ReqCreateIssue)inpObj;

                    if (reqCreateIssue != null)
                    {

                        DataGridView inpDGV = reqCreateIssue.dgElemDetails;
                        String strProject   = AppDelegate.strSEL_JIRA_PROJECT;

                        /*  This Will Contain Final Out String to be displayed on dialog,
                         *  This string will contain the final infomation , that is independent issue id,
                         *  Parent Issue id, Sub task ids etc. The information is displayed on dialog and this dialog is called SUCCESS Dialog.
                         */
                        if (strProject != null && strProject.Length > 0 && inpDGV != null && inpDGV.Rows.Count > 0)
                        {
                            createConsolidatedLists(inpDGV);

                            strOutString = genData.genDataForIssueCreationAndProceed(strProject, objParentTik, lMergedElemList, objIndependentNotLinkWithParent, objIndependentLinkWithParent);

                            if (strOutString != null && strOutString.Length > 0)
                            {
                                AppDelegate.strOutputIDs = strOutString;
                            }
                        
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Entry point to create consolidated lists */
        public void createConsolidatedLists(DataGridView inpDGV)
        {
            try
            {
                for (int iDx = 0; iDx < inpDGV.Rows.Count; iDx++)
                {
                    List<string> temp = new List<string>();
                    
                    DataGridViewRow Row = inpDGV.Rows[iDx];

                    if (Row != null)
                    {
                        LMIIssueObj tempIssueObj = new LMIIssueObj();

                        if (Row.Tag != null)
                        {
                            tempIssueObj.strElemGUID = Row.Tag.ToString();
                        }

                        if (Row.Cells[1].Value != null)
                        {
                            tempIssueObj.strIssueType = Row.Cells[1].Value.ToString();
                        }

                        if (Row.Cells[2].Value != null)
                        {
                            tempIssueObj.strTikID = Row.Cells[2].Value.ToString();
                        }

                        if (Row.Cells[3].Value != null)
                        {
                            tempIssueObj.strAssignee = Row.Cells[3].Value.ToString();
                        }

                        if (Row.Cells[4].Value != null)
                        {
                            tempIssueObj.strOperation = Row.Cells[4].Value.ToString();
                        }

                        if (Row.Cells[5].Value != null)
                        {
                            tempIssueObj.strPriority = Row.Cells[5].Value.ToString();
                        }

                        if (Row.Cells[6].Value != null)
                        {
                            tempIssueObj.strComponents = Row.Cells[6].Value.ToString();
                        }

                        /*  Parent Ticket   */
                        if (iDx == 0)
                        {
                            objParentTik = tempIssueObj;
                        }
                        else
                        {
                            if (tempIssueObj.strOperation != null && tempIssueObj.strOperation.CompareTo(LMprimeConst.SUBTASK) == 0)
                            {
                                tempIssueObj.strOperation = LMprimeConst.SUBTASK_OPERATION;
                            }

                            /*  Merge with parent element id's, Note merging order is preserved */
                            if (tempIssueObj.strOperation.CompareTo(LMprimeConst.MERGE_WITH_PARENT) == 0)
                            {
                                lMergedElemList.Add(tempIssueObj.strElemGUID);
                            }
                            else if (tempIssueObj.strOperation.CompareTo(LMprimeConst.INDEPENDENT_TICKET_CMP_STR) == 0 ||
                                   tempIssueObj.strOperation.CompareTo(LMprimeConst.SUBTASK_OPERATION) == 0)
                            {
                                /*  Independent, not link with parent */
                                objIndependentNotLinkWithParent.Add(tempIssueObj);
                            }
                            else if (tempIssueObj.strOperation.CompareTo(LMprimeConst.INDEPENDENT_TICKET_LINK_WITH_PAR) == 0)
                            {
                                /*  Independent, link with parent */
                                objIndependentLinkWithParent.Add(tempIssueObj);
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
