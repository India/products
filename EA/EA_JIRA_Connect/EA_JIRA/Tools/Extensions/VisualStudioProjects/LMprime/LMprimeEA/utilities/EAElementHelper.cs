﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAElementHelper.cs

   Description: This file is helper file specific to ea elements.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.constants;

namespace LMprimeEA.Utilities
{
    class EAElementHelper
    {
        /*  Function returns the list of all elements based on input diagram object.
         */
        public List<EA.Element> getAllElementsFromDiagram(EA.Diagram inpDiagram, EA.Repository repository)
        {
            List<EA.Element> ltElemList = new List<EA.Element>();

            try
            {
                /*  Running a loop on all the diagram objects of input diagram */
                foreach (DiagramObject diagObj in inpDiagram.DiagramObjects)
                {
                    int elementID = diagObj.ElementID;

                    EA.Element elem = null;

                    elem = repository.GetElementByID(elementID);

                    ltElemList.Add(elem);
                }

                if (ltElemList.Count == 0)
                {
                    throw new Exception(LMprimeEAConst.ELEMENTS_NOT_PRESENT);
                }

                return ltElemList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

       /*  Function returns specific element from input element list.
        *  The type of element which is to be removed is provided in input string 
        */
        public List<EA.Element> removeEAElementByTypefromList(List<EA.Element> ltInpElemList, string strInpString)
        {
            List<EA.Element> ltOutElementList = new List<EA.Element>();

            try
            {
                if (ltInpElemList != null && ltInpElemList.Count > 0 && strInpString != null && strInpString.Length > 0)
                {
                    for (int iDx = 0; iDx < ltInpElemList.Count; iDx++)
                    {
                        if (ltInpElemList[iDx].Type.CompareTo(strInpString) != 0)
                        {
                            ltOutElementList.Add(ltInpElemList[iDx]);
                        }
                    }
                }

                if (ltOutElementList.Count == 0)
                {
                    throw new Exception(LMprimeEAConst.ELEMENTS_NOT_PRESENT);
                }

                return ltOutElementList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This functions finds a element with from input list with same stereotype as provided in input argument
         *  If the stereotype is found then it breaks the loop and returns boolean value true, If no Element is found with 
         *  input stereotype then false is returned
         * */
        public Boolean isStereoTypeSame(List<EA.Element> ltInpElemList, int iPos, String strInpStereoType)
        {
            Boolean bOutValue = false;

            try
            {
                if (ltInpElemList != null && ltInpElemList.Count > 0 && strInpStereoType != null && strInpStereoType.Length > 0)
                {
                    for (int iDx = 0; iDx < iPos; iDx++)
                    {
                        string strStereoType = ltInpElemList[iDx].Stereotype;

                        if (strStereoType != null)
                        {
                            if (strStereoType.CompareTo(strInpStereoType) == 0)
                            {
                                bOutValue = true;
                                break;
                            }
                        }
                    }
                }
                return bOutValue;
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }
    }
}
