﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPConnection.cs

   Description: This file contains logic related to EAPConnection Class

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.Utilities;
using LMprimeEA.constants;
using LMprimeEA.Utilities;
using LMprimeEA.eapropmanager;

namespace LMprimeEA.eabeans
{
    public class EAPConnectionColl
    {
        /*  Initializing class variables */
        public List<EAPConnectionSgl> lteapConnectionSgl = new List<EAPConnectionSgl>();
       
        /*  Function for getting connection object  */
        public void getConnectionObj(EAPMapping eapMapping, List<EATaggedValue> ltTagValues)
        {
            List<EATaggedValue> ltConn = new List<EATaggedValue>();
            EATaggedValuesHelper eTagHelper = new EATaggedValuesHelper();

            /*  Getting spearated list of tagged Values that is of Type LMI_Connection  */
            ltConn = eTagHelper.getSeparatedTVListFrMapping(LMprimeEAConst.LMI_CONNECTION, ltTagValues);

            try
            {
                if (ltConn != null && ltConn.Count > 0)
                {
                    /*  Running a loop on all connection objects */
                    for (int iDx = 0; iDx < ltConn.Count; iDx++)
                    {
                        EAPConnectionSgl eaConnSGL = null;

                        /*  Checking whether the same relation exists or not, if the relation already exists then we removes the object from list and updates the same object and then we finally add the same object.
                         *  If the object does not exists then we just simply creates a new object and adds the same object to list.
                         */ 
                        eaConnSGL = checkElemExistsWithRel(ref eapMapping.eapConnection.lteapConnectionSgl, ltConn[iDx].strTagValue);

                        /*  If relation does not exists the initializing the object */
                        if (eaConnSGL == null)
                        {
                            eaConnSGL = new EAPConnectionSgl();
                        }

                        /*  Getting single connection object */
                        eaConnSGL.getConnectionFinalObj(ref eaConnSGL, ltConn[iDx].strTagValue);
                            
                        /*  Adding the object to final list */
                        eapMapping.eapConnection.lteapConnectionSgl.Add(eaConnSGL);
                           
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function for checking that mapping for same relation type exists or not. */
        public EAPConnectionSgl checkElemExistsWithRel(ref List<EAPConnectionSgl> lteapConnectionSgl, string inpString)
        {
            EAPConnectionSgl temp = null;

            try
            {
                if (inpString != null && inpString.Length > 0 && lteapConnectionSgl != null)
                {
                    string strRelName = getRelationName(inpString);

                    if (lteapConnectionSgl.Count > 0)
                    {
                        for (int iDx = 0; iDx < lteapConnectionSgl.Count; iDx++)
                        {
                            if (lteapConnectionSgl[iDx].strRelationName.CompareTo(strRelName) == 0)
                            {
                                temp = lteapConnectionSgl[iDx];

                                lteapConnectionSgl.RemoveAt(iDx);

                                break;
                            }
                        }
                    }
                }

                return temp;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function take LMI_Connection value as input string, It extracts out relation name from it and returns back to calling function
         *  The first element in the strings consists of relation.
         */
        public string getRelationName(string strInpString)
        {
            string strOutString = null;

            try
            {
                if (strInpString != null)
                {
                    /*  Storing the elements in array separated by '§' separator */
                    string[] strTokens = strInpString.Split(LMprimeEAConst.VAL_SEPARATOR);

                    if (strTokens[0] != null && strTokens[0].Length > 0)
                    {
                        /*  First element in the separated array is connection name */
                        strOutString = strTokens[0];
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return strOutString;
        }
    }
}



