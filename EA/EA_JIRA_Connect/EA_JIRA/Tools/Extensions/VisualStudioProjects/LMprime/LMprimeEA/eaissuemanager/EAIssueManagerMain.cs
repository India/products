﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.utilities;
using LMprimeEA.eabeans;

namespace LMprimeEA.eaissuemanager
{
    class EAIssueManagerMain
    {
        EAMappingUtilities eMapUtil = new EAMappingUtilities();
        LMPEAToJIRA eaTOJIRA = new LMPEAToJIRA();
        
        public Dictionary<string, string> getIssueDictionary(EA.Repository repository, string strElemGUID)
        {
            try
            {
                Dictionary<string, string> outDictionary = new Dictionary<string, string>();

                if (repository != null && strElemGUID != null)
                {
                    EA.Element inpElement = repository.GetElementByGuid(strElemGUID);

                    eaTOJIRA = eMapUtil.getEAToJiraObj(inpElement);

                    if (eaTOJIRA != null)
                    {
                        outDictionary = createEAToJiraFinalDict(eaTOJIRA);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public Dictionary<string, string> createEAToJiraFinalDict(LMPEAToJIRA ea2Jira)
        {
            Dictionary<string, string> temp = new Dictionary<string, string>();

            try
            {
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
    }
}
