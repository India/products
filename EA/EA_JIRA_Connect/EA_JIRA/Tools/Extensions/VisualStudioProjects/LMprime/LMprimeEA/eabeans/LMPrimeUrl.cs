﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeUrl.cs

   Description: This file is for storing URL for Selected Object.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/

/**  Note :- This file is for storing URL for LMPrime.
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeCommon.Utilities;
using LMprimeEA.constants;
using LMprimeEA.Utilities;
using LMprimeEA.eapropmanager;

namespace LMprimeEA.eabeans
{
    public class LMprimeUrl
    {
        public String strUrl     = null;
        List<EATaggedValue> ltURL = new List<EATaggedValue>();
        EATaggedValuesHelper eTagHelper = new EATaggedValuesHelper();
        
        /*  This function extracts JIRA URL From tagged values, if more then Jone tagged values are defined then first one is taken */
        public void getJIRAUrl(EAPMapping eapMapping, List<EATaggedValue> ltTagValues)
        {
            try
            {
                ltURL = eTagHelper.getSeparatedTVListFrMapping(LMprimeEAConst.strJIRA_URL, ltTagValues);

                if (ltURL.Count > 0)
                {
                    /*  The first element of the temp list will contain tag value name and second will contain value */
                    eapMapping.lmpURL.strUrl = ltURL[0].strTagValue;
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}






