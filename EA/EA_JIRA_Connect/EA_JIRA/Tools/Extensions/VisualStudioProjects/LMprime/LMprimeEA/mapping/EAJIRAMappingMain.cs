﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAJIRAMappingMain.cs

   Description: This file is Entry point for EA JIRA Mapping.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.Utilities;
using LMprimeEA.constants;
using LMprimeEA.eabeans;
using LMprimeEA.eapropmanager;
using LMprimeCommon.commonbeans;

namespace LMprimeEA.Mapping
{
    public class EAJIRAMappingMain
    {
        EAPackageHelper      eaPackageHelper = new EAPackageHelper();
        EADiagramHelper      eaDiagramHelper = new EADiagramHelper();
        EAElementHelper      eaElementHelper = new EAElementHelper();
        EATaggedValuesHelper eaTaggedValue   = new EATaggedValuesHelper();
      
        /*  Default Constructor */
        public EAJIRAMappingMain()
        {
        }

        /*  Entry  function to get consolidated mapping.This functions returns the mapping dictionary.
            Structue of Dictionary
            Dictionary<string, LMPrimeMapping>
            Dictionary Key will be element Type/Stereotype.And value will be object of lmprime mapping.
        */
        public Dictionary<string, EAPMapping> getConsolidateMapping(EA.Repository repository)
        {
            EA.Package package                           = null;
            EA.Diagram diagram                           = null;
            List<EA.Element> ltElemList                  = new List<EA.Element>();
            List<EA.Element> ltFinalElemList             = new List<Element>();
            Dictionary<string, EAPMapping> finalMapping  = new Dictionary<string, EAPMapping>();

            try
            {
                /* Getting the package in which Mapping template is placed from current repository  */
                package = eaPackageHelper.getFirstPackageByName(repository, LMprimeEAConst.MAIN_PACKAGE_NAME);

                /* Getting the diagram from  in which Mapping template is placed from current repository  */
                diagram = eaDiagramHelper.getFirstDiagramFromPackage(package, LMprimeEAConst.MAIN_DIAGRAM_NAME);

                /* Getting all elements from from current Mapping diagram  */
                ltElemList = eaElementHelper.getAllElementsFromDiagram(diagram, repository);

                /*  Removing specific element  i.e of type boundary */
                ltFinalElemList = eaElementHelper.removeEAElementByTypefromList(ltElemList, LMprimeEAConst.BOUNDARY);

                /*Note :- As of now element list is formed whose mapping (Dictionary) is to be created 
                 *        Calling the function to generate consolidated mapping, passing ref finalmapping 
                 *        mapping dictionary which is to be filled.Function takes finalElemList which 
                 *        contains the list of elements whose mapping is to be generated.
                 *        
                 **/
                 genrateConsolidatedMapping(ref finalMapping, ltFinalElemList);

                 /* Statically saving the mapping specific to this application   */
                 LMprimeEAConst.MAPPING = finalMapping;
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return finalMapping;
        }

        /* Function entry point to generation of consolidated mapping.  */
        public void genrateConsolidatedMapping(ref Dictionary<string, EAPMapping> finalMapping, List<EA.Element> elemList)
        {
           try
            {
                /*  Checking if element list is not null    */
                if (elemList != null && elemList.Count > 0)
                {
                    for (int iDx = 0; iDx < elemList.Count; iDx++)
                    {
                        String strType                 = "";
                        List<EATaggedValue> ltTagValue = new List<EATaggedValue>();
                        EAPMapping lmprimeMapping      = new EAPMapping();
                        
                        ltTagValue = eaTaggedValue.getTaggedValueList(elemList[iDx]);

                        strType = elemList[iDx].Type;

                        if (ltTagValue != null && ltTagValue.Count > 0)
                        {
                            /*  Consolidated Mapping object is received */
                            lmprimeMapping = lmprimeMapping.getConsolidatedObj(ltTagValue);

                            if (lmprimeMapping != null)
                            {
                                /*
                                 * Important Note: This entire conditonal logic of checking element type and its stereotype is for below reasons.
                                 * It is found the two diffrent elements in Enterprise architect can have same type but their Stereotype is diffrent.
                                 * Hence it is restriction of dictionary that two keys in it cannot have same name.
                                 * So first we checks that whether dictionary contains key with element name, if not then we add it to dictionary and if yes,
                                 * then we add the same element with its stereotype as key to dictionary, while extration of element from mapping reverse order is followed.
                                 * 
                                 * */
                                if (!finalMapping.ContainsKey(strType))
                                {
                                    finalMapping.Add(strType, lmprimeMapping);
                                }
                                else if (!eaElementHelper.isStereoTypeSame(elemList, iDx, elemList[iDx].Stereotype))
                                {
                                    /* Note :- Understand this conditional check by the below given scenario.
                                     *         
                                     **/
                                    finalMapping.Add(elemList[iDx].Stereotype, lmprimeMapping);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}












