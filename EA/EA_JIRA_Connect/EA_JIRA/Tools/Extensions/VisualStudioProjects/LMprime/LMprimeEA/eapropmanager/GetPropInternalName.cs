﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.constants;

namespace LMprimeEA.eapropmanager
{
    class GetPropInternalName
    {
        public string getInternalName(string strInpString)
        {
            if(strInpString != null && strInpString.Length > 0)
            {
                if(strInpString.CompareTo(LMprimePropConstants.KEYWORD) == 0)
                {
                    return LMprimePropConstants.TAG;
                }
            }
            return strInpString;
        }
    }
}
