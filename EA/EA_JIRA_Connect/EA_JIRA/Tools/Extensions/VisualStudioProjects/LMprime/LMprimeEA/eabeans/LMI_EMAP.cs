﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMI_EMAP.cs

   Description: This file contains logic related to LMI_EMAP related.

========================================================================================================================================================================

Date                Name                   Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-Feb-2018    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.Utilities;
using LMprimeEA.constants;
using LMprimeEA.Utilities;
using LMprimeEA.eapropmanager;

namespace LMprimeEA.eabeans
{
    public class LMI_EMAP
    {
        public List<string>  ltIssueTypeList = new List<string>();
        ListUtilities        lUtil           = new ListUtilities();
        EATaggedValuesHelper eTagHelper      = new EATaggedValuesHelper();

        public List<string> getLMIEMAPList(EAPMapping lmpMapping, List<EATaggedValue> ltTagValues)
        {
            List<EATaggedValue> ltlmiEMAP = new List<EATaggedValue>();
            try
            {

                ltlmiEMAP = eTagHelper.getSeparatedTVListFrMapping(LMprimeEAConst.LMI_EMAP, ltTagValues);

                if (ltlmiEMAP.Count > 0)
                {
                    for (int iDx = 0; iDx < ltlmiEMAP.Count; iDx++)
                    {
                        string[] token = ltlmiEMAP[iDx].strTagValue.Split(LMprimeEAConst.VAL_SEPARATOR);

                        lUtil.addElementsFromArrayToList(token, ref ltIssueTypeList);

                    }
                }
             }
            catch (Exception exception)
            {
                throw exception;
            }

            return ltIssueTypeList;
        }
    }


}
