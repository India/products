﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeEAConst.cs

   Description: This file is for storing constants specific to LMprimeEA dll.
                It also stores static constants specific to this dll.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eabeans;

namespace LMprimeEA.constants
{
    class LMprimeEAConst
    {
        /*  Main mapping package name */
        public static string MAIN_PACKAGE_NAME                         = "LMIEAP_Connector";
        
        /*  Main mapping Diagram name */
        public static string MAIN_DIAGRAM_NAME                         = "JIRA_Connector_Mappings";
        
        /*Mapping content variables */
        public static string LMI_JIRA2EA                               = "LMI_JIRA2EA";
        public static string LMI_EA2JIRA                               = "LMI_EA2JIRA";
        public static string LMI_CONNECTION                            = "LMI_Connection";
        public static string strJIRA_URL                               = "LMI_strJIRA_URL";
        public static string LMI_EMAP                                  = "LMI_EMAP";
        
        /*  Separators  */
        public static char SEMICOLON_SEPARATOR                         = ';';
        public static char COMMA_SEPARATOR_CHAR                        = ',';
        public static char VAL_SEPARATOR                               = '§';
        public static char DASH_SEPRATOR                               = '-';
        public static char CURLY_BRAC_START                            = '{';
        public static char CURLY_BRAC_CLS                              = '}';
        public static char EXCLAMATION                                 = '!';
        public static string COMMA_SEPARATOR                           = ",";
        public static string ASTRIK_SEPARATOR                          = "*";
        public static string BLANK                                     = "";
        public static string DEFAULT_SEPARATOR                         = ".";
        
        /*  Mapping, It is also statically set on app delegate of LMprime dll but this dll will also more often require it so it is also placed here */
        public static Dictionary<string, EAPMapping> MAPPING           = null; 

        /*  Custom errors specific to this dll  */
        public static string PACKAGE_NOT_EXISTS                        = "Package does not exists";
        public static string DIAGRAM_NOT_EXISTS                        = "Diagram does not exists";
        public static string ELEMENTS_NOT_PRESENT                      = "Elements not present in diagram";
        public static string INVALID_JIRA_EA_MAPPING                   = "Value for JIRA2EA Mapping Incorrectly defined";
        public static string INVALID_EA_TO_JIRA_MAPPING                = "Value for EA2JIRA Mapping Incorrectly defined";

        /* EA DATA ENGINE RELATED CONSTANTS */
        public static string ALL_RELATED_ELEMENTS                      = "All";

        /*  General Constants */
        public static string KEY                                       = "Key";
        public static string TAGGED_VALUE                              = "Tagged Value";
        public static string BOUNDARY                                  = "Boundary";
        
        /*  EA SQL Queries */
        public static string QUERY_EXTRACT_PACKAGE_WITH_NAME_FROM_REPO = "select o.Object_ID from t_object o inner join t_package p on p.ea_guid = o.ea_guid where o.Name = '";
    }
}










