﻿/*======================================================================================================================================================================
                                      Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EATaggedValue.cs

   Description: This file is container for storing EA Tagged values

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-March-2018       M.Singh                Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeEA.eapropmanager
{
    public class EATaggedValue
    {
        public string strTagName { get; set; }
        public string strTagValue{ get; set; }
    }
}
