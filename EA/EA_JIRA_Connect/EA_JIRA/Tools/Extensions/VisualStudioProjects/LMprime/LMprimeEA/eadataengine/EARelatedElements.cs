﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EARelatedElements.cs

   Description: This File contains API's related to Related Elements.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.constants;
using LMprimeEA.utilities;
using LMprimeEA.eabeans;

namespace LMprimeEA.eadataengine
{
    public class EARelatedElements
    {
        public List<EA.Element> getChildElementsForTree(EA.Repository repositiory, EAPConnectionColl lmpConnection, EA.Element inpElement, Boolean isBiderectionalReq)
        {
            try
            {
                List<EA.Element> temp               = new List<EA.Element>();
                EARelatedElementUtilities eaRelElem = new EARelatedElementUtilities();

                if (inpElement != null && lmpConnection != null)
                {
                    if (lmpConnection != null && lmpConnection.lteapConnectionSgl.Count > 0)
                    {
                        int iCount = lmpConnection.lteapConnectionSgl.Count;

                        for (int iDx = 0; iDx < iCount; iDx++)
                        {
                            eaRelElem.getRelatedElem(repositiory, ref temp, inpElement, lmpConnection.lteapConnectionSgl[iDx], isBiderectionalReq);
                        }
                    }
                }
                return temp;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



