﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAPackageHelper.cs

   Description: This file is helper file for EA Package Related Activities.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.constants;

namespace LMprimeEA.Utilities
{
    class EAPackageHelper
    {
        /*  Return EA.Package object based on input package name from input repository provided.
         *  If more than one package is found then it return the first package
         */
        public EA.Package getFirstPackageByName(EA.Repository repository, string strName)
        {
            List<EA.Package> ltPackages = new List<EA.Package>();

            try
            {
                var packageElements = repository.GetElementSet(LMprimeEAConst.QUERY_EXTRACT_PACKAGE_WITH_NAME_FROM_REPO + strName + "'", 2);

                foreach (EA.Element packageElement in packageElements)
                {
                    ltPackages.Add(repository.GetPackageByGuid(packageElement.ElementGUID));
                }

                if (ltPackages.Count == 0)
                {
                    throw new Exception(strName + LMprimeEAConst.PACKAGE_NOT_EXISTS);
                }

                return ltPackages[0];
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
      }
}



