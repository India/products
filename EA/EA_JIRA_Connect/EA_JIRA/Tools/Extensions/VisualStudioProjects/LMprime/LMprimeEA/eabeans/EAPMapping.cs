﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPrimeMapping.cs

   Description: This file is helper file for EA Diagram related Activities.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/

/**  Note :- This file is specific for storing mapping related to single Enterprise Architect element
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eapropmanager;
    
namespace LMprimeEA.eabeans
{
    public class EAPMapping
    {
        public EAPConnectionColl eapConnection= new EAPConnectionColl();
        public EA2JIRAColl ea2JIRAColl = new EA2JIRAColl();
        public JIRA2EAColl jira2EAColl = new JIRA2EAColl();
        public LMI_EMAP lmiEAP = new LMI_EMAP();
        public LMprimeUrl lmpURL = new LMprimeUrl();

        /*  For getting Consolidated Objects */
        public EAPMapping getConsolidatedObj(List<EATaggedValue> ltTagValues)
        {
            /*  Receiving Jira TO Enterprise Architect object   */
            jira2EAColl.getJIRATOEAObj(this, ltTagValues);

            /*  Receiving Enterprise Architect TO JIRA object   */
            ea2JIRAColl.getEATOJIRAObj(this, ltTagValues);

            /*  Receiving Connection object   */
            eapConnection.getConnectionObj(this, ltTagValues);
                
            /*  Getting JIRA URL    */
            lmpURL.getJIRAUrl(this, ltTagValues);

            /*  Getting Issue type lists    */
            lmiEAP.getLMIEMAPList(this, ltTagValues);

            return this;
        }
    }
}



