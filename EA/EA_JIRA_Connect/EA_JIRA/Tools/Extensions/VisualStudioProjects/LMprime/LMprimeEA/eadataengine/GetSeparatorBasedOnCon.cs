﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;
using LMprimeEA.constants;
using EA;



namespace LMprimeEA.eadataengine
{
    class GetSeparatorBasedOnCon
    {
        /* Parent Element will Always be main Element */
        public string getSeparatorbasedOnConObject(EA.Element parentElem, EA.Element childElement)
        {
            EAPConnectionColl lmpConnection = null;
            EAMappingUtilities eMapUtil = new EAMappingUtilities();
            EARelatedElementUtilities eaRelElemUtility = new EARelatedElementUtilities();
            
            try
            {
                if(parentElem !=null & childElement != null)
                {
                    lmpConnection = eMapUtil.getSelConnectionObj(parentElem);

                    if (lmpConnection != null && lmpConnection.lteapConnectionSgl.Count > 0)
                    {
                        string strType = "";
                        
                        string strStereotype = "";

                        eaRelElemUtility.getRelnType(parentElem, childElement, ref strType, ref strStereotype);

                        for (int iDx = 0; iDx < lmpConnection.lteapConnectionSgl.Count; iDx++)
                        {
                            if (lmpConnection.lteapConnectionSgl[iDx] != null)
                            {
                               if (strStereotype != null && strStereotype.Length > 0)
                               {
                                   if (lmpConnection.lteapConnectionSgl[iDx].strRelationName.Equals(strStereotype, StringComparison.CurrentCultureIgnoreCase))
                                   {
                                       return lmpConnection.lteapConnectionSgl[iDx].strSeparator;
                                   }
                               }
                               else if(strType != null && strType.Length > 0)
                               {
                                   if (lmpConnection.lteapConnectionSgl[iDx].strRelationName.Equals(strType, StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        return lmpConnection.lteapConnectionSgl[iDx].strSeparator;
                                    }
                               }
                               
                            }
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
            return LMprimeEAConst.DEFAULT_SEPARATOR;
         }
    }
}
