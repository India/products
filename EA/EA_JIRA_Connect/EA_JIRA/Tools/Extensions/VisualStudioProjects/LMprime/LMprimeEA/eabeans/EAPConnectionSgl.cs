﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPConnectionFinal.cs

   Description: This file is for processing LMP_Connection EA Tagged Value.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.constants;

namespace LMprimeEA.eabeans
{
    public class EAPConnectionSgl
    {
        public string strRelationName = "";
        public string strSeparator = "";
        public Boolean bIsAllElemTypAllowed = false;
        public List<string> lsExcludeElem = new List<string>();
        public List<string> lsIncludeOnlyElem = new List<string>();

        /*  This function returns single connection object */
        public void getConnectionFinalObj(ref EAPConnectionSgl lmpConFinal, string strInpString)
        {
            
            if (strInpString != null && strInpString.Length > 0)
            {
                string[] strTokens = strInpString.Split(LMprimeEAConst.VAL_SEPARATOR);

                for (int iDx = 0; iDx < strTokens.Length; iDx++)
                {
                    if (strTokens[iDx] != null && strTokens[iDx].Length > 0)
                    {
                        if (iDx == 0)
                        {
                            lmpConFinal.strRelationName = strTokens[iDx];
                        }
                        else if (iDx == 1)
                        {
                            string szTemp = strTokens[iDx];

                            if (szTemp[0] == LMprimeEAConst.EXCLAMATION)
                            {
                                getexcludedElemList(ref lmpConFinal, strTokens[iDx]);
                            }

                            else
                            {
                                getIncludedElemList(ref lmpConFinal, strTokens[iDx]);
                            }
                        }
                        else if (strTokens[iDx] != null && strTokens[iDx].Length > 0)
                        {
                            if (lmpConFinal != null && lmpConFinal.strSeparator.CompareTo(LMprimeEAConst.BLANK) == 0)
                            {
                                lmpConFinal.strSeparator = strTokens[iDx];
                            }
                        }
                    }
                }
            }
       }

        /*  This function gives the excluded element list */
        public void getexcludedElemList(ref EAPConnectionSgl lmpConFinal, string strInp)
        {
            String strTemp = strInp.Substring(1, strInp.Length - 1);
            
            string[] strTokens = strTemp.Split(LMprimeEAConst.SEMICOLON_SEPARATOR);

            if (strTokens != null && strTokens.Length > 0)
            {
                for (int iDx = 0; iDx < strTokens.Count(); iDx++)
                {
                    if (!lsExcludeElem.Contains(strTokens[iDx]))
                    {
                        lmpConFinal.lsExcludeElem.Add(strTokens[iDx]);
                    }
                }
            }
        }

        /*  This function gives the included element list */
        public void getIncludedElemList(ref EAPConnectionSgl lmpConFinal, string strToken)
        {
            if (strToken != null && strToken.Length > 0)
            {
                if (lmpConFinal.bIsAllElemTypAllowed == false)
                {
                    if (strToken.CompareTo(LMprimeEAConst.ASTRIK_SEPARATOR) == 0)
                    {
                        lmpConFinal.bIsAllElemTypAllowed = true;

                        lmpConFinal.lsIncludeOnlyElem.Clear();
                    }
                    else
                    {
                        string[] strTokens = strToken.Split(LMprimeEAConst.SEMICOLON_SEPARATOR);

                        lmpConFinal.lsIncludeOnlyElem = strTokens.ToList();
                    }
                }
            }
        }
    }
}
