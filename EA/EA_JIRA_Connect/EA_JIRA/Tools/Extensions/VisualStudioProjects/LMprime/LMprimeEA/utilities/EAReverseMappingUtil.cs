﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAReverseMappingUtil.cs

   Description: This file is specific to reverse mapping utilities.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eabeans;
using LMprimeEA.constants;
using LMprimeEA.Utilities;
using LMprimeCommon.Utilities;
using LMprimeEA.eapropmanager;

namespace LMprimeEA.utilities
{
    public class EAReverseMappingUtil
    {
        EATaggedValuesHelper eTagValues = new EATaggedValuesHelper();
        PropertyInfoUtil pInfo          = new PropertyInfoUtil();
        
        /*
         * This functions returns the list of JIRA Id's for the input element. It takes the input project and the jira id's are in context fo current selected project.
         * for. eg if for a particular element two tickets are creates LMP-1, SAM-2 then it will return LMP-1 if the selected project is LMP and SAM-2 if selected project is SAM.
         * 
         * Logic: First it finds out all the destination ea attributes to which JIRA Key is mapped from reverese mapping,
         * It tokeninzes them with comma as separator and creates a list that needs to be returned in context of the input project.
         * 
         */ 
        public List<string> getJIRAID(JIRA2EAColl lmpJIRA2EA, EA.Element inpElem, string strSelProj)
        {
            try
            {
                string       strEAPropValue = null;
                List<string> ltIssueId       = new List<string>();

                if (lmpJIRA2EA != null && lmpJIRA2EA.ltJira2EASgl != null && lmpJIRA2EA.ltJira2EASgl.Count > 0 && inpElem != null && strSelProj != null && strSelProj.Length > 0)
                {
                    /*  Running a loop on all jira2EA Objects */
                    for (int iDx = 0; iDx < lmpJIRA2EA.ltJira2EASgl.Count; iDx++)
                    {
                        string strTemp = lmpJIRA2EA.ltJira2EASgl[iDx].strSourceAttrName;

                        if (strTemp != null && strTemp.Equals(LMprimeEAConst.KEY, StringComparison.InvariantCultureIgnoreCase))
                        {
                            string strEApropName = lmpJIRA2EA.ltJira2EASgl[iDx].strDesAttrName;
                            
                            string strEApropTyp  = lmpJIRA2EA.ltJira2EASgl[iDx].strDesAttrTyp;

                            /*  If destination attribute is of tagged value type */
                            if (strEApropTyp != null && strEApropTyp.Equals(LMprimeEAConst.TAGGED_VALUE, StringComparison.InvariantCultureIgnoreCase))
                            {
                                strEAPropValue = eTagValues.getFirstTaggedValue(inpElem, strEApropName);
                            }
                            else
                            {
                                strEAPropValue = pInfo.GetPropertyValue(inpElem, strEApropName);
                            }

                            if (strEAPropValue != null && strEAPropValue.Length > 0)
                            {
                                /*  Creating a list from Property String */
                                getIssueListFrmStr(strEAPropValue, strSelProj, ref ltIssueId);
                            }
                        }
                    }
                }
                return ltIssueId;
            }
            catch (Exception Exception)
            {
                throw Exception;
            }
        }

        /*  This function takes comma separated list of JIRA ID's, and input project key,
         *  As Output argument it create a distict list of JIRA ID's in context of current project
         *  
         * */
        public void getIssueListFrmStr(string strEAPropValue, string selProject, ref List<string> ltIssueId)
        {
            try
            {
                if (strEAPropValue != null && strEAPropValue.Length > 0)
                {
                    List<string> ltTempId = new List<string>();

                    /*  Splting the input array with comma to get individual issue ID's*/
                    string[] temp = strEAPropValue.Split(LMprimeEAConst.COMMA_SEPARATOR_CHAR);

                    ltTempId = temp.ToList();

                    if (ltTempId != null)
                    {
                        for (int iDx = 0; iDx < ltTempId.Count; iDx++)
                        {
                            if (ltTempId[iDx] != null && ltTempId[iDx].Length > 0)
                            {
                                /* Spliting the single issue with dash to extract project key as JIRA ID's are in the for SAM-1 */
                                string[] tempArr = ltTempId[iDx].Split(LMprimeEAConst.DASH_SEPRATOR);

                                for (int iFx = 0; iFx < tempArr.Length; iFx++)
                                {
                                    if (iFx == 0 && tempArr[iFx] != null && tempArr[iFx].Length > 0)
                                    {
                                        /*  Checking whether JIRA ID is in context with the input project, also it is distinct */
                                        if (selProject.CompareTo(tempArr[iFx]) == 0 && !(ltIssueId.Contains(ltTempId[iDx])))
                                        {
                                            ltIssueId.Add(ltTempId[iDx]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
    }
}



