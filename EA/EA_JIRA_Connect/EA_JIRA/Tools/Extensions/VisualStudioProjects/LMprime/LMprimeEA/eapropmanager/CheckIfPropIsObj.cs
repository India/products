﻿/*======================================================================================================================================================================
                                      Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CheckIfPropIsObj.cs

   Description: This file checks whether prperty is Object or not

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-March-2018       M.Singh                Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.constants;

namespace LMprimeEA.eapropmanager
{
    class CheckIfPropIsObj
    {
        public Boolean checkIfPropIsObj(string strInpPropName)
        {
            Boolean bResult = false;
            try
            {
                if (strInpPropName != null && strInpPropName.Length > 0)
                {
                    if (strInpPropName.CompareTo(LMprimePropConstants.COMPONENTS) == 0)
                    {
                        bResult = true;
                    }
                    else if (strInpPropName.CompareTo(LMprimePropConstants.LABELS) == 0)
                    {
                        bResult = true;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return bResult;
        }
    }
}
