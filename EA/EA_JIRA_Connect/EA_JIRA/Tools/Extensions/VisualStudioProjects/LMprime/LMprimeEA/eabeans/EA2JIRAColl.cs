﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPEAToJIRA.cs

   Description: This file is container for mapping of EA to JIRA side.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.Utilities;
using LMprimeEA.constants;
using LMprimeEA.Utilities;
using LMprimeEA.eapropmanager;

namespace LMprimeEA.eabeans
{
    public class EA2JIRAColl
    {
        /*  Class level variables */
        public  List<EA2JIRASgl> ltEAtoJiraFinal = new List<EA2JIRASgl>();
        private EA2JIRASgl       eaToJIRAFinal   = new EA2JIRASgl();

        /*  For getting EA to JIRA Object */
        public void getEATOJIRAObj(EAPMapping eapMapping, List<EATaggedValue> ltTagValues)
        {
            EA2JIRAColl lmpEATOJiraObj   = new EA2JIRAColl();
            List<EATaggedValue> ltEA2JIRA = new List<EATaggedValue>();
            EATaggedValuesHelper eTagHelper = new EATaggedValuesHelper();

            try
            {
                /*  Getting Separated list of tagged values i.e EA To JIRA */
                ltEA2JIRA = eTagHelper.getSeparatedTVListFrMapping(LMprimeEAConst.LMI_EA2JIRA, ltTagValues);

                for (int iDx = 0; iDx < ltEA2JIRA.Count; iDx++)
                {
                    List<string> ltTemp = new List<string>();

                    EA2JIRASgl temp = new EA2JIRASgl();
                            
                    /* Separating the key LMI_EA2JIRA and supplying its value for next Function */
                    temp = temp.getEATOJIRAFinalObj(ltEA2JIRA[iDx].strTagValue);

                    /* Adding ea2Jira Object to list */
                    eapMapping.ea2JIRAColl.ltEAtoJiraFinal.Add(temp);
                         
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}






