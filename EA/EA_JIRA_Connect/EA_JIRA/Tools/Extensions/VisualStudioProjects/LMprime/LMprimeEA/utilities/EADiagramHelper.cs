﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EADiagramHelper.cs

   Description: This file contains EA diagram related utilities.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;

using LMprimeEA.constants;

namespace LMprimeEA.Utilities
{
    class EADiagramHelper
    {
        /*  Function returns the diagram object based on input diagram name
         *  If more then one diagram is found in input package then it returns the first diagram
         */

        public EA.Diagram getFirstDiagramFromPackage(EA.Package inpPackage, string strDiagramName)
        {
            EA.Diagram outDiagram = null;

            try
            {
                if (inpPackage != null)
                {
                    int iCount = inpPackage.Diagrams.Count;

                    for (int iDx = 0; iDx < iCount; iDx++)
                    {
                        EA.Diagram diagram = null;

                        diagram = (EA.Diagram)inpPackage.Diagrams.GetAt((short)iDx);

                        if (strDiagramName.CompareTo(diagram.Name) == 0)
                        {
                            outDiagram = diagram;

                            break;
                        }
                    }
                }

                if (outDiagram == null)
                {
                    throw new Exception(strDiagramName + LMprimeEAConst.DIAGRAM_NOT_EXISTS);
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
            
            return outDiagram;
        }
    }
}
