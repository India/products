﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EATaggedValues.cs

   Description: This file is specific for contains functions related to Tagged Values.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeEA.constants;
using LMprimeCommon.commonbeans;
using LMprimeEA.eapropmanager;
namespace LMprimeEA.Utilities
{
    public class EATaggedValuesHelper
    {
        /* This function takes EA.Element as argument and it returns list of tagged values.
         * Tagged Value list is a list of List of strings.
         * List<List<string>>
         * Note each sub list contains tag value name as 1st elemnet and its value as second element
         * List<List<string>> = {tagvalv1Name, tagvalv1Value} List1
         *                      {tagvalv2Name, tagvalv2Value} List2
         *                      {tagvalv3Name, tagvalv4Value} List3
         *                      -----
         *                      -----
         *                      -----                         nth List
         */
        public List<EATaggedValue> getTaggedValueList(EA.Element inpElem)
        {
            List<EATaggedValue> ltTaggedValues = new List<EATaggedValue>();
            
            try
            {
                if (inpElem != null)
                {
                    foreach (EA.TaggedValue tValue in inpElem.TaggedValues)
                    {

                        EATaggedValue temp = new EATaggedValue();

                        if (tValue != null && tValue.Name != null && tValue.Value != null)
                        {
                            temp.strTagName  = tValue.Name.Trim();
                            
                            temp.strTagValue = tValue.Value.Trim();

                            ltTaggedValues.Add(temp);
                        }
                        
                     }
                }
                
                return ltTaggedValues;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /*  This function return the value of tagged value from the input element based on the input name provided,
         *  If more then one tagged value exists with same name then only first is returned 
         */
        public string getFirstTaggedValue(EA.Element inpElement, string strInpString)
        {
            string strOutString = null;

            try
            {
                if (inpElement != null && strInpString != null && strInpString.Length > 0)
                {
                    /*  Runnng the loop on all tagged values */
                    foreach (EA.TaggedValue tValue in inpElement.TaggedValues)
                    {
                        string strTempString = null;

                        strTempString = tValue.Name;

                        if (strTempString != null && strTempString.Length > 0 && strTempString.CompareTo(strInpString) == 0)
                        {
                            /*  If tagged value name matches then returning the same and breaking the loop */
                            strOutString = tValue.Value;
                            break;
                        }
                     }
                }
                return strOutString;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* Function to set Tagged Value by name.
         * This function is genralized function for updating tagged values of both cases.
         * Case 1: Specific to update of reverse mapping ie. update of jira key in Tagged Value.
         * Case 2: Updating any tagged value.
         * The input arguments in funtcion are Tagged value name, Tagged value, Input Element, Source Attribute.
         * Whenever there is update of tagged value for JIRA key then strSrcAttr will contain Key, else for all other cases it will be none.
         * It only appends the value on in case of Key, for all other tagged values, if value already exists then the existing tagged value is replaced.
         * Assuming that in each case whenever keys are updated in reverese amapping then they will be comma separated.
         */
        public void setTaggedValueByName(string strTaggedValueName, string strTaggedValue, ref EA.Element element,string strSrcAttr)
        {
            try
            {
                if (strTaggedValueName != null || strTaggedValueName.Length > 0 || strTaggedValue != null || strTaggedValue.Length != 0 || element != null)
                {
                    foreach (EA.TaggedValue tValue in element.TaggedValues)
                    {
                        if (tValue.Name.CompareTo(strTaggedValueName) == 0)
                        {
                            string strTemp = tValue.Value;

                            /*  This casee is for handling when updating reverse mapping for key */
                            if (strSrcAttr != null && strSrcAttr.Length > 0 && strSrcAttr.CompareTo(LMprimeEAConst.KEY) == 0)
                            {
                                if (strTemp != null && strTemp.Length > 0)
                                {
                                    string[] tempArr = strTemp.Split(',');

                                    if (tempArr != null && tempArr.Length > 0)
                                    {
                                        /*  Checking if the same key exists or not in value */
                                        for (int iDx = 0; iDx < tempArr.Length; iDx++)
                                        {
                                            if (tempArr[iDx].CompareTo(strTaggedValue) == 0)
                                            {
                                                /*  If the same key exists then return , No need to enter it additional case handled here */
                                                return;
                                            }
                                        }
                                    }
                                    if (strTemp != null && strTemp.Length > 0)
                                    {
                                        tValue.Value = strTemp + LMprimeEAConst.COMMA_SEPARATOR_CHAR + strTaggedValue;
                                    }
                                }
                                else
                                {
                                    tValue.Value = strTaggedValue;
                                }
                            }
                            else
                            {
                                if (strTemp != null && strTemp.Length > 0)
                                {
                                    tValue.Value = strTaggedValue;
                                }
                            }
                            tValue.Update();
                            element.Update();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        /*
         * This function is to segeragate list of specific types from a collection of lists.
         * This function traverses all the input list of tagged value objects and compares their name with input type name
         * and returns List<tagged values> with same tag name as provided in input string.
         */
        public List<EATaggedValue> getSeparatedTVListFrMapping(string strKey, List<EATaggedValue> ltInpTVList)
        {
            List<EATaggedValue> ltOutTV = new List<EATaggedValue>();
            try
            {
                /*  Validations on input list and key */
                if (ltInpTVList != null && ltInpTVList.Count > 0 && strKey != null && strKey.Length > 0)
                {
                    /*  Running a loop on all sub lists */
                    for (int iDx = 0; iDx < ltInpTVList.Count; iDx++)
                    {
                        EATaggedValue temp = new EATaggedValue();

                        temp = ltInpTVList[iDx];

                        if (temp != null)
                        {
                            /*  Comparing the list 1st elemet i.e tagname with input key */
                            if (temp.strTagName.CompareTo(strKey) == 0)
                            {
                                ltOutTV.Add(temp);
                            }
                        }
                    }
                }

                return ltOutTV;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}



