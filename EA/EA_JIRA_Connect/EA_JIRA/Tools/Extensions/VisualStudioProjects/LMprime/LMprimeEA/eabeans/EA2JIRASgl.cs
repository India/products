﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPEAToJIRAFinal.cs

   Description: This file contains final logic for parsing attributes from EA to Jira side.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.constants;

namespace LMprimeEA.eabeans
{
    public class EA2JIRASgl
    {
        public string strSourceAttr = "";
        public string strSourceAttrTyp = "";
        public string strDestAttr = "";
        public string strSrcPreSepr = "";
        public string strSrcPostSepr = "";
         public List<string> ltExcludedElem = new List<string>();
        public List<string> ltInclElem = new List<string>();

        public EA2JIRASgl getEATOJIRAFinalObj(string szInpString)
        {
            EA2JIRASgl lmpEAToJIRAFinal = new EA2JIRASgl();

            try
            {
                if (szInpString != null && szInpString.Length > 0)
                {
                    string[] tokens = szInpString.Split(LMprimeEAConst.VAL_SEPARATOR);

                    for (int iDx = 0; iDx < tokens.Length; iDx++)
                    {
                        if (iDx == 0)
                        {
                            if (tokens[iDx] != null && tokens[iDx].Length > 0)
                            {
                                if (iDx == 0)
                                {
                                    lmpEAToJIRAFinal.strSourceAttrTyp = tokens[iDx];
                                }
                            }
                        }
                        else if (iDx == 1)
                        {
                            List<string> ltTemp = findPrePostAndInfix(tokens[iDx]);

                            if (ltTemp.Count > 3)
                            {
                                throw new Exception(LMprimeEAConst.INVALID_EA_TO_JIRA_MAPPING + tokens[iDx]);
                            }

                            fillPrePostAndInfix(ref lmpEAToJIRAFinal, ltTemp);
                        }
                        else if (iDx == 2)
                        {
                            lmpEAToJIRAFinal.strDestAttr = tokens[iDx];
                        }
                        else if (iDx == 3)
                        {
                            string strTemp = tokens[iDx];

                            if (strTemp[0] == LMprimeEAConst.EXCLAMATION)
                            {
                                getexcludedElemList(ref lmpEAToJIRAFinal, tokens[3]);
                            }
                            else
                            {
                                getIncludedElemList(ref lmpEAToJIRAFinal, tokens[3]);
                            }
                        }
                    }
                }
                return lmpEAToJIRAFinal;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /*  Getting excluded element list */
        public void getexcludedElemList(ref EA2JIRASgl lmpEAToJIRAFinal, string strToken)
        {
            string[] tokens = strToken.Split(LMprimeEAConst.SEMICOLON_SEPARATOR);
            try
            {
                for (int iDx = 0; iDx < tokens.Count(); iDx++)
                {
                    if (iDx == 0)
                    {
                        string szSub = tokens[0].Substring(1, tokens[0].Length - 1);
            
                        lmpEAToJIRAFinal.ltExcludedElem.Add(szSub);
                    }
                    else
                    {
                        lmpEAToJIRAFinal.ltExcludedElem.Add(tokens[iDx]);
                    }
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        /*  Getting Allowed elements lists */
        public void getIncludedElemList(ref EA2JIRASgl lmpEAToJIRAFinal, string szToken)
        {
            string[] tokens = szToken.Split(LMprimeEAConst.SEMICOLON_SEPARATOR);
            
            try
            {
                for (int iDx = 0; iDx < tokens.Count(); iDx++)
                {
                    lmpEAToJIRAFinal.ltInclElem.Add(tokens[iDx]);
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }

        /*  To find Pre and Post Fix */
        public List<string> findPrePostAndInfix(string strInpString)
        {
            int iCount          = 0;
            String strTemp      = "";
            Boolean bFound      = false;
            List<string> ltTemp = new List<string>();
            try
            {
                for(int iDx=0; iDx < strInpString.Length; iDx++)
                {
                    strTemp = strTemp + strInpString[iDx];
                
                    if(strInpString[iDx] == LMprimeEAConst.CURLY_BRAC_START) 
                    {
                        bFound = true;

                        iCount++;
                    }
                    else if(strInpString[iDx] == LMprimeEAConst.CURLY_BRAC_CLS)
                    {
                        if(bFound == true && iCount == 1)
                        {
                            ltTemp.Add(strTemp);

                            strTemp = "";
                        
                            iCount = 0;
                        }
                        else
                        {
                            throw new Exception(LMprimeEAConst.INVALID_EA_TO_JIRA_MAPPING + strInpString);
                        }
                    }
                    else
                    {
                        if(bFound == false && (iDx + 1) == strInpString.Length)
                        {
                            ltTemp.Add(strTemp);
                        }
                        else
                        {
                            if((iDx + 1) == strInpString.Length)
                            {
                                throw new Exception(LMprimeEAConst.INVALID_EA_TO_JIRA_MAPPING + strInpString);
                            }
                        }
                    }
                }
                return ltTemp;
            }
            catch(Exception exException)
            {
                throw exException;
            }
         }

        /*  To Fill Pre, Post and Infix expression */
        public void fillPrePostAndInfix(ref EA2JIRASgl lmpEAToJIRAFinal, List<string> inpList)
        {
            try
            {
                for(int iDx = 0; iDx < inpList.Count; iDx++)
                {
                    if(iDx == 0)
                    {
                       if(inpList[iDx][0] == LMprimeEAConst.CURLY_BRAC_START)
                       {
                           lmpEAToJIRAFinal.strSrcPreSepr = inpList[iDx].Substring(1, inpList[iDx].Length - 2);
                       }
                       else
                       {
                           lmpEAToJIRAFinal.strSourceAttr = inpList[iDx]; 
                       }
                    }
                    else if(iDx == 1)
                    {
                        lmpEAToJIRAFinal.strSourceAttr = inpList[iDx];
                    }
                    else if(iDx == 2)
                    {
                       if(inpList[iDx][0] == LMprimeEAConst.CURLY_BRAC_START)
                       {
                           lmpEAToJIRAFinal.strSrcPostSepr = inpList[iDx].Substring(1, inpList[iDx].Length - 2);
                       }
                    }
                }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
    }
}



