﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EARelatedElementUtilities.cs

   Description: This File contains utility functions related to Related Elements.
========================================================================================================================================================================
Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
25-June-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LMprimeEA.constants;
using LMprimeEA.eabeans;

namespace LMprimeEA.utilities
{
    public class EARelatedElementUtilities
    {
        /*  This function is entry point for getting related elements, It takes input element, its connection mapping Object, Birectional flag,
         *  and returns a list of related element based on the connection inp object.
         *  It breaks the entire funcionality into two parts: 1) Getting related elements for all types. 2) Getting specific type of elements
         *  related with input element. These two conditions are driven by the exclude, include list/bIsAllElemTypAllowed attributes of connection Object.
         **/  
        public void getRelatedElem
        (
            EA.Repository repository,
            ref List<EA.Element> lOutElem,
            EA.Element inpElem ,
            EAPConnectionSgl lmpConFinal, 
            Boolean isBidirectional
        )
        {
            try
            {
                if (repository != null && lmpConFinal != null && inpElem != null)
                {
                    string strRelName = lmpConFinal.strRelationName;

                    if (lmpConFinal.bIsAllElemTypAllowed && lmpConFinal.lsExcludeElem.Count == 0 && strRelName != null && strRelName.Length > 0)
                    {
                        /*  Getting related elements of all types */
                        getRelElemAllTypes(repository, ref lOutElem, strRelName, inpElem, isBidirectional);
                    }
                    else
                    {
                        /*  Getting specific type of related elements */
                        getRelElemSpecifcTyp(repository, ref  lOutElem, strRelName, inpElem, lmpConFinal.lsIncludeOnlyElem, lmpConFinal.lsExcludeElem, isBidirectional, lmpConFinal.bIsAllElemTypAllowed);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }
        
        /*  This function gets all related elements based on specific relationship type.
         * 
         *  For e.g. - 
         *  If the input type is Realization, then it will find all the elements 
         *  related with input element with realization relation. No specific checking 
         *  is performed on the  related element types.
         *  
         * */
        public void getRelElemAllTypes
        (
            EA.Repository repository,
            ref List<EA.Element> lOutElem,
            string strConnectorType,
            EA.Element inpElem,
            Boolean isBidirectional
        )
        {
            try
            {
                if (inpElem != null && strConnectorType != null && strConnectorType.Length > 0)
                {
                    /*  Finding all connectors with related element */
                    EA.Collection con = inpElem.Connectors;

                    if (con != null)
                    {
                        /* Individualy processing each connector    */
                        for (int iDx = 0; iDx < con.Count; iDx++)
                        {
                            EA.Connector tempCon = null;
                            EA.Element tempElem  = null;

                            tempCon = ((EA.Connector)(con.GetAt((short)iDx)));

                            /* First checking the connection stereotype with the input relationship type */
                            if (tempCon != null && tempCon.Stereotype != null && tempCon.Stereotype.CompareTo("") != 0)
                            {
                                if (tempCon.Stereotype.Equals(strConnectorType, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    /*  Getting related element */
                                    tempElem = getSegregatedElem(repository, isBidirectional, inpElem, tempCon);
                                }
                            }
                            else if (tempCon != null && tempCon.Type != null && tempCon.Type.CompareTo("") != 0)
                            {
                                /* Secondly checking the connection stereotype with the input relationship type */
                                if (tempCon.Type.Equals(strConnectorType, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    /*  Getting related element */
                                    tempElem = getSegregatedElem(repository, isBidirectional, inpElem, tempCon);
                                }
                            }

                            if (tempElem != null)
                            {
                                lOutElem.Add(tempElem);
                            }
                        }

                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This function gets all related elements based on specific relationship type.
         * 
         *  For e.g. - 
         *  If the input type is Realization, then it will find  elements of specific type
         *  related with input element with realization relation. The type of related element is
         *  checked on the basis of connection object
         *  
         * */
        public void getRelElemSpecifcTyp
        (
            EA.Repository repository,
            ref List<EA.Element> lOutElem,
            string strConnectorType,
            EA.Element inpElem, 
            List<string> ltIncElem,
            List<string> ltExcElemElem,
            Boolean isBidirectional,
            Boolean bIsAllAllowed
        )
        {
            try
            {
                if (inpElem != null && strConnectorType != null && strConnectorType.Length > 0)
                {
                    /*  Finding all connectors with related element */
                    EA.Collection con = inpElem.Connectors;

                    if (con != null)
                    {
                        for (int iDx = 0; iDx < con.Count; iDx++)
                        {
                            EA.Connector tempCon = null;
                            EA.Element tempElem  = null;

                            tempCon = ((EA.Connector)(con.GetAt((short)iDx)));

                            /* First checking the connection stereotype with the input relationship type */
                            if (tempCon != null && tempCon.Stereotype != null && tempCon.Stereotype.CompareTo("") != 0)
                            {
                                if (tempCon.Stereotype.Equals(strConnectorType, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    /*  Getting related element */
                                    tempElem = getSegregatedElem(repository, isBidirectional, inpElem, tempCon);
                                }
                            }
                            else if (tempCon != null && tempCon.Type != null && tempCon.Type.CompareTo("") != 0)
                            {
                                /* Secondly checking the connection stereotype with the input relationship type */
                                if (tempCon.Type.Equals(strConnectorType, StringComparison.InvariantCultureIgnoreCase))
                                {
                                    /*  Getting related element */
                                    tempElem = getSegregatedElem(repository, isBidirectional, inpElem, tempCon);
                                }
                            }

                            /*Checking that if this element exists in include element list or not if yes then adding it to output list */
                            if (ltIncElem != null && ltIncElem.Count > 0 && tempElem != null)
                            {
                                if (isElemTypeSame(tempElem, ltIncElem))
                                {
                                    lOutElem.Add(tempElem);
                                }
                            }
                            else if (ltExcElemElem != null && ltExcElemElem.Count > 0 && tempElem != null)
                            {
                                /*Checking that if this element exists in exclude element list or not if no then adding it to output list */
                                if (!isElemTypeSame(tempElem, ltExcElemElem))
                                {
                                    lOutElem.Add(tempElem);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }

        /* Getting related elements based on Input parameters, i.e Bidirectional/Uni Directional   */
        public EA.Element getSegregatedElem
        (
            EA.Repository repository,
            Boolean isBiDirectional,
            EA.Element inpElem,
            EA.Connector connector
        )
        {
            try
            {
                EA.Element tempElem = null;

                if (isBiDirectional && connector.SupplierID == inpElem.ElementID)
                {
                    tempElem = repository.GetElementByID(connector.ClientID);
                }
                if (isBiDirectional && connector.SupplierID != inpElem.ElementID)
                {
                    tempElem = repository.GetElementByID(connector.SupplierID);
                }
                else if (!isBiDirectional && connector.SupplierID != inpElem.ElementID)
                {
                    tempElem = repository.GetElementByID(connector.SupplierID);
                }
                return tempElem;
            }
            catch (Exception exception)
            {
                throw exception;
            }
         }

        /*  This functions check whether the type of input element exists in input list of element types,
         *  if yes then it return true else it returns false
         * */
        public Boolean isElemTypeSame
        (
            EA.Element inpElement,
            List<string> ltElemTypList
        )
        {
            string strStereoType = inpElement.Stereotype;
            string strType       = inpElement.Type;
            Boolean bResult   = false;

            try
            {
                if (inpElement != null && ltElemTypList != null)
                {
                    for (int iDx = 0; iDx < ltElemTypList.Count; iDx++)
                    {
                        if (strType != null && strType.Length > 0)
                        {
                            if (strType.Equals(ltElemTypList[iDx], StringComparison.InvariantCultureIgnoreCase))
                            {
                                bResult = true;
                                break;
                            }
                        }

                        if (strStereoType != null && strStereoType.Length > 0)
                        {

                            if (strStereoType.Equals(ltElemTypList[iDx], StringComparison.InvariantCultureIgnoreCase))
                            {
                                bResult = true;
                                break;
                            }

                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return bResult;
        }

        /*  This function gives relation type and  stereotype of connector between source and dest element */
        public void getRelnType(EA.Element sourceElem, EA.Element destElem, ref String strType, ref string strStereoType)
        {
            try
            {
                if (sourceElem != null && destElem != null)
                {
                    /*  Finding all connectors with related element */
                    EA.Collection con = sourceElem.Connectors;

                    if (con != null && con.Count > 0)
                    {
                        for (int iDx = 0; iDx < con.Count; iDx++)
                        {
                            EA.Connector tempCon = null;

                            tempCon = ((EA.Connector)(con.GetAt((short)iDx)));

                            if (tempCon != null)
                            {
                                /*  This will be the case when there is outward relationship between parent and child */
                                if (destElem.ElementID == tempCon.SupplierID)
                                {
                                    strType = tempCon.Type;

                                    strStereoType = tempCon.Stereotype;

                                    break;
                                }
                                else if (sourceElem.ElementID == tempCon.SupplierID && destElem.ElementID == tempCon.ClientID)
                                {
                                    /*  This is the case when there is inward relation ship between child and parent */

                                    strType = tempCon.Type;

                                    strStereoType = tempCon.Stereotype;

                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }
     }
}



