﻿/*======================================================================================================================================================================
                                      Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimePropConstants.cs

   Description: This file is contains constants related to properties
 
========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-March-2018       M.Singh                Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprimeEA.constants
{
    class LMprimePropConstants
    {
        /*  Input EA Property Constants */
        public static string KEYWORD    = "Keyword";

        /* Output EA Property Constants */
        public static string TAG        = "Tag";

        /* JIRA Property Constants */
        public static string COMPONENTS = "Components";
        public static string LABELS     = "Labels";
    }
}
