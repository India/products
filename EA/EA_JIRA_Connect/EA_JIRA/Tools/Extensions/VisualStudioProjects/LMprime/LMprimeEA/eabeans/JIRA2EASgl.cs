﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                     Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPJIRAToEAFinal.cs

   Description: This file creates final container for JIRATOEA object.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.constants;

namespace LMprimeEA.eabeans
{
    public class JIRA2EASgl
    {
        public string strSourceAttrName = "";
        public string strDesAttrName = "";
        public string strDesAttrTyp = "";
        
        /*  For getting JIRA 2 EA Single object i.e corresponding to one tagged value */
        public JIRA2EASgl getFinalObj( string strInpString)
        {
            JIRA2EASgl lmpJira2EAFinal = new JIRA2EASgl();
            
            try
            {
                if (strInpString != null)
                {
                    string[] tokens = strInpString.Split(LMprimeEAConst.VAL_SEPARATOR);

                    if(tokens.Length > 3)
                    {
                        throw new Exception(LMprimeEAConst.INVALID_EA_TO_JIRA_MAPPING + strInpString);
                    }

                    for (int iDx = 0; iDx < tokens.Length; iDx++)
                    {
                        if (tokens[iDx] != null && tokens[iDx].Length > 0)
                        {
                            if(iDx == 0)
                            {
                                lmpJira2EAFinal.strSourceAttrName = tokens[iDx];
                            }
                            else if (iDx == 1)
                            {
                                lmpJira2EAFinal.strDesAttrTyp = tokens[iDx];
                            }
                            else if (iDx == 2)
                            {
                                lmpJira2EAFinal.strDesAttrName = tokens[iDx];
                            }
                        }
                    }
                }
                return lmpJira2EAFinal;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
