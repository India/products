﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eabeans;
using LMprimeEA.utilities;
using LMprimeEA.Utilities;
using LMprimeEA.constants;
using LMprimeEA.eapropmanager;
using LMprimeEA.eadataengine;

namespace LMprimeEA.issuemanager
{
    public class GenerateIssueDataMain
    {


        EATaggedValuesHelper eTagValue = new EATaggedValuesHelper();
        GetPropertyValueMain gPropValMain = new GetPropertyValueMain();

        /*  This is genric function that can create information of parent ticket and as well as merged ticket,
         *  Not as and output argument it returns a dictionary that contains <key,value> destination attribute, value
         *  Destination attribute in this case Jira attribute.
         * */

        public void getIssueDictionaryForSingleElem
        (
         EA.Repository repository,
         EA.Element parentElem,
         EA.Element inpElem, 
         ref Dictionary<string, string> inpDict, 
         Boolean bIsMerged
        )
        {
            try
            {
                /* From EA to JIRA */
                EA2JIRAColl lmpEATOJIRA = new EA2JIRAColl();
                EAMappingUtilities eMapUtil = new EAMappingUtilities();

                if (repository != null && inpElem != null)
                {
                    lmpEATOJIRA = eMapUtil.getEAToJiraObj(inpElem);

                    if (lmpEATOJIRA != null && lmpEATOJIRA.ltEAtoJiraFinal != null && lmpEATOJIRA.ltEAtoJiraFinal.Count > 0)
                    {
                        /*   For parent Element this merged Elem Will be false   */

                        for (int iDx = 0; iDx < lmpEATOJIRA.ltEAtoJiraFinal.Count; iDx++)
                        {
                            if (lmpEATOJIRA.ltEAtoJiraFinal[iDx] != null)
                            {
                                if (bIsMerged)
                                {
                                    prepareMergedData(ref inpDict, lmpEATOJIRA.ltEAtoJiraFinal[iDx], inpElem, parentElem);
                                }
                                else
                                {
                                    prepareSeparateData(ref inpDict, lmpEATOJIRA.ltEAtoJiraFinal[iDx], inpElem);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  For Separate Task   */
        public void prepareSeparateData(ref Dictionary<string, string> outIssueDict, EA2JIRASgl lmEa2JIRAFinal, EA.Element inpElement)
        {

            if (outIssueDict != null && lmEa2JIRAFinal != null)
            {
                try
                {
                    string valForKey = null;

                    /*  EA Attribute */
                    string strSrcAttr = lmEa2JIRAFinal.strSourceAttr;

                    /*  EA Attribute, Can be tagged value */
                    string strSrcAttrTyp = lmEa2JIRAFinal.strSourceAttrTyp;

                    /*  JIRA Attribute */
                    string strDesAttrName = lmEa2JIRAFinal.strDestAttr;

                    /* Pre Separator */
                    string strPreFix = lmEa2JIRAFinal.strSrcPreSepr;

                    /*  Post Separator */
                    string strPostFix = lmEa2JIRAFinal.strSrcPostSepr;

                    if (strDesAttrName != null)
                    {
                        if (strSrcAttr != null && strSrcAttr.Length > 0 && strSrcAttrTyp != null && strSrcAttrTyp.Length > 0)
                        {
                            if (strSrcAttrTyp.Equals(LMprimeEAConst.TAGGED_VALUE, StringComparison.InvariantCultureIgnoreCase))
                            {
                                valForKey = eTagValue.getFirstTaggedValue(inpElement, strSrcAttr);
                            }
                        }

                        else if (strSrcAttr != null && strSrcAttr.Length > 0)
                        {
                            valForKey = gPropValMain.getPropertyValueMain(strSrcAttr, inpElement);
                        }

                        if (strPreFix != null && strPreFix.Length > 0)
                        {
                            valForKey = strPreFix + valForKey;
                        }
                        else if (strPostFix != null && strPostFix.Length > 0)
                        {
                            valForKey = valForKey + strPostFix;
                        }

                        if (valForKey != null && valForKey.Length > 0)
                        {
                            if (!outIssueDict.ContainsKey(strDesAttrName))
                            {
                                outIssueDict.Add(strDesAttrName, valForKey);
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
         }
        /*  For Mergerd Element Task   */
        public void prepareMergedData(ref Dictionary<string, string> outIssueDict, EA2JIRASgl lmEa2JIRAFinal, EA.Element inpElement, EA.Element parentElem)
        {
            CheckIfPropIsObj csPropObj = new CheckIfPropIsObj();
            GetSeparatorBasedOnCon gSepBasedOnCon = new GetSeparatorBasedOnCon();

            if (outIssueDict != null && lmEa2JIRAFinal != null)
            {
                try
                {
                    string valForKey = null;
                    string temp = null;

                    /*  EA Attribute */
                    string strSrcAttr = lmEa2JIRAFinal.strSourceAttr;

                    /*  EA Attribute, Can be tagged value */
                    string strSrcAttrTyp = lmEa2JIRAFinal.strSourceAttrTyp;

                    /*  JIRA Attribute */
                    string strDesAttrName = lmEa2JIRAFinal.strDestAttr;

                    /* Pre Separator */ 
                    string strPreFix = lmEa2JIRAFinal.strSrcPreSepr;

                    /*  Post Separator */
                    string strPostFix = lmEa2JIRAFinal.strSrcPostSepr;

                    string strSeparator = gSepBasedOnCon.getSeparatorbasedOnConObject(parentElem, inpElement);

                    if (strDesAttrName != null)
                    {
                        if (strSrcAttr != null && strSrcAttr.Length > 0 && strSrcAttrTyp != null && strSrcAttrTyp.Length > 0)
                        {
                            if (strSrcAttrTyp.Equals(LMprimeEAConst.TAGGED_VALUE, StringComparison.InvariantCultureIgnoreCase))
                            {
                                valForKey = eTagValue.getFirstTaggedValue(inpElement, strSrcAttr);
                            }
                        }

                        else if (strSrcAttr != null && strSrcAttr.Length > 0)
                        {
                            valForKey = gPropValMain.getPropertyValueMain(strSrcAttr, inpElement);
                        }

                        if (valForKey != null && valForKey.Length > 0)
                        {
                            if(strPreFix != null && strPreFix.Length > 0)
                            {
                                valForKey = strPreFix + valForKey;
                            }
                            else if(strPostFix != null && strPostFix.Length > 0)
                            {
                                valForKey = valForKey + strPostFix;
                            }
                            if (outIssueDict.ContainsKey(strDesAttrName))
                            {
                                /*  Case if key exists  */
                                string strTemp = outIssueDict[strDesAttrName];

                                if (csPropObj.checkIfPropIsObj(strDesAttrName))
                                {
                                    valForKey = strTemp + LMprimeEAConst.VAL_SEPARATOR + valForKey;
                                }
                                else
                                {
                                    valForKey = strTemp + strSeparator + valForKey;
                                }
                                
                                outIssueDict[strDesAttrName] = valForKey;
                            }
                            else
                            {
                                outIssueDict.Add(strDesAttrName, valForKey); 
                            }
                        }  
                         
                      }
                    }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
        }
    }
}



