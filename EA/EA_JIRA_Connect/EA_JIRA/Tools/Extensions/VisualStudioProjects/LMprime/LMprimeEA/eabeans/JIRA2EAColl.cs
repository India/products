﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec India
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMPJIRAToEA.cs

   Description: This file is container for mapping of JIRA TO EA.

========================================================================================================================================================================

Date                Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
5-July-2017    Akshay Kumar Singh          Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeCommon.Utilities;
using LMprimeEA.constants;
using LMprimeEA.eapropmanager;
using LMprimeEA.Utilities;
using LMprimeCommon.commonbeans;

namespace LMprimeEA.eabeans
{
    public class JIRA2EAColl
    {
        /*  Class level variables */
        private JIRA2EASgl        jira2EASgl = new JIRA2EASgl();
        public List<JIRA2EASgl> ltJira2EASgl = new List<JIRA2EASgl>();

        public void getJIRATOEAObj(EAPMapping lmpMapping, List<EATaggedValue> ltTagValues)
        {
            EATaggedValuesHelper eTagHelper = new EATaggedValuesHelper();
            List<EATaggedValue> ltJIRA2EA   = new List<EATaggedValue>();

            try
            {
                /*  Separating the values of JIRA 2 EA tagged values */
                ltJIRA2EA = eTagHelper.getSeparatedTVListFrMapping(LMprimeEAConst.LMI_JIRA2EA, ltTagValues);

                for (int iDx = 0; iDx < ltJIRA2EA.Count; iDx++)
                {
                      JIRA2EASgl temp = new JIRA2EASgl();

                      /*  Getting JIRA 2 EA single object i.e corresponding to one tagged values */ 
                      temp = jira2EASgl.getFinalObj(ltJIRA2EA[iDx].strTagValue);

                      lmpMapping.jira2EAColl.ltJira2EASgl.Add(temp);      
                 }
            }
            catch(Exception exception)
            {
                throw exception;
            }
        }
     }
}












