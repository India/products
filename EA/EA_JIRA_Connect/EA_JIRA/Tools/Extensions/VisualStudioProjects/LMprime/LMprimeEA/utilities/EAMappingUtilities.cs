﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprimeEA.eabeans;
using LMprimeEA.constants;

namespace LMprimeEA.utilities
{
    public class EAMappingUtilities
    {
        /* This fuction returns Connection Object for Imput element, If input element does not contains, connection object, or mapping does not exists then null value is returned
         * */
        public EAPConnectionColl getSelConnectionObj(EA.Element inpElem)
        {
            EAPConnectionColl eapCon    = null;
            EAPMapping eapMapping       = null;
            
            try
            {
                if(inpElem != null)
                {
                    eapMapping = getSelectedObjMapping(inpElem);
                    
                    if (eapMapping != null)
                    {
                        eapCon = eapMapping.eapConnection;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            
            return eapCon;
        }

        /* This fuction returns EA2JIRA Object for input element, null object is returned in case mapping is not found or the input element does not have ea2Jira object.
         * */
        public EA2JIRAColl getEAToJiraObj(EA.Element inpElem)
        {
            EA2JIRAColl ea2JIRA   = null;
            EAPMapping eapMapping = null;

            try
            {
                if (inpElem != null)
                {
                    eapMapping = getSelectedObjMapping(inpElem);

                    if (eapMapping != null)
                    {
                        ea2JIRA = eapMapping.ea2JIRAColl;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return ea2JIRA;
        }

        /* This fuction returns JIRA2EA Object for input element, null object is returned in case mapping is not found or the input element does not have jira2ea object
         * */
        public JIRA2EAColl getJIRAToEAObj(EA.Element inpElem)
        {
            JIRA2EAColl jira2EAColl = null;
            EAPMapping  eapMapping  = null;

            try
            {
                if (inpElem != null)
                {
                    eapMapping = getSelectedObjMapping(inpElem);

                    if (eapMapping != null)
                    {
                        jira2EAColl = eapMapping.jira2EAColl;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return jira2EAColl;
        }

        /* This function finds out the EAMapping object for current element, It finds out the type and Stereotype of input Object,
         * It checks in the mapping dictionary whether Stereotype exists in it as Key, if yes then it returns EA Mapping Object corresponding to that stereotype,
         * If it does not exists then it checks for its Type, if its type exists then it retuns EA Mapping Object corresponding to its type.
         * It both of them is not found then null object is returned. 
         * */
        public EAPMapping getSelectedObjMapping(EA.Element inpElem)
        {
            string     strElemType       = "";
            string     strElemStereoType = "";
            EAPMapping eapMapping        = null;

            try
            {
                /*  Seleceted element type  */
                strElemType = inpElem.Type;

                /*  Seleceted element Stereotype  */
                strElemStereoType = inpElem.Stereotype;

                if (LMprimeEAConst.MAPPING != null && LMprimeEAConst.MAPPING.Count > 0)
                {
                    /*  First checking element with its stereotype.This dual checking is performed because when in ea mapping template two 
                     *  elements can have same types but there stereotype will always be diffrent.So when we construct dictionary for storing
                     *  mapping file(i.e AppDelegate.Final Mapping) we checks any key exists with the element type name. If yes that means the 
                     *  element are having same type so we store second element with its stereotype name as key in dictionary. 
                     */
                    if (strElemStereoType != null && LMprimeEAConst.MAPPING.ContainsKey(strElemStereoType))
                    {
                        eapMapping = new EAPMapping();

                        eapMapping = LMprimeEAConst.MAPPING[strElemStereoType];
                    }
                    else if (strElemType != null && LMprimeEAConst.MAPPING.ContainsKey(strElemType))
                    {
                        /*  Secondly Checking for element type   */
                        eapMapping = new EAPMapping();

                        eapMapping = LMprimeEAConst.MAPPING[strElemType];
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return eapMapping;
        }

    }
}



