﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprimeCommon.Utilities;

namespace LMprimeEA.eapropmanager
{
    class GetPropertyValueMain
    {
        PropertyInfoUtil pInfoGet = new PropertyInfoUtil();
        
        /*  For Getting Property Value Name */
        public string getPropertyValueMain(string strInpPropName, EA.Element inpElem)
        {
            GetPropInternalName gPropInternalName = new GetPropInternalName();
            try
            {
                string strPropValue = null;
                
                if(strInpPropName != null && strInpPropName.Length > 0 && inpElem != null)
                {
                    string strPropInternalName = gPropInternalName.getInternalName(strInpPropName);

                    if (strPropInternalName != null && strPropInternalName.Length > 0)
                    {
                         strPropValue = pInfoGet.GetPropertyValue(inpElem, strPropInternalName);
                    }
                }
                return strPropValue;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
