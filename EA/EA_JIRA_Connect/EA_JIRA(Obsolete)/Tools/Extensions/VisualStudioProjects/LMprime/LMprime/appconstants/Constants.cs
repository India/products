﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Constants.cs

   Description: This File contains Constants to be used in entire application.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.appconstants
{
    class Constants
    {
        /*  Default Project */
        public static string LMPRIME = "LMprime";
        /*  Success and Failure Macros  */
        public static int SUCCESS = 1;
        public static int FAILURE = -1;
        public static string SUCCESS_STRING = "Success";
        public static string FAILURE_STRING = "FAILURE";

        /*   Error Handling Macros  */
        public static String ERROR_OCCURED = "Error Occured";
        public static string INTERNAL_ERROR_OCCURED = "Internal Error Occured";

        /*  Request and Responce Macros */
        public static string GET = "GET";
        public static string POST = "POST";
        public static string APPLICATIONJSON = "application/json";
        public static string AUTHORIZATION = "Authorization";
        public static string BASIC = "Basic";

        /* EA Related   */
        public static int FIRST_ELEMENT = 0;
        public static string TRACE = "trace";
        public static string TAGGED_VALUE = "TaggedValue";
        public static string ATTRIBUTE = "Attribute";

        /* Error Constants  */
        public static string PLUGIN_INIT_FAILURE = "Failed to initialize Enterprise Architect Addin.Please try Again.";
        public static string INIT_DATA_FAILED = "Failed to initialize data";

        /*  Error Message Captions  */
        public static string CAP_ERROR = "Error";

        /*  Parsing Error Constants */
        public static string ERROR_IN_READING_XML_FILE = "Error in reading xml file";
        public static string NULL_EA_MAPPING_FILE = "Null EA Mapping File";
        public static string NULL_EA_MAPPNG_OBJECT = "Null EA Mapping Object";
        public static string NO_MAPPING_ELEMENT = "No Mapping Element found on parsing file";
        public static string NO_EA_PROJECT_ELEM_TAG_EXIST = "No EAProject tag exists in mapping file";
        public static string NULL_EA_PROJECT = "Null EA Project";
        public static string PROJECT_NAME_NULL = "Project name is null or of length zero in EAProject element";
        public static string NULL_JIRA_ELEMENT = "Null Jira element";
        public static string NULL_JIRA_URL = "JIRA URL in mapping file not defined. Modify the mapping under appropriate project to access EA-JIRA integrator.";
        public static string NULL_PROJECT_LIST = "Null project List in project element tag";
        public static string NULL_PROJECT_LIST_IN_ELEM = "Jira Project not defined in mapping file. Modify the mapping under appropriate project to access EA-JIRA integrator.";
        public static string NULL_JIRA_ELEM_TAG = "Null Jira Element tag in jira Project";
        public static string NULL_JIRA_PROPERTY_TAG = "Null Jira Property Element Tag";
        public static string INVALID_JIRA_PROPERTY = "Invalid Jira Property Tag";
        public static string NULL_JIRA_ELEM_TYPE = "JIRA element type is null or of zero length";
        public static string NULL_EA_PROPERTY = "NULL or Zero length EA Property";
        public static string INVALID_EA_PROPERTY = "Invalid EA PROPERTY";
        public static string NULL_EA_ELEMENT_LIST = "Null EA element List";
        public static string NULL_EA_ELEMENT = "Null EA Element";
        public static string NULL_EA_ELEMET_TYPE = "Null EA element type";
        public static string INVALID_EA_ELEMENT_TYPE = "Invalid EA Element Type";
        public static string INVALID_JIRA_ISSUE_TYPE = "Invalid Jira Issue Type";
        public static string NULL_OR_ZERO_LENGTH_EAPROPERTY = "Null or zero length EA property in EA element";
        public static string NULL_EA_PROPERTY_ELEM_IN_PROP_LIST = "Null EA Property element in EA property List";
        public static string NULL_PROPERTY_NAME_OR_REF_TYPE = "Null property name or reference type";
        public static string INVALID_PROPERTY_NAME_OR_REF_TYPE = "Invalid property name or reference type";
        public static string NULL_JIRA_PROPERTY_IN_EA_ELEM_TAG = "Null Jira property in ea element tag";
        public static string INVALID_CONNECTION_TYPE = "Invalid Connection Type";
        public static string NO_JIRA_ISSUE_EXISTS = "No Jira Issue Type Exist";
        public static string PROJECT_VALIDATION_FAILED = "Project Validation Failed";
        public static string NULL_PROP_STRING = "Property XML File is null or of zero length";
        public static string NULL_PROP_FILE_PARSED_OBJ = "Null Property file parsed object";
        public static string EXCEP_OCCUR_IN_ASSIGN_PROP_OBJ = "Exception occured in assigning property file object";

        /*  File parsing Logic related Macros   */
        public static string Direct_EA_PRJ_CHILD = "Direct EAProject Child";
        public static string CHILD_OF_CONNECTION = "Child of Connection Tag";

        /*  Logger Constants    */
        public static int VERBOSE = 0;
        public static int DEBUG = 1;
        public static int INFO = 2;
        public static int WARNING = 3;
        public static int ERROR = 4;

        /*  Keys for validations    */
        public const int KEY_MAPPING = 0;
        public const int KEY_EAPROJECT = 1;
        public const int KEY_JIRA = 2;
        public const int KEY_JIRA_ELEMENT = 3;

        /*  JIRA ISSUE LINK NAME    */
        public static string RELATES = "Relates";

        /*  Property Type Macros    */
        public static int TYPE_STRING = 0;
        public static int TYPE_OBJECT = 1;

        /*  Custom Exception String Constants   */
        public static string UNAUTHORIZED = "(401) Unauthorized";
        public static string INVALID_ISSUE_TYPE = "The issue type selected is invalid";
        public static string COMPONENT_REQUIRED = "Component/s is required";
        public static string SUBTASK_ERROR = "Issue type is a sub-task but parent issue key or id not specified";

    }
}
