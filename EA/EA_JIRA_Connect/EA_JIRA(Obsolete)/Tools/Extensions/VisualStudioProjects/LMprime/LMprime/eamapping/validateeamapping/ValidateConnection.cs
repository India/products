﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateConnection.cs

   Description: This File is specific to loading Connection element validation file.

========================================================================================================================================================================

Date          Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.ea;
using LMprime.jira;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{

    class ValidateConnection
    {
        Logger log = Logger.getInstance();

        public void validateEAConnection(List<Connection> conList)
        {
            try
            {
                if (conList == null || conList.Count == 0)
                {
                    return;
                }
                else
                {
                    processConnectionList(conList);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void processConnectionList(List<Connection> conList)
        {
            ValidateConnectionSingleElem vCSElem = new ValidateConnectionSingleElem();

            try
            {
                for (int iDX = 0; iDX < conList.Count; iDX++)
                {
                    vCSElem.validateConnectionSingleElem(conList[iDX]);

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
