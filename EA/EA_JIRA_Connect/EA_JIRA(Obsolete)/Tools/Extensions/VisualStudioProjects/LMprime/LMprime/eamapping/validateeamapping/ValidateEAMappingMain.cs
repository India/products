﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAMappingMain.cs

   Description: This File is an entry point to entire mapping file validation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.exception;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateEAMappingMain
    {
        private Logger log = Logger.getInstance();

        /*  This Function is an Entry point to Entire Mapping File Validation   */
        public void ValidateEAMapping(EAMapping eaMapping)
        {
            ValidateEAMapping vEaMapping = new ValidateEAMapping();

            ValidateObject vObj = new ValidateObject();

            try
            {
                vObj.ValidateObj(Constants.NULL_EA_MAPPNG_OBJECT, eaMapping, Constants.KEY_MAPPING);

                vEaMapping.validateEAMapping(eaMapping);
            }
            catch (Exception exception)
            {
                log.error("ValidateEAMappingMain : ValidateEAMappingMain() Exception occured in validating xml file ");
                throw exception;
            }

        }
    }
}






