﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EnviromentUtility.cs

   Description: This File contains API's specific to Enviroment variables.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
29-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LMprime.others
{
    class EnviromentUtility
    {
        /*  Default Constructor */
        public EnviromentUtility()
        {
        }

        /*  This is utility function too get Enviroment variables   */

        public string getEnvVariable(String strPathVarName)
        {
            string strPath = null;
            try
            {
                strPath = Environment.GetEnvironmentVariable(strPathVarName);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return strPath;
        }

        /*  This is a utility function to set Enviroment variable   */

        public void setEnvVariable(string strKey, string strValue)
        {
            try
            {
                Environment.SetEnvironmentVariable(strKey, strValue);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}


