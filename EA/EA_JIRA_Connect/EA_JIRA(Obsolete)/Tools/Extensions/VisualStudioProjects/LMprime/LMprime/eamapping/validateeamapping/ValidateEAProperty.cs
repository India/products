﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAProperty.cs

   Description: This File is specific to EAProperty validation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.ea;
using LMprime.jira;
using LMprime.exception;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.ea;
using LMprime.logger;
using LMprime.helpers;
using LMprime.appdelegate;

namespace LMprime.eamapping.validateeamapping
{

    class ValidateEAProperty
    {
        Logger log = Logger.getInstance();
        static PropertiesInfo pInfo = new PropertiesInfo();

        public ValidateEAProperty()
        {

        }

        public void validateEAProperty(List<EAProperty> ePropList)
        {
            try
            {
                if (ePropList == null || ePropList.Count <= 0)
                {
                    log.error("ValidateEAProperty : validateEAProperty : NULL or zero length ea property");

                    throw new Exception(Constants.NULL_OR_ZERO_LENGTH_EAPROPERTY);
                }

                processEAPropertyList(ePropList);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void processEAPropertyList(List<EAProperty> ePropList)
        {
            try
            {
                for (int iDx = 0; iDx < ePropList.Count; iDx++)
                {
                    validateEAPropertySingleElem(ePropList[iDx]);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void validateEAPropertySingleElem(EAProperty eProp)
        {
            EAElementUtils eUtils = new EAElementUtils();

            try
            {
                if (eProp == null)
                {
                    log.error("Validate EAProperty : calidateEAPropertySingleElement: Null property in list");
                    throw new Exception(Constants.NULL_EA_PROPERTY_ELEM_IN_PROP_LIST);
                }

                if (eProp.Name == null || eProp.Name.Length <= 0 || eProp.Type == null || eProp.Type.Length <= 0)
                {
                    log.error("Validate EAProperty : calidateEAPropertySingleElement: Null Prop Name or type");
                    throw new Exception(Constants.NULL_PROPERTY_NAME_OR_REF_TYPE);
                }

                validateJiraProperty(eProp.JiraProperty);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void validateJiraProperty(string szJProp)
        {
            try
            {
                if (szJProp == null || szJProp.Length <= 0)
                {
                    log.error("Validate EAProperty : calidateEAPropertySingleElement: Null Prop Name or type");
                    throw new Exception("Constants.NULL_JIRA_PROPERTY_IN_EA_ELEM_TAG");
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
