﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: PrepareMergedElement.cs

   Description: This class is for preparing MergedElementData

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.logger;
using LMprime.appconstants;
using LMprime.appdelegate;
using LMprime.ea;
using LMprime.helpers;
using Atlassian.Jira;
using LMprime.network;

namespace LMprime.issuemanager.mergeddataprep
{
    class PrepareMergedElement
    {
        Logger log = new Logger();

        public void prepareMergedData(EA.Element mainElem, List<EA.Element> merElemList, ref Dictionary<String, List<String>> issueDictionary)
        {
            int iCount = 0;

            iCount = merElemList.Count;

            for (int iDx = 0; iDx < iCount; iDx++)
            {
                EA.Element merElem = (EA.Element)merElemList[iDx];
                getMatchedElement(mainElem, merElem, ref issueDictionary);
            }
        }

        public void getMatchedElement(EA.Element mainElem, EA.Element merElem, ref Dictionary<String, List<String>> issueDictionary)
        {
            int iCount = 0;

            iCount = AppDelegate.eProject.EAElement.Count;

            for (int iDx = 0; iDx < iCount; iDx++)
            {
                if (mainElem.Type.CompareTo(AppDelegate.eProject.EAElement[iDx].Type) == 0)
                {
                    getMatchedConnectionType(mainElem, merElem, AppDelegate.eProject.EAElement[iDx], ref issueDictionary);

                    break;
                }
            }
        }

        public void getMatchedConnectionType(EA.Element mainElem, EA.Element merElem, EAElement matchElem, ref Dictionary<String, List<String>> issueDictionary)
        {
            bool isRealize = false;
            bool isTrace = false;
            int iCount = 0;
            String type = "";

            Connection con = new Connection();
            EAGenHelpers eGenHelpers = new EAGenHelpers();


            isRealize = eGenHelpers.isByRealize(mainElem, merElem);
            isTrace = eGenHelpers.isByTrace(mainElem, merElem);

            if (isRealize)
                type = "Realization";
            if (isTrace)
                type = "Trace";

            iCount = matchElem.Connection.Count;

            for (int iDx = 0; iDx < iCount; iDx++)
            {
                if (type.CompareTo(matchElem.Connection[iDx].Type) == 0)
                {
                    getMatchedSubElement(matchElem.Connection[iDx], merElem, ref issueDictionary);

                    break;
                }
            }
        }

        public void getMatchedSubElement(Connection con, EA.Element merElem, ref Dictionary<String, List<String>> issueDictionary)
        {
            int iCount = 0;

            iCount = con.EAElement.Count;

            for (int iDx = 0; iDx < iCount; iDx++)
            {
                if (con.EAElement[iDx].Type.CompareTo(merElem.Type) == 0)
                {
                    int iPropCount = con.EAElement[iDx].EAProperty.Count;

                    for (int iFx = 0; iFx < iPropCount; iFx++)
                    {
                        createFinalMergedData(con.EAElement[iDx].EAProperty[iFx], merElem, ref issueDictionary);
                    }
                    break;
                }
            }
        }

        public void createFinalMergedData(EAProperty eProp, EA.Element eaElem, ref Dictionary<String, List<String>> issueDictionary)
        {

            string tempKey = "";
            var tempEAVal = "";
           
            tempKey = eProp.JiraProperty;

            if (eProp.ReferenceType != null && eProp.ReferenceType != "" && (eProp.ReferenceType.CompareTo("TaggedValue") == 0))
            {
                EATaggedValue eTagValue = new EATaggedValue();

                tempEAVal = eTagValue.getTaggedValueByName(eProp.Name, ref eaElem);
            }
            else
            {
                tempEAVal = PropertiesInfo.GetPropertyValue(eaElem, eProp.Name).ToString();
            }

            if (tempEAVal != null)
            {
                List<string> tempStringList = new List<string>();
                Issue issue = null;

                if (Network.sJira != null)
                {
                    issue = Network.sJira.CreateIssue(null);
                }

                if (issueDictionary.ContainsKey(tempKey))
                {

                    string tempVal = "";
                    issueDictionary.TryGetValue(tempKey, out tempStringList);
                    Type type = issue.GetType();
                    System.Reflection.PropertyInfo propertyInfo = type.GetProperty(tempKey);

                    if (propertyInfo != null)
                    {
                        PropertyTypeInfo pTypeInfo = new PropertyTypeInfo();

                        int iPtype = pTypeInfo.getPropertyType(issue, tempKey);

                        if (iPtype == Constants.TYPE_OBJECT)
                        {
                            tempStringList.Add(tempEAVal);
                        }
                        else
                        {
                            if (eProp.Separator != null)
                            {
                                tempVal = tempStringList[0];
                                if (tempVal == null || (tempVal.CompareTo("") == 0))
                                {
                                    if (tempEAVal != null)
                                    {
                                        tempVal = tempEAVal.ToString();
                                    }
                                }

                                else if (tempEAVal != null)
                                {
                                    if (eProp.Separator.CompareTo("\\n") == 0 || eProp.Separator.CompareTo("\\r") == 0)
                                    {
                                        tempVal = tempVal + Environment.NewLine + tempEAVal.ToString();
                                    }
                                    else if (eProp.Separator.CompareTo("\\t") == 0)
                                    {
                                        StringBuilder stringBuilder = new StringBuilder();
                                        tempVal = stringBuilder.Append(tempVal).Append("\t") + tempEAVal.ToString();
                                    }
                                    else
                                    {
                                        tempVal = tempVal + eProp.Separator + tempEAVal.ToString();
                                    }
                                }
                            }
                            else
                            {
                                tempVal = tempStringList[0];
                                if (tempVal == null || (tempVal.CompareTo("") == 0))
                                {
                                    if (tempEAVal != null)
                                    {
                                        tempVal = tempEAVal.ToString();
                                    }
                                }
                                else
                                {
                                    if (tempEAVal != null)
                                    {
                                        tempVal = tempVal + "," + tempEAVal.ToString(); //Default seperator
                                    }
                                    
                                }
                            }
                            tempStringList.Clear();
                            tempStringList.Add(tempVal);
                            issueDictionary[tempKey] = tempStringList;
                        }
                        
                    }
                 }
                else
                {
                    tempEAVal = tempEAVal.ToString();
                    tempStringList.Add(tempEAVal);
                    issueDictionary.Add(tempKey,tempStringList);
                }
            }
        }
    }
}
