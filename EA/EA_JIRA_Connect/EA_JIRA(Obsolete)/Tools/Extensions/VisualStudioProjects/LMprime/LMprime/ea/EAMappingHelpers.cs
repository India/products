﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAMappingHelper.cs

   Description: This File contains helper API's specific to EA Mapping.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprime.logger;
using LMprime.beans;

namespace LMprime.ea
{
    class EAMappingHelpers
    {
        public EAElement getEAElementForSelected(EA.Element element)
        {
            EAElement elem = new EAElement();

            try
            {
                if (AppDelegate.eaMapping.EAProject[0].EAElement != null && AppDelegate.eaMapping.EAProject[0].EAElement.Count > 0 && element != null)
                {

                    int iCount = AppDelegate.eaMapping.EAProject[0].EAElement.Count;

                    for (int iDx = 0; iDx < iCount; iDx++)
                    {
                        if (AppDelegate.eaMapping.EAProject[0].EAElement[iDx].Type.Equals(element.Type))
                        {
                            elem = AppDelegate.eaMapping.EAProject[0].EAElement[iDx];
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return elem;
        }
    }
}
