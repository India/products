﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateFinalSepDataList.cs

   Description: This File is for creating final issue Dictionary.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appdelegate;
using LMprime.beans;
using LMprime.helpers;
using LMprime.ea;

namespace LMprime.issuemanager.separatedataprep
{
    class CreateFinalSepDataList
    {
        Logger log = Logger.getInstance();

        public void createFinalSepList(ref List<Dictionary<String, List<String>>> issueDictionaryList, List<EA.Element> sepDataList)
        {
            try
            {
                if (sepDataList == null || sepDataList.Count <= 0)
                {
                    log.error("CreateFinalSepDataList :: createFinalSepList() : Null or zero length sep data list");

                    throw new Exception(" Null or zero length sep data list");
                }

                for (int iDx = 0; iDx < sepDataList.Count; iDx++)
                {
                    EAElement tempElem = getMatchedElement(sepDataList[iDx]);

                    createIssueDictionary(ref issueDictionaryList, tempElem, sepDataList[iDx]);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public EAElement getMatchedElement(EA.Element tempElem)
        {
            try
            {
                int count = AppDelegate.eProject.EAElement.Count;

                string type = tempElem.Type;

                for (int iDx = 0; iDx < count; iDx++)
                {
                    if (type.CompareTo(AppDelegate.eProject.EAElement[iDx].Type) == 0)
                    {
                        return AppDelegate.eProject.EAElement[iDx];
                    }
                }

                log.error("CreateFinalSepDataList :: getMatchedElement : No matched element found in Mapping File");

                throw new Exception("Issue cant not be created for Element" + tempElem.Name + "as Mapping File does not contains defination for it.");
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return null;
        }

        public void createIssueDictionary(ref List<Dictionary<String, List<String>>> issueDictionaryList, EAElement tempElem, EA.Element sepElement)
        {
            String key = "";
            String value = "";
            Dictionary<string, List<string>> temp = new Dictionary<string, List<string>>();
            
            
            try
            {
                for (int iDx = 0; iDx < tempElem.EAProperty.Count; iDx++)
                {
                    List<string> tempString = new List<string>();
                    if (tempElem.EAProperty[iDx] == null || sepElement == null)
                    {

                        log.error("CreateFinalSepDataList :: createIssueDictioary : Null EA Property or Seperated Element");

                        throw new Exception("CreateFinalSepDataList :: createIssueDictioary : Null EA Property or Seperated Element");

                    }

                    key = tempElem.EAProperty[iDx].JiraProperty;

                    string tempValue = getEAElementValue(tempElem.EAProperty[iDx], sepElement);

                    if (tempValue != null && tempValue.Length > 0)
                    {
                        value = tempValue;
                        tempString.Add(value);
                        temp.Add(key, tempString);
                    }
                    else
                    {
                        log.info("CreateFinalSepDataList :: createIssueDictioary : value for" + tempElem.EAProperty[iDx] + "is null for element" + sepElement.Name);
                    }
                }
                if (temp != null && temp.Count > 0)
                {
                    issueDictionaryList.Add(temp);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public string getEAElementValue(EAProperty eProp, EA.Element sepElement)
        {
            string value = "";

            try
            {
                if (eProp.ReferenceType != null && eProp.ReferenceType != "" && (eProp.ReferenceType.CompareTo("TaggedValue") == 0))
                {
                    EATaggedValue eTagValue = new EATaggedValue();

                    value = eTagValue.getTaggedValueByName(eProp.Name, ref sepElement);
                }
                else
                {
                    value = PropertiesInfo.GetPropertyValue(sepElement, eProp.Name).ToString();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return value;
        }

    }
}
