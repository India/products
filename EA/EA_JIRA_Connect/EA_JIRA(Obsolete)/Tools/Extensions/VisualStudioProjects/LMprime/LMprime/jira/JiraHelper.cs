﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: JiraHelper.cs

   Description: This File is Helper File For Jira.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
02-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.exception;
using LMprime.appconstants;
using LMprime.appdelegate;
using LMprime.beans;
using LMprime.logger;

namespace LMprime.jira
{
    class JiraHelper
    {
        List<string> propList = null;
        int iMatchFlag = Constants.FAILURE;
        Logger log = Logger.getInstance();

        public void checkValidJiraProperty(string szPropName)
        {
            if (AppDelegate.lmProp.JiraProp.Jprop == null)
            {
                log.error("JiraHelper : checkValidJiraProperty : Null Jira property file List in App Delegate");
                throw new Exception(Constants.NULL_PROP_STRING);
            }

            try
            {
                for (int iDx = 0; iDx < AppDelegate.lmProp.JiraProp.Jprop.Count; iDx++)
                {
                    if (szPropName.Equals(AppDelegate.lmProp.JiraProp.Jprop[iDx]))
                    {
                        iMatchFlag = Constants.SUCCESS;
                    }
                }

                if (iMatchFlag == Constants.SUCCESS)
                    return;
                else
                {
                    log.error("JiraHelper : checkValidJiraProperty : Invalid Jira Property" + szPropName);
                    throw new Exception(Constants.INVALID_JIRA_PROPERTY);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
