﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: IssueTyp.cs

   Description: This File Container for IssueTup Request

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note:-Beans are specialized containers that are used to store information.
 *  These Beans are request specific and cannot be used for any other purpose,Use of such thing may result
 *  in Loss of data.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;

namespace LMprime.beans
{

    public class IssueTyp
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public bool subtask { get; set; }
        public int avatarId { get; set; }

    }
}
