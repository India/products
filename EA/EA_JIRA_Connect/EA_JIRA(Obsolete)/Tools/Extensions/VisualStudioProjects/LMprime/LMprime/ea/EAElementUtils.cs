﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAUtils.cs

   Description: This File is contains EA specific utility functions.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* 
 * Note :- Very specific Utility functions is defined in this class
 *         Every function in this class requires one argument that is EA.Element which is generally.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.appdelegate;
using LMprime.logger;
using LMprime.helpers;
using LMprime.appdelegate;

namespace LMprime.ea
{
    class EAElementUtils
    {
        Logger log = Logger.getInstance();

        public void checkValidEAProperty(string szPropName)
        {
            int iMatchFlag = Constants.FAILURE;
            if (AppDelegate.lmProp.EAProp.Eprop == null)
            {
                log.error("EAUtil : checkValidEAProperty : Null EA property file List in App Delegate");
                throw new Exception(Constants.NULL_PROP_STRING);
            }

            if (szPropName == null || szPropName.Length <= 0)
            {
                log.error("EAUtil : checkValidProperty : Null oe zero length property name in EAProperty tag");
                throw new Exception(Constants.NULL_EA_PROPERTY);
            }

            try
            {
                for (int iDx = 0; iDx < AppDelegate.lmProp.EAProp.Eprop.Count; iDx++)
                {
                    if (szPropName.Equals(AppDelegate.lmProp.EAProp.Eprop[iDx]))
                    {
                        iMatchFlag = Constants.SUCCESS;
                    }
                }

                if (iMatchFlag == Constants.SUCCESS)
                    return;
                else
                {
                    log.error("EAUtil : checkValidEAProperty : Invalid EA Property" + szPropName);
                    throw new Exception(Constants.INVALID_EA_PROPERTY);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


        /*  A getter Function to get any property Dynamically
         *  It takes property name and instance of object from which property value should be taken out.
         *  And It returns property value.
         * */
        public string getPropertyByName(string szPropertyName, EA.Element element)
        {
            string szPropValue = "";
            try
            {
                if (szPropertyName == null || szPropertyName.Length <= 0 || element == null)
                {
                    log.error("EAUtils : getPropertyByName : Null or zero length EA property or null ea Element");
                    throw new Exception("Null or zero length EA property or null ea Element");
                }

                szPropValue = PropertiesInfo.GetPropertyValue(element, szPropertyName).ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return szPropValue;
        }

        /*  To get Selected Element */
        public EA.Element getSelectedElement()
        {
            /*  Initializing Local variables    */

            EA.Diagram currentDiagram = null;
            EA.Collection selObjects = null;
            EA.DiagramObject diagObj = null;
            EA.Element element = null;
            EAGenHelpers eaGenHelp = new EAGenHelpers();

            /*  Getting instance of current Diagram */
            currentDiagram = eaGenHelp.getCurrentDiagram();

            if (currentDiagram != null)
            {
                try
                {
                    /*  Getting Selected Object */
                    selObjects = currentDiagram.SelectedObjects;

                    if (selObjects != null && selObjects.Count > 0)
                    {
                        diagObj = (EA.DiagramObject)selObjects.GetAt((short)Constants.FIRST_ELEMENT); //Need to discuss Taking Only one object as selected

                        if (diagObj != null)
                        {
                            //Taking zeroth element as we want to take only one selected elemenmt
                            element = AppDelegate.selectedRepo.GetElementByID(diagObj.ElementID);
                        }

                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
            return element;
        }

    }
}
