﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseEAMapping.cs

   Description: This File is specific to loading EAMapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
05-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;
using LMprime.exception;
using LMprime.beans;
using LMprime.xmlutilities;
using LMprime.logger;
using LMprime.appconstants;

namespace LMprime.eamapping.parseeamapping
{
    class ParseEAMapping : ReadXml
    {
        EAMapping eaMapping = null;
        Logger log = Logger.getInstance();

        public EAMapping parseEAMappingFile()
        {

            string szEaMappingFilePath = "";
            string szEaMappingFile = "";

            try
            {
                szEaMappingFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;

                szEaMappingFilePath = Path.GetDirectoryName(szEaMappingFilePath);

                log.info("Dll location is " + szEaMappingFilePath);

                szEaMappingFilePath = szEaMappingFilePath + "/EAMapping.xml";

                szEaMappingFilePath = szEaMappingFilePath.Replace(@"\", @"/");

                log.info("Mapping file path is " + szEaMappingFilePath);

                szEaMappingFile = readXmlFile(szEaMappingFilePath);

               // szEaMappingFile = readXmlFile("C:/EAMapping.xml");      //TODO:: Read this mapping file from installation path not from hardcoded path

                if (szEaMappingFile != null)
                {
                    convertFileToObject(szEaMappingFile);
                }
                else
                {
                    log.error("ParseEAMapping : parseEAMappingFile(): EA Mapping File not found");
                    throw new Exception(Constants.NULL_EA_MAPPING_FILE);
                }

            }
            catch (Exception exception)
            {
                log.error("ParseEAMapping : parseEAMappingFile(): Error in Parsing  Maapping XML File");
                throw exception;
            }

            return eaMapping;
        }

        public void convertFileToObject(string szEaMappingFile)
        {

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EAMapping));

                StringReader szrMappingString = new StringReader(szEaMappingFile);

                eaMapping = (EAMapping)serializer.Deserialize(szrMappingString);
            }

            catch (Exception exception)
            {
                log.error("ParseEAMapping : convertFileToObject(): Error in Serializing xml file");
                throw exception;
            }

        }
    }
}
