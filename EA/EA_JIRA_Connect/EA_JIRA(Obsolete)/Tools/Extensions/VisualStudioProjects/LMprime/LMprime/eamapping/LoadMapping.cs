﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoadMapping.cs

   Description: This File is specific to loading EAMapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
05-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/*  Note:- This is main file for Mapping file related operations.
 *         It is Entry point to entire mapping file operations(Read,Parse,Validate).
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.exception;
using LMprime.beans;
using LMprime.eamapping.parseeamapping;
using LMprime.appconstants;
using LMprime.eamapping.validateeamapping;
using LMprime.appdelegate;
using LMprime.logger;
using LMprime.helpers;
using EA;
using LMprime.ea;

namespace LMprime.eamapping
{
    class LoadMapping
    {
        private Logger log = Logger.getInstance();

        public void loadAndValidateMapping()
        {
            ParseEAMapping pEAMapping = new ParseEAMapping();
            ValidateEAMappingMain vEAMappingMain = new ValidateEAMappingMain();
            EAMapping eaMapping = new EAMapping();

            try
            {
                eaMapping = pEAMapping.parseEAMappingFile();

                if (eaMapping == null)
                {
                    log.error("LoadMapping : loadAndValidateMapping() : Null EA Mapping File object");
                    throw new Exception(Constants.NULL_EA_MAPPNG_OBJECT);
                }
                else
                {
                    log.info("LoadMapping : loadAndValidateMapping() : FILE SUCCESSFULLY FILLED IN STRUCTURES VALIDATION GETTING STARTED");

                    /*  Validating the Entire Mapping File  */
                    vEAMappingMain.ValidateEAMapping(eaMapping);

                    log.info("LoadMapping : loadAndValidateMapping() : eaMapping File Successfully Validated");
                    /*  Setting AppDelegate EAMapping Object    */
                    AppDelegate.eaMapping = eaMapping;

                    /*  Setting Current Project to AppDelegate  */
                    setProjectToAppDelegate();

                    /*  Setting Selected Element to App Delegate    */
                    setSelectedElementToAppdeleate();
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        /*  Function for setting project to AppDelegate */
        public void setProjectToAppDelegate()
        {
            EAGenHelpers eGenHelp = new EAGenHelpers();
            bool isProjectMatched = false;
            EAProject eProj = null;

            string packageName = eGenHelp.getCurrentRootPackageName();

            for (int iDx = 0; iDx < AppDelegate.eaMapping.EAProject.Count; iDx++)
            {
                eProj = AppDelegate.eaMapping.EAProject.ElementAt(iDx);

                if (eProj.Name == packageName)
                {
                    isProjectMatched = true;
                    break;
                }
            }
            if (isProjectMatched == true)
            {
                AppDelegate.eProject = eProj;
                log.error("LoadMapping : setProjectToAppDelegate : Using Confidured Project" + eProj.Name);
                return;
            }
            else
            {
                packageName = Constants.LMPRIME;
                for (int iDx = 0; iDx < AppDelegate.eaMapping.EAProject.Count; iDx++)
                {
                    eProj = AppDelegate.eaMapping.EAProject.ElementAt(iDx);

                    if (eProj.Name == packageName)
                    {
                        isProjectMatched = true;
                        break;
                    }
                }
                if (isProjectMatched == true)
                {
                    AppDelegate.eProject = eProj;
                    log.error("LoadMapping : setProjectToAppDelegate : Using Default Project" + eProj.Name);

                }
                else
                {
                    log.error("LoadMapping : setProjectToAppDelegate : No Mapping configuration found for EAProject" + packageName);

                    throw new Exception("No Mapping configurations defined for EA Project :" + packageName + " in mapping file.");
                }
            }
        }

        /*  Function for setting selected element to AppDelegate   */
        public void setSelectedElementToAppdeleate()
        {
            EA.Element tempElem = null;
            EAElementUtils eaElemUtils = new EAElementUtils();

            try
            {
                //tempElem = eaElemUtils.getSelectedElement();
                Object obj = AppDelegate.selectedRepo.GetContextObject();

                if (obj != null)
                {

                    if (AppDelegate.selectedRepo.GetContextItemType() == EA.ObjectType.otElement)
                    {

                        tempElem = (EA.Element)AppDelegate.selectedRepo.GetContextObject();

                        if (tempElem == null)
                        {
                            throw new Exception("Selected Element is Null");
                        }
                        AppDelegate.selectedElement = tempElem;
                    }
                    else
                    {
                        throw new Exception("Selected Element is Null");
                    }
                }
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}



