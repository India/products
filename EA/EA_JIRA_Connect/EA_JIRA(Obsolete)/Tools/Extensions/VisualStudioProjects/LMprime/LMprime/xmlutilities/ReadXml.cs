﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReadXml.cs

   Description: This File for reading xml file .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.logger;

namespace LMprime.xmlutilities
{
    public class ReadXml
    {
        Logger log = Logger.getInstance();

        public ReadXml()
        {
        }

        public string readXmlFile(string szFilepath)
        {
            string szXmlFile = null;

            if (szFilepath == null)
            {
                log.error("ReadXml : readXmlFile() : Error Occured in reading XML File");
                throw new Exception(Constants.ERROR_IN_READING_XML_FILE);
            }
            try
            {
                szXmlFile = System.IO.File.ReadAllText(szFilepath);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szXmlFile;
        }

    }
}
