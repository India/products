﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: AppDelegate.cs

   Description: This File contains API's specific to entire Application.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using LMprime.beans;

namespace LMprime.appdelegate
{
    class AppDelegate
    {
        /*  Static Variables    */
        public static EA.Repository selectedRepo = null;
        public static EA.Element selectedElement = null;
        public static bool isUserLoggedIn = false;

        /* This object stores mapping file  */
        public static EAMapping eaMapping = null;
        public static EAProject eProject = null;
        public static LMprimeProp lmProp = null;
        public static string username = "";
        public static string password = "";
    }
}
