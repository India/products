﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appdelegate;
using LMprime.appconstants;

namespace LMprime.ea
{
    class EAGenHelpers
    {
        Logger log = Logger.getInstance();

        /*  To get Current Diagram  */
        public EA.Diagram getCurrentDiagram()
        {
            EA.Diagram currentDiagram = null;

            try
            {
                if (AppDelegate.selectedRepo != null)
                {
                    currentDiagram = AppDelegate.selectedRepo.GetCurrentDiagram();
                }
                else
                {
                    log.error("EAGenHelpers : getCurrentDiagram() : App delegate selected reprostiory is null");

                    throw new Exception("EAGenHelpers : getCurrentDiagram() : App delegate selected reprostiory is null");
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return currentDiagram;
        }

        /*  Get Current Package name */
        public string getCurrentPackageName()
        {
            String szPackageName = null;
            try
            {
                if (AppDelegate.selectedRepo != null)
                {

                    int packageId = AppDelegate.selectedRepo.GetCurrentDiagram().PackageID;

                    szPackageName = AppDelegate.selectedRepo.GetPackageByID(packageId).Name;

                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szPackageName;
        }

        /*     Get Current Root Package name    */

        /*  Get Current Package name */
        public string getCurrentRootPackageName()
        {
            String szPackageName = null;
            try
            {
                if (AppDelegate.selectedRepo != null)
                {

                    if (AppDelegate.selectedRepo.Models != null)
                    {
                        EA.Package ePack = (EA.Package)AppDelegate.selectedRepo.Models.GetAt(0);
                        szPackageName = ePack.Name;
                    }

                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szPackageName;
        }

        /*  Check whether relationship type is realization or not   */

        public bool isByRealize(EA.Element primaryElem, EA.Element secElem)
        {
            bool result = false;
            try
            {
                EA.Collection elemCollect = primaryElem.Realizes;

                for (int iDx = 0; iDx < elemCollect.Count; iDx++)
                {
                    EA.Element tempElem = (EA.Element)elemCollect.GetAt((short)iDx);
                    if (secElem.ElementID == tempElem.ElementID)
                    {
                        result = true;
                        break;
                    }
                }
                if(!result)
                {
                    elemCollect = secElem.Realizes;
                    for (int iDx = 0; iDx < elemCollect.Count; iDx++)
                    {
                        EA.Element tempElem = (EA.Element)elemCollect.GetAt((short)iDx);
                        if (primaryElem.ElementID == tempElem.ElementID)
                        {
                            result = true;
                            break;
                        }
                    }
                }
            }

            catch (Exception exception)
            {
                throw exception;
            }
            return result;
        }

        public bool isByTrace(EA.Element primaryElem, EA.Element secElem)
        {
            bool result = false;

            List<EA.Element> tempList = new List<EA.Element>();

            int iCount = 0;

            EA.Connector con = null;

            iCount = primaryElem.Connectors.Count;

            EA.Element trElem = null;


            for (int iDx = 0; iDx < iCount; iDx++)
            {
                try
                {
                    con = (EA.Connector)primaryElem.Connectors.GetAt((short)iDx);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                if (con == null)
                {
                    continue;
                }

                if (con.Stereotype == Constants.TRACE)
                {
                    try
                    {

                        trElem = AppDelegate.selectedRepo.GetElementByID(con.ClientID);

                        if (trElem == null)
                        {
                            continue;
                        }

                        if (trElem.ElementID == primaryElem.ElementID)
                        {

                            trElem = AppDelegate.selectedRepo.GetElementByID(con.SupplierID);
                        
                            if (trElem == null)
                            {
                                continue;
                            }

                         }
                        tempList.Add(trElem);
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
            }

            for (int iDx = 0; iDx < tempList.Count; iDx++)
            {
                if (secElem.ElementID == tempList[iDx].ElementID)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

    }
}
