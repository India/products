﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateJiraProperty.cs

   Description: This File is specific to JiraProperty element validation in mapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.exception;
using LMprime.appconstants;
using LMprime.jira;
using LMprime.ea;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateJiraProperty
    {
        Logger log = Logger.getInstance();

        public void validateJiraProperty(List<JiraProperty> jProperty)
        {
            try
            {
                if (jProperty == null || jProperty.Count <= 0)
                {
                    log.error(" ValidateJiraProperty : validateJiraProperty() : Null Jra Property List ");
                    throw new Exception(Constants.NULL_JIRA_PROPERTY_TAG);
                }

                processJiraPropertyElement(jProperty);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void processJiraPropertyElement(List<JiraProperty> jProp)
        {
            try
            {
                for (int iDx = 0; iDx < jProp.Count; iDx++)
                {
                    if (jProp[iDx].Name == null || jProp[iDx].Name.Length <= 0)
                    {
                        log.error("processJiraPropertyELement : Null or Zero Length JIRA Property");
                        throw new Exception(Constants.NULL_JIRA_PROPERTY_IN_EA_ELEM_TAG);
                    }

                    validateEAProperty(jProp[iDx].EAProperty.Name);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void validateEAProperty(string szEAproperty)
        {
            try
            {
                if (szEAproperty == null || szEAproperty.Length <= 0)
                {
                    log.error("ValidateJiraProperty : validateEAProperty() Null EA Property");
                    throw new Exception(Constants.NULL_EA_PROPERTY);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


    }
}
