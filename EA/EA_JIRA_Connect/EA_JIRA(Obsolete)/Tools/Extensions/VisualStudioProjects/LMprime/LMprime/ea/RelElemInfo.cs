﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: RelElemInfo.cs

   Description: This Class is specific for getting Related Element info for EAElement.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appdelegate;
using LMprime.appconstants;
using LMprime.helpers;


namespace LMprime.ea
{
    class RelElemInfo
    {
        Logger log = Logger.getInstance();
        EAElementUtils eUtils = new EAElementUtils();

        /*  This function give list of  related elements by realization */

        public List<EA.Element> getRelatedElementsByRealize(EA.Element element,Boolean isBiDirReq)
        {
            int iCount = 0;
            List<EA.Element> relElemList = new List<EA.Element>();

            if (element == null)
            {
                return relElemList;
            }

            EA.Collection elemCollect;

           
            try
            {
                elemCollect = element.Realizes;
                
                if (elemCollect != null)
                {

                    iCount = elemCollect.Count;

                    EA.Element relElem = null;

                    for (int iDx = 0; iDx < iCount; iDx++)
                    {
                        relElem = (EA.Element)elemCollect.GetAt((short)iDx);

                        if (relElem == null)
                        {
                            continue;
                        }

                        relElemList.Add(relElem);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return relElemList;
        }
        /* Get Related Elements By Trace    */

        public void getRelatedElementByTrace(EA.Element element, Boolean isBiDirReq)
        {
            int iCount = 0;
            List<EA.Element> relElemList = new List<EA.Element>();

            if (element == null)
            {
                return;
            }

            EA.Connector con = null;

            iCount = element.Connectors.Count;

            EA.Element trElem = null;


            for (int iDx = 0; iDx < iCount; iDx++)
            {
                try
                {
                    con = element.Connectors.GetAt((short)iDx);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                if (con == null)
                {
                    continue;
                }

                if (con.Stereotype == Constants.TRACE)
                {
                    try
                    {

                        trElem = AppDelegate.selectedRepo.GetElementByID(con.ClientID);

                        if (trElem == null)
                        {
                            continue;
                        }

                        if (trElem.ElementID == element.ElementID)
                        {

                            trElem = AppDelegate.selectedRepo.GetElementByID(con.SupplierID);



                            if (trElem == null)
                            {
                                continue;
                            }

                            relElemList.Add(trElem);

                        }
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
            }
        }

        /*   New API Added for Bi directional purpose */

        public List<EA.Element> getRelatedElement(EA.Element element, Boolean isbiDirElemReq,String szType)
        {
            int iCount = 0;
            List<EA.Element> relElemList = new List<EA.Element>();

            if (element == null)
            {
                return relElemList;
            }

            EA.Connector con = null;

            iCount = element.Connectors.Count;

            EA.Element trElem = null;


            for (int iDx = 0; iDx < iCount; iDx++)
            {
                try
                {
                    con = (EA.Connector)element.Connectors.GetAt((short)iDx);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
                if (con == null)
                {
                    continue;
                }

                if (con.Type.CompareTo(szType) == 0 ||  PropertiesInfo.GetPropertyValue(con, "Stereotype").ToString().CompareTo(Constants.TRACE) == 0)
                {
                    try
                    {
                        trElem = null;
                        if (isbiDirElemReq)
                        {
                            trElem = AppDelegate.selectedRepo.GetElementByID(con.ClientID);
                            if (trElem.ElementID == element.ElementID)
                            {
                                trElem = AppDelegate.selectedRepo.GetElementByID(con.SupplierID);
                            }
                            relElemList.Add(trElem);

                        }
                        else
                        {
                            trElem = AppDelegate.selectedRepo.GetElementByID(con.ClientID);
                            if (trElem.ElementID == element.ElementID)
                            {
                                trElem = AppDelegate.selectedRepo.GetElementByID(con.SupplierID);
                                relElemList.Add(trElem);
                            }
                        }
                        if (trElem == null)
                        {
                            continue;
                        }
                        
                    
                    }
                    catch (Exception exception)
                    {
                        throw exception;
                    }
                }
            }
            return relElemList;
        }
    
    }
}
