﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmbH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: IssueDetails.cs

   Description: This function takes outinformation form JIRA.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Feb-2017    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using LMprime.network;

namespace LMprime.jiramanager
{
    class IssueDetails
    {
        public Issue getIssueDetailsFromJira(string szIssueId)
        {
            Issue issue = null;
            try
            {
                Network netwrk = new Network();

                netwrk.createJiraRestClient();

                issue = netwrk.getIssueDetailsFromJira(szIssueId);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return issue;
        }
    }
}



