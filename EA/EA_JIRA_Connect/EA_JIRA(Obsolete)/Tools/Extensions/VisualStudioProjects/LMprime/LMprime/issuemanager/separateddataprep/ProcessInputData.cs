﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ProcessInputDataS.cs

   Description: This File is an Entry point for Creating Seperated Issues.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/*This class Fires out request in Jira for creating separate Issues*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.jiramanager;
using LMprime.issuemanager.mergeddataprep;

namespace LMprime.issuemanager.separatedataprep
{
    class ProcessInputDataS
    {
        Logger log = Logger.getInstance();
        CreateFinalSepDataList cFinalSepDataList = new CreateFinalSepDataList();
        List<Dictionary<String, List<String>>> issueDictionaryList = new List<Dictionary<string, List<string>>>();
        JiraManager jManager = new JiraManager();


        public string processInputDataList(string szIssueType, string szProjName, List<EA.Element> sepElemList,List<string>issueTypList)
        {
            string szIssueId = "";

            /*No need to for null check as it has already been checked by input data element    */
            try
            {
                createSeparateDataList(sepElemList);

                /* Note :- Issue Dictionary List contains Jira property as key and value of EAProperty as value.
                 *         Seperate Element List contains List of EA Elements it as one-one mapping with issue Dictionsary List
                 **/

                szIssueId = jManager.createIssue(null, szProjName, issueDictionaryList, sepElemList, null, false,issueTypList);
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szIssueId;
        }

        public void createSeparateDataList(List<EA.Element> sepElemList)
        {
            try
            {
                if (issueDictionaryList != null)
                {
                    issueDictionaryList.Clear();
                }
                
                cFinalSepDataList.createFinalSepList(ref issueDictionaryList, sepElemList);

                if (issueDictionaryList == null || issueDictionaryList.Count < 0)
                {
                    throw new Exception("Seperated Element create Issue occured due to Null or Zero length issue Dictionary");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
