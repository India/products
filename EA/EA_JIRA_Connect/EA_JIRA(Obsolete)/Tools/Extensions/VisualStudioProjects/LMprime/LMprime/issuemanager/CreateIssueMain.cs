﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueMain.cs

   Description: This File is an Entry point for Creating an Issue in Jira.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Dec-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprime.issuemanager.mergeddataprep;
using LMprime.issuemanager.separatedataprep;
using LMprime.issuemanager.linkedIssuePrep;
using LMprime.logger;
using LMprime.issuemanager.subtaskdataprep;

namespace LMprime.issuemanager
{
    class CreateIssueMain
    {
        Logger log = Logger.getInstance();
        List<EA.Element> subTaskElemList = new List<Element>();
        List<string> subTaskIssueTypList = new List<string>();

        /* This function is an entry point for creating issue inside JIRA.
         *  @param szIssueType:                         Issue Type taking from Mapping File(As user selects it from DropDown List).
         *  @param szProjectName:                       Project name taken as User Input.
         *  @param EA.Element rootElement:              Root Element of which ticket is to be created.
         *  @param List<EA.Element> mrgIssueRelElem:    List of those related elements whose information should be merged with root element.
         *  @param List<EA.Element> sepIssueRelElem:    List of those elements whose seperate tickets should be created.    
         *  @param List<EA.Element> linkIssueList:      List of elements whose issue has to be linked by their parent
         * *  */
        
        public String createJiraIssue(String szIssueType, String szProjectName, EA.Element rootElement, List<EA.Element> mrgIssueRelElem, List<EA.Element> sepIssueRelElem, List<EA.Element> linkIssueList,List<string> issueTypList)
        {
            PrepareMergedData pMerData = new PrepareMergedData();
            PrepareSeparateData pSepData = new PrepareSeparateData();
            List<string> linkIssueTypeList = new List<string>();
            
            
            String szJiraIssueId = "";

            try
            {
                validateRequest(szIssueType, szProjectName, rootElement);

                /*
                 * Note :- Firstly the Seperated Issues are created then merged issues are created.  
                 *         Separted issue element list can be null but merged element list cant be null.
                 */

                /* Note :- Firstly the Merged issue is Created.Then Seperated Issue are created including those of
                           Sub tasks also and then the issues are linked together
                 **/
    
                if (sepIssueRelElem != null && sepIssueRelElem.Count > 0)
                {
                    PrepareSubTaskList pSubTaskList = new PrepareSubTaskList();

                    if (subTaskElemList != null)
                    {
                        subTaskElemList.Clear();
                    }
                    if (subTaskIssueTypList != null)
                    {
                        subTaskIssueTypList.Clear();
                    }
                    
                    pSubTaskList.prepareSubTaskList(ref sepIssueRelElem, ref issueTypList, ref subTaskElemList, ref subTaskIssueTypList);

                    if (sepIssueRelElem != null && sepIssueRelElem.Count > 0 && issueTypList != null && issueTypList.Count > 0)
                    {
                        generateLinkIssueTypeList(sepIssueRelElem, linkIssueList, issueTypList);
                        
                        szJiraIssueId = pSepData.prepareSeparateData(szIssueType, szProjectName, sepIssueRelElem, issueTypList);
                    }
                }

                
                if (linkIssueTypeList != null)
                {
                    linkIssueTypeList.Clear();
                }

                linkIssueTypeList = generateLinkIssueTypeList(sepIssueRelElem, linkIssueList, issueTypList);

                if (szJiraIssueId.CompareTo("") != 0)
                {
                    szJiraIssueId = generateDispKeyDialogString(szJiraIssueId, linkIssueList, linkIssueTypeList);
                }

                szJiraIssueId = "Main JIRA Ticket : " + pMerData.prepareMergedData(szIssueType, szProjectName, rootElement, mrgIssueRelElem, linkIssueList, linkIssueTypeList) + "\n" + szJiraIssueId;

                if (subTaskIssueTypList != null && subTaskIssueTypList.Count > 0 && subTaskElemList != null && subTaskElemList.Count > 0)
                {
                    string strSubTaskIds = "";
                    strSubTaskIds = pSepData.prepareSeparateData(szIssueType, szProjectName, subTaskElemList, subTaskIssueTypList) + "\n";
                }
           }
            catch (Exception exception)
            {
                throw exception;
            }
            return szJiraIssueId;
        }

        public void validateRequest(String szIssueType, String szProjectName, EA.Element rootElement)
        {
            try
            {
                if (szIssueType == null || szIssueType.Length <= 0 || szProjectName == null || szProjectName.Length <= 0 || rootElement == null)
                {
                    log.error("CreateIssueMain : validateRequest : Null Issue type or Project Name or root Element");
                    throw new Exception("Null Issue type or Project Name or root Element");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public List<string> generateLinkIssueTypeList(List<EA.Element> sepIssueRelElem, List<EA.Element> linkIssueList, List<string> issueTypList)
        {
            List<string> linkedIssueTypeList = new List<String>();
            try
            {
                for (int iDx = 0; iDx < linkIssueList.Count; iDx++)
                {
                    for (int iFx = 0; iFx < sepIssueRelElem.Count; iFx++)
                    {
                        if (linkIssueList[iDx].ElementID == sepIssueRelElem[iFx].ElementID)
                        {
                            linkedIssueTypeList.Add(issueTypList[iFx]);
                        }
                    }
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return linkedIssueTypeList;
        }
        
        public string generateDispKeyDialogString(string szIssueIds,List<EA.Element> LinkElementList,List<string> IssueTypList)
        {
            string szOutString = "";
            LinkIssueMain lIssueMain = new LinkIssueMain();
            try 
            {
                if (LinkElementList != null && IssueTypList != null)
                {
                    string[] tempArray = szIssueIds.Split(' ');
                    
                    List<string> mainList = tempArray.ToList();
                    
                    List<string> linkIssueList = lIssueMain.getLinkIssueKeyList(IssueTypList, LinkElementList);

                    for (int iDx = 0; iDx < linkIssueList.Count; iDx++)
                    {
                        if(mainList.Contains(linkIssueList[iDx]))
                        {
                            mainList.Remove(linkIssueList[iDx]);
                        }
                    }

                    for(int iDx = 0; iDx <mainList.Count;iDx++)
                    {
                        if (iDx == 0)
                            szOutString = "Independent Tickets : " + mainList[iDx];
                        else
                            szOutString = szOutString + "," + mainList[iDx];
                    }
                    if(mainList.Count > 0)
                        szOutString = szOutString+"\n";
                  /*  for (int iDx = 0; iDx < linkIssueList.Count; iDx++)
                    {
                        if (iDx == 0)
                            szOutString = szOutString + "Linked Tickets" + linkIssueList[iDx];
                        else
                            szOutString = szOutString + "," + linkIssueList[iDx];
                    }
                    if (linkIssueList.Count > 0)
                    szOutString = szOutString + "\n";*/
                }
                else
                {
                    szOutString  = "Independent Tickets : "+szIssueIds+"\n";
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szOutString;
        }
    }
}
