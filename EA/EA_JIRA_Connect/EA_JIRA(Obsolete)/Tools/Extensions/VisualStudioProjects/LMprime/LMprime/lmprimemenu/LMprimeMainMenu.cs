﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMainMenu.cs

   Description: This File is returns Main Menu.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-May-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file return main menu.
 * 
 * getMainMenu()-
 *  this function returns Main Menu
 *
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMprimeMainMenu
    {
        string szMainMenu = LMPrimeMenuConstants.menuHeader;

        public string getMainMenu()
        {
            return szMainMenu;
        }
    }
}
