﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EATaggedValue.cs

   Description: This File is contains EA specific for EATagged Value.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Feb-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/*
 * This class is specific to EA Tagged Value.
 * Contains API for getting and Setting EA Tagged Value.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;

namespace LMprime.ea
{
    class EATaggedValue
    {
        Logger log = Logger.getInstance();

        /*Function to get Tagged Value by name.No need for null checking on input data as it has already been checked by EAElementUtils Class*/
        public string getTaggedValueByName(String szTaggedValueName, ref EA.Element element)
        {
            string szReturnTagValue = null;
            try
            {
                foreach (EA.TaggedValue tValue in element.TaggedValues)
                {
                    if (tValue.Name.CompareTo(szTaggedValueName) == 0)
                    {
                        szReturnTagValue = tValue.Value;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szReturnTagValue;
        }

        /*Function to set Tagged Value by name*/
        public void setTaggedValueByName(string szTaggedValueName, string szTaggedValue, ref EA.Element element)
        {
            try
            {
                if (szTaggedValueName == null || szTaggedValueName.Length <= 0 || szTaggedValue == null || szTaggedValue.Length <= 0 || element == null)
                {
                    log.error("EATaggedValue : setTaggedValueByName : Null Input Parameters ");

                    throw new Exception("Input parameter are null in stetting Tagged Values");
                }
                foreach (EA.TaggedValue tValue in element.TaggedValues)
                {
                    if (tValue.Name.CompareTo(szTaggedValueName) == 0)
                    {
                        tValue.Value = szTaggedValue;
                        tValue.Update();
                        element.Update();
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function to create new Tagged Value */
        public void createTaggedValue(ref EA.Element elem, string szTagValueName, string szType)
        {
            if (elem == null || szTagValueName == null || szTagValueName.Length < 0 || szType == null || szType.Length < 0)
            {
                log.error("EATaggedValue : createTaggedValue : Null Input parameters ");

                throw new Exception("Null input parameters in creating tagged value");
            }
            try
            {

                EA.TaggedValue newTagVal = (EA.TaggedValue)elem.TaggedValues.AddNew(szTagValueName, szType);

                newTagVal.Update();

                elem.TaggedValues.Refresh();

                elem.Update();
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function to check whether tag value exists or not   */
        public bool checkTagValueExist(ref EA.Element elem, string szTagValueName)
        {
            if (elem == null || szTagValueName == null || szTagValueName.Length < 0)
            {
                log.error("EATaggedValue : checkTagValueExist : Null Input parameters ");

                throw new Exception("Null input parameters in creating tagged value");
            }

            try
            {
                foreach (EA.TaggedValue tValue in elem.TaggedValues)
                {
                    if (tValue.Name.CompareTo(szTagValueName) == 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return false;
        }

    }
}
