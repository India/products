﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ProcessInputData.cs

   Description: This class is specific to Merged Issue creation data preparation

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.logger;
using LMprime.appdelegate;
using LMprime.jiramanager;
using LMprime.helpers;
using LMprime.issuemanager.linkedIssuePrep;

namespace LMprime.issuemanager.mergeddataprep
{
    class ProcessInputDataM
    {
        Dictionary<String, List<String>> issueDictionary = new Dictionary<String, List<String>>();
        List<Dictionary<String, List<String>>> issueDictionaryList = new List<Dictionary<string, List<string>>>();
        JiraManager jManager = new JiraManager();
        Logger log = Logger.getInstance();
        string szIssueId = "";

        /* No need to check null for Inp data of this functionas already checked for previous class.  */
        public string processInputDataM(string szIssueType, string szProject, EA.Element mainElem, List<EA.Element> merElem, List<EA.Element> linkIssueElemList,List<string>LinkIssueTypeList)
        {
            PrepareMainElem pMainElem = new PrepareMainElem();

            PrepareMergedElement pMergedElem = new PrepareMergedElement();

            LinkIssueMain lIssueMain = new LinkIssueMain();

            List<String> linkIssueList = new List<string>();

            try
            {
                pMainElem.prepareMainElem(mainElem, ref issueDictionary);

                if (merElem != null && merElem.Count > 0)
                {
                    pMergedElem.prepareMergedData(mainElem, merElem, ref issueDictionary);
                }
                else
                {
                    log.info("ProcessInputData : processInputData : No Merged Element list available");
                }
                if (issueDictionary != null)
                {
                    issueDictionaryList.Add(issueDictionary);

                    merElem.Add(mainElem);          //Passing main Element as it also requires reverse mapping updation

                    if (linkIssueElemList != null && linkIssueElemList.Count > 0)
                    {
                        linkIssueList = lIssueMain.getLinkIssueKeyList(LinkIssueTypeList, linkIssueElemList);
                    }

                    szIssueId = jManager.createIssue(szIssueType, szProject, issueDictionaryList, merElem, linkIssueList, true,null);
                        
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szIssueId;
        }
    }
}
