﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeProp.cs

   Description: This File is container for entire EA and Jira Property File.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
27-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note:-Beans are specialized containers that are used to store information.
 *  These Beans are request specific and cannot be used for any other purpose,Use of such thing may result
 *  in Loss of data.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMprime.beans
{

    [XmlRoot(ElementName = "EAProp")]
    public class EAProp
    {
        [XmlElement(ElementName = "Eprop")]
        public List<string> Eprop { get; set; }
    }

    [XmlRoot(ElementName = "JiraProp")]
    public class JiraProp
    {
        [XmlElement(ElementName = "Jprop")]
        public List<string> Jprop { get; set; }
    }

    [XmlRoot(ElementName = "LMprimeProp")]
    public class LMprimeProp
    {
        [XmlElement(ElementName = "EAProp")]
        public EAProp EAProp { get; set; }
        [XmlElement(ElementName = "JiraProp")]
        public JiraProp JiraProp { get; set; }
    }
}
