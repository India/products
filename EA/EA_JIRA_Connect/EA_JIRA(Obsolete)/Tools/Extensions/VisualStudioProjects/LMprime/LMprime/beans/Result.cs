﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: result.cs

   Description: This File is container for result data

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
05-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note:-Beans are specialized containers that are used to store information.
 *  These Beans are request specific and cannot be used for any other purpose,Use of such thing may result
 *  in Loss of data.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;

namespace LMprime.beans
{
    class Result
    {
        int result = Constants.FAILURE;
        string szMessage = null;

        public int getResult()
        {
            return result;
        }
        public string getMessage()
        {
            return szMessage;
        }

        public void setResult(int result)
        {
            this.result = result;
        }

        public void setMessage(string szMessage)
        {
            if (szMessage != null)
            {
                this.szMessage = szMessage;
            }
        }
    }
}
