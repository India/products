﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeJIRAMain.cs

   Description: This File is entry point for Creating JIRA Issue.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-May-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file is entry point for creating JIRA issue
 * 
 * void createJiraIssueMain()-
 *  Entry Function for creating JIRA Issue.It first checks whether user is Logged In or not.
 *  If user is Logged In then it displays Create Issue Dialog.
 *  If user is not Logged In then it displays Login dialog.
 *  Prior to display any dialog it first checks and validates mapping file Syntactically.
 *  
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.exception;
using LMprime.eamapping;
using LMprime.appdelegate;
using LMprime.userinterface.login;
using LMprime.userinterface.createissue;
using LMprime.logger;
using System.Windows.Forms;

namespace LMprime.lmprimemenu
{
    class LMprimeJIRAMain
    {
        DefaultExceptionHandler defExcepHandler = new DefaultExceptionHandler();
        LoadMapping lMapping = new LoadMapping();
        Logger log = Logger.getInstance();

        public void createJiraIssueMain()
        {
            if (!AppDelegate.isUserLoggedIn)
            {
                try
                {
                    lMapping.loadAndValidateMapping();

                    Login login = new Login();

                    login.ShowDialog();
                }
                catch (Exception exception)
                {
                    string strErrorString = "";

                    AppDelegate.isUserLoggedIn = false;

                    log.error("LMprimeMain : checkAndProceed : " + exception.Message);

                    strErrorString = defExcepHandler.handleGenralException(exception);

                    MessageBox.Show(strErrorString);
                }

            }
            else
            {
                try
                {
                    lMapping.loadAndValidateMapping();

                    CreateIssue cIssue = new CreateIssue();

                    cIssue.ShowDialog();

                }
                catch (Exception exception)
                {
                    string strErrorString = "";

                    AppDelegate.isUserLoggedIn = false;

                    log.error("LMprimeMain : checkAndProceed : " + exception.Message);

                    strErrorString = defExcepHandler.handleGenralException(exception);

                    MessageBox.Show(strErrorString);
                }
            }
        }
    }
 }



