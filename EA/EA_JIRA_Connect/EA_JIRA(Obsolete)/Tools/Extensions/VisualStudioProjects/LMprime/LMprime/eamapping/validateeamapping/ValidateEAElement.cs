﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAElement.cs

   Description: This File is specific to validation of EA Element in mapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateEAElement
    {
        Logger log = Logger.getInstance();

        public void validateEAElement(List<EAElement> eaElemList, string szEAElemntPosition)
        {
            try
            {
                if (eaElemList == null || eaElemList.Count <= 0)
                {
                    log.error("ValidateEAElement : ValidateEAElement() : EA element list is null or of zero length");
                    throw new Exception(Constants.NULL_EA_ELEMENT_LIST);
                }

                processEAElementList(eaElemList, szEAElemntPosition);

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void processEAElementList(List<EAElement> eaElemList, string szEAElementPosition)
        {
            ValidateEASingleElement vEASingleElem = new ValidateEASingleElement();

            try
            {
                for (int iDX = 0; iDX < eaElemList.Count; iDX++)
                {
                    vEASingleElem.validateEAsingleElem(eaElemList[iDX], szEAElementPosition);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }
    }
}
