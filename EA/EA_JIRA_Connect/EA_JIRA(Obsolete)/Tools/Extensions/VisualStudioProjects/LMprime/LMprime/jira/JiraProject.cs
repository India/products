﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Project.cs

   Description: This File contains API's specific to JIRA project.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.exception;

namespace LMprime.jira
{
    class JiraProject
    {
        DefaultExceptionHandler exHandler = null;

        public JiraProject()
        {
            if (exHandler == null)
            {
                exHandler = new DefaultExceptionHandler();
            }
        }

        /*  This function takes out Project List and keys and forms a new List of it.   */
        public List<ProjectKeyAndName> setProjectNameAndKeys(List<ProjectDescription> projectDescription)
        {
            List<ProjectKeyAndName> tempList = new List<ProjectKeyAndName>();

            try
            {
                if (projectDescription != null && projectDescription.Count > 0)
                {
                    tempList = processInputList(projectDescription);
                    return tempList;
                }
            }
            catch (Exception exception)
            {
                exHandler.handleException(exception);
                return tempList;
            }
            return tempList;
        }

        /*  This functions process input list of project Keys and names   */
        public List<ProjectKeyAndName> processInputList(List<ProjectDescription> projectDescription)
        {
            List<ProjectKeyAndName> projKeyAndName = null;
            projKeyAndName = new List<ProjectKeyAndName>();

            if (projectDescription != null && projectDescription.Count > 0)
            {
                /*  Running loop on Entire project description list which we have got after parsing.    */
                foreach (ProjectDescription pDesc in projectDescription)
                {
                    if (pDesc == null)
                    {
                        continue;
                    }
                    else
                    {
                        ProjectKeyAndName pKeyAndName = new ProjectKeyAndName();

                        if (pDesc.name == null || pDesc.key == null)
                        {
                            continue;
                        }
                        else
                        {
                            pKeyAndName.name = pDesc.name;

                            pKeyAndName.key = pDesc.key;

                            projKeyAndName.Add(pKeyAndName);
                        }
                    }
                }
            }

            return projKeyAndName;
        }
    }
}
