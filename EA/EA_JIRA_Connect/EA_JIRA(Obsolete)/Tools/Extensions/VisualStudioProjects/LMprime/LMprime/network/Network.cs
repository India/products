﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Network.cs

   Description: This File contains API's specific to Network(requests and responces).

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text.RegularExpressions;
using Atlassian.Jira;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.session;
using LMprime.exception;
using LMprime.helpers;
using LMprime.logger;
using LMprime.appdelegate;
using DotNetWikiBot;
using LMprime.others;


namespace LMprime.network
{
    class Network
    {
        /*      static Jira Variable    */
        public static Atlassian.Jira.Jira sJira = null;
        Session sSession = new Session();
        Logger log = Logger.getInstance();
        DefaultExceptionHandler exHandler = new DefaultExceptionHandler();
        static string sstrUrl = null;
        static string szSDKURL = AppDelegate.eProject.Jira.Url;
        Atlassian.Jira.ProjectComponentCollection pcc;


        /*  Function to create Jira rest client.This client will be used for handling OTB atlassian sdk requests */
        public void createJiraRestClient()
        {
            try
            {
                StringUtility sUtil = new StringUtility();

                szSDKURL = sUtil.correctURLending(szSDKURL);
                
                sJira = Atlassian.Jira.Jira.CreateRestClient(szSDKURL, Session.sstrUsername, Session.sstrPassword);
            }
            catch (Exception exception)
            {
                exHandler.handleNetworkException(exception);
            }

        }

        /*  To get Issue details form JIRA  */

        public Issue getIssueDetailsFromJira(string szIssueId)
        {
            Issue issue = null;
            try
            {
                issue = sJira.GetIssue(szIssueId);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return issue;
        }


        /*  To cretae JIRA Issue    */

        public Issue createJiraIssue(string szIssueType, string szProject, Dictionary<string,List<string>> issueDict, List<String> linkIssueList,string szParentKey)
        {
            String issueId = "";        /*  Output parameter    */
            Issue issue = null;
            StringUtility sUtil = new StringUtility();
            List<string> label = new List<string>();
            try
            {
                if (szIssueType == null || szIssueType.Length <= 0 || szProject == null || szProject.Length <= 0 || issueDict == null)
                {
                    log.error("Network :: createJiraIssue : IssueType, project issueDict is null or of zero length");
                    throw new Exception("Invalid Parameters for Request");
                }

                if (szIssueType.CompareTo("Sub-task") == 0)
                {
                    issue = sJira.CreateIssue(szProject, szParentKey);
                
                }
                else
                {
                    issue = sJira.CreateIssue(szProject);
                }

                foreach (string key in issueDict.Keys)
                {
                    List<string> value = new List<string>();

                    issueDict.TryGetValue(key, out value);

                    Type type = issue.GetType();

                    System.Reflection.PropertyInfo propertyInfo = type.GetProperty(key);

                    if (propertyInfo != null)
                    {
                        PropertyTypeInfo pTypeInfo = new PropertyTypeInfo();

                        int iPtype = pTypeInfo.getPropertyType(issue, key);

                        if (iPtype == Constants.TYPE_STRING)
                        {
                            if (propertyInfo.CanWrite && propertyInfo != null)
                            {
                                /*  To check whether it is possible to set Jira Property or not */
                                value[0] = sUtil.removeHTMLFormating(value[0]);
                                
                                propertyInfo.SetValue(issue, value[0], null);
                            }
                            else
                            {
                                log.error("Network :: createJiraIssue : Write access not available on " + key + "Jira Property");
                                throw new Exception("Write access not available on " + key + "Jira Property");
                            }
                        }
                        else if (iPtype == Constants.TYPE_OBJECT)
                        {
                           SetPropertyObject sPropObj = new SetPropertyObject();
                           for(int iDx = 0; iDx < value.Count;iDx++)
                           {
                               String[] splitedString = value[iDx].Split('§');
                                for (int iFx = 0; iFx < splitedString.Count(); iFx++)
                                {
                                    if (key.CompareTo("Labels") == 0)
                                    {
                                        if (!label.Contains(splitedString[iFx]))
                                        {
                                            if (splitedString[iFx].Contains(" ")) ;
                                            {
                                                splitedString[iFx] = Regex.Replace(splitedString[iFx], @"\s", "");
                                                
                                                splitedString[iFx] = sUtil.removeHTMLFormating(splitedString[iFx]);
                                            }
                                            label.Add(splitedString[iFx]);
                                        }
                                    }
                                    else
                                    {
                                        splitedString[iFx] = sUtil.removeHTMLFormating(splitedString[iFx]);

                                        sPropObj.setPropertyObject(key, splitedString[iFx], ref issue);
                                    }
                                }

                            }
                           
                        }
                    }
                }

                issue.Type = szIssueType;
                
                issue.SaveChanges();

                if (label.Count() > 0)
                {
                    string[] labelArray = label.ToArray();
                    issue.SetLabelsAsync(labelArray);
                    labelArray = null;
                    label.Clear();
                }

                if (linkIssueList != null && linkIssueList.Count > 0)
                {
                    for (int iDx = 0; iDx < linkIssueList.Count; iDx++)
                    {
                        issue.LinkToIssue(linkIssueList[iDx], Constants.RELATES);
                    }
                }
            }
            catch (Exception exception)
            {
                log.error("Network :: createJiraIssue : Exception is " + exception.Message);
                throw exception;
            }
            return issue;
        }

         /** This method is for sending http requests to server. It creates http requests
        * @param strReqType   :   Request type(for project,creeating issues etc)
        * @param strArgument  :   Query Paramenters if any
        * @param strData      :   Json data for post Requests
        * @param strMethod    :   Method for requests GET/POST etc
        */
        public string sendHttpRequest
         (
             string strReqType,
             string strArgument,
             string strData,
             string strMethod
         )
        {
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            string strResponceString = null;
            string strBase64Credentials = null;
            string strUrl = null;
            HttpRequest httpReq = null;

            /*  Creating url for requests which is followed by query parameters   */
            strUrl = createUrlWithQueryParameters(strReqType, strArgument);

            try
            {
                httpReq = new HttpRequest(strUrl);

                /*  Creating Requests   */
                request = httpReq.createHttpRequest(strReqType);

                /*  Setting Content Type    */
                httpReq.setContentType(ref request, Constants.APPLICATIONJSON);

                /*  Setting Method type of Request  */
                httpReq.setMethod(ref request, strMethod);

                log.error("Network : sendHttpRequest : Jira Url is " + strUrl);

                if (strData != null)
                {
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                    {
                        writer.Write(strData);
                    }
                }

                /*  Getting Encoded coded Credentials */
                strBase64Credentials = sSession.getEncodedCredentials();

                /*  Adding Header Node  */
                httpReq.addHeader(ref request, Constants.AUTHORIZATION, Constants.BASIC + " " + strBase64Credentials);


                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls; // comparable to modern browsers


                response = request.GetResponse() as HttpWebResponse;

                strResponceString = generateResponceString(response);

            }
            catch (Exception exception)
            {
                throw exception;
            }

            return strResponceString;
        }

        /*  This Function generates String from responce stream */

        private string generateResponceString(HttpWebResponse response)
        {
            string strResponceString = null;

            try
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responceStream = response.GetResponseStream();
                    StreamReader strReader = new StreamReader(responceStream);
                    strResponceString = strReader.ReadToEnd();
                }
            }
            catch (Exception exception)
            {
                exHandler.handleException(exception);
            }

            return strResponceString;

        }

        /*  Function creates URL with query parameters  */

        private string createUrlWithQueryParameters(string strReqType, string strArgument)
        {
            StringUtility sUtil = new StringUtility();

            sstrUrl = sUtil.correctURLending(AppDelegate.eProject.Jira.Url) + "rest/api/2";
            
            string strUrl = string.Format("{0}/{1}/", sstrUrl, strReqType);

            if (strArgument != null)
            {
                strUrl = string.Format("{0}{1}/", strUrl, strArgument);
            }

            return strUrl;
        }

    }
}





