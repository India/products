﻿
/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Issue.cs

   Description: This File contains API's specific to Jira Issues.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;

namespace LMprime.jira
{
    class JiraIssue
    {

        /*  This function takes issue type list in argument and creates list of issue names */

        public List<String> setIssueTypeList(List<IssueTyp> issueTypelist)
        {
            List<String> tempList = new List<string>();

            if (issueTypelist != null && issueTypelist.Count > 0)
            {
                tempList = processInputList(issueTypelist);
            }
            return tempList;
        }

        /*  This functions seperates Issue name and creates its list    */
        public List<string> processInputList(List<IssueTyp> issueTypelist)
        {
            List<string> issueTypeList = null;
            issueTypeList = new List<string>();

            if (issueTypelist != null || issueTypelist.Count > 0)
            {
                foreach (IssueTyp iType in issueTypelist)
                {
                    if (iType == null)
                    {
                        continue;
                    }

                    if (iType.name == null)
                    {
                        continue;
                    }

                    issueTypeList.Add(iType.name);
                }
            }
            return issueTypeList;
        }
    }
}

