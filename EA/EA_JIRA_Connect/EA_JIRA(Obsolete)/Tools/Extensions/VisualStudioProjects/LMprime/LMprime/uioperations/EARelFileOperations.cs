﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EARelFileOperations.cs

   Description: This File is logical implementation.

========================================================================================================================================================================

Date          Name                  Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-Jan-2017   Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprime.ea;
using LMprime.logger;
using LMprime.beans;
using LMprime.appconstants;

namespace LMprime.uioperations
{
    class EARelFileOperations
    {
        string szElemType = null;
        Logger log = Logger.getInstance();

        /*  Function to get Related Elements List need to modify this function if any new Realtionship will come in EnterPrise Architect    */

        public List<EA.Element> getRelElemList(Boolean isBiDirReq)
        {
            Boolean bTrace = false;
            Boolean bRealize = false;
            int iCount = 0;
            List<EA.Element> relElemList = new List<EA.Element>();
            List<EA.Element> tempELem = new List<EA.Element>();
            RelElemInfo rElemInfo = new RelElemInfo();


            szElemType = AppDelegate.selectedElement.Type;

            iCount = AppDelegate.eProject.EAElement.Count;

            for (int iDx = 0; iDx < iCount; iDx++)
            {
                if (szElemType.CompareTo(AppDelegate.eProject.EAElement[iDx].Type) == 0)
                {
                    int iConCount = AppDelegate.eProject.EAElement[iDx].Connection.Count;

                    for (int iIndex = 0; iIndex < iConCount; iIndex++)
                    {
                        if (AppDelegate.eProject.EAElement[iDx].Connection[iIndex].Type.CompareTo("Trace") == 0)
                        {
                            bTrace = true;
                        }
                        if (AppDelegate.eProject.EAElement[iDx].Connection[iIndex].Type.CompareTo("Realization") == 0)
                        {
                            bRealize = true;
                        }
                    }
                }
            }
            if (bTrace)
            {
                try
                {
                    //relElemList = rElemInfo.getRelatedElementByTrace(AppDelegate.selectedElement, isBiDirReq);
                    relElemList = rElemInfo.getRelatedElement(AppDelegate.selectedElement, isBiDirReq, "Trace");
                    tempELem = relElemList;
                    relElemList.Clear();
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
            if (bRealize)               //Use this for all Stereotypes other then trace
            {
                try
                {
                   // relElemList = rElemInfo.getRelatedElementsByRealize(AppDelegate.selectedElement, isBiDirReq);
                    relElemList = rElemInfo.getRelatedElement(AppDelegate.selectedElement, isBiDirReq,"Realisation");
                    if (tempELem != null && tempELem.Count > 0)
                        tempELem.Concat(relElemList);
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return relElemList;
        }

        /*  Function to get Existing Jira Issue Id  */

        public string getExistingIssueJiraId(string szIssueType, EA.Element element)
        {
            string szJiraId = "";

            if (szIssueType == null || szIssueType.Length <= 0 || element == null)
            {
                throw new Exception("Null Input Parameters in getting existing JIRA Issue Id");

                log.error("EARelFileOperations : getExistingIssueJiraId : Null Input Parameters");
            }

            if (AppDelegate.eProject != null)
            {
                for (int iDx = 0; iDx < AppDelegate.eProject.Jira.JiraElement.Count; iDx++)
                {
                    if (szIssueType.CompareTo(AppDelegate.eProject.Jira.JiraElement[iDx].Type) == 0)
                    {
                        szJiraId = getIssueIdFrmMacthedJiraElem(AppDelegate.eProject.Jira.JiraElement[iDx], element);
                        break;
                    }
                }
            }
            return szJiraId;
        }

        public string getIssueIdFrmMacthedJiraElem(JiraElement jElem, EA.Element element)
        {
            string szJiraId = "";

            if (jElem != null)
            {
                for (int iDx = 0; iDx < jElem.JiraProperty.Count; iDx++)
                {
                    if (jElem.JiraProperty[iDx].Name.CompareTo("Key") == 0 || jElem.JiraProperty[iDx].Name.CompareTo("KEY") == 0)
                    {
                        szJiraId = getIssueIdFrmMatchedJiraProp(jElem.JiraProperty[iDx], element);
                        break;
                    }
                }
            }
            return szJiraId;
        }

        public string getIssueIdFrmMatchedJiraProp(JiraProperty jProp, EA.Element element)
        {
            string szJiraProp = "";

            if (jProp != null)
            {
                if (jProp.EAProperty.ReferenceType.CompareTo(Constants.TAGGED_VALUE) == 0)
                {
                    EATaggedValue eTagValue = new EATaggedValue();

                    szJiraProp = eTagValue.getTaggedValueByName(jProp.EAProperty.Name, ref element);
                }
            }

            return szJiraProp;
        }

        public List<string> getIssueTypListFrmElem(EA.Element elem)
        {
            List<string> issueTyp = new List<string>();
            int count = 0;
            try
            {
                count = AppDelegate.eProject.EAElement.Count;
                for (int iDx = 0; iDx < count; iDx++)
                {
                    if (elem.Type.CompareTo(AppDelegate.eProject.EAElement[iDx].Type) == 0)
                    {
                        int jCount = AppDelegate.eProject.EAElement[iDx].JiraIssueType.Count;

                        for (int iFx = 0; iFx < jCount; iFx++)
                        {
                            String type = AppDelegate.eProject.EAElement[iDx].JiraIssueType[iFx];
                            issueTyp.Add(type);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return issueTyp;
            

        }

    }
}

