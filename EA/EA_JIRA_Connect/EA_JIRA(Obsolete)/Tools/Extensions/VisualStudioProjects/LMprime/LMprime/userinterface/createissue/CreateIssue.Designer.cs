﻿namespace LMprime.userinterface.createissue
{
    partial class CreateIssue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CreateIssue));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clMergeIntoParent = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clCreateNewIssue = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clLinkWithParent = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clFeatureId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clexistJiraId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbIssueTyp = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.cbRelInfo = new System.Windows.Forms.CheckBox();
            this.btCancel = new System.Windows.Forms.Button();
            this.btSubmit = new System.Windows.Forms.Button();
            this.tbDescription = new System.Windows.Forms.TextBox();
            this.lbDescription = new System.Windows.Forms.Label();
            this.tbElementName = new System.Windows.Forms.TextBox();
            this.lbElemName = new System.Windows.Forms.Label();
            this.cbProject = new System.Windows.Forms.ComboBox();
            this.lbProject = new System.Windows.Forms.Label();
            this.cbIssueType = new System.Windows.Forms.ComboBox();
            this.lbIssueType = new System.Windows.Forms.Label();
            this.lbBottomSeperator = new System.Windows.Forms.Label();
            this.lbSeprator = new System.Windows.Forms.Label();
            this.cbBidirectionalMem = new System.Windows.Forms.CheckBox();
            this.MainPannel = new System.Windows.Forms.GroupBox();
            this.tbExistingJiraIssueId = new System.Windows.Forms.TextBox();
            this.lbExistJiraIssId = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.MainPannel.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clMergeIntoParent,
            this.clCreateNewIssue,
            this.clLinkWithParent,
            this.clFeatureId,
            this.clexistJiraId,
            this.cbIssueTyp});
            this.dataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.ButtonFace;
            this.dataGridView1.Location = new System.Drawing.Point(35, 285);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(766, 282);
            this.dataGridView1.TabIndex = 26;
            this.dataGridView1.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView1_CellBeginEdit);
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellValueChanged);
            this.dataGridView1.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView1_CurrentCellDirtyStateChanged);
            // 
            // clMergeIntoParent
            // 
            this.clMergeIntoParent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clMergeIntoParent.HeaderText = "Merge Content into Main Issue";
            this.clMergeIntoParent.Name = "clMergeIntoParent";
            this.clMergeIntoParent.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clMergeIntoParent.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // clCreateNewIssue
            // 
            this.clCreateNewIssue.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clCreateNewIssue.HeaderText = "Create New Issue";
            this.clCreateNewIssue.Name = "clCreateNewIssue";
            // 
            // clLinkWithParent
            // 
            this.clLinkWithParent.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clLinkWithParent.HeaderText = "Link with Main Issue";
            this.clLinkWithParent.Name = "clLinkWithParent";
            // 
            // clFeatureId
            // 
            this.clFeatureId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clFeatureId.HeaderText = "Feature ID";
            this.clFeatureId.Name = "clFeatureId";
            this.clFeatureId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clFeatureId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // clexistJiraId
            // 
            this.clexistJiraId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clexistJiraId.HeaderText = "Existing Jira Issue ID";
            this.clexistJiraId.Name = "clexistJiraId";
            this.clexistJiraId.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clexistJiraId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // cbIssueTyp
            // 
            this.cbIssueTyp.AutoComplete = false;
            this.cbIssueTyp.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox;
            this.cbIssueTyp.HeaderText = "Select Issue Type";
            this.cbIssueTyp.MaxDropDownItems = 10;
            this.cbIssueTyp.Name = "cbIssueTyp";
            // 
            // cbRelInfo
            // 
            this.cbRelInfo.AutoSize = true;
            this.cbRelInfo.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRelInfo.Location = new System.Drawing.Point(35, 231);
            this.cbRelInfo.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbRelInfo.Name = "cbRelInfo";
            this.cbRelInfo.Size = new System.Drawing.Size(182, 28);
            this.cbRelInfo.TabIndex = 25;
            this.cbRelInfo.Text = "Related Elements";
            this.cbRelInfo.UseVisualStyleBackColor = true;
            this.cbRelInfo.CheckedChanged += new System.EventHandler(this.cbRelInfo_CheckedChanged);
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(523, 599);
            this.btCancel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btCancel.Name = "btCancel";
            this.btCancel.Size = new System.Drawing.Size(112, 35);
            this.btCancel.TabIndex = 24;
            this.btCancel.Text = "Cancel";
            this.btCancel.UseVisualStyleBackColor = true;
            this.btCancel.Click += new System.EventHandler(this.btCancel_Click);
            // 
            // btSubmit
            // 
            this.btSubmit.Location = new System.Drawing.Point(689, 599);
            this.btSubmit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btSubmit.Name = "btSubmit";
            this.btSubmit.Size = new System.Drawing.Size(112, 35);
            this.btSubmit.TabIndex = 23;
            this.btSubmit.Text = "Submit";
            this.btSubmit.UseVisualStyleBackColor = true;
            this.btSubmit.Click += new System.EventHandler(this.btSubmit_Click);
            // 
            // tbDescription
            // 
            this.tbDescription.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDescription.Location = new System.Drawing.Point(577, 89);
            this.tbDescription.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbDescription.Name = "tbDescription";
            this.tbDescription.ReadOnly = true;
            this.tbDescription.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbDescription.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.tbDescription.Size = new System.Drawing.Size(188, 27);
            this.tbDescription.TabIndex = 22;
            // 
            // lbDescription
            // 
            this.lbDescription.AutoSize = true;
            this.lbDescription.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDescription.Location = new System.Drawing.Point(440, 96);
            this.lbDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbDescription.Name = "lbDescription";
            this.lbDescription.Size = new System.Drawing.Size(104, 24);
            this.lbDescription.TabIndex = 21;
            this.lbDescription.Text = "Description";
            // 
            // tbElementName
            // 
            this.tbElementName.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbElementName.Location = new System.Drawing.Point(176, 93);
            this.tbElementName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbElementName.Name = "tbElementName";
            this.tbElementName.ReadOnly = true;
            this.tbElementName.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbElementName.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.tbElementName.Size = new System.Drawing.Size(188, 27);
            this.tbElementName.TabIndex = 20;
            // 
            // lbElemName
            // 
            this.lbElemName.AutoSize = true;
            this.lbElemName.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbElemName.Location = new System.Drawing.Point(7, 95);
            this.lbElemName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbElemName.Name = "lbElemName";
            this.lbElemName.Size = new System.Drawing.Size(133, 24);
            this.lbElemName.TabIndex = 19;
            this.lbElemName.Text = "Element Name";
            // 
            // cbProject
            // 
            this.cbProject.FormattingEnabled = true;
            this.cbProject.Location = new System.Drawing.Point(205, 34);
            this.cbProject.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbProject.Name = "cbProject";
            this.cbProject.Size = new System.Drawing.Size(188, 28);
            this.cbProject.TabIndex = 18;
            // 
            // lbProject
            // 
            this.lbProject.AutoSize = true;
            this.lbProject.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbProject.Location = new System.Drawing.Point(31, 34);
            this.lbProject.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbProject.Name = "lbProject";
            this.lbProject.Size = new System.Drawing.Size(166, 24);
            this.lbProject.TabIndex = 17;
            this.lbProject.Text = "Select JIRA Project:";
            // 
            // cbIssueType
            // 
            this.cbIssueType.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIssueType.Location = new System.Drawing.Point(176, 28);
            this.cbIssueType.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cbIssueType.Name = "cbIssueType";
            this.cbIssueType.Size = new System.Drawing.Size(188, 27);
            this.cbIssueType.TabIndex = 16;
            this.cbIssueType.SelectedIndexChanged += new System.EventHandler(this.cbIssueType_SelectedIndexChanged);
            // 
            // lbIssueType
            // 
            this.lbIssueType.AutoSize = true;
            this.lbIssueType.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIssueType.Location = new System.Drawing.Point(7, 31);
            this.lbIssueType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbIssueType.Name = "lbIssueType";
            this.lbIssueType.Size = new System.Drawing.Size(106, 24);
            this.lbIssueType.TabIndex = 15;
            this.lbIssueType.Text = "Issue Type :";
            // 
            // lbBottomSeperator
            // 
            this.lbBottomSeperator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbBottomSeperator.Location = new System.Drawing.Point(35, 572);
            this.lbBottomSeperator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbBottomSeperator.Name = "lbBottomSeperator";
            this.lbBottomSeperator.Size = new System.Drawing.Size(750, 3);
            this.lbBottomSeperator.TabIndex = 28;
            // 
            // lbSeprator
            // 
            this.lbSeprator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbSeprator.Location = new System.Drawing.Point(35, 278);
            this.lbSeprator.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbSeprator.Name = "lbSeprator";
            this.lbSeprator.Size = new System.Drawing.Size(750, 3);
            this.lbSeprator.TabIndex = 27;
            // 
            // cbBidirectionalMem
            // 
            this.cbBidirectionalMem.AutoSize = true;
            this.cbBidirectionalMem.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBidirectionalMem.Location = new System.Drawing.Point(238, 231);
            this.cbBidirectionalMem.Name = "cbBidirectionalMem";
            this.cbBidirectionalMem.Size = new System.Drawing.Size(279, 28);
            this.cbBidirectionalMem.TabIndex = 29;
            this.cbBidirectionalMem.Text = "Show Bi Directional Relations";
            this.cbBidirectionalMem.UseVisualStyleBackColor = true;
            this.cbBidirectionalMem.CheckedChanged += new System.EventHandler(this.cbBidirectionalMem_CheckedChanged);
            // 
            // MainPannel
            // 
            this.MainPannel.BackColor = System.Drawing.SystemColors.Control;
            this.MainPannel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MainPannel.Controls.Add(this.tbExistingJiraIssueId);
            this.MainPannel.Controls.Add(this.lbExistJiraIssId);
            this.MainPannel.Controls.Add(this.lbElemName);
            this.MainPannel.Controls.Add(this.tbElementName);
            this.MainPannel.Controls.Add(this.lbDescription);
            this.MainPannel.Controls.Add(this.tbDescription);
            this.MainPannel.Controls.Add(this.cbIssueType);
            this.MainPannel.Controls.Add(this.lbIssueType);
            this.MainPannel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MainPannel.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainPannel.Location = new System.Drawing.Point(29, 87);
            this.MainPannel.Name = "MainPannel";
            this.MainPannel.Size = new System.Drawing.Size(772, 138);
            this.MainPannel.TabIndex = 30;
            this.MainPannel.TabStop = false;
            this.MainPannel.Text = "Main JIRA Issue Content";
            // 
            // tbExistingJiraIssueId
            // 
            this.tbExistingJiraIssueId.AcceptsReturn = true;
            this.tbExistingJiraIssueId.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbExistingJiraIssueId.Location = new System.Drawing.Point(577, 31);
            this.tbExistingJiraIssueId.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tbExistingJiraIssueId.Name = "tbExistingJiraIssueId";
            this.tbExistingJiraIssueId.ReadOnly = true;
            this.tbExistingJiraIssueId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbExistingJiraIssueId.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.tbExistingJiraIssueId.Size = new System.Drawing.Size(188, 27);
            this.tbExistingJiraIssueId.TabIndex = 24;
            // 
            // lbExistJiraIssId
            // 
            this.lbExistJiraIssId.AutoSize = true;
            this.lbExistJiraIssId.Font = new System.Drawing.Font("Calibri", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbExistJiraIssId.Location = new System.Drawing.Point(440, 31);
            this.lbExistJiraIssId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbExistJiraIssId.Name = "lbExistJiraIssId";
            this.lbExistJiraIssId.Size = new System.Drawing.Size(111, 48);
            this.lbExistJiraIssId.TabIndex = 23;
            this.lbExistJiraIssId.Text = "Existing Jira \r\nIssue Id";
            // 
            // CreateIssue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 668);
            this.Controls.Add(this.cbBidirectionalMem);
            this.Controls.Add(this.lbBottomSeperator);
            this.Controls.Add(this.lbSeprator);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cbRelInfo);
            this.Controls.Add(this.btCancel);
            this.Controls.Add(this.btSubmit);
            this.Controls.Add(this.cbProject);
            this.Controls.Add(this.lbProject);
            this.Controls.Add(this.MainPannel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CreateIssue";
            this.Text = "CreateIssue";
            this.Load += new System.EventHandler(this.CreateIssue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.MainPannel.ResumeLayout(false);
            this.MainPannel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox cbRelInfo;
        private System.Windows.Forms.Button btCancel;
        private System.Windows.Forms.Button btSubmit;
        private System.Windows.Forms.TextBox tbDescription;
        private System.Windows.Forms.Label lbDescription;
        private System.Windows.Forms.TextBox tbElementName;
        private System.Windows.Forms.Label lbElemName;
        private System.Windows.Forms.ComboBox cbProject;
        private System.Windows.Forms.Label lbProject;
        private System.Windows.Forms.ComboBox cbIssueType;
        private System.Windows.Forms.Label lbIssueType;
        private System.Windows.Forms.Label lbBottomSeperator;
        private System.Windows.Forms.Label lbSeprator;
        private System.Windows.Forms.CheckBox cbBidirectionalMem;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clMergeIntoParent;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clCreateNewIssue;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clLinkWithParent;
        private System.Windows.Forms.DataGridViewTextBoxColumn clFeatureId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clexistJiraId;
        private System.Windows.Forms.DataGridViewComboBoxColumn cbIssueTyp;
        private System.Windows.Forms.GroupBox MainPannel;
        private System.Windows.Forms.TextBox tbExistingJiraIssueId;
        private System.Windows.Forms.Label lbExistJiraIssId;
        
    }
}