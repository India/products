﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ExceptionHandler.cs

   Description: This Class handle all types of exceptions.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.logger;


namespace LMprime.exception
{
    class DefaultExceptionHandler : ExceptionHandler
    {
        Logger log = Logger.getInstance();

        /*  Default Constructor   */
        public DefaultExceptionHandler()
        {
        }

        /*  Default Exception handler for all types of exception */
        public void handleException(Exception exception)
        {
            //TODO:: Handle Exception here
        }

        /*Exception handler for network related requests    */
        public void handleNetworkException(Exception exception)
        {
            //TODO:: handle network related exception
        }

        /* Exception handler for parsing related exceptions */
        public void handleParsingException(Exception exception)
        {
            log.error("********************************" + exception.Message + "***************************");

        }

        /*  Exception handler for EA related exceptions */
        public void handleEAExceptions(Exception exception)
        {

        }

        /*Exception handler for handling xml file related exceptions    */
        public void handleXMLException(Exception exception)
        {

        }

        /*Exception handler for handling General Exceptions */
        public string handleGenralException(Exception exception)
        {
            string strExeption = "";

            if (exception.InnerException != null)
            {
                strExeption = "Error Occured :- " + "\n" + exception.InnerException.Message;
            }
            else
            {
                strExeption = "Error Occured :- " + exception.Message;
            }

            return strExeption;
        }
    }
}
