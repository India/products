﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;

namespace LMprime.helpers
{
    class ListUtilities
    {
        Logger log = Logger.getInstance();

        public int returnStringPosInList(string szValue, List<string> inpList)
        {
            int position = 0;
            if (szValue == null || inpList == null || inpList.Count == 0)
            {
                log.error("ListUtilities : returnStringPosInList : Input Parameters are null");
                throw new Exception("Input parameters are null in finding position");
            }

            for (int iDx = 0; iDx < inpList.Count; iDx++)
            {
                if (szValue.CompareTo(inpList[iDx]) == 0)
                {
                    position = iDx;
                    break;
                }
            }
            return position;
        }
    }
}
