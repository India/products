﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMenuConstants.cs

   Description: This File contains constants specific to Menu options of pluggin.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-May-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file contains constants specific to Menu Options
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMPrimeMenuConstants
    {
        /*  Main Menu Name */
        public const string menuHeader = "-&LMprime";
        
        /*  Sub Menu Names */
        public const string menuCreateJiraIssue = "&Create Jira Issue";
        public const string menuAbout = "&About";
    }
}



