﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAProjSingleElement.cs

   Description: This File is an specific to EAProject single element validation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.exception;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateEAProjSingleElement
    {
        Logger log = Logger.getInstance();

        public void validateEAProjectContent(EAProject eaProject)
        {
            ValidateObject vObj = new ValidateObject();
            ValidateJira vJira = new ValidateJira();
            ValidateEAElement vEAElement = new ValidateEAElement();

            try
            {
                vObj.ValidateObj(Constants.NULL_EA_PROJECT, eaProject, Constants.KEY_EAPROJECT);

                validateEAProjectName(eaProject);

                vJira.validateJiraElement(eaProject.Jira);

                vEAElement.validateEAElement(eaProject.EAElement, Constants.Direct_EA_PRJ_CHILD);

            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public void validateEAProjectName(EAProject eaProject)
        {
            try
            {
                if (eaProject.Name == null || eaProject.Name.Length <= 0)
                {
                    log.error("ValidateEAProjSingleElement : validateEAProjectName : Project name is null or of zero length");
                    throw new Exception(Constants.PROJECT_NAME_NULL);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}








