﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.network;
using LMprime.appdelegate;
using LMprime.beans;
using LMprime.userinterface.createissue;

namespace LMprime.userinterface.login
{
    public partial class Login : Form
    {
        /*  Class Level Variables   */

        String userName = null;
        String passWord = null;
        Network network = new Network();
        
        
        
        public Login()
        {
            InitializeComponent();
            CenterToScreen();
            tbPassword.PasswordChar = '*';
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();

        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            getUserDetails();
            validateUser();
            
        }
        /*  Function takes user details from login form */

        private void getUserDetails()
        {
            userName = tbUsername.Text;
            passWord = tbPassword.Text;
        }

        /*  Function to validate User   */

        private void validateUser()
        {
            if ((userName == null || userName.Length <= 0) && (passWord == null || passWord.Length <= 0))
            {
                MessageBox.Show("Please enter Username and Password.");
            }
            else if (userName == null || userName.Length <= 0)
            {
                MessageBox.Show("Username cannot be empty.");
            }
            else if (passWord == null || passWord.Length <= 0)
            {
                MessageBox.Show("Password cannot be empty.");
            }
            else
            {
                sendLoginRequest();
            }
        }
        /*      Function to send Login Request  */

        private void sendLoginRequest()
        {
            try
            {
                AppDelegate.username = tbUsername.Text;

                AppDelegate.password = tbPassword.Text;

                AppDelegate.isUserLoggedIn = true;

                this.Dispose();

                network.createJiraRestClient();

                CreateIssue cIssue = new CreateIssue();

                cIssue.ShowDialog();
            }
            catch (Exception exception)
            {
                this.Dispose();

                AppDelegate.isUserLoggedIn = false;

                MessageBox.Show("ERROR OCCURED - " + exception.Message);
            }
        }

    }
}
