﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: JiraManager.cs

   Description: This File is contains API's specific to JIRA.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
1-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Atlassian.Jira;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using LMprime.beans;
using LMprime.network;
using LMprime.appconstants;
using LMprime.parsers;
using LMprime.exception;
using LMprime.jira;
using LMprime.ea.ReverseMapping;

namespace LMprime.jiramanager
{
    class JiraManager
    {
        /*  Class level Variables   */
        Network network = null;
        ParseJson pJson = null;
        DefaultExceptionHandler exHandler = null;
        JiraIssue jIssue = null;
        JiraProject jProject = null;
        string strReqType = null;
        string strResponceString = null;
        static List<ProjectKeyAndName> sProjectKeyAndName = new List<ProjectKeyAndName>();
        static List<string> sIssueTypeList = new List<string>();
        ReverseMappingMain rMappingMain = new ReverseMappingMain();
        static string szParentIssueKey = null;


        /*   Enum to store Request Types     */
        public enum JiraResource
        {
            project,
            issuetype
        }

        /*  Jira Manager Desault Constructor    */

        public JiraManager()
        {
            /*  Taking instance of network  */
            if (network == null)
            {
                network = new Network();
            }

            /*  Taking instance of parseJson class  */
            if (pJson == null)
            {
                pJson = new ParseJson();
            }

            /*  Taking instance of DefaultExceptionHandler class    */
            if (exHandler == null)
            {
                exHandler = new DefaultExceptionHandler();
            }

            /*  Taking Instance of JiraIssue Class */
            if (jIssue == null)
            {
                jIssue = new JiraIssue();
            }

            /*  Taking Instance of JiraProjectClass */
            if (jProject == null)
            {
                jProject = new JiraProject();
            }

        }

        /*  This function initializes Jira Manager  */
        public void initJiraManager()
        {
            //  sendProjectDetailsReq();

            //  sendIssueTypeReq();
        }

        /*  Function to get project Detail Requests */
        public void sendProjectDetailsReq()
        {

            strReqType = JiraResource.project.ToString();

            try
            {
                strResponceString = network.sendHttpRequest(strReqType, null, null, Constants.GET);

                if (strResponceString != null && strResponceString.Length > 0)
                {
                    List<ProjectDescription> projectDescription = null;

                    try
                    {
                        projectDescription = new List<ProjectDescription>();

                        projectDescription = pJson.JsonDeserialize<List<ProjectDescription>>(strResponceString);

                        if (projectDescription != null && projectDescription.Count > 0)
                        {
                            sProjectKeyAndName.Clear();

                            sProjectKeyAndName = jProject.setProjectNameAndKeys(projectDescription);
                        }
                    }

                    catch (Exception exception)
                    {
                        exHandler.handleException(exception);
                    }
                }

            }
            catch (Exception exception)
            {
                exHandler.handleException(exception);
            }
        }

        /*  Function to send Issue type requests    */
        public void sendIssueTypeReq(string strProjectKey)
        {
            strReqType = JiraResource.issuetype.ToString();

            if (strProjectKey != null)
            {
                strReqType = strReqType + "?key=" + strProjectKey;

                try
                {
                    strResponceString = network.sendHttpRequest(strReqType, null, null, Constants.GET);
                }
                catch (Exception exception)
                {
                    throw exception;
                }

                if (strResponceString != null && strResponceString.Length > 0)
                {
                    List<IssueTyp> issueTypeList = null;

                    try
                    {
                        issueTypeList = new List<IssueTyp>();

                        issueTypeList = pJson.JsonDeserialize<List<IssueTyp>>(strResponceString);

                        sIssueTypeList = jIssue.setIssueTypeList(issueTypeList);
                    }
                    catch (Exception exception)
                    {
                        exHandler.handleException(exception);
                    }

                }
            }
        }

        /*  This function is takes list of issue dictionary which contains jira property name as key and EAproperty value as value.
         *  It also takes List of EA Element which is to be used for reverse updattion. 
         *  It runs a loop on Issue Dictionary and creates issues.
         *  And From network it gets Jira issue Id For reverse updation
         *  bIsMergeOrSep is true for Merged Issue and false for seperated Issue
         *  issueTypList contains the list of issue types that are used for individual issues
         **/

        public string createIssue
        (
            string szIssueType, 
            string szProject, 
            List<Dictionary<string, List<string>>> issueDictionary, 
            List<EA.Element> elemList, 
            List<String> linkIssueList, 
            bool bIsMergeOrSep,
            List<string>issueTypList
       )
        {
            string szIssueId = "";
            try
            {
                network.createJiraRestClient();

                for (int iDx = 0; iDx < issueDictionary.Count; iDx++)
                {
                    Issue issue = null;
                    /*  This will be the case when main issue is getting created    */
                    if (issueTypList == null)
                    {
                        issue = network.createJiraIssue(szIssueType, szProject, issueDictionary[iDx], linkIssueList,null);
                        
                        szParentIssueKey = issue.Key.ToString();
                    }   
                    /*  This is the case when seperated issues are getting created  */
                    else
                    {
                        //Here we will seperate the list of Subtasks
                        if (issueTypList[iDx].CompareTo("Sub-task") == 0)
                        {
                            issue = network.createJiraIssue(issueTypList[iDx], szProject, issueDictionary[iDx], linkIssueList, szParentIssueKey);
                        }
                        else
                        {
                            issue = network.createJiraIssue(issueTypList[iDx], szProject, issueDictionary[iDx], linkIssueList, null);
                        }
                    }
                    if (issue != null)
                    {
                        if (szIssueId.CompareTo("") == 0)
                        {
                            szIssueId = szIssueId + issue.Key.ToString();
                        }
                        else
                        {
                            szIssueId = szIssueId + " " + issue.Key.ToString();
                        }
                    }

                    /* Code for Reverse updation */
                    
                    if (bIsMergeOrSep)
                    {
                        if (issue != null)
                        {
                            updateEAElement(issue, szIssueType, elemList);
                        }
                    }
                    else
                    {
                        List<EA.Element> tempElem = new List<EA.Element>();

                        tempElem.Add(elemList[iDx]);

                        updateEAElement(issue, issueTypList[iDx], tempElem);
                    }

                }
            }

            catch (Exception exception)
            {
                throw exception;
            }
            return szIssueId;
        }

        /*  Function to get issue details form JIRA */

        public Issue getIssueDetailsFromJira(string szIssueId)
        {
            Issue issue = null;
            try
            {
                IssueDetails iDetail = new IssueDetails();

                issue = iDetail.getIssueDetailsFromJira(szIssueId);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return issue;
        }

        /*  Getter Functions    */

        public List<ProjectKeyAndName> getProjectNameAndKeys()
        {
            return sProjectKeyAndName;
        }

        public List<string> getIssueTypeList()
        {
            return sIssueTypeList;
        }

        public void updateEAElement(Issue issue, string szIssueType, List<EA.Element> elemList)
        {
            rMappingMain.updateEAElement(issue, szIssueType, elemList);
        }
    }
}



