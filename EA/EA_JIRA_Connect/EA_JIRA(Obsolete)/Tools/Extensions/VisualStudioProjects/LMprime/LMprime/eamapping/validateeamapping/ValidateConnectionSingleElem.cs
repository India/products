﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.exception;
using LMprime.ea;
using LMprime.jira;
using LMprime.appconstants;
using LMprime.logger;



namespace LMprime.eamapping.validateeamapping
{

    class ValidateConnectionSingleElem
    {
        Logger log = Logger.getInstance();

        public void validateConnectionSingleElem(Connection Conn)
        {
            ValidateEAElement vEAElem = new ValidateEAElement();
            EAElementUtils eUtils = new EAElementUtils();

            try
            {
                if(Conn.Type == null || Conn.Type.Length <= 0)
                {
                    log.error("ValidateConnectionSingleELem : Null Connection Type");
                    throw new Exception(Constants.NULL_EA_ELEMET_TYPE + "In Connection Tag");
                }
                
                vEAElem.validateEAElement(Conn.EAElement, Constants.CHILD_OF_CONNECTION);
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}

