﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateJiraProject.cs

   Description: This File is specific to Jira Project Validation validation.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release


========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.exception;
using LMprime.beans;
using LMprime.exception;
using LMprime.appconstants;
using LMprime.logger;


namespace LMprime.eamapping.validateeamapping
{
    class ValidateJiraProject
    {
        Logger log = Logger.getInstance();

        public void validateJiraProject(List<string> prjList)
        {
            try
            {
                if (prjList == null)
                {
                    log.error("ValidateJiraProject : validateJiraProject () : Project list is null");
                    throw new Exception(Constants.NULL_JIRA_URL);
                }
                validateJiraProjectList(prjList);
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public void validateJiraProjectList(List<string> prjList)
        {
            try
            {
                for (int iDx = 0; iDx < prjList.Count; iDx++)
                {
                    if (prjList[iDx] == null || prjList[iDx].Length <= 0)
                    {
                        log.error("ValidateJiraProject : validateJiraProjectList () : Null or zeroLength length Project element");
                        throw new Exception(Constants.NULL_PROJECT_LIST_IN_ELEM);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
