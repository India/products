﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAMappingInterface.cs

   Description: This File is container for entire mapping file

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
23-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note : - This is a generic Interface that every class in EAMapping bean Implements.Specifcally it can be typecasted to
 * to individual class objects as required.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.beans
{
    public interface EAMappingInterface
    {
    }
}
