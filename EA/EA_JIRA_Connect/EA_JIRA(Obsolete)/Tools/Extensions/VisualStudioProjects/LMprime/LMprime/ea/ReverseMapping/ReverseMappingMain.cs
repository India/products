﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReverseMappingMain.cs

   Description: This File is an entry point for reverse mapping.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
26-Jan-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appdelegate;
using LMprime.helpers;
using LMprime.beans;
using Atlassian.Jira;

namespace LMprime.ea.ReverseMapping
{
    class ReverseMappingMain
    {
        Logger log = Logger.getInstance();

        public void updateEAElement(Issue issue, string szIssueType, List<EA.Element> elemList)
        {
            try
            {
                if (issue == null || szIssueType == null || szIssueType.Length <= 0 || elemList == null || elemList.Count == 0)
                {
                    log.error("ReverseMappingMain : updateEAElement : Reverse Mapping data is Null");
                    throw new Exception("ReverseMappingMain : updateEAElement : Reverse Mapping data is Null");
                }

                setPropertyValue(issue, szIssueType, elemList);
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public void setPropertyValue(Issue issue, string szIssueType, List<EA.Element> elemList)
        {
            int iElemCount = elemList.Count;
            int iCount = AppDelegate.eProject.Jira.JiraElement.Count;
            bool isMatchedType = false;

            try
            {

                for (int iIndex = 0; iIndex < elemList.Count; iIndex++)
                {
                    for (int iDx = 0; iDx < iCount; iDx++)
                    {
                        if (AppDelegate.eProject.Jira.JiraElement[iDx].Type.CompareTo(szIssueType) == 0)
                        {
                            isMatchedType = true;
                            EA.Element tempElem = elemList[iIndex];
                            updateElement(issue, ref tempElem, AppDelegate.eProject.Jira.JiraElement[iDx]);
                        }
                    }

                    if (isMatchedType == false)
                    {
                        log.error("ReverseMappingMain : setPropertyValue : No issueTypeMatched");
                        throw new Exception("No issueTypeMatched in reverse mapping updation");
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void updateElement(Issue issue, ref EA.Element elem, JiraElement jElem)
        {
            int count = jElem.JiraProperty.Count;
            try
            {
                if (count == 0)
                {
                    log.error("ReverseMappingMain : updateElement : No Jira Property found");
                    throw new Exception("No Jira Property found in reverse mapping updation");
                }

                for (int iDx = 0; iDx < count; iDx++)
                {
                    String propValue = PropertiesInfo.GetPropertyValue(issue, jElem.JiraProperty[iDx].Name).ToString();

                    if (propValue == null || propValue.Length <= 0)
                    {
                        log.error("ReverseMappingMain : updateElement : No Jira Property Value found");
                        throw new Exception("No Jira Property Value found in reverse mapping updation");
                    }

                    //Assuming Reference type will only be available in Tagged Values
                    if (jElem.JiraProperty[iDx].EAProperty.ReferenceType != null && jElem.JiraProperty[iDx].EAProperty.ReferenceType.Length > 0)
                    {
                        EATaggedValue eTagValue = new EATaggedValue();

                        if (eTagValue.checkTagValueExist(ref elem, jElem.JiraProperty[iDx].EAProperty.Name))
                        {
                            eTagValue.setTaggedValueByName(jElem.JiraProperty[iDx].EAProperty.Name, propValue, ref elem);
                        }

                        else
                        {
                            eTagValue.createTaggedValue(ref elem, jElem.JiraProperty[iDx].EAProperty.Name, jElem.JiraProperty[iDx].EAProperty.Type);

                            eTagValue.setTaggedValueByName(jElem.JiraProperty[iDx].EAProperty.Name, propValue, ref elem);
                        }
                    }
                    else
                    {
                        EAElementUtils eUtils = new EAElementUtils();
                        //TODO:: Set COM OBject properties Dynamically
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
