﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueMain.cs

   Description: This File is an Entry point for Creating Seperated Issues.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
22-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;
using LMprime.logger;

namespace LMprime.issuemanager.separatedataprep
{
    class PrepareSeparateData
    {
        Logger log = Logger.getInstance();
        ProcessInputDataS pInputData = new ProcessInputDataS();


        public string prepareSeparateData(string szIssueType, string szProject, List<EA.Element> sepElementList,List<string> issueTypList)
        {
            String szIssueId = "";

            try
            {
                validateData(szIssueType, szProject, sepElementList, issueTypList);

                szIssueId = pInputData.processInputDataList(szIssueType, szProject, sepElementList,issueTypList);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szIssueId;
        }

        public void validateData(string szIssueType, string szProject, List<EA.Element> sepElemList,List<string> issueTypList)
        {
            try
            {
                if (szIssueType == null || szIssueType.Length < 0 || szProject == null || szProject.Length <= 0 || sepElemList == null || sepElemList.Count <= 0 || issueTypList == null || issueTypList.Count == 0)
                {
                    log.error("PrepareSeperateData :: validateData() : NUll or zero length IssueType/ProjectName/SeperateElementList/MainElement");
                    throw new Exception("PrepareSeperateData :: validateData() : NUll or zero length IssueType/ProjectName/SeperateElementList/MainElement");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }


    }

}
