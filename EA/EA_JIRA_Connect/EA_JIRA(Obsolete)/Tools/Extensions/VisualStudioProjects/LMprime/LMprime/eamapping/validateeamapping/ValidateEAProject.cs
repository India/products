﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAProject.cs

   Description: This File is an specific to EAProject validation element.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release


========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateEAProject
    {
        Logger log = Logger.getInstance();

        public void validateElementEAProject(List<EAProject> eaProject)
        {
            try
            {
                if (eaProject == null || eaProject.Count <= 0)
                {
                    log.error("ValidateEAProject : validateElementEAProject() : No EA Project Element Exists");
                    throw new Exception(Constants.NO_EA_PROJECT_ELEM_TAG_EXIST);
                }

                processEAProjectList(eaProject);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function validating individual project Element in Entire project List  */
        public void processEAProjectList(List<EAProject> eaProject)
        {
            ValidateEAProjSingleElement vEAPrjSingleElem = new ValidateEAProjSingleElement();

            try
            {
                for (int iDx = 0; iDx < eaProject.Count; iDx++)
                {
                    vEAPrjSingleElem.validateEAProjectContent(eaProject[iDx]);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}


