﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: GenrateModifiedExceptionString.cs

   Description: This File is an Entry point for Creating an Issue in Jira.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
8-Dec-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;

namespace LMprime.exception
{
    class GenrateModifiedExceptionString
    {
        public string returnModifiedExceptionString(string strExceptionString)
        {
            string strErrorString = "";
            if (strExceptionString.Contains(Constants.UNAUTHORIZED))
            {
                strErrorString = "The username or password provided are wrong. Please try again.";
            }
            else if (strExceptionString.Contains(Constants.INVALID_ISSUE_TYPE))
            {
                strErrorString = "The selected issue type is defined in mapping file but it is not configured for selected project of current user.";
            }
            else if (strExceptionString.Contains(Constants.COMPONENT_REQUIRED))
            {
                strErrorString = "Components/s are required.Please specifiy Components as they are mandatory field";
            }
            else if (strExceptionString.Contains(Constants.SUBTASK_ERROR))
            {
                strErrorString = "Sub-task cannot be created without main ticket.";
            }
            else
            {
                strErrorString = strExceptionString;
            }

            return strErrorString;
        }
    }
}
