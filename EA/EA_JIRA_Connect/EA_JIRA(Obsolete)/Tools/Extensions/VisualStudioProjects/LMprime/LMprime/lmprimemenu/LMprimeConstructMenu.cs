﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeConstructMenu.cs

   Description: This File is entry point for Menu definations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
2-May-17    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This File is entry point for Menu definations. It defines Main Menu and Sub Menus.
 *                     This class is entry point to two specific classes that defines Main and Sub Menus.
 * getMainMenu()-
 *  Reutrns Main Menu
 * 
 * string[] getSubMenu()-
 *  Return arrays of strings which contains Sub Menus.
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.lmprimemenu
{
    class LMprimeConstructMenu
    {
        LMprimeMainMenu lmMainMenu = new LMprimeMainMenu();
        LMprimeSubMenu lmSubMenu = new LMprimeSubMenu();

        public string getMainMenu()
        {
            string strMainMenu = lmMainMenu.getMainMenu();
            
            return strMainMenu;
        }

        public string[] getSubMenu()
        {
            List<string> tempList = lmSubMenu.getSubMenu();
            
            string[] subMenu = tempList.ToArray();
            
            return subMenu;
        }
      }
}


