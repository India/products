﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseJson.cs

   Description: This File is contains API's for Json Parsing.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
7-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Note :- Every Method of this class must be called within try catch block as no internal 
 *         exception handling has been done.
 *         Class Designed for very specific purpose
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Json;

namespace LMprime.parsers
{
    class ParseJson
    {

        /*   Defaullt Constructor    */
        public ParseJson()
        {
        }

        /* This method is used for deserializing JSON into class object type,
         * It takes json string as argument and and serializing object type
         * On successful seerialization it returns the required Object.
         */

        public inpObjType JsonDeserialize<inpObjType>(string strJsonString)
        {
            DataContractJsonSerializer serialize = new DataContractJsonSerializer(typeof(inpObjType));

            MemoryStream mStream = new MemoryStream(Encoding.UTF8.GetBytes(strJsonString));

            inpObjType outObj = (inpObjType)serialize.ReadObject(mStream);

            return outObj;
        }
    }
}



