﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.ea;
using LMprime.jira;
using LMprime.logger;


namespace LMprime.eamapping.validateeamapping
{
    class ValidateEASingleElement
    {
        Logger log = Logger.getInstance();

        public void validateEAsingleElem(EAElement eaElem, string szEAElemPosition)
        {
            try
            {
                if (eaElem == null)
                {
                    log.error("ValidateEASingleElement : validateEAsingleElem() : Nullable EA EAElement");
                    throw new Exception(Constants.NULL_EA_ELEMENT);
                }

                processEAElement(eaElem, szEAElemPosition);
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void processEAElement(EAElement eaElem, string szEAElemPosition)
        {
            EAElementUtils eutils = new EAElementUtils();
            JiraHelper jHelper = new JiraHelper();
            ValidateEAProperty vEAProperty = new ValidateEAProperty();
            ValidateConnection vConn = new ValidateConnection();

            try
            {
                if (eaElem.Type == null || eaElem.Type.Length <= 0)
                {
                    log.error("ValidateEASingleElement : validateEAsingleElem() : Null EA ELem type is null");
                    throw new Exception(Constants.NULL_EA_ELEMET_TYPE);
                }

                if (String.Compare(szEAElemPosition, Constants.Direct_EA_PRJ_CHILD) == 0)
                {
                    if (eaElem.JiraIssueType == null || eaElem.JiraIssueType.Count == 0)
                    {
                        log.error("ValidateEASingleElement : validateEAsingleElem() : Jira Issue type is null");
                        throw new Exception(Constants.NO_JIRA_ISSUE_EXISTS);
                    }
                }

                vEAProperty.validateEAProperty(eaElem.EAProperty);

                vConn.validateEAConnection(eaElem.Connection);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}



