﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: PrepareMainElem.cs

   Description: This class is for preparing Main Element data

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appconstants;
using LMprime.appdelegate;
using LMprime.helpers;
using LMprime.ea;

namespace LMprime.issuemanager.mergeddataprep
{
    class PrepareMainElem
    {
        Logger log = Logger.getInstance();

        public void prepareMainElem(EA.Element mainElem, ref Dictionary<String, List<String>> issueDictionary)
        {
            try
            {
                string type = mainElem.Type;
                int iCount = 0;

                if (type == null)
                {
                    log.error("PrepareMainElem : prepareMainElem() : Selected Element type is null");

                    throw new Exception("Selected Element type is null");
                }

                iCount = AppDelegate.eProject.EAElement.Count;

                for (int iDx = 0; iDx < iCount; iDx++)
                {
                    
                    if (type.CompareTo(AppDelegate.eProject.EAElement[iDx].Type) == 0)
                    {
                        int iPropCount = AppDelegate.eProject.EAElement[iDx].EAProperty.Count;

                        for (int iIndex = 0; iIndex < iPropCount; iIndex++)
                        {
                            var tempVal = "";
                            List<string> tempString = new List<string>();
                            if (AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].ReferenceType != null && AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].ReferenceType != "" && (AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].ReferenceType.CompareTo("TaggedValue") == 0))
                            {
                                EATaggedValue eTagValue = new EATaggedValue();

                                tempVal = eTagValue.getTaggedValueByName(AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].Name, ref mainElem);
                            }
                            else
                            {
                                tempVal = PropertiesInfo.GetPropertyValue(mainElem, AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].Name).ToString();
                            }

                            if (tempVal != null)
                            {
                                tempString.Add(tempVal);
                                issueDictionary.Add(AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].JiraProperty, tempString);
                            }
                            else
                            {
                                log.info("PrepareMainElem :: prepareMainElem : value for" + AppDelegate.eProject.EAElement[iDx].EAProperty[iIndex].Name + "is null for element" + mainElem.Name);
                            }
                        }
                        break;
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
