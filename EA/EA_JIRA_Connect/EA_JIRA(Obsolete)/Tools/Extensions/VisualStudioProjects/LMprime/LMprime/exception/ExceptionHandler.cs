﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ExceptionHandler.cs

   Description: This File in an Interface for handling Exception in entire Pluggin.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.exception
{
    interface ExceptionHandler
    {
        /* This method is for handling each type of exception Classes implementing this interface can override this*/
        void handleException(Exception exception);

        /*  This method is for handling network related exception   */
        void handleNetworkException(Exception exception);

        /*  This method handles parsing related exception   */
        void handleParsingException(Exception exception);

        /*  This methods handle EA related Exceptions   */
        void handleEAExceptions(Exception exception);

        /*  This method is for handling xml file related exceptions */
        void handleXMLException(Exception exception);

        string handleGenralException(Exception exception);
    }
}
