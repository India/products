﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: HttpRequest.cs

   Description: This File contains API's specific to Http Requests.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
06-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace LMprime.network
{
    class HttpRequest
    {
        String strUrl = null;

        /*  Defaut Consstructor */
        public HttpRequest(String strUrl)
        {
            this.strUrl = strUrl;
        }

        /*  This Methods creates Http requests  */
        public HttpWebRequest createHttpRequest(string strRequestType)
        {
            HttpWebRequest request = null;

            if (strUrl != null && strUrl.Length > 0 && strRequestType != null && strRequestType.Length > 0)
            {
                request = WebRequest.Create(strUrl + "/" + strRequestType + "/") as HttpWebRequest;
            }

            return request;
        }

        /*  This Methods sets Content Type  */
        public void setContentType(ref HttpWebRequest request, string strContentType)
        {
            if (request != null && strContentType != null && strContentType.Length > 0)
            {
                request.ContentType = strContentType;
            }
        }

        /*  This Method sets method for Request */
        public void setMethod(ref HttpWebRequest request, string strMethodType)
        {
            if (request != null && strMethodType != null && strMethodType.Length > 0)
            {
                request.Method = strMethodType;
            }
        }

        /*  This method adds headers    */
        public void addHeader(ref HttpWebRequest request, string strHeaderKey, string strHeaderValue)
        {
            if (request != null && strHeaderKey != null && strHeaderKey.Length > 0 && strHeaderValue != null && strHeaderValue.Length > 0)
            {
                request.Headers.Add(strHeaderKey, strHeaderValue);
            }
        }
    }
}














