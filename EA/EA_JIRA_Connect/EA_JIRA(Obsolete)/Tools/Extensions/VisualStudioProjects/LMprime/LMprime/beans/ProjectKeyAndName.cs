﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ProjectDescription

   Description: This File is a container for getting project description From Jira.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note:-Beans are specialized containers that are used to store information.
 *  In ProjectKeyAndName project name corresponding to keys are stored. 
 *  These Beans are request specific and cannot be used for any other purpose,Use of such thing may result
 *  in Loss of data.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.beans
{
    class ProjectKeyAndName
    {
        public string key { get; set; }
        public string name { get; set; }
    }
}
