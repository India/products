﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateJiraElement.cs

   Description: This File is specific to Jira element Validation in mapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release


========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.exception;
using LMprime.jira;
using LMprime.logger;
using LMprime.helpers;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateJiraElement
    {
        Logger log = Logger.getInstance();

        public void validateJiraElement(List<JiraElement> jElem)
        {
            try
            {
                if (jElem == null)
                {
                    log.info("ValidateJiraElement : validateJiraElement () : Null Jira Element");
                    return;
                }

                processJiraElement(jElem);
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }

        public void processJiraElement(List<JiraElement> jElem)
        {
            ValidateJiraProperty jProperty = new ValidateJiraProperty();

            try
            {
                for (int iDx = 0; iDx < jElem.Count; iDx++)
                {
                    validateJiraType(jElem[iDx]);

                    jProperty.validateJiraProperty(jElem[iDx].JiraProperty);

                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void validateJiraType(JiraElement jElem)
        {
            ValidateObject vObj = new ValidateObject();
            
            try
            {
                vObj.ValidateObj(Constants.NULL_JIRA_ELEM_TYPE, jElem, Constants.KEY_JIRA_ELEMENT);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
