﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: PrepareMergedData.cs

   Description: This File is an Entry point for Creating Data for Merged Issue Type.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
6-Dec-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.beans;

namespace LMprime.issuemanager.mergeddataprep
{
    class PrepareMergedData
    {
        Logger log = Logger.getInstance();
        ProcessInputDataM pInputData = new ProcessInputDataM();

        public string prepareMergedData(string szIssueType, string szProject, EA.Element mainElem, List<EA.Element> merElem, List<EA.Element> linkIssueList,List<string> linkIssueTypList)
        {
            string szIssueId = "";
            try
            {
                validateData(szIssueType, szProject, mainElem);

                /*  Note: merElement i.e the list of elements whose information should be merged can be null.    */
                szIssueId = pInputData.processInputDataM(szIssueType, szProject, mainElem, merElem, linkIssueList, linkIssueTypList);

            }
            catch (Exception exception)
            {
                throw exception;
            }
            return szIssueId;
        }

        public void validateData(string szIssueType, string szProject, EA.Element mainElem)
        {
            try
            {
                if (szIssueType == null || szProject == null || mainElem == null)
                {
                    log.error("PrepareMergedData : validateData() : Null or Data for Creating merged data Issue Type ");

                    throw new Exception("Invalid  Input Data for Creating Issue on Jira");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }
    }
}

