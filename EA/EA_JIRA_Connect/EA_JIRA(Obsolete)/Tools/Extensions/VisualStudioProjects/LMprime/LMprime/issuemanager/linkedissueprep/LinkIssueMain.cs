﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.ea;
using LMprime.appdelegate;

namespace LMprime.issuemanager.linkedIssuePrep
{
    class LinkIssueMain
    {
        public List<String> getLinkIssueKeyList(List<string> LinkIssueTypeList, List<EA.Element> linkIssueElemList)
        {
            List<String> linkIssueKeyList = new List<string>();
            EATaggedValue eaTagValue = new EATaggedValue();
            try
            {
                if (linkIssueElemList != null && linkIssueElemList.Count > 0 && LinkIssueTypeList != null && LinkIssueTypeList.Count > 0)
                {
                    for (int iDx = 0; iDx < linkIssueElemList.Count; iDx++)
                    {
                        EA.Element tempElem = AppDelegate.selectedRepo.GetElementByID(linkIssueElemList.ElementAt(iDx).ElementID);

                        for (int iFx = 0; iFx < AppDelegate.eProject.Jira.JiraElement.Count; iFx++)
                        {
                            if (AppDelegate.eProject.Jira.JiraElement[iFx].Type.CompareTo(LinkIssueTypeList[iDx]) == 0)
                            {
                                for (int iGx = 0; iGx < AppDelegate.eProject.Jira.JiraElement[iFx].JiraProperty.Count; iGx++)
                                {
                                    if (AppDelegate.eProject.Jira.JiraElement[iFx].JiraProperty[iGx].Name.CompareTo("Key") == 0)
                                    {
                                        
                                        string name = AppDelegate.eProject.Jira.JiraElement[iFx].JiraProperty[iGx].EAProperty.Name;
                                        string szTempKey = eaTagValue.getTaggedValueByName(name, ref tempElem);
                                        linkIssueKeyList.Add(szTempKey);
                                    }
                                }
                            }
                        }
                        
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return linkIssueKeyList;
        }
    }
}
