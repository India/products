﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateEAMapping.cs

   Description: This File is specific to validation of EAMapping tag in mapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release

========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.exception;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateEAMapping
    {
        Logger log = Logger.getInstance();

        public void validateEAMapping(EAMapping eaMapping)
        {
            try
            {
                validateEAProject(eaMapping);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  For checking whether project tag exists or not  */
        public void validateEAProject(EAMapping eaMapping)
        {
            ValidateEAProject vEAProject = new ValidateEAProject();

            try
            {
                if (eaMapping.EAProject == null || eaMapping.EAProject.Count <= 0)
                {
                    log.info("ValidateEAMapping : validateEAProject() No EA Project Element <EAPROJECT> Exists in Mapping File");
                    throw new Exception(Constants.NO_EA_PROJECT_ELEM_TAG_EXIST);
                }
                else
                {
                    /*   Passing the entire list of projects for validation. */
                    vEAProject.validateElementEAProject(eaMapping.EAProject);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
