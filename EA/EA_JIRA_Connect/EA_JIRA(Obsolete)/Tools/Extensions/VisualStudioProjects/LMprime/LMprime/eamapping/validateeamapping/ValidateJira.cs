﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateJira.cs

   Description: This File is specific to Jira element validation in mapping file.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
07-Nov-16    Akshay Kumar Singh    Initial Release


========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.beans;
using LMprime.appconstants;
using LMprime.exception;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateJira
    {
        Logger log = Logger.getInstance();

        public void validateJiraElement(Jira jira)
        {
            ValidateObject vObj = new ValidateObject();
            ValidateJiraProject vJiraProject = new ValidateJiraProject();
            ValidateJiraElement vJiraElement = new ValidateJiraElement();

            try
            {
                vObj.ValidateObj(Constants.NULL_JIRA_ELEMENT, jira, Constants.KEY_JIRA);

                validateJiraUrl(jira.Url);

                vJiraProject.validateJiraProject(jira.JiraProject);

                vJiraElement.validateJiraElement(jira.JiraElement);
            }

            catch (Exception exception)
            {
                throw exception;
            }
        }

        public void validateJiraUrl(string url)
        {
            try
            {
                if (url == null || url.Length <= 0)
                {
                    log.error("ValidateJira : validateJiraUrl () : Jira Url is null or of zero length");
                    throw new Exception(Constants.NULL_JIRA_URL);
                }

            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

    }
}
