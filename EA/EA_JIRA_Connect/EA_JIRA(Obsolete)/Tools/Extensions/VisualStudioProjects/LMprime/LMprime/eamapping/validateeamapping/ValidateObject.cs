﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ValidateObject.cs

   Description: Generic Class for validating any type of object.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
14-Jan-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appconstants;
using LMprime.beans;
using LMprime.logger;

namespace LMprime.eamapping.validateeamapping
{
    class ValidateObject
    {
        Logger log = Logger.getInstance();

        /*  This Function takes three arguments 
         *  1)Exception message to be thrown if object is invalid.
         *  2)Object which needs to be validated.
         *  3)Key that is required for object file typecasting.
         * */

        public void ValidateObj(String exceptionMessage, EAMappingInterface eMappIntrf, int key)
        {
            switch (key)
            {
                case Constants.KEY_MAPPING:
                    EAMapping eMap = (EAMapping)eMappIntrf;
                    if (eMap == null)
                    {
                        log.error("ValidateObject : ValidateObj() Null EA mapping object ");
                        throw new Exception(exceptionMessage);
                    }
                    break;

                case Constants.KEY_EAPROJECT:
                    EAProject eaProj = (EAProject)eMappIntrf;
                    if (eaProj == null)
                    {
                        log.error("ValidateObject : ValidateObj() : NULL EA Project Element");
                        throw new Exception(exceptionMessage);
                    }
                    break;

                case Constants.KEY_JIRA:
                    Jira jira = (Jira)eMappIntrf;
                    if (jira == null)
                    {
                        log.error("ValidateObject : ValidateObj () : Null Jira Element");
                        throw new Exception(Constants.NULL_JIRA_ELEMENT);
                    }
                    break;

                case Constants.KEY_JIRA_ELEMENT:
                    JiraElement jElem = (JiraElement)eMappIntrf;
                    if (jElem == null || jElem.Type == null || jElem.Type.Length <= 0)
                    {
                        log.error("ValidateObject : ValidateObj () : Null jira Element Type");
                        throw new Exception(Constants.NULL_JIRA_ELEM_TYPE);
                    }
                    break;
            }
        }


    }
}
