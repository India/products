﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EA;
using LMprime.uioperations;
using LMprime.appdelegate;
using LMprime.uioperations;
using LMprime.issuemanager;
using LMprime.appconstants;
using LMprime.logger;
using LMprime.jiramanager;
using Atlassian.Jira;
using LMprime.helpers;
using LMprime.eamapping;
using LMprime.exception;
using System.Text.RegularExpressions;
using LMprime.others;

namespace LMprime.userinterface.createissue
{
    public partial class CreateIssue : Form
    {
        Logger log = Logger.getInstance();

        CreateIssueImplMain cIssueImpl = new CreateIssueImplMain();
        CreateIssueMain cIssueMain = new CreateIssueMain();
        EA.Element mainElem = null;
        Boolean bIsDataGridViewCellChanging = false;
        Boolean bIsDialogLoadedFirstTime = true;
        string szIssueType = "";
        List<EA.Element> merElemList = new List<EA.Element>();
        List<EA.Element> sepElemList = new List<EA.Element>();
        List<EA.Element> relElemList = new List<EA.Element>();
        List<string> issueTypList = new List<string>();
        List<EA.Element> linkedIssueElemList = new List<Element>();
        ListUtilities lUtil = new ListUtilities();
        
        public CreateIssue()
        {
            InitializeComponent();
            CenterToScreen();
        }

        /* Function that gets called when Form is loaded Entry Point to whole form*/
        private void CreateIssue_Load(object sender, EventArgs e)
        {
            initGUI();
        }

        /*This function is responsible for initializing the whole GUI elements of the form.*/
        public void initGUI()
        {
            try
            {
                /*  For Setting Main Element    */
                setSelectedElem();

                /*  For setting Project Name    */
                setProjectName();

                /*  For Setting Issue Type  */
                setIssueType();

                /*  For setting Main Element Name and Description   */
                setElemNameAndDesc();

                /*  For Setting Related Elements    */
                setRelatedElements();

                /*  For Setting Existing JIRA issue Id  */
                setExistingJiraIssueId();

            }
            catch (Exception exception)
            {
                /*  Ending entire process */
                GenrateModifiedExceptionString genModifiedExceptionString = new GenrateModifiedExceptionString();

                MessageBox.Show("ERROR OCCURED - " + genModifiedExceptionString.returnModifiedExceptionString(exception.Message));
                
                this.Dispose();
            }
        }

        /*  For selecting main element  */
        public void setSelectedElem()
        {
            try
            {
                mainElem = (EA.Element)AppDelegate.selectedElement;
                if (mainElem == null)
                {
                    log.error("CreateIssue : setSelectedElement : No Element Selected");
                    throw new Exception("No Element Selected.Please Select Element");
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function to set project name list   */
        public void setProjectName()
        {
            try
            {
                List<string> projName = cIssueImpl.getJiraProjectList();
                if (projName == null || projName.Count == 0)
                {
                    log.error("CreateIssue : setProjectName : Project name is null or count is zero");
                    throw new Exception("Null or Empty project List");
                }
                cbProject.DataSource = projName;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  Function to set Issue Type List */
        public void setIssueType()
        {
            string projectName = null;

            try
            {
                projectName = cbProject.SelectedItem.ToString();

                List<string> issueTypeList = new List<string>();

                issueTypeList = cIssueImpl.getIssueTypeList(projectName);

                if (issueTypeList == null || issueTypeList.Count == 0)
                {
                    log.error("CreateIssue : setIssueType : No Issue Type available for this project");
                    throw new Exception("No Issue Type available for this project");
                }
                else
                {
                    szIssueType = issueTypeList[0];
                    cbIssueType.DataSource = issueTypeList;
                }
            }
            catch (Exception exception)
            {
                AppDelegate.isUserLoggedIn = false;

                log.error("CreateIssue : setIssueType : Error occured in getting Issue type list");

                throw exception;
            }
        }

        /*  Function to set element name and description in form     */
        public void setElemNameAndDesc()
        {
            string szName = null;
            string szDesc = null;

            try
            {
                szName = AppDelegate.selectedElement.Name;
                szDesc = AppDelegate.selectedElement.Notes;

                if (szName == null)
                {
                    log.error("CreateIssue : setElemNameAndDesc : Main Element Name is null");
                    throw new Exception("Main Element Name is null");
                }

                if (szDesc == null)
                {
                    log.error("CreateIssue : setElemNameAndDesc : Main Element Description is null");
                    throw new Exception("No Main Element Description is Null");
                }

                tbElementName.Text = szName;

                StringUtility sUtil = new StringUtility();

                String strDescText = sUtil.removeHTMLFormating(szDesc);

                tbDescription.Text = strDescText;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        /*  Setting Existing JIRA issue Id  */

        private void setExistingJiraIssueId()
        {
            tbExistingJiraIssueId.Text = cIssueImpl.getExistingIssueJiraId(szIssueType, AppDelegate.selectedElement);
        }

        /*******************************************Related Elements Section***********************************************/

        /*  Function called when related Elements Checkbox is clicked   */

        private void cbRelInfo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbRelInfo.Checked == true)
            {
                showRelElemView();
            }
            else
            {
                hideRelElemView();
            }

        }

        /*  Function to set Related Elements    */
        public void setRelatedElements()
        {
            int count = 0;

            cbRelInfo.Checked = true;

            bIsDialogLoadedFirstTime = false;

            Boolean bIsBiDirReq = false;

            if (relElemList != null)
            {
                relElemList.Clear();
            }

            try
            {
                if (cbBidirectionalMem.Checked)
                {
                    bIsBiDirReq = true;
                }
                else
                {
                    bIsBiDirReq = false;
                }

                relElemList = cIssueImpl.getRelElemList(bIsBiDirReq);

                count = relElemList.Count;

                dataGridView1.RowCount = count;

                if (relElemList != null && count != 0)
                {
                    for (int iDx = 0; iDx < count; iDx++)
                    {
                        DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[iDx];

                        row.Cells[0].Value = false;
                        row.Cells[0].ReadOnly = false;
                        
                        row.Cells[1].Value = false;
                        row.Cells[1].ReadOnly = false;

                        row.Cells[2].Value = false;
                        row.Cells[2].ReadOnly = false;

                        row.Cells[3].Value = relElemList[iDx].Name;

                        row.Cells[4].Value = cIssueImpl.getExistingIssueJiraId(szIssueType, relElemList[iDx]);

                        row.Cells[3].ReadOnly = true;

                        row.Cells[4].ReadOnly = true;

                        DataGridViewComboBoxCell dChkCellSep = (DataGridViewComboBoxCell)dataGridView1.Rows[iDx].Cells[5];

                        List<string> issueTypList = new List<string>();

                        issueTypList = cIssueImpl.getIssueTypeListForRelElem(relElemList[iDx]);

                        dChkCellSep.DataSource = issueTypList;

                        dChkCellSep.Value = "";
                        
                        dChkCellSep.FlatStyle = FlatStyle.Flat;
                    }
                }
                dataGridView1.AllowUserToAddRows = false;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        private void btSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                String szProjName = cbProject.SelectedValue.ToString();

                String szIssueType = cbIssueType.SelectedValue.ToString();

                String szIssueId = "";

                processIssueList();

                szIssueId = cIssueMain.createJiraIssue(szIssueType, szProjName, mainElem, merElemList, sepElemList, linkedIssueElemList, issueTypList);

                if (szIssueId.CompareTo("") != 0)
                {
                    MessageBox.Show("Successfully Created Jira Issues\n" + szIssueId, "Success");
                }
                setRelatedElements();
                
                /*  Updating intance of selected element to app delegate as its has now been updated with reverse mapping information */
                LoadMapping lMapping = new LoadMapping();
                
                lMapping.setSelectedElementToAppdeleate();
                
                setExistingJiraIssueId();
                
            }
            catch (Exception exception)
            {
                this.Dispose();
                AppDelegate.isUserLoggedIn = false;

                log.error("CreateIssue : btSubmit_Click : " + exception.Message);

                if (exception.InnerException != null)
                {
                    log.error("CreateIssue : btSubmit_Click : " + exception.InnerException.Message);
                    GenrateModifiedExceptionString genModifiedExceptionString = new GenrateModifiedExceptionString();
                    MessageBox.Show("Error Occured - " + genModifiedExceptionString.returnModifiedExceptionString(exception.InnerException.Message));
                }
                else
                {
                    GenrateModifiedExceptionString genModifiedExceptionString = new GenrateModifiedExceptionString();
                    MessageBox.Show("Error Occured - " + genModifiedExceptionString.returnModifiedExceptionString(exception.Message));
                }
            }

        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        public void processIssueList()
        {
            int iCount = dataGridView1.Rows.Count;

            if (merElemList != null && merElemList.Count > 0)
            {
                merElemList.Clear();
            }

            if (sepElemList != null && sepElemList.Count > 0)
            {
                sepElemList.Clear();
            }

            if (linkedIssueElemList != null && linkedIssueElemList.Count > 0)
            {
                linkedIssueElemList.Clear();
            }

            if (issueTypList != null && issueTypList.Count > 0)
            {
                issueTypList.Clear();
            }
            
            for (int iDx = 0; iDx < iCount; iDx++)
            {
                DataGridViewCheckBoxCell dChkCellMer = (DataGridViewCheckBoxCell)dataGridView1.Rows[iDx].Cells[0];
                DataGridViewCheckBoxCell dChkCellSep = (DataGridViewCheckBoxCell)dataGridView1.Rows[iDx].Cells[1];
                DataGridViewCheckBoxCell dChkLinkIssue = (DataGridViewCheckBoxCell)dataGridView1.Rows[iDx].Cells[2];
                DataGridViewComboBoxCell dCombCell = (DataGridViewComboBoxCell)dataGridView1.Rows[iDx].Cells[5];
                
                if (Convert.ToBoolean(dChkCellMer.Value))
                {
                    merElemList.Add(relElemList[iDx]);
                    continue;
                }

                if (Convert.ToBoolean(dChkCellSep.Value))
                {
                    sepElemList.Add(relElemList[iDx]);
                    issueTypList.Add(dCombCell.Value.ToString());
                }

                if (Convert.ToBoolean(dChkLinkIssue.Value))
                {
                    linkedIssueElemList.Add(relElemList[iDx]);
                }
            }
        }

        /*  Functions for showing Related Elements View   */
        private void hideRelElemView()
        {
            int iDGVX = dataGridView1.Size.Width - ((2 * btSubmit.Size.Width) + (btSubmit.Size.Width / 2));

            int iDCVY = dataGridView1.Location.Y;

            lbSeprator.Visible = false;

            dataGridView1.Visible = false;

            lbBottomSeperator.Visible = false;

            cbBidirectionalMem.Enabled = false;

            this.Height = this.Height - (lbSeprator.Height + dataGridView1.Height + lbBottomSeperator.Height);

            btCancel.Location = new Point(iDGVX, iDCVY);

            btSubmit.Location = new Point((iDGVX + ((btSubmit.Size.Width) + btSubmit.Size.Width / 2)), iDCVY);
        }

        /*  Function for hiding Related Elements View   */
        private void showRelElemView()
        {
            if (!bIsDialogLoadedFirstTime)
            {
                int iDGVX = dataGridView1.Size.Width - ((2 * btSubmit.Size.Width) + (btSubmit.Size.Width / 2));

                lbSeprator.Visible = true;

                dataGridView1.Visible = true;

                lbBottomSeperator.Visible = true;

                cbBidirectionalMem.Enabled = true;

                this.Height = this.Height + (lbSeprator.Height + dataGridView1.Height + lbBottomSeperator.Height);

                btCancel.Location = new Point(iDGVX, (lbSeprator.Height + dataGridView1.Height + lbBottomSeperator.Height) + btCancel.Location.Y + 10);

                btSubmit.Location = new Point((iDGVX + ((btCancel.Size.Width) + btCancel.Size.Width / 2)), (lbSeprator.Height + dataGridView1.Height + lbBottomSeperator.Height) + btSubmit.Location.Y + 10);
            }
        }

        private void dataGridView1_CellBeginEdit(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell.GetType() == typeof(DataGridViewComboBoxCell))
            {
                if ((cIssueImpl.getIssueTypeListForRelElem(relElemList[dataGridView1.CurrentCell.RowIndex])).Count > 0)
                {
                    bIsDataGridViewCellChanging = true;
                    ((DataGridViewComboBoxCell)dataGridView1.CurrentCell).Value = (cIssueImpl.getIssueTypeListForRelElem(relElemList[dataGridView1.CurrentCell.RowIndex]))[0]; ;
                    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentCell.GetType() == typeof(DataGridViewCheckBoxCell) ||
                dataGridView1.CurrentCell.GetType() == typeof(DataGridViewComboBoxCell) )
            {
                if (dataGridView1.CurrentCell.IsInEditMode)
                {
                    if (dataGridView1.IsCurrentCellDirty)
                    {
                        bIsDataGridViewCellChanging = true;
                        dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);

                    }
                }
            }
        }

        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1.CurrentCell.GetType() == typeof(DataGridViewCheckBoxCell) ||
                dataGridView1.CurrentCell.GetType() == typeof(DataGridViewComboBoxCell))
            {
                if (dataGridView1.CurrentCell.IsInEditMode)
                {
                    if (dataGridView1.IsCurrentCellDirty)
                    {
                        bIsDataGridViewCellChanging = true;
                        dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
                    }
                }
            }
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (bIsDataGridViewCellChanging)
            {
                bIsDataGridViewCellChanging = false;
                DataGridViewComboBoxCell dCmbCell = (DataGridViewComboBoxCell)dataGridView1.Rows[e.RowIndex].Cells[5];

                if (e.ColumnIndex == 0)
                {
                    DataGridViewCheckBoxCell dChkCell = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells[0];
                    if (Convert.ToBoolean(dChkCell.Value))
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[1].Value = false;
                        dataGridView1.Rows[e.RowIndex].Cells[1].ReadOnly = false;
                        dataGridView1.Rows[e.RowIndex].Cells[2].Value = false;
                        dataGridView1.Rows[e.RowIndex].Cells[2].ReadOnly = false;
                        dCmbCell.Value = "";
                        dCmbCell.ReadOnly = true;

                    }
                    else
                    {
                        dCmbCell.ReadOnly = false;
                    }
                }

                if (e.ColumnIndex == 1)
                {
                    DataGridViewCheckBoxCell dChkCell = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells[1];
                    if (Convert.ToBoolean(dChkCell.Value))
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[0].Value = false;
                        dataGridView1.Rows[e.RowIndex].Cells[5].ReadOnly = false;
                        if (dCmbCell.Value.ToString().CompareTo("") == 0)
                        {
                            if (dCmbCell.Items.Contains(szIssueType))
                            {
                                dCmbCell.Value = szIssueType;
                            }
                            else
                            {
                                dCmbCell.Value = dCmbCell.Items[0];
                            }
                            if (dCmbCell.Value.ToString().CompareTo("Sub-task") == 0)
                            {
                                dataGridView1.Rows[e.RowIndex].Cells[1].ReadOnly = true;
                                dataGridView1.Rows[e.RowIndex].Cells[1].Value = true;
                                dataGridView1.Rows[e.RowIndex].Cells[2].ReadOnly = true;
                                dataGridView1.Rows[e.RowIndex].Cells[2].Value = false;
                            }
                        }
                    }
                    else
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[2].Value = false;
                        dCmbCell.Value = "";
                    }

                }
                if (e.ColumnIndex == 2)
                {
                    DataGridViewCheckBoxCell dChkCell = (DataGridViewCheckBoxCell)dataGridView1.Rows[e.RowIndex].Cells[2];
                    if (Convert.ToBoolean(dChkCell.Value))
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[0].Value = false;
                        dataGridView1.Rows[e.RowIndex].Cells[1].Value = true;
                        dataGridView1.Rows[e.RowIndex].Cells[5].ReadOnly = false;
                        if (dCmbCell.Value.ToString().CompareTo("") == 0)
                        {
                            if (dCmbCell.Items.Contains(szIssueType))
                            {
                                dCmbCell.Value = szIssueType;
                            }
                            else
                            {
                                dCmbCell.Value = dCmbCell.Items[0];
                            }
                            if (dCmbCell.Value.ToString().CompareTo("Sub-task") == 0)
                            {
                                dataGridView1.Rows[e.RowIndex].Cells[1].ReadOnly = true;
                                dataGridView1.Rows[e.RowIndex].Cells[1].Value = true;
                                dataGridView1.Rows[e.RowIndex].Cells[2].ReadOnly = true;
                                dataGridView1.Rows[e.RowIndex].Cells[2].Value = false;
                            }
                        }
                    }
                }
                if (e.ColumnIndex == 5)
                {
                    if (dCmbCell.Value.ToString().CompareTo("Sub-task") == 0)
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[1].ReadOnly = true;
                        dataGridView1.Rows[e.RowIndex].Cells[1].Value = true;
                        dataGridView1.Rows[e.RowIndex].Cells[2].ReadOnly = true;
                        dataGridView1.Rows[e.RowIndex].Cells[2].Value = false;
                    }
                    else
                    {
                        dataGridView1.Rows[e.RowIndex].Cells[1].ReadOnly = false;
                        dataGridView1.Rows[e.RowIndex].Cells[2].ReadOnly = false;
                        dataGridView1.Rows[e.RowIndex].Cells[1].Value = true;
                    }
                }

            }

        }
        //Callback function that gets invoked once user clicks on bi directional Rel
        private void cbBidirectionalMem_CheckedChanged(object sender, EventArgs e)
        {
            setRelatedElements();

        }

        private void cbIssueType_SelectedIndexChanged(object sender, EventArgs e)
        {
            szIssueType = cbIssueType.SelectedValue.ToString();
        }

      
        
    }
}
