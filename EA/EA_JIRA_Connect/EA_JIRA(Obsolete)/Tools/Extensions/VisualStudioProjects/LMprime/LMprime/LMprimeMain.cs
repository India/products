﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LMprimeMain.cs

   Description: This File is an entry point to an application.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
17-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This class is entry point of application.It is main class of Application.
 *                      Name of this class cannot be changed as it is defined in registry settings.
 *  
 * EA_Connect(EA.Repository Repository)- 
 *  This function establishes connection with repository.This function needs to remain same
 *  and it should not be changed.This function cannot be removed as addin will not work without 
 *  this function.
 *
 * EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)- 
 *  This function defines Main Menu and Context menus.Individual cases are defined  for each menu
 *  within this function.
 *                
 * IsProjectOpen(EA.Repository Repository)-
 *  Delegate function called by plugin itself.This function should not be changed externally.
 * 
 * EA_MenuClick(EA.Repository Repository, string Location, string MenuName, string ItemName)-
 *  This function is event handler function which gets called when user click on any menu
 *  @param ItemName is the name of menu option.
 *  for eg . if the menu is "About" them itemname will contain "About"
 *  This is entry point to whole plugin.
 * 
 * checkAndProceed(EA.Repository Repository)-
 *  This function is called when user clicks on "Create JIRA Issue" menu.
 *  This function is entry point to "Create JIRA Issue".
 *  
 * showAboutDialog()
 *  This function is called when user clicks on show about dialog menu option.
 *  
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LMprime.appdelegate;
using LMprime.userinterface.login;
using LMprime.beans;
using LMprime.eamapping;
using LMprime.appconstants;
using LMprime.logger;
using LMprime.userinterface.createissue;
using LMprime.userinterface.about;
using LMprime.lmprimemenu;
using LMprime.exception;

namespace LMprime
{
    public class LMprimeMain
    {
        Logger log = Logger.getInstance();

        public LMprimeMain()
        {
            /*  Initializing Logger */
            log.init(Constants.VERBOSE);
        }

        public String EA_Connect(EA.Repository Repository)
        {
            /* No special processing required. */
            return "a string";
        }

        public object EA_GetMenuItems(EA.Repository Repository, string Location, string MenuName)
        {
            LMprimeConstructMenu lmConstructMenu = new LMprimeConstructMenu();
            string strMainMenu = lmConstructMenu.getMainMenu();

            switch (MenuName)
            {
              /*  For top level Menu  */
              case "":
                    
                return strMainMenu;
               
              /* defines the submenu options  */
              case LMPrimeMenuConstants.menuHeader:
                    
                string[] subMenus = lmConstructMenu.getSubMenu();
                    
                return subMenus;
            }

            return "";
        }

        /*  This function returns true if project is opened and false if it is not opened   */
        bool IsProjectOpen(EA.Repository Repository)
        {
            try
            {
                EA.Collection c = Repository.Models;
        
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void EA_MenuClick(EA.Repository Repository, string Location, string MenuName, string ItemName)
        {
            LMprimeMenuEvent lmpEvent  =  new LMprimeMenuEvent();
            
            lmpEvent.peformMenuClickEvent(ItemName, Repository);
        }

        public void checkAndProceed(EA.Repository Repository)
        {
            LMprimeJIRAMain lmpJiraMain = new LMprimeJIRAMain();
           
            if (Repository != null)
            {
                AppDelegate.selectedRepo = Repository;
            }
            
            else
            {
                log.error("LMprimeMain : checkAndProceed : Selected Repository is null");

                MessageBox.Show("Selected repository is Null");

                return;
            }
            
            lmpJiraMain.createJiraIssueMain();
        }

        public void showAboutDialog()
        {
            DefaultExceptionHandler defExHandler = new DefaultExceptionHandler();
            
            try
            {
                About about = new About();
                
                about.StartPosition = FormStartPosition.CenterScreen;
                
                about.ShowDialog();
            }
            
            catch (Exception exception)
            {
                string strErrorString = "";

                AppDelegate.isUserLoggedIn = false;

                log.error("LMprimeMain : showAbouDialog : " + exception.Message);

                strErrorString = defExHandler.handleGenralException(exception);

                MessageBox.Show(strErrorString);
            }
        }
    }
}



