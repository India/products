﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.issuemanager.subtaskdataprep
{
    class PrepareSubTaskList
    {
        public void prepareSubTaskList(ref List<EA.Element> sepIssueRelElem, ref List<String> issueTypList, ref List<EA.Element> subTaskElemList,ref List<string> subTaskIssueTypList)
        {
           /* No need for Null check of Input data as it has been checked by Input Data */

            for (int iDx = 0; iDx < sepIssueRelElem.Count; iDx++)
            {
                if (issueTypList[iDx].CompareTo("Sub-task") == 0)
                {
                    subTaskElemList.Add(sepIssueRelElem[iDx]);

                    subTaskIssueTypList.Add(issueTypList[iDx]);

                    sepIssueRelElem.RemoveAt(iDx);

                    issueTypList.RemoveAt(iDx);

                    iDx--;

                }
            }


        }
    }
}
