﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAMapping.cs

   Description: This File is container for entire mapping file

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
04-Nov-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/*  Note:-Beans are specialized containers that are used to store information.
 *  These Beans are request specific and cannot be used for any other purpose,Use of such thing may result
 *  in Loss of data.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace LMprime.beans
{

    [XmlRoot(ElementName = "JiraProperty")]
    public class JiraProperty : EAMappingInterface
    {
        [XmlElement(ElementName = "EAProperty")]
        public EAProperty EAProperty { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "JiraElement")]
    public class JiraElement : EAMappingInterface
    {
        [XmlElement(ElementName = "JiraProperty")]
        public List<JiraProperty> JiraProperty { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Jira")]
    public class Jira : EAMappingInterface
    {
        [XmlElement(ElementName = "JiraProject")]
        public List<string> JiraProject { get; set; }
        [XmlElement(ElementName = "JiraElement")]
        public List<JiraElement> JiraElement { get; set; }
        [XmlAttribute(AttributeName = "url")]
        public string Url { get; set; }
    }

    [XmlRoot(ElementName = "EAProperty")]
    public class EAProperty : EAMappingInterface
    {
        [XmlElement(ElementName = "JiraProperty")]
        public string JiraProperty { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlAttribute(AttributeName = "referenceType")]
        public string ReferenceType { get; set; }
        [XmlElement(ElementName = "Separator")]
        public string Separator { get; set; }
    }

    [XmlRoot(ElementName = "EAElement")]
    public class EAElement : EAMappingInterface
    {
        [XmlElement(ElementName = "EAProperty")]
        public List<EAProperty> EAProperty { get; set; }
        [XmlElement(ElementName = "Connection")]
        public List<Connection> Connection { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "JiraIssueType")]
        public List<string> JiraIssueType { get; set; }
    }

    [XmlRoot(ElementName = "Connection")]
    public class Connection : EAMappingInterface
    {
        [XmlElement(ElementName = "EAElement")]
        public List<EAElement> EAElement { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "EAProject")]
    public class EAProject : EAMappingInterface
    {
        [XmlElement(ElementName = "Jira")]
        public Jira Jira { get; set; }
        [XmlElement(ElementName = "EAElement")]
        public List<EAElement> EAElement { get; set; }
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }

    [XmlRoot(ElementName = "EAMapping")]
    public class EAMapping : EAMappingInterface
    {
        [XmlElement(ElementName = "EAProject")]
        public List<EAProject> EAProject { get; set; }
    }

}
