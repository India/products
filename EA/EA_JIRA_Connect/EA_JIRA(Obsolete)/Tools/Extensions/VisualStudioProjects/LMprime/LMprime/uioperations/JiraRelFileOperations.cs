﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: JiraRelFileOperations.cs

   Description: This File is Jira related logical opertation from file.

========================================================================================================================================================================

Date          Name                  Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
09-Jan-2017   Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;

namespace LMprime.uioperations
{
    class JiraRelFileOperations
    {
        public List<string> getProjectListForCurrentEAPackage()
        {
            List<string> prjList = null;

            try
            {
                prjList = AppDelegate.eProject.Jira.JiraProject;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return prjList;
        }

        /*  Function to get Issue Type list from file based on current project  */
        public List<string> getIssueTypeListFrmFile()
        {
            List<string> issueTyp = new List<string>();
            int count = 0;
            try
            {
                count = AppDelegate.eProject.Jira.JiraElement.Count;

                for (int iDx = 0; iDx < count; iDx++)
                {
                    string type = AppDelegate.eProject.Jira.JiraElement[iDx].Type;
                    if (type == null)
                    {
                        continue;
                    }
                    issueTyp.Add(type);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return issueTyp;
        }
    }
}
