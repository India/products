﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: CreateIssueImpl.cs

   Description: This File is logical implementation.

========================================================================================================================================================================

Date          Name                  Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Jan-2017   Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
/** This file is an entry point for all logical opertions.
 *  Any data required for GUI is driven by this file
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.appdelegate;
using LMprime.jiramanager;
using LMprime.ea;

namespace LMprime.uioperations
{
    class CreateIssueImplMain
    {
        EARelFileOperations eRelFOp = new EARelFileOperations();
        JiraRelFileOperations jRelFOp = new JiraRelFileOperations();
        List<string> issueTypeListFrmJira = new List<string>();
        JiraManager jManager = new JiraManager();
        EAElementUtils eUtils = new EAElementUtils();

        public List<string> getJiraProjectList()
        {
            List<string> jiraProjList = null;
            try
            {
                jiraProjList = jRelFOp.getProjectListForCurrentEAPackage();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return jiraProjList;
        }

        public List<string> getIssueTypeList(string prjName)
        {
            List<string> issueTypeListFrmFile = new List<string>();
            List<string> mainTempIssueTypList = new List<string>();
            List<string> mainIssueList = new List<string>();

            try
            {
                issueTypeListFrmFile = jRelFOp.getIssueTypeListFrmFile();

                jManager.sendIssueTypeReq(prjName);

                issueTypeListFrmJira = jManager.getIssueTypeList();

                mainTempIssueTypList = getCommonIssueTypList(issueTypeListFrmFile, issueTypeListFrmJira);

                mainIssueList = getCommonIssueTypList(mainTempIssueTypList, eRelFOp.getIssueTypListFrmElem(AppDelegate.selectedElement));
            }
            catch (Exception exception)
            {
                throw exception;
            }
            return mainIssueList;
        }

        public List<string> getCommonIssueTypList(List<string> firstList, List<string> secList)
        {
            List<string> mainList = new List<string>();
            try
            {
                var result = firstList.Intersect(secList);

                foreach (string elem in result)
                {
                    mainList.Add(elem);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return mainList;
        }

        /*  Function for getting Related Element List   */
        public List<EA.Element> getRelElemList(Boolean bIsBiDirReq)
        {
            List<EA.Element> relElemList = null;

            try
            {
                relElemList = new List<EA.Element>();

                relElemList = eRelFOp.getRelElemList(bIsBiDirReq);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return relElemList;
        }

        /*  Function to get Existing JIRA Issue ID  */

        public String getExistingIssueJiraId(string szIssueType, EA.Element element)
        {
            string szJiraId = "";

            EARelFileOperations eRefFop = new EARelFileOperations();

            try
            {
                szJiraId = eRefFop.getExistingIssueJiraId(szIssueType, element);
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return szJiraId;
        }
        public List<string> getIssueTypeListForRelElem(EA.Element relElem)
        {
            List<string> issueTypeListFrmFile = new List<string>();
            List<string> issueTypListFrmEAElemTag = new List<string>();
            List<string> finalMainList = new List<string>();
            try
            {
                issueTypeListFrmFile = jRelFOp.getIssueTypeListFrmFile();
                issueTypListFrmEAElemTag = eRelFOp.getIssueTypListFrmElem(relElem);
                
                List<string> mainList = new List<string>();
                

            
                var result = issueTypeListFrmJira.Intersect(issueTypeListFrmFile);

                foreach (string elem in result)
                {
                    mainList.Add(elem);
                }
                var finalResult = mainList.Intersect(issueTypListFrmEAElemTag);
                foreach (string elem in finalResult)
                {
                    finalMainList.Add(elem);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }

            return finalMainList;
        }

    }
}
