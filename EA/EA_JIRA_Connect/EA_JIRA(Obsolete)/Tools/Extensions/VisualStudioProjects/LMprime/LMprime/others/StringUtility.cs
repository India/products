﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: StringUtility.cs

   Description: This File contains API's specific to String Utilities.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
29-Sept-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace LMprime.others
{
    class StringUtility
    {
        public string removeHTMLFormating(string strInpString)
        {
            string strOutString = "";

            if (strInpString != null)
            {
                strOutString = Regex.Replace(strInpString, "<.*?>", String.Empty);
            }
            return strOutString;
        }

        public string correctURLending(string strUrl)
        {
            string strOutUrl = "";

            if (strUrl != null && strUrl.Count() > 0)
            {
                int iSize = strUrl.Length;

                if (strUrl[iSize -1].CompareTo('/') == 0)
                {
                    strOutUrl = strUrl;
                }
                else
                {
                    strOutUrl = strUrl + "/";
                }
            }
            return strOutUrl;
        }
    }
}
