﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;
using LMprime.appconstants;

namespace LMprime.helpers
{
    class PropertyTypeInfo
    {
        Logger log = Logger.getInstance();

        /*  Function for getting property type  */
        public int getPropertyType(Object obj, String szPropName)
        {
            int iPropertyType = -1;
            if (obj == null || szPropName == null || szPropName.Length <= 0)
            {
                log.error("PropertyTypeInfo : getPropertyType : Null Input Parameters");
                throw new Exception("Null Input parameters in checking property type");
            }

            try
            {
                Type type = obj.GetType();

                System.Reflection.PropertyInfo propertyInfo = type.GetProperty(szPropName);

                if (propertyInfo != null)
                {
                    if (propertyInfo.PropertyType == typeof(string))
                    {
                        iPropertyType = Constants.TYPE_STRING;
                    }
                    else if (propertyInfo.PropertyType.IsClass)
                    {
                        iPropertyType = Constants.TYPE_OBJECT;
                    }
                }
            }
            catch (Exception exception)
            {
                log.error("PropertyTypeInfo : getPropertyType :Exception occured" + exception.Message);
                throw exception;
            }
            return iPropertyType;
        }

    }
}
