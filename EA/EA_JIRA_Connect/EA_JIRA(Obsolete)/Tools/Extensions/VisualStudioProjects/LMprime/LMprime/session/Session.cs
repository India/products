﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Session.cs

   Description: This File contains API's specific to JIRA session Management.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Oct-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

/* 
 * Note:- This class is designed for handling session related informations 
 * 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.exception;
using LMprime.appdelegate;

namespace LMprime.session
{
    class Session
    {
        public DefaultExceptionHandler oExHandler = null;
        public static string sstrUsername = "";
        public static string sstrPassword = "";
        public string strEncodedCreden = null;

        /*  Default Constructor */
        public Session()
        {
            if (oExHandler == null)
            {
                oExHandler = new DefaultExceptionHandler();
            }
        }

        /*  To Return Encoded Credentials   */
        public string getEncodedCredentials()
        {
            try
            {
                sstrUsername = AppDelegate.username;
                sstrPassword = AppDelegate.password;


                string mergedCredentials = string.Format("{0}:{1}", sstrUsername, sstrPassword);

                byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);

                strEncodedCreden = Convert.ToBase64String(byteCredentials);
            }
            catch (Exception exception)
            {
                oExHandler.handleException(exception);
            }

            return strEncodedCreden;
        }
    }
}
