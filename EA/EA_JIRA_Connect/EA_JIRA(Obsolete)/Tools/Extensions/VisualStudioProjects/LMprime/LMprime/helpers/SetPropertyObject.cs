﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMprime.helpers
{
    class SetPropertyObject
    {

        public void setPropertyObject(String szPropName, String szPropValue, ref Atlassian.Jira.Issue obj)
        {
            if (szPropName.CompareTo("Components") == 0)
            {
                obj.Components.Add(szPropValue);
            }
            if (szPropName.CompareTo("Fix Version") == 0)
            {
                obj.FixVersions.Add(szPropValue);
            }
            if (szPropName.CompareTo("Affects Version") == 0)
            {
                obj.AffectsVersions.Add(szPropValue);
            }
        }

    }
}
