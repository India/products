﻿/*======================================================================================================================================================================
                                       Copyright 2016  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: PropertiesInfo.cs

   Description: This File is helper class to get and set class properties dynamically.

========================================================================================================================================================================

Date          Name                Description of Change
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
31-Dec-16    Akshay Kumar Singh    Initial Release
========================================================================================================================================================================*/

using EA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LMprime.logger;

namespace LMprime.helpers
{
    class PropertiesInfo
    {

        /*  This function is used to get  class properties dynamically */
        public static object GetPropertyValue(object instance, string szPropertyName)
        {

            try
            {
                Type type = instance.GetType();
                var propertyInfo = "";

                try
                {
                    propertyInfo = type.InvokeMember(szPropertyName, System.Reflection.BindingFlags.GetProperty, null, instance, null).ToString();
                }
                catch (Exception exception)
                {
                    return "";
                }

                if (propertyInfo == null)
                {
                    return null;
                }
                else
                {
                    return propertyInfo;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /*  This functions sets properties Dynamically specific to JIRA issue property */
        public static void SetJiraIssuePropertyValue(ref Atlassian.Jira.Issue instance, string szPropertyName, object newValue)
        {
            try
            {
                Type type = instance.GetType();

                System.Reflection.PropertyInfo propertyInfo = type.GetProperty(szPropertyName);

                if (propertyInfo != null)
                {
                    if (propertyInfo.CanWrite)
                    {
                        propertyInfo.SetValue(instance, newValue, null);
                    }
                    else
                    {
                        throw new Exception("Write Access not allowed on JIRA property" + szPropertyName);
                    }
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
