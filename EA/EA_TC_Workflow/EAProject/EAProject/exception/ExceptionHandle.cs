﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ExceptionHandle.cs

   Description: This File is specific to exception handling .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which returns a string to be displayed in message box.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.xmlutilities;
using System.Windows.Forms;
using EAProject.utilities;
using EAProject.constants;

namespace EAProject.exception
{
    class ExceptionHandle
    {
        /* This function tokenizes the exception string(argument) containing the error code and checks if the dictionary contains that errorcode as its key. If error code is
         * found in Dictionary than the value corresponding to it is extracted and tokenized, then both the tokenized error code and error string(value of Dictionary) 
         * from dictionary are appended to form the error message which is returned by the function. If the error code is not found in the dictionary
         * than the same exception string(argument) is returned by the function.  
         **/ 
        public string exceptionHandling(string strException)
        {
            try
            {               
                ReadErrorXML    readErrorXML   = new ReadErrorXML();
                StringUtilities utilityString  = new StringUtilities();
                string strValue                = "";                     
                
                /* Tokenizing input string(strException) */
                string[] astrTokenizedCode = utilityString.stringTokenize(strException, Constants.ERROR_TOKEN_CONSTANT);

                int iCodeCount = astrTokenizedCode.Count();

                /* Obtaining Dictionary(strDictionary) by calling readErrorXml function of ReadErrorXML class */               
                Dictionary<string, string> strDictionary = readErrorXML.readErrorXML();

                /* Checking if Dictionary contains input string(tokenized) as key*/  
                if (strDictionary.ContainsKey(astrTokenizedCode[0]) == true)
                {
                    /* Upon successfull validation errorvalue is extracted from Dictionary corresponding to key(errorcode)*/
                    strDictionary.TryGetValue(astrTokenizedCode[0], out strValue);

                    /* Tokenizing extracted error value from Dictionary */
                    string[] astrTokenizedValue = utilityString.stringTokenize(strValue, Constants.ERROR_TOKEN_CONSTANT);

                    int iTokenCount = astrTokenizedValue.Count();
                    
                    StringBuilder lmBuilder = new StringBuilder();

                    /* Checking if error string contains separator */
                    if (iTokenCount > 1)
                    {
                        /* Checking if count of errorCode minus one is less than count of error strings. Error Code count is reduced by one to exclude 
                         * the error code constant LIN-000*/
                        if ((iCodeCount-1) <= iTokenCount)
                        {
                            for( int iCount = 0; iCount < (iCodeCount-1); iCount++)
                            {
                                /* Appending strings obtained from both the arrays(input string and dictionary value) */ 
                                lmBuilder.Append(astrTokenizedValue[iCount]).Append(astrTokenizedCode[iCount+1]);
                            }                        
                            
                            int iCountDiff = (iTokenCount - (iCodeCount-1));
                                                        
                            while(iCountDiff > 0)
                            {         
                                lmBuilder.Append(astrTokenizedValue[(iCodeCount - 1)]);
                                iCountDiff--;
                                iCodeCount++;
                            }
                        } 
                        else 
                        {
                            for (int iCount = 0; iCount < iTokenCount; iCount++)
                            {
                                lmBuilder.Append(astrTokenizedValue[iCount]).Append(astrTokenizedCode[iCount+1]);
                            }                            
                        }                                                  
                        return (lmBuilder.ToString());
                    }
                    else
                    {
                        return (astrTokenizedValue[0]);
                    }
                }
                /* If error code is not found in Dictionary as key */
                else
                {
                    return strException;
                }
            }
            catch 
            {
                throw new Exception(ErrorConstant.CUSTOM_ERROR_CODE_5);
            }
        }
    }
}


































