﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ShowException.cs

   Description: This File is specific Message Box display.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which displays Message Box with custom Error.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAProject.constants;

namespace EAProject.exception
{
    class ShowException
    { 
        /* This function displays Message Box with error message */
        public void showException(string strException)
        {
            try
            {
                ExceptionHandle handleException = new ExceptionHandle();
                
                /* Getting ErrorMessage */
                string strErrorMesg = handleException.exceptionHandling(strException);

                MessageBox.Show(strErrorMesg);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}

















