﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: LoadXML.cs

   Description: This File is specific to loading XML.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
18-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains function that loads selected Xml File and returns its root.
 *
 **/
using System;
using System.Xml;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Collections.Generic;
using EAProject.constants;
using EAProject.exception;

namespace EAProject.xmlutilities
{
    class LoadXML
    {
        /* This function loads the xml file present at the location(strPath) and returns its root node */
        public XmlNode loadPLMXML(string strFilePath)
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();

                xmlDocument.Load(strFilePath);

                /* Selects the first XmlNode that matches the XPath expression.(*) is used as wildcard at any level of the tree */
                XmlNode xmlRoot = xmlDocument.SelectSingleNode(Constants.ASTERIK);

                return xmlRoot;
            }
            catch
            {
                string strErrorCode = string.Concat(ErrorConstant.CUSTOM_ERROR_CODE_1 , ErrorConstant.CUSTOM_ERROR_STRING_1);
                
                throw new Exception(strErrorCode);
            }
        }    
    }
}


































































