﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseXML.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which parse PLMXML and stores the information present on the handlers of every WorkflowTemplate 
 *                     node in a dictionary. 
 **/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.constants;
using EAProject.containers;
using EAProject.utilities;

namespace EAProject.xmlutilities
{
    class ParseXML
    {
        WFTemplateInfo  infoWFTemplate   = new WFTemplateInfo();
        StringUtilities utilString       = new StringUtilities();
        List<int>       ltNodesTraversed = new List<int>();
        TemplateInfo    infoTemplate     = new TemplateInfo();

        /* This function extracts the ids of all WorkflowTemplates of the imported Workflow present in the Attribute(traverseRootRefs). The value of this 
         * attribute is present in the variable(strAttrTraverseRootRefs). All the comma separeated ids present in this variable are traversed one by one
         * in the same sequence except the id of the first WorkflowTemplate which is present as the last value of the variable(strAttrTraverseRootRefs).         
         **/
        public void parsePLMXML(XmlNode xmlRoot, ref Dictionary<string, WFTemplateInfo> dictWFInfo)
        {
            try
            {
                if (xmlRoot is XmlElement && (xmlRoot.HasChildNodes) && (xmlRoot.ChildNodes.Count) != 0)
                {
                    int    iNodesCount       = (xmlRoot.ChildNodes.Count);                   
                    string strFirstChildName = (xmlRoot.FirstChild.Name);

                    if (String.Compare(strFirstChildName, Constants.HEADER) == 0)
                    {
                        XmlAttributeCollection strAttrCollection = (xmlRoot.FirstChild.Attributes);

                        if (String.Compare(strAttrCollection[Constants.TRAVERSE_ROOT_REFS].Value, null) != 0)
                        {
                            /* Getting the value of attribute 'TraverseRootRefs' of WorkflowTemplate.  */
                            string strAttrTraverseRootRefs = (strAttrCollection[Constants.TRAVERSE_ROOT_REFS].Value);

                            List<string> ltAttrTraverseRootRefs = utilString.tokenStringToList(strAttrTraverseRootRefs, Constants.LIST_TOKEN_CONSTANT);

                            ltAttrTraverseRootRefs.RemoveAt(0);

                            int iWFTemplatesCount = (ltAttrTraverseRootRefs.Count);

                            for (int iDx = 0; iDx <= iWFTemplatesCount; iDx++)
                            {
                                int iFx = 0;

                                if (iDx == 0)
                                {
                                    /* Getting last id present in the list since it is the id of first WorkflowTemplate */
                                    iFx = (iWFTemplatesCount - 1);

                                    /* Reducing the traversal count by 1 since last id of list has been traversed */
                                    iWFTemplatesCount = (iWFTemplatesCount - 1);
                                }
                                else if (iDx != 0)
                                {
                                    /* Starting traversal of ids from begining as they appear in list. */
                                    iFx = iDx - 1;
                                }

                                string strTravRootRefsID = ltAttrTraverseRootRefs[iFx];
                                string strTravID         = strTravRootRefsID;

                                /* Checking current node if traversed and recursively traversing it to extract handler info present on it. */
                                checkNodes(xmlRoot, iNodesCount, strTravRootRefsID, ref infoWFTemplate);                               

                                /* Adding information present on Worktemplate to Dictionary. */
                                dictWFInfo.Add(strTravRootRefsID, infoWFTemplate);

                                /*  Creating an instance of WFTemplateInfo class. */
                                infoWFTemplate = new WFTemplateInfo();                               
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function is recursively called to get handler info present in attributes 'RuleRefs' and 'ActionRefs' belonging to same
         * 'WorkflowTemplate' node.
         **/
        public void checkNodes(XmlNode xmlRoot, int iNodesCount, string strTravRootRefsID, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {
                string       strTravID    = strTravRootRefsID;
                List<string> ltRuleRefs   = new List<string>();
                List<string> ltActionRefs = new List<string>();
                       
                for(int iDx = 1; iDx < iNodesCount; iDx++)
                {                                       
                    /* Check if index at which WorkflowTemplate is found has been traversed. Returns false if not traversed. */                   
                    if(ltNodesTraversed.Contains(iDx) == false)                    
                    {
                        XmlAttributeCollection strNodesAttrCollection = (xmlRoot.ChildNodes[iDx].Attributes);

                        /* Comparing WorkflowTemplate id present in TraverseRootRefs attribute with the id of each node traversed with help of index(iDx)  */
                        if(String.Compare(strTravID, strNodesAttrCollection[Constants.ID].Value) == 0)
                        {
                            /* Getting handler ids present on the WorkflowTemplate */
                            infoTemplate.compareNodesNGetInfo(xmlRoot.ChildNodes[iDx], ref ltRuleRefs, ref ltActionRefs, ref infoWFTemplate);

                            /* Checking for rulerefs id */
                            if(ltRuleRefs.Count != 0)
                            {
                                for(int iFx = 0; iFx < ltRuleRefs.Count; iFx++)
                                {
                                    strTravRootRefsID = ltRuleRefs[iFx];

                                    /* Adding index of traversed node to list */
                                    ltNodesTraversed.Add(iDx);

                                    /* Recursive calling */
                                    checkNodes(xmlRoot, iNodesCount, strTravRootRefsID, ref infoWFTemplate);
                                }
                            }

                            /* Checking for actionrefs id */
                            if (ltActionRefs.Count != 0)
                            {
                                for (int iFx = 0; iFx < ltActionRefs.Count; iFx++)
                                {
                                    strTravRootRefsID = ltActionRefs[iFx];

                                    /* Adding index of traversed node to list */
                                    ltNodesTraversed.Add(iDx);

                                    /* Recursive calling */
                                    checkNodes(xmlRoot, iNodesCount, strTravRootRefsID, ref infoWFTemplate);  //Recursion 
                                }
                            }

                            return;
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }        
    }
}









































       
        






















    

