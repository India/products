﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ReadErrorXML.cs

   Description: This File is specific to custom error message.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
16-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which returns dictionary after loading error xml file. 
 *
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Windows.Forms;
using EAProject.constants;

namespace EAProject.xmlutilities
{
    class ReadErrorXML
    {
        Dictionary<string, string> strDictionary;

        /* This function returns a Dictionary.
         * Structue of Dictionary
         * Dictionary<string, string>
         * Dictionary Key will be errorcode. And value will be error string.
         **/
        public Dictionary<string, string> readErrorXML()
        {
            try
            {
                string strFilePath  = "";
                XmlDocument xmlDocument = new XmlDocument();

                /* Uncomment the following when using the DLL for production */
                /*  Assuming that error file will be at same place where DLL is present, Gettting path of Dll 
                 strFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;

                 strFilePath = Path.GetDirectoryName(strFilePath);

                Appending file name to path of Directory. File name cannot be other then "LMError.xml" 
                 strFilePath = strFilePath + ErrorConstant.ERROR_FILE_NAME;

                strFilePath = strFilePath.Replace(@"\", @"/");

                xmlDocument.Load(strFilePath);
                */

                /*  Read xml file from harcoded purpose only for testing purpose */
                xmlDocument.Load(ErrorConstant.ERROR_FILE_PATH);         

                strDictionary = new Dictionary<string, string>();
                
                foreach (XmlElement node in xmlDocument.DocumentElement)
                {                    
                    string strName = node.Attributes[0].Value;

                    /* Filling dictionary with help of values obtained from error xml file */
                    strDictionary.Add(node.Attributes[0].Value, node.InnerText);
                }
                return this.strDictionary;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }        
    }
}













































