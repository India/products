﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: BrowseForm.cs

   Description: This File is an entry point to an application.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAProject.constants;
using EAProject.xmlutilities;
using EAProject.exception;
using EAProject.utilities;
using EAProject.eahelpers;
using EAProject.eawfmanager;
using EAProject.appdelegate;

namespace EAProject.userinterface
{
    public partial class BrowseForm : Form
    {
        OpenFileDialog dialogOpenFile  = new OpenFileDialog();
        ShowException  exceptionCustom = new ShowException();
                                
        public BrowseForm()
        {
            InitializeComponent();
        }

        /*  Function called when Browse Button is clicked   */
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            try
            {                                                     
                dialogOpenFile.Filter = Constants.FILTER;

                EnvironmentUtilities utilityEnv = new EnvironmentUtilities();

                dialogOpenFile.InitialDirectory = utilityEnv.getEnvVariable(Constants.ENV_TEMP);
                                            
                dialogOpenFile.ShowDialog();

                string strPath = (dialogOpenFile.FileName);

                if (strPath !=null && strPath.Length > 0)
                {
                    txtPath.Text = strPath;
                }
                else
                {
                    MessageBox.Show(ErrorConstant.SELECT_FILE_TO_IMPORT);
                }
            }
            catch (Exception exException)
            {
                exceptionCustom.showException(exException.Message);               
            }
        }

        /*  Function called when Import button is clicked   */
        private void btnImport_Click_1(object sender, EventArgs e)
        {                  
            try
            {
                string strPath = txtPath.Text;

                if (strPath != null && strPath.Length > 0)
                {
                    EADiagram utilityEADiagram = new EADiagram();

                    EA.Diagram newDiagram = utilityEADiagram.addDiagram(); 

                    AppDelegate.statSelectedDiagram = newDiagram;

                    EAWFMain mainEAWF = new EAWFMain();

                    mainEAWF.eaMain(txtPath.Text);
                }
                else
                {
                   MessageBox.Show(ErrorConstant.SELECT_FILE_TO_IMPORT);
                }                    
            }           
            catch(Exception exException)
            {                         
                exceptionCustom.showException(exException.Message);               
            }
        }
        
        private void BrowseForm_Load(object sender, EventArgs e)
        {
            TreeNode tempNode = new TreeNode();

            tempNode.Text = AppDelegate.statSelectedPackage.Name;
            tempNode.Tag  = AppDelegate.statSelectedPackage.PackageGUID;

            treeView1.Nodes.Add(tempNode);

            /* code for creating package tree */
            addChildNodes(tempNode, AppDelegate.statSelectedPackage);
        }

        private void txtPath_TextChanged(object sender, EventArgs e)
        {
        }

        /* This function calls another function(addnode) and obtains a list of child packages if present below the user selected package(input argument - parentPackage). It traverses all the child
         * Packages obtained and calls the function recursively by passing each childPackage to it. And agains obtains childPackage if present on the passed package. This way this  gets the tree
         * structure and also adds all obtained child package to the parent node. */
        public void addChildNodes(TreeNode parentNode, EA.Package parentPackage)
        {
            try
            {
                EAPackageHelper helpPackage = new EAPackageHelper();

                List<EA.Package> ltChildPackage = helpPackage.getChildPackage(parentPackage);

                if(ltChildPackage.Count != 0)
                {
                    addNode(parentNode, ltChildPackage);
                }

                for (int iDx = 0; iDx < ltChildPackage.Count; iDx++)
                {
                    addChildNodes(parentNode.Nodes[iDx], ltChildPackage[iDx]);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /* This function traverses all Packages present in list(ltChildPackage). It adds the name and GUID of each Package to each object of TreeNode. And Finally adds the obtained TreeNode object to
         * the TreeNode object present as input argument. */
        public void addNode(TreeNode childNode, List<EA.Package> ltChildPackList)
        {
            try
            {
                if(childNode != null && ltChildPackList.Count != 0)
                {
                    for(int iDx = 0; iDx < ltChildPackList.Count; iDx++)
                    {
                        TreeNode node = new TreeNode();

                        node.Text = ltChildPackList[iDx].Name;
                        node.Tag  = ltChildPackList[iDx].PackageGUID;
                        
                        childNode.Nodes.Add(node);
                    }
                }
            }
            catch(Exception exException)
            {
                throw exException;
            }
        }

        /* This function gets invoked when user selects child Package from the TreeView of 'Browse Dialog Box'. It sets the value of Package in Appdelgate class to the new user selected Package 
         * Hence the elemnts are now added to the new Package and in GUI the diagram appears below the new user selected child Package. */
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
             AppDelegate.statSelectedPackage = null;

            AppDelegate.statSelectedPackage = AppDelegate.statSelectedRepo.GetPackageByGuid(e.Node.Tag.ToString());           
        }              
    }       
}






















































                  

        