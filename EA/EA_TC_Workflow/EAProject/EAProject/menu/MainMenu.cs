﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: MainMenu.cs

   Description: This File returns Main Menu.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This file return main menu.
 * 
 * getMainMenu()-
 *  this function returns Main Menu
 *
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.menu
{
    class MainMenu
    {
        string strMainMenu = MenuConstants.strMenuHeader;

        public string getMainMenu()
        {
            return strMainMenu;
        }
    }
}



















