﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: MenuEvent.cs

   Description: This File is is sepcific for handling click event of context menus.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18    Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This File is is sepcific for handling click event of context menus.
 * 
 *         peformMenuClickEvent(string strEventName, EA.Repository repository)
 *          Function which defines individual cases for handling context menus click.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EA;

namespace EAProject.menu
{
    class MenuEvent
    {
        Main main = new Main();

        public void peformMenuClickEvent(string strEventName)
        {
            if (strEventName == MenuConstants.strMenuContext)
            {
                main.showBrowseDialog();
            }
        }
    }
}
























