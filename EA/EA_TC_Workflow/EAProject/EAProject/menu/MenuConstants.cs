﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: MenuConstants.cs

   Description: This File contains constants specific to Menu options of pluggin.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18     Megha Singh         Initial Release
========================================================================================================================================================================*/

/* Class Description - This file contains constants specific to Menu Options
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.menu
{
    class MenuConstants
    {
        /*  Main Menu Name */
        public const string strMenuHeader = "-&Teamcenter";
       
        /*  Sub Menu Names */
        public const string strMenuContext = "&Import Workflow";
    }
}











