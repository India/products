﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ConstructMenu.cs

   Description: This File is entry point for Menu definations.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18     Megha Singh         Initial Release
========================================================================================================================================================================*/

/* Class Description - This File is entry point for Menu definations. It defines Main Menu and Sub Menus.
 *                     This class is entry point to two specific classes that defines Main and Sub Menus.
 * getMainMenu()-
 *  Returns Main Menu
 * 
 * string[] getSubMenu()-
 *  Return arrays of strings which contains Sub Menus.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.menu
{
    class ConstructMenu
    {
        MainMenu menuMain = new MainMenu();
        SubMenu  menuSub  = new SubMenu();

        public string getMainMenu()
        {
            string strMainMenu = menuMain.getMainMenu();

            return strMainMenu;
        }

        public string[] getSubMenu()
        {
            List<string> ltTempList = menuSub.getSubMenu();

            string[] strSubMenu = ltTempList.ToArray();

            return strSubMenu;
        }
    }
}





















