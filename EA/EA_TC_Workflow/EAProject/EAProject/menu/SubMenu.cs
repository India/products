﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: SubMenu.cs

   Description: This File is specific to sub menus.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
08-Jan-18    Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This file is specific to sub menus.
 * 
 * List<String> getSubMenu()-
 * Function to return list of Sub menus.
 *  
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.menu
{
    class SubMenu
    {
        public List<String> getSubMenu()
        {
            List<string> ltTempList = new List<string>();

            ltTempList.Add(MenuConstants.strMenuContext);

            return ltTempList;
        }
    }
}



























