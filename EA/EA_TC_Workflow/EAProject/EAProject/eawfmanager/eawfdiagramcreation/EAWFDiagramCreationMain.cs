﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAWFDiagramCreationMain.cs

   Description: This File is specific to Diagram creation. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.eahelpers;
using EAProject.containers;
using EAProject.utilities;
using EAProject.constants;

namespace EAProject.eawfmanager.eawfdiagramcreation
{
    class EAWFDiagramCreationMain
    {
        StringUtilities utilitiesString = new StringUtilities();       

        /* This function reads PLMXML and extracts information related to handlers associated with each WorkflowTemplate from dictionary and draws 
         * diagramObjects(tasks) in diagram. */
        public void drawDiagramMain(XmlNode xmlRoot, Dictionary<string, WFTemplateInfo> dictTemplateInfo)
        {
            try
            {
                EADiagramObject                objectEADiagram  = new EADiagramObject();
                EAConnectorsnLabels            labelsConnectors = new EAConnectorsnLabels();
                Dictionary<string, EA.Element> dictElement      = new Dictionary<string, EA.Element>();
                            
                if (xmlRoot is XmlElement && (xmlRoot.HasChildNodes) == true)
                {
                    int iDx    = 0;
                    int iIndex = 0;

                    /* Counting all XmlNodes present in WorkflowTemplate. */
                    int iNodesCount = xmlRoot.ChildNodes.Count;

                    while (iDx < iNodesCount)
                    {
                        string strAttrId   = null;
                        string strTaskType = null;
                        
                        XmlAttributeCollection collectXmlAttribute = (xmlRoot.ChildNodes[iDx].Attributes);
                        
                        /* Checking if name of XmlNode is WorkflowTemplate */
                        if (String.Compare((xmlRoot.ChildNodes[iDx].Name), Constants.WORKFLOW_TEMPLATE) == 0)
                        {
                            WFTemplateInfo infoWFTemplate       = new WFTemplateInfo();
                            List<string>   ltDependencyTemplate = new List<string>();

                            /* Getting id of WorkflowTemplate encountered at index 'iDx' from PLMXML */
                            strAttrId = collectXmlAttribute[Constants.ID].Value;

                            /* Checking if WorkflowTemplate id exist in dictionary */ 
                            if (dictTemplateInfo.ContainsKey(strAttrId) == true)
                            {
                                /* Getting WorkflowTemeplate info from dictionary with "id value" as key */
                                dictTemplateInfo.TryGetValue(strAttrId, out infoWFTemplate);

                                /* Getting value of 'ObjectType' attribute present in WorkflowTemplate(XmlNode) from PlMXML. */
                                strTaskType = collectXmlAttribute[Constants.OBJECT_TYPE].Value;                                
                            }
                            /*Calling Function to insert diagramObjects */
                            objectEADiagram.insertDiagramObjects(strAttrId, infoWFTemplate, ref iIndex, ref dictElement, dictTemplateInfo);
                        }

                        iDx++;
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }        
    }
}



