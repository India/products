﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAWFMain.cs

   Description: This File .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using EAProject.xmlutilities;
using EAProject.containers;
using EAProject.eawfmanager.eawfdatacreation;
using EAProject.eawfmanager.eawfdiagramcreation;

namespace EAProject.eawfmanager
{
    class EAWFMain
    {
        /* This function gets the root element of the PLMXML, parses the PLMXML and stores the information of the handlers present on each WorkflowTemplate
         * in Dictionary. It also rereads entire PLMXML once again and extract info of each WFTemplate node from Dictionary to draw diagramobjects(tasks) in diagram.
         **/
        public void eaMain(string strFilePath)
        {
            try
            {                
                LoadXML                            xmlLoad              = new LoadXML();
                EAWFDataCreationMain               eawfDataCreationMain = new EAWFDataCreationMain();                
                EAWFDiagramCreationMain            createWFDiagram      = new EAWFDiagramCreationMain();
                Dictionary<string, WFTemplateInfo> dictWFInfo           = new Dictionary<string, WFTemplateInfo>();

                /* Getting root of the PLMXML */
                System.Xml.XmlNode xmlRoot = xmlLoad.loadPLMXML(strFilePath);

                /* Parsing xml and filling info of all WFTemplates in dictionary. */
                eawfDataCreationMain.fillWFDictMain(xmlRoot, ref dictWFInfo);

                /* ReReading the PLM XML and getting info about encountered WFTemplate from Dictionary to create  diagramobjects. */
                createWFDiagram.drawDiagramMain(xmlRoot, dictWFInfo);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}





































