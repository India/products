﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: FillMainWFTaskTpl.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the required info present on WFTemplate node of WF PLMXML.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.constants;
using EAProject.utilities;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class FillMainWFTaskTpl
    {
        StringUtilities utilString     = new StringUtilities();
        WFTemplateInfo  infoWFTemplate = new WFTemplateInfo();

        /* This function gets the info(name, location, objecttype and list of action ids of WFTask). This function also gets the required info present on the child of 'TaskDescription' and 'UserData'
         * child of WFTemplate node.
         **/
        public void getWFTplInfo(XmlNode xmlNodeInfo, ref List<string> ltActionIDs, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {
                EAWFDataCreationUtil utilDataCreation = new EAWFDataCreationUtil();

                if (xmlNodeInfo.Attributes.Count != 0)
                {
                    XmlAttributeCollection strNodesAttrCollection = xmlNodeInfo.Attributes;                    

                    /* Checking if XmlNode is WorkflowTemplate */
                    if (String.Compare(xmlNodeInfo.Name, Constants.WORKFLOW_TEMPLATE) == 0)
                    {
                        /* Filling the variables of WFTemplateInfo Class. */
                        infoWFTemplate.strTaskName       = strNodesAttrCollection[Constants.NAME].Value;
                        infoWFTemplate.strTaskLocation   = strNodesAttrCollection[Constants.LOCATION].Value;
                        infoWFTemplate.strTaskObjectType = strNodesAttrCollection[Constants.OBJECT_TYPE].Value;

                        /* Tokenizing the comma separated ids(handlers) present in the attribute 'action' of WorkFlowTemplate XmlNode. */
                        ltActionIDs = utilString.tokenStringToList(strNodesAttrCollection[Constants.ACTIONS].Value, Constants.LIST_TOKEN_CONSTANT);

                        /* Checking if XmlNode contains childnodes */
                        if (xmlNodeInfo.HasChildNodes == true)
                        {
                             int iNodesCount = xmlNodeInfo.ChildNodes.Count;

                             for (int iDx = 0; iDx < iNodesCount; iDx++)
                             {
                                 string strNodeName = xmlNodeInfo.ChildNodes[iDx].Name;

                                 /* Checking for 'Task Description' child node in WorkflowTemplate XmlNode */
                                 if (String.Compare(strNodeName, Constants.TASK_DESCRIPTION) == 0)
                                 {
                                     utilDataCreation.processTaskDesc(xmlNodeInfo.ChildNodes[iDx], ref infoWFTemplate);
                                 }
                                 else if (String.Compare(strNodeName, Constants.USER_DATA) == 0)
                                 {
                                     utilDataCreation.processUserData(xmlNodeInfo.ChildNodes[iDx], ref infoWFTemplate);
                                 }
                             }
                        }
                    }
                }
            }        
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}













        


