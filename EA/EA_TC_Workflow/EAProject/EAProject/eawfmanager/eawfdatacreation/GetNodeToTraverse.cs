﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: GetNodeToTraverse.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which traverses all the nodes of PLMXML and compares the value of attribute 'id' of each XmlNode with the id(strTravseID) received as argument and 
 * returns the XmlNode element corresponding to the id(strTravId) received as argument of function.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.containers;
using EAProject.utilities;
using EAProject.constants;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class GetNodeToTraverse
    {
        public XmlNode getXmlNode(XmlNode xmlRoot, string strTravID, ref List<string> ltIDsTraversed)
        {
            try
            {
                XmlNode outNode     = null;
                int     iNodesCount = xmlRoot.ChildNodes.Count;

                if (iNodesCount != 0)
                {
                    /* Traversing all the nodes of PLMXML.  */
                    for (int iDx = 1; iDx < iNodesCount; iDx++)
                    {
                        if (xmlRoot.ChildNodes[iDx].Attributes.Count != 0)
                        {
                            /*  */
                            XmlAttributeCollection strNodesAttrCollection = xmlRoot.ChildNodes[iDx].Attributes;

                            if (strNodesAttrCollection[Constants.ID].Value != null)
                            {
                                /* Comparing 'id'(strTravID) with the id of XmlNode present at index(iDx)  */
                                if (String.Compare(strTravID, strNodesAttrCollection[Constants.ID].Value) == 0)
                                {
                                    ltIDsTraversed.Add(strTravID);
                                    /* Returning the XmlNode. */
                                    outNode  = xmlRoot.ChildNodes[iDx];
                                    break;
                                }
                            }
                        }
                    }
                }

                return outNode;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}















           

