﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: FillActRefData.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the info present on WFHandler node of WFTemplate node.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.containers;
using EAProject.constants;
using EAProject.utilities;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class FillActRefData
    {
       /* This function gets the action Handlers info(name) present on 'WorkflowHandler' XmlNode of WFTemplate. */       
        public void getActionRefsInfo(XmlNode xmlNodeAction, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {
                FillNodeChildData      dataChildNode          = new FillNodeChildData();
                XmlAttributeCollection strNodesAttrCollection = xmlNodeAction.Attributes;

                /* Checking if XmlNOde is 'WorkflowHandler' */
                if (String.Compare(xmlNodeAction.Name, Constants.WORKFLOW_HANDLER) == 0 && strNodesAttrCollection[Constants.NAME].Value != null)
                {
                    string strHandlerName = Constants.PREFIX_HANDLER_NAME + strNodesAttrCollection[Constants.NAME].Value;

                    /* Adding value of attribute 'Name' to list variable in WFTemplateInfo class. */
                    infoWFTemplate.ltTaskHandlers.Add(strHandlerName);

                    /* Checking if ChildNode exist below WorkflowHandler */
                    if (xmlNodeAction.HasChildNodes == true)
                    {
                        dataChildNode.extractChildInfo(xmlNodeAction, ref infoWFTemplate);

                        infoWFTemplate.ltTaskHandlers.Add(Constants.SEPARATOR_SPL_CHARACTER);
                    }
                    else
                    {
                        infoWFTemplate.ltTaskHandlers.Add(Constants.SEPARATOR_SPL_CHARACTER);
                    }                  
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
         }
    }
}

















