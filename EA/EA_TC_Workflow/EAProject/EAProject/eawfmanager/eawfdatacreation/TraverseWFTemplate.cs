﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: TraverseWFTemplate.cs

   Description: This File is specific to WorkflowTemplate traversal. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which traverses PLMXML and also stores the information present on handlers of each WorkflowTemplate 
 * node in a dictionary.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class TraverseWFTemplate
    {
        /* This function traverses all the WFTemplate nodes with the help of ids present in 'traverseRootRefs' attribute of WF . It collects the information present on all handlers of WFTemplate nodes
         * and adds WFTemplate node info in Dictionary. The structure of dictionary - Dictionary<string, WFTemplate>
         * where, WFTemplate node 'id' - key and WFTemplate node 'info' - value. 
         * All comma separated ids present in this variable are traversed one by one in the same sequence as present in variable('strAttrTraverseRootRefs').
         **/
        public void getInfoWFTemplate(List<string> ltAttrTravRootRefs, XmlNode xRoot, ref Dictionary<string, WFTemplateInfo> dictWFInfo)
        {
            try
            {
                if (ltAttrTravRootRefs.Count != 0)
                {
                    int iTravRootRefsCount = ltAttrTravRootRefs.Count;
                    List<string>   ltIDsTraversed = new List<string>();
                    
                    for (int iDx = 0; iDx < iTravRootRefsCount; iDx++)
                    {
                        WFTemplateInfo infoWFTemplate = new WFTemplateInfo();

                        /* Calling function 'getWFTplObj' to get WFTemplate nodes info. All the ids of traversed nodes are added to reference of list(ltIDsTraversed) passed and WFTemplate node info 
                         * is added to reference of instance of class WFTemplateInfo passed. */
                        getWFTplObj(xRoot, ltAttrTravRootRefs[iDx], ref infoWFTemplate, ref ltIDsTraversed);

                        /* The WFTemplate node info obtained in instance of object of class WFTemplate serves as value of Dictionary while the WFTemplate node 'id' is the key of Dictionary. The values
                         * corresponding to the key are added to Dictionary */
                        dictWFInfo.Add(ltAttrTravRootRefs[iDx], infoWFTemplate);
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function gets the information of WFTemplate node in the object('infoWFTemplate') of WFTemplateInfo class with the help of WFTemplate id(strTrvRtRef). */
        public void getWFTplObj(XmlNode xRoot, string strTrvRtRef, ref WFTemplateInfo infoWFTemplate, ref List<string> ltIDsTraversed)
        {
            GetWFTemplObject getTemplateObj = new GetWFTemplObject();

            try
            {
                getTemplateObj.getSingleWFTplObj(xRoot, strTrvRtRef, ref infoWFTemplate, ref ltIDsTraversed);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}




















