﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ParseWFPLMXMLMain.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which parse PLMXML and also stores the information present on the handlers of every WorkflowTemplate 
 * node in a dictionary.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.constants;
using EAProject.containers;
using EAProject.utilities;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class ParseWFPLMXMLMain
    {
        /* This function extracts ids of all WorkflowTemplate elements present as value of the Attribute('traverseRootRefs') in the imported Workflow.
         * The value of this attribute is represented by the variable('strAttrTraverseRootRefs'). 
         **/
        public void parsePLMXML(XmlNode xRoot, ref Dictionary<string, WFTemplateInfo> dictWFInfo)
        {
            try
            {
                TraverseWFTemplate traversalWFTemplate = new TraverseWFTemplate();
                StringUtilities    utilString          = new StringUtilities();

                /* Checking if root node contains child nodes below it. */
                if (xRoot is XmlElement && xRoot.HasChildNodes == true && xRoot.ChildNodes.Count != 0)
                {
                    string  strFirstChildName = xRoot.FirstChild.Name;

                    /* Checking if 'Header' is First child of root node(xRoot). */
                    if (String.Compare(strFirstChildName, Constants.HEADER) == 0 && xRoot.FirstChild.Attributes.Count != 0)
                    {
                        XmlAttributeCollection strAttrCollection = xRoot.FirstChild.Attributes;

                        /* Checking for attribute(traverseRootRefs) in 'Header' node of PLMXML. */
                        if (strAttrCollection[Constants.TRAVERSE_ROOT_REFS].Value != null)
                        {                            
                            string strAttrTravRootRefs = strAttrCollection[Constants.TRAVERSE_ROOT_REFS].Value;

                            if (strAttrTravRootRefs != null)
                            {
                                /* Tokenizing '#' separated list of WFTemplateNode ids present in 'traverseRootRefs' attribute of PLMXML. String variable 'strAttrTravRootRefs' representing attribute 
                                 * (traverseRootRefs)  and delimiter(#) are passed as arguments to tokenization function (tokenizeTravIDs). */                                             
                                List<string> ltAttrTraverseRootRefs = utilString.tokenizeTravIDs(strAttrTravRootRefs, Constants.LIST_TOKEN_CONSTANT);
                                                                
                                /* Passing traversed WFTemplate ids list(ltAttrTraverseRootRefs), Root node of PLMXML and reference of dictionary to 'getInfoWFTemplate' function to obtain dictionary
                                 * filled with WFTemplate node information(value) corresponding to each WFTemplate id(key) of PLMXML.*/
                                traversalWFTemplate.getInfoWFTemplate(ltAttrTraverseRootRefs, xRoot, ref dictWFInfo);
                            }
                        }
                    }
                }
                else
                {
                    /* Throwing exception any child node is not found below root node. */               
                    throw new Exception(ErrorConstant.CUSTOM_ERROR_CODE_6);
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}
























        












































       
        






















    

