﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: FillRuleRefData.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which gets the info present on the rule handlers of WFTemplate.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.constants;
using EAProject.utilities;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class FillRuleRefData
    {
        StringUtilities   utilString    = new StringUtilities();
        GetNodeToTraverse checkTravrsd  = new GetNodeToTraverse();
        FillNodeChildData dataChildNode = new FillNodeChildData();

        /* This function gets the Handlers info(name and list of ruleRefs) present on WorkflowBusinessRule node of WFTemplate. */ 
        public void getRuleRefsInfo(XmlNode xmlRoot, XmlNode nodeWFAction, ref WFTemplateInfo infoWFTemplate, List<string> ltIDsTraversed)
        {
            try
            {
                if (nodeWFAction.Attributes.Count != 0)
                {
                    XmlAttributeCollection strNodesAttrCollection = nodeWFAction.Attributes;

                    /* Checking if XmlNode is 'WorkflowBusinessRule' and contains 'ruleQuorum' attribute */
                    if (String.Compare(nodeWFAction.Name, Constants.WORKFLOW_BUSINESS_RULE) == 0 && strNodesAttrCollection[Constants.RULE_QUORUM].Value != null)
                    {
                        string strHandlerName = Constants.PREFIX_QUORUM_VALUE + strNodesAttrCollection[Constants.RULE_QUORUM].Value;

                        /* Adding value of attribute ruleQuorum to list variable in WFTemplateInfo class. */
                        infoWFTemplate.ltTaskHandlers.Add(strHandlerName);

                        if (strNodesAttrCollection[Constants.RULE_HANDLER_REFS].Value != null)
                        {
                            /* Tokenizing comma separated ids present in the attribute 'ruleRefs' */
                            List<string> ltRuleHandlerRefs = utilString.tokenStringToList((strNodesAttrCollection[Constants.RULE_HANDLER_REFS].Value), Constants.LIST_TOKEN_CONSTANT);

                            if (ltRuleHandlerRefs.Count != 0)
                            {
                                getWFRuleHandler(xmlRoot, ltRuleHandlerRefs, ref infoWFTemplate, ref ltIDsTraversed);
                            }
                        }
                    }
                }
            }
            catch(Exception exException)
            {
                throw exException;
            }
        }
        /* This function gets the info present on id representing WFBusinessRuleHandler node of WFTemplate. */
        public void getWFRuleHandler(XmlNode xmlRoot, List<string> ltRuleHandlerRefs, ref WFTemplateInfo infoWFTemplate, ref List<string> ltIDsTraversed)
        {
            try
            {
                if (ltRuleHandlerRefs.Count != 0)
                {
                    for (int iDx = 0; iDx < ltRuleHandlerRefs.Count; iDx++)
                    {
                        /* Checking if list(ltRuleHandlerRefs[iDx]) contains the id of WorkflowBusinessRuleHandler. */
                        if (ltIDsTraversed.Contains(ltRuleHandlerRefs[iDx]) == false)
                        {
                            XmlNode nodeWFRuleHandlerRef = checkTravrsd.getXmlNode(xmlRoot, ltRuleHandlerRefs[iDx], ref ltIDsTraversed);

                            if (nodeWFRuleHandlerRef != null && nodeWFRuleHandlerRef.Attributes.Count != 0)
                            {
                                XmlAttributeCollection strNodesAttrCollection = nodeWFRuleHandlerRef.Attributes;

                                if (String.Compare(nodeWFRuleHandlerRef.Name, Constants.WORKFLOW_BUSINESS_RULE_HANDLER) == 0 && strNodesAttrCollection[Constants.NAME].Value != null)
                                {
                                    string strHandlerName = Constants.PREFIX_HANDLER_NAME + strNodesAttrCollection[Constants.NAME].Value;

                                    /* Adding Handler's Name to class WFTemplateInfo */
                                    infoWFTemplate.ltTaskHandlers.Add(strHandlerName);

                                    /* Checking if ChildNode exist below WorkflowBusinessRuleHandler XmlNode */
                                    if (nodeWFRuleHandlerRef.HasChildNodes == true)
                                    {
                                        dataChildNode.extractChildInfo(nodeWFRuleHandlerRef, ref infoWFTemplate);

                                        infoWFTemplate.ltTaskHandlers.Add(Constants.SEPARATOR_SPL_CHARACTER);
                                    }
                                    else
                                    {
                                        infoWFTemplate.ltTaskHandlers.Add(Constants.SEPARATOR_SPL_CHARACTER);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }         
    }
}









                
    
