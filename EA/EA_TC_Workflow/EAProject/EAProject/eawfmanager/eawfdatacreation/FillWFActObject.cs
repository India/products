﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: FillWFActObject.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the ids present in 'actionHandlerRefs', 'ruleRefs' and 'actionType' attributes of WFAction node of WFTemplate.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.containers;
using EAProject.constants;
using EAProject.utilities;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class FillWFActObject
    {
        public void getRuleAndActionInfo(XmlNode xmlNodeInfo, ref List<string> ltRuleRefs, ref List<string> ltActionRefs, ref WFTemplateInfo infoWFTemplate, ref string strActTyp)
        {
            try
            {
                StringUtilities        utilString             = new StringUtilities();

                if (String.Compare(xmlNodeInfo.Name, Constants.WORKFLOW_ACTION) == 0 && xmlNodeInfo.Attributes.Count != 0)
                {
                    XmlAttributeCollection strNodesAttrCollection = xmlNodeInfo.Attributes;
                    int                    iNodesAttrCount        = strNodesAttrCollection.Count;
                    
                    for (int iAttrCount = 0; iAttrCount < iNodesAttrCount; iAttrCount++)
                    {
                        /* Checking if attribute name is 'actionHandlerRefs' and getting the value of attribute in list(ltActionRefs).*/
                        if (strNodesAttrCollection[iAttrCount].Name != null && strNodesAttrCollection[iAttrCount].Name.Equals(Constants.ACTION_HANDLER_REFS) && strNodesAttrCollection[Constants.ACTION_HANDLER_REFS].Value != null)
                        {
                            ltActionRefs = utilString.tokenStringToList(strNodesAttrCollection[Constants.ACTION_HANDLER_REFS].Value, Constants.LIST_TOKEN_CONSTANT);                                                      
                        }

                        /* Checking if attribute name is 'ruleRefs' and getting the value of attribute in variable 'ltRuleRefs'.*/
                        else if (strNodesAttrCollection[iAttrCount].Name != null && strNodesAttrCollection[iAttrCount].Name.Equals(Constants.RULE_REFS) && strNodesAttrCollection[Constants.RULE_REFS].Value != null)
                        {
                            ltRuleRefs = utilString.tokenStringToList(strNodesAttrCollection[Constants.RULE_REFS].Value, Constants.LIST_TOKEN_CONSTANT);                                               
                        }
                        /* Checkingif attribute name is 'actionType' and getting the value attribute in the variable 'strActTyp'. */
                        else if (strNodesAttrCollection[iAttrCount].Name != null && strNodesAttrCollection[iAttrCount].Name.Equals(Constants.ACTION_TYPE) && strNodesAttrCollection[Constants.ACTION_TYPE].Value != null)
                        {
                            strActTyp = strNodesAttrCollection[iAttrCount].Value;
                        }
                    }
                }                    
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}






























