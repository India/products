﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.containers;
using EAProject.utilities;
using EAProject.constants;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class CheckNodeTraversed
    {
        public XmlNode checkNodeIfTravrsd(XmlNode xmlRoot, int iNodesCount, string strTravseID, ref List<string> ltIDsTraversed)
        {
            try
            {
                for (int iDx = 1; iDx < iNodesCount; iDx++)
                {
                    /* Checking if index is present in list containing indexes of XmlNodes Traversed. */
                    XmlAttributeCollection strNodesAttrCollection = (xmlRoot.ChildNodes[iDx].Attributes);

                    /* Comparing 'id' of XmlNode present in TraverseRootRefs attribute with the id of XmlNode present at index(iDx)  */
                    if (String.Compare(strTravseID, strNodesAttrCollection[Constants.ID].Value) == 0)
                    {
                        ltIDsTraversed.Add(strTravseID);

                        return xmlRoot.ChildNodes[iDx];
                    }                    
                }

                return xmlRoot;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}
           

