﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: FillNodeChildData.cs

   Description: This File is specific to parsing PLMXML. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the info present on Childnodes under any node present on WF PLMXML. It also gets required info on child
 * if present below the Child XmlNode.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.utilities;
using EAProject.constants;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class FillNodeChildData
    {
        /* This function extracts the information on childnodes present below a node(xNode) and stores it in the instance of class WFTemplateInfo. */
        public void extractChildInfo(XmlNode xNode, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {
                if (xNode.ChildNodes.Count != 0)
                {
                    int iChildCount = xNode.ChildNodes.Count;

                    for (int iDx = 0; iDx < iChildCount; iDx++)
                    {
                        /* Checking if the childnode is 'Arguments'. */
                        if (xNode.ChildNodes[iDx].Name.Equals(Constants.ARGUMENT) == true && xNode.ChildNodes[iDx].ChildNodes.Count != 0)
                        {
                            int iArgChildCount = xNode.ChildNodes[iDx].ChildNodes.Count;

                            for (int iFx = 0; iFx < iArgChildCount; iFx++)
                            {
                                /* Checking if 'UserValue' is the childnode of 'Arguments'node. */
                                if (xNode.ChildNodes[iDx].ChildNodes[iFx].Name.Equals(Constants.USER_VALUE) == true && xNode.ChildNodes[iDx].ChildNodes[iFx].Attributes.Count != 0)
                                {
                                    XmlAttributeCollection strNodesAttrCollection = xNode.ChildNodes[iDx].ChildNodes[iFx].Attributes;

                                    int iAttrCount = strNodesAttrCollection.Count;

                                    for (int iZx = 0; iZx < iAttrCount; iZx++)
                                    {
                                        /* Checking if 'value' is the name of attribute present on 'UserValue' chilnode. */
                                        if (strNodesAttrCollection[iZx].Name.Equals(Constants.VALUE) == true && strNodesAttrCollection[Constants.VALUE].Value != null)
                                        {
                                            infoWFTemplate.ltTaskHandlers.Add(strNodesAttrCollection[Constants.VALUE].Value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}




















