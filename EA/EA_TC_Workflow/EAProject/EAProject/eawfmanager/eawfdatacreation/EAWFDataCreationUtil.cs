﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAWFDataCreationUtil.cs

   Description: This File is specific to data creation in WFTemplate node. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the required info present on WFTemplate child nodes of WF PLMXML.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using EAProject.utilities;
using EAProject.constants;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class EAWFDataCreationUtil
    {
        /* This function gets the template info present on 'value' attribute of 'Item' node. 'Item' node is present as child(childnode) of 'Task Description' node. Also 'TaskDescription' is childnode
         * of WFTemplate. */
        public void processTaskDesc(XmlNode xmlNode, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {                
                /* Checking if 'TaskDescription' contains childnodes under it */
                if (xmlNode.HasChildNodes == true)
                {
                    /* Counting the number of childnodes present below 'TaskDescription' */
                    int iTaskDescChildNodesCount = xmlNode.ChildNodes.Count;

                    for (int iFx = 0; iFx < iTaskDescChildNodesCount; iFx++)
                    {
                        /* Checking for 'Item' childnode under 'TaskDescription' */
                        if (String.Compare(xmlNode.ChildNodes[iFx].Name, Constants.ITEM) == 0 && xmlNode.ChildNodes[iFx].Attributes.Count != 0)
                        {
                            XmlAttributeCollection strCollection = xmlNode.ChildNodes[iFx].Attributes;

                            /* Adding value of 'Value' attribute present on 'Item' childnode of TaskDescription to  WFTemplateInfo class. */
                            infoWFTemplate.strTaskInstruction = strCollection[Constants.VALUE].Value;

                            /* If more than one 'Item' child nodes of TaskDescription exists than only first 'item' childnode would be considered for 'TaskDescription'. */
                            break;
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function gets the template info present on 'title' and 'dataRef' attributes of 'UserValue' node. 'UserNode' node is present as child(childnode) of 'UserData' node. Also 'UserData' is childnode
         * of WFTemplate. */
        public void processUserData(XmlNode xmlNode, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {              
                /* Checking if 'UserData' contains childnodes under it */
                if (xmlNode.HasChildNodes == true)
                {
                    /* Counting the number of childnodes present below 'UserData' */
                    int iUserDataChildNodesCount = xmlNode.ChildNodes.Count;

                    for (int iFx = 0; iFx < iUserDataChildNodesCount; iFx++)
                    {
                        /* Check for 'UserValue' chilnode below 'UserData'. */
                        if (String.Compare(xmlNode.ChildNodes[iFx].Name, Constants.USER_VALUE) == 0 && (xmlNode.ChildNodes[iFx].Attributes.Count) != 0)
                        {
                            XmlAttributeCollection strCollection = xmlNode.ChildNodes[iFx].Attributes;
                            List<string>           ltTaskRefIDs  = new List<string>();
                            string                 strTaskRefId  = null;

                            /* Adding value of attribute 'title' present on 'UserValue' childnode of 'UserData' to class WFTemplateInfo. */
                            if (infoWFTemplate.dictTaskDependTaskRef.ContainsKey(strCollection[Constants.TITLE].Value) == false)
                            {
                                strTaskRefId = strCollection[Constants.DATA_REF].Value.Remove(0, 1);

                                ltTaskRefIDs.Add(strTaskRefId);

                                (infoWFTemplate.dictTaskDependTaskRef).Add(strCollection[Constants.TITLE].Value, ltTaskRefIDs);
                            }
                            else
                            {
                                infoWFTemplate.dictTaskDependTaskRef.TryGetValue((strCollection[Constants.TITLE].Value), out ltTaskRefIDs);

                                strTaskRefId = strCollection[Constants.DATA_REF].Value.Remove(0, 1);

                                if (ltTaskRefIDs.Contains(strTaskRefId) == false)
                                {
                                    ltTaskRefIDs.Add(strTaskRefId);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }        
    }
}



                           

