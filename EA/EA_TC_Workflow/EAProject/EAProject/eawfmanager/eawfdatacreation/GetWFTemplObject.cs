﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: GetWFTemplateObject.cs

   Description: This File is specific to WFTemplate node traversal. 

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which traverses a WFTemplate node. It traverses all action and rule handlers ids prsent on it and stores the handler information in a dictionary. 
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using EAProject.eawfmanager.eawfdatacreation;
using EAProject.containers;
using EAProject.constants;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class GetWFTemplObject
    {
        WFTemplateInfo    infoWFTemplate    = new WFTemplateInfo();
        GetNodeToTraverse checkTravrsd      = new GetNodeToTraverse();
        FillMainWFTaskTpl fillWFTaskInfo    = new FillMainWFTaskTpl();
        FillWFActObject   fillWFActionData  = new FillWFActObject();
        FillActRefData    fillActionRefData = new FillActRefData();
        FillRuleRefData   fillRuleRefData   = new FillRuleRefData();
        List<string>      ltActionIDs       = new List<string>();

        /* This function traverses a single WFTemplate node whose 'id' is represented by variable(strTravRootRefsID). It first traverses the WFTemplate node and gets the list of action ids present in
         * attribute 'actions' . Then all WFActions nodes whose ids are present in 'action' attribute of WFTemplate node are traversed. And all the action and rule handler ids represented by 'actionRef'
         * and 'ruleRef' attributes are get. Further on the basis of ids present in actionRef and ruleRef attributes -WFHandler, WFBusinessRule and WFBusinessRuleHandler nodes are traversed and the info
         * present on them are stored in dictionary.
         **/
        public void getSingleWFTplObj(XmlNode xmlRoot, string strTravRootRefsID, ref WFTemplateInfo infoWFTemplate, ref List<string> ltIDsTraversed)
        {
            try
            {
                /* Checking if WFTemplatenode id(strTravRootRefsID) is present in list containing ids of XmlNodes Traversed. */
                if (ltIDsTraversed.Contains(strTravRootRefsID) == false)
                {
                    XmlNode nodeWFTpl = checkTravrsd.getXmlNode(xmlRoot, strTravRootRefsID, ref ltIDsTraversed);

                    if (nodeWFTpl != null)
                    {
                        /* Getting WFTemplate info in the reference of object of class WFTemplate passed to function and list of action ids in reference of 'ltActionIDs)passed to this function. */
                        fillWFTaskInfo.getWFTplInfo(nodeWFTpl, ref ltActionIDs, ref infoWFTemplate);

                        if (ltActionIDs.Count != 0)
                        {
                            /* Traversing action ids. */
                            for (int iDx = 0; iDx < ltActionIDs.Count; iDx++)
                            {
                                List<string> ltRuleRefs = new List<string>();
                                List<string> ltActionRefs = new List<string>();
                                String strActType = null;

                                if (ltIDsTraversed.Contains(ltActionIDs[iDx]) == false)
                                {
                                    /* Getting XmlNode element of action id(ltActionIDs[iDx]) and adding the action id(ltActionIDs[iDx]) to list of Traveresd Ids(ltIDsTraversed). */
                                    XmlNode nodeWFAct = checkTravrsd.getXmlNode(xmlRoot, ltActionIDs[iDx], ref ltIDsTraversed);

                                    if (nodeWFAct != null)
                                    {
                                        /* Getting list of Rule handler ids(ltRuleRefs), list of Action handler ids(ltActionRefs) and Action type info present on WFAction node of WFTEmplate with the 
                                         * help of refrences of list variables passed and adding the required info to object of WFTemplate class(infoWFTemplate) passesd as refrence to this function. */
                                        fillWFActionData.getRuleAndActionInfo(nodeWFAct, ref ltRuleRefs, ref  ltActionRefs, ref infoWFTemplate, ref strActType);

                                        strActType = Constants.PREFIX_ACTION_TYPE + strActType;

                                        /* Adding value of attribute 'actionType' to list variable(ltTaskHandlers) present in class WFTemplateInfo class */
                                        infoWFTemplate.ltTaskHandlers.Add(strActType);

                                        if (ltRuleRefs.Count != 0)
                                        {
                                            /* Adding rule handlers info to object of WFTemplate class. */
                                            processRuleRefs(xmlRoot, ltRuleRefs, ltIDsTraversed, ref infoWFTemplate);
                                        }
                                        else if (ltActionRefs.Count != 0)
                                        {
                                            /*Adding action handlers info to object of WFTemplate class. */
                                            processActionRefs(xmlRoot, ltActionRefs, ltIDsTraversed, ref infoWFTemplate);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    return;
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* Function traverses all the ids present in 'ltRuleRefs' argument variable and adds the required rulehandlers info to object of WFTemplate class(infoWFtemplate). Further it adds all the ids
         * traversed to list containing traversed ids(ltIDsTraversed) pased as argument of function.*/
        public void processRuleRefs(XmlNode xmlRoot, List<string> ltRuleRefs, List<string> ltIDsTraversed, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {               
                if (ltRuleRefs.Count != 0)
                {
                    for (int iFx = 0; iFx < ltRuleRefs.Count; iFx++)
                    {
                        /* Checking if rule handler id(ltRuleRefs[iFx]) is present in list of traversed ids(ltIDsTraversed). */
                        if (ltIDsTraversed.Contains(ltRuleRefs[iFx]) == false)
                        {
                            /* Getting XmlNode element corresponding to rule handler 'id'(ltRuleRefs[iFx]). */
                            XmlNode nodeWFAction = checkTravrsd.getXmlNode(xmlRoot, ltRuleRefs[iFx], ref ltIDsTraversed);

                            if (nodeWFAction != null)
                            {
                                /* Getting the info present on 'WFBusinessRule' and 'WFBusinessRuleHandler' nodes of rulerefs and adding the traversed ids to list(ltIDsTraversed) and ruel handler info
                                 * to the reference of object of class passed to this function. */
                                fillRuleRefData.getRuleRefsInfo(xmlRoot, nodeWFAction, ref infoWFTemplate, ltIDsTraversed);
                            }
                        }
                    }
                }

            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* Function traverses all the ids present in 'ltActionRefs' argument variable and adds the required actionhandlers info to object of WFTemplate class(infoWFtemplate). Further it adds all the ids
         * traversed to list containing traversed ids(ltIDsTraversed) passed as argument of function.*/
         public void processActionRefs(XmlNode xmlRoot, List<string> ltActionRefs, List<string> ltIDsTraversed, ref WFTemplateInfo infoWFTemplate)
        {
            try
            {                
                if (ltActionRefs.Count != 0)
                {
                    for (int iFx = 0; iFx < ltActionRefs.Count; iFx++)
                    {
                        /* Checking if action id(ltActionRefs[iFx]) is present in list of traversed ids(ltIDsTraversed). */
                        if (ltIDsTraversed.Contains(ltActionRefs[iFx]) == false)
                        {
                            /* Getting XmlNode element corresponding to action handler 'id'(ltRuleRefs[iFx]). */
                            XmlNode nodeWFAction = checkTravrsd.getXmlNode(xmlRoot, ltActionRefs[iFx], ref ltIDsTraversed);

                            if (nodeWFAction != null)
                            {
                                /* Getting the info present on 'WFHandler' nodes of actionRefs. It adds the traversed ids to list(ltIDsTraversed) and  action handler info to the reference of object of
                                 * class passed to this function. */
                                fillActionRefData.getActionRefsInfo(nodeWFAction, ref infoWFTemplate);
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}


