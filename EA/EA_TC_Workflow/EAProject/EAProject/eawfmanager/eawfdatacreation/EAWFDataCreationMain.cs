﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAWFDataCreationMain.cs

   Description: This File is specific to Parse Xml  .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which calls parseXML function and passes reference of dictionary to get the dictionary filled with info present on WFTemplate.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Threading.Tasks;
using System.Xml.Serialization;
using EAProject.containers;

namespace EAProject.eawfmanager.eawfdatacreation
{
    class EAWFDataCreationMain
    {
        /* This function calls the parsePLMXML function and passes the root node of the PLMXML and reference of Dictionary<string, WFTemplateInfo> where Key(string) holds the id of WFTemplate
         * and Value(class WFTemplateInfo) containing datamembers to hold information on WFTaskTemplate. Finally a dictionary filled with Task Template info(value) corresponding to every taskTemplate
         * id(key) is obtainted at the end. 
         **/
        public void fillWFDictMain(XmlNode xmlRoot, ref Dictionary<string, WFTemplateInfo> dictWFInfo)
        {
            ParseWFPLMXMLMain parseWFXMLMain = new ParseWFPLMXMLMain();

            try
            {
                parseWFXMLMain.parsePLMXML(xmlRoot, ref dictWFInfo);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}













