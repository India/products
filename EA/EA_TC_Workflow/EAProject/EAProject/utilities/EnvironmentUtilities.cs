﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EnvironmentUtuilities.cs

   Description: This File is specific to environment utilities.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.utilities
{
    class EnvironmentUtilities
    {
        /*  This utility function returns value of Enviroment variables */
        public string getEnvVariable(String strEnvironVar)
        {
            string strPath = null;
            
            try
            {
                /* Getting value of Environment variable */
                strPath = Environment.GetEnvironmentVariable(strEnvironVar);
            }
            catch (Exception exException)
            {
                throw exException;
            }
            return strPath;
        }
    }
}






