﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: StringUtilities.cs

   Description: This File is specific to string utilities.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.constants;

namespace EAProject.utilities
{
    class StringUtilities
    {
       /* This function returns list of tokens of strings. The array of strings is obtained on tokenizing the String(strUntokenizedString) on the basis of delimiter(cDelim). The array obtained is
        * transferred to list of strings. In the obtained list all the spaces are removed from each string of list and string at first position is removed from list. Since UntokenizedString contains
        * the character delimiter at the beginning therefore the first token obtained is space in list of Tokenized strings and is therefore removed from list*/
        public List<string> tokenStringToList(string strUntokenizedString, char cDelim)
        {
            try
            {
                string[] astrToken = strUntokenizedString.Split(cDelim);         

                List<string> ltTokenizedStrings = astrToken.ToList();

                ltTokenizedStrings = ltTokenizedStrings.Select(t => t.Trim()).ToList();

                ltTokenizedStrings.RemoveAt(0);

                return ltTokenizedStrings;
            }
            catch(Exception exException)
            {
               throw exException;
            }
        }

        /* The function takes string - (strTraverseRootRefIds) which contains all the (space and #) separated WFTemplate ids and char -cDelim(#). It tokenizes the string on the basis of character 
         * delimeter(#) and transfers the array of tokens to a list of strings. It first removes all the spaces from each string contained in List and then removes the string present at the first
         * position for the reason clearly explained in the comments above. Also the string present at the last position in the list is transferred to the first position since in the attribute
         * (traverseRootRefs) the id of first WFTemplate is present at the end of list.Therefore to sequentially traverse the WF the string prsent at the end of list is brought to the first position 
         * in the final list. */
        public List<string> tokenizeTravIDs(string strTraverseRootRefIds, char cDelim)
        {
            try
            {
                string[] astrToken = strTraverseRootRefIds.Split(cDelim);         

                List<string> ltTokenizedStrings = astrToken.ToList();

                ltTokenizedStrings = ltTokenizedStrings.Select(t => t.Trim()).ToList();

                ltTokenizedStrings.RemoveAt(0);

                ltTokenizedStrings.Insert(0, ltTokenizedStrings[ltTokenizedStrings.Count - 1]);

                ltTokenizedStrings.RemoveAt(ltTokenizedStrings.Count - 1);

                return ltTokenizedStrings;
            }
            catch(Exception exException)
            {
               throw exException;
            }
        }
       
        /*  This function returns array of strings after tokenization it on the basis of separator. */
        public string[] stringTokenize(string strCode, char cSeparator)
        {
            try
            {
                string[] astrSplit = strCode.Split(cSeparator);
                
                return astrSplit;
            }            
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}




















