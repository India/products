﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Main.cs

   Description: This File is an entry point to an application.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
10-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class is entry point of application.It is main class of Application.
 *                    Name of this class cannot be changed as it is defined in registry settings.
 *                    
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using EAProject.constants;
using EAProject.menu;
using EAProject.xmlutilities;
using EAProject.eahelpers;
using EAProject.appdelegate;
using EAProject.userinterface;
using EA;

namespace EAProject
{
    public class Main
    {       
        /* This function establishes connection with repository.This function needs to remain same and it should not be changed.
         * This function cannot be removed as addin will not work without this function.
         */
        public String EA_Connect(EA.Repository Repository)
        {
            /* No special processing required. */
            return Constants.EA_CONNECT;
        }

        /* This function defines Main Menu and Context menus.Individual cases are defined  for each menu within this function.
         **/
        public object EA_GetMenuItems(EA.Repository Repository, string strLocation, string strMenuName)
        {
            try
            {
                ConstructMenu constructMenu = new ConstructMenu();

                string strMainMenu = constructMenu.getMainMenu();

                string strMenuInfo = null;

                switch (strMenuName)
                {
                    /* For top level Menu */
                    case Constants.BLANK_CONST:
                        strMenuInfo = strMainMenu;
                        
                        return strMenuInfo;

                    /* Defines the submenu options */
                    case MenuConstants.strMenuHeader:
                        string[] strSubMenus = constructMenu.getSubMenu();
                        
                        return strSubMenus;
                }                
            }
            catch (Exception exException)
            {
                throw exException;
            }

            return Constants.BLANK;
        }
                
        /* This function returns true if project is opened and false if it is not opened
         **/
        bool isProjectOpen(EA.Repository Repository)
        {
            try
            {
                EA.Collection collect = Repository.Models;

                return true;
            }
            catch
            {
                throw new Exception(ErrorConstant.CUSTOM_ERROR_CODE_3);
            }
        }

        /* This function is event handler function which gets called when user click on any menu
        * @param ItemName is the name of menu option.
        * for eg . if the menu is "About" them itemname will contain "About"
        * This is entry point to whole plugin.
        **/
        public void EA_MenuClick(EA.Repository Repository, string strLocation, string strMenuName, string strItemName)
        {
            try
            {
                if (Repository != null)
                {
                    AppDelegate.statSelectedRepo = Repository;
                }
                else
                {
                    MessageBox.Show(ErrorConstant.SELECTED_REPOSITORY_IS_NULL);

                    return;
                }
                
                MenuEvent menuEvent = new MenuEvent();

                menuEvent.peformMenuClickEvent(strItemName);
            }
            catch (Exception exException)
            {
                MessageBox.Show(exException.Message + Constants.BLANK);
            }
        }

        /* This function is called when user clicks on Browse dialog menu option.*/
        public void showBrowseDialog()
        {
            try
            {            
                EA.Package Package = (EA.Package)(AppDelegate.statSelectedRepo.GetContextObject());

                if (Package == null)
                {
                    MessageBox.Show(ErrorConstant.SELECTED_OBJECT_IS_NOT_PACKAGE);

                    return;
                }
                else
                {
                    AppDelegate.statSelectedPackage = Package;
                                                                                
                    BrowseForm browseForm = new BrowseForm();
                    
                    browseForm.ShowDialog(); 
                }
                
            }
            catch (Exception exException)
            {
                MessageBox.Show(exException.Message + Constants.BLANK);
            }
        }        
    }
}









































        
          
    
    






























