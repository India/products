﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: AppDelegate.cs

   Description: This File is specific to EA.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains member variables specific to EA.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.appdelegate
{
    class AppDelegate
    {
        public static EA.Repository statSelectedRepo    = null;
        public static EA.Package    statSelectedPackage = null;
        public static EA.Diagram    statSelectedDiagram = null;
    }
}









