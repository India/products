﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtecIndia
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: WFTempplateInfo.cs

   Description: This File is specific to WorkflowTemplate information Collection.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Jan-18    Megha Singh        Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes variables to collect WorkflowTemplate information.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.containers
{
    class WFTemplateInfo
    {
        public string strTaskName        { get; set; }
        public string strTaskObjectType  { get; set; }
        public string strTaskLocation    { get; set; }
        public string strTaskInstruction { get; set; }
        public Dictionary<string, List<string>> dictTaskDependTaskRef = new Dictionary<string, List<string>>();
        public List<string>                     ltTaskHandlers        = new List<string>();
    }
}








