﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: Constants.cs

   Description: This File contains Constants to be used in entire application.

========================================================================================================================================================================

Date          Name          Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Jan-18    Megha Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file contains the constants used in this project.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.constants
{
    class Constants
    {
        /*  Default Project */
        public static string LMPRIME                           = "LMprime";

        /* EA Specific Macros */
        public static string EA_CONNECT                        = "a string";
        public static string FILTER                            = "XML|*.xml";
     
        /* PLMXML Attributes Related Macros */
        public static string ID                                = "id";
        public static string NAME                              = "name";
        public static string VALUE                             = "value";
        public static string TITLE                             = "title";
        public static string ACTIONS                           = "actions";
        public static string LOCATION                          = "location";
        public static string DATA_REF                          = "dataRef";
        public static string RULE_REFS                         = "ruleRefs";
        public static string OBJECT_TYPE                       = "objectType";
        public static string ACTION_TYPE                       = "actionType";
        public static string RULE_QUORUM                       = "ruleQuorum";
        public static string RULE_HANDLER_REFS                 = "ruleHandlerRefs";
        public static string TRAVERSE_ROOT_REFS                = "traverseRootRefs";
        public static string ACTION_HANDLER_REFS               = "actionHandlerRefs";
        public static string DEPENDENCY_TASK_TEMPLATE_REFS     = "dependencyTaskTemplateRefs";

        /* PLMXML Attribute-Value Related Macros */
        public static string DECISION                          = "Decision";
        public static string ARG_DECISION                      = "-decision";
        public static string FAIL_DEPENDENCY_TASK_REF          = "failDependencyTaskRef";
        public static string EPM_TASK_TEMPLATE                 = "EPMTaskTemplate";
        public static string EPM_OR_TASK_TEMPLATE              = "EPMOrTaskTemplate";
        public static string EPM_DO_TASK_TEMPLATE              = "EPMDoTaskTemplate";
        public static string EPM_REVIEW_TASK_TEMPLATE          = "EPMReviewTaskTemplate";
        public static string EPM_CONDITION_TASK_TEMPLATE       = "EPMConditionTaskTemplate";
        public static string EPM_VALIDATE_TASK_TEMPLATE        = "EPMValidateTaskTemplate";
        public static string EPM_PERFORM_SIGNOFF_TASK_TEMPLATE = "EPMPerformSignoffTaskTemplate";
        public static string EPM_SELECT_SIGNOFF_TASK_TEMPLATE  = "EPMSelectSignoffTaskTemplate";

        /* PLMXML Nodes Related Macros */
        public static string HEADER                            = "Header";
        public static string ITEM                              = "Item";
        public static string USER_VALUE                        = "UserValue";
        public static string USER_DATA                         = "UserData";
        public static string ARGUMENT                          = "Arguments";
        public static string WORKFLOW_TEMPLATE                 = "WorkflowTemplate";
        public static string WORKFLOW_ACTION                   = "WorkflowAction";
        public static string WORKFLOW_HANDLER                  = "WorkflowHandler";
        public static string WORKFLOW_BUSINESS_RULE            = "WorkflowBusinessRule";
        public static string WORKFLOW_BUSINESS_RULE_HANDLER    = "WorkflowBusinessRuleHandler";

        /* Workflow Action Type Related Macros */
        public static string UNDO                              = "Undo:";
        public static string SKIP                              = "Skip:";
        public static string START                             = "Start:";
        public static string ABORT                             = "Abort:";
        public static string FINAL                             = "Final";
        public static string ASSIGN                            = "Assign:";
        public static string RESUME                            = "Resume:";
        public static string PERFORM                           = "Perform:";
        public static string ACTION                            = "Action";
        public static string ACTIONS_CAPSOFF                   = "actions";    
        public static string SUSPEND                           = "Suspend:";
        public static string COMPLETE                          = "Complete:";

        /* EAHelpers Related Macros */
        public static string EXTEND                            = "Extend";
        public static string FINISH                            = "Finish";
        public static string STATE_NODE                        = "StateNode";
        public static string CONTROL_FLOW                      = "ControlFlow";
        public static string DIAGRAM_NAME                      = "Logical Workflow Diagram";
        public static string DIAGRAM_TYPE                      = "Activity";
        public static string TASK_DETAILS                      = "Task Details:";
        public static string TASK_DESCRIPTION                  = "TaskDescription";
        public static string TASK_INSTRUCTIONS                 = "TaskInstructions: ";
        public static string STARTS_WITH_ACT                   = "Act";
        public static string STARTS_WITH_HANDLER               = "Handler";
        public static string STARTS_WITH_QUORUM                = "Quorum";
        public static string PREFIX_QUORUM_VALUE               = "QuorumValue: ";
        public static string PREFIX_HANDLER_NAME               = "HandlerName: ";
        public static string PREFIX_ACTION_TYPE                = "ActionType: ";
        public static string CONTAINS_START                    = "ActionType: 2";
        public static string CONTAINS_COMPLETE                 = "ActionType: 4";
        public static string CONTAINS_UNDO                     = "ActionType: 8";
        public static string CONTAINS_ASSIGN                   = "ActionType: 1";
        public static string CONTAINS_SKIP                     = "ActionType: 5";
        public static string CONTAINS_RESUME                   = "ActionType: 7";
        public static string CONTAINS_SUSPEND                  = "ActionType: 6";
        public static string CONTAINS_ABORT                    = "ActionType: 9";
        public static string CONTAINS_PERFORM                  = "ActionTYpe: 100";

        /* General String Macros */
        public static string BACKSPACE_START                   = "<b>";
        public static string BACKSPACE_END                     = "</b>";
        public static string NEWLINE                           = "\r\n";
        public static string ONE                               = "1";
        public static string TWO                               = "2";
        public static string FOUR                              = "4";
        public static string FIVE                              = "5";
        public static string SIX                               = "6";
        public static string SEVEN                             = "7";
        public static string EIGHT                             = "8";
        public static string NINE                              = "9";
        public static string HUNDRED                           = "100";
        public static string SEPARATOR_SPL_CHARACTER           = "§§§";
        public static string HORIZONTAL_TAB                    = "\t";
        public static string DASH                              = "-";
        public static string BULLET_POINT                      = "\u2022 ";
        public static string ASTERIK                           = "*";
        public static string BLANK                             = "";
        public const string BLANK_CONST                        = "";

        /* Separator Character Macros*/
        public static char ERROR_TOKEN_CONSTANT                = '$';
        public static char LIST_TOKEN_CONSTANT                 = '#';
        public static char ARG_TOKEN_CONSTANT                  = '=';
        public static char SEPARATOR_COLON                     = ':';

        /*EA Related  Integer Macros*/
        public static int FIRST_ELEMENT                        = 0;
        public static int TASK_SUB_TYPE                        = 0;
        public static int FINAL_SUB_TYPE                       = 101;
        public static int START_SUB_TYPE                       = 100;

        /* User Environment Variable */
        public static string ENV_TEMP                          = "TEMP";            
    }
}












