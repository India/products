﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec GmBH
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: ErrorConstant.cs

   Description: This File is specific to Error Constants to be used in entire application.

========================================================================================================================================================================

Date          Name          Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
01-Jan-18    Megha Singh    Initial Release
========================================================================================================================================================================*/

/* Class Description - This file contains error constants used in this project.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.constants
{
    class ErrorConstant
    {
         /* Parsing Error String Macros */     
        public static string ERROR_FILE_PATH                  = "C:\\EAProject\\err.xml";
        public static string ERROR_FILE_NAME                  = "Error.xml";
        public static string ERROR_GETTING_MAIN_AND_SUB_MENUS = "Error in getting main and sub menus";
        public static string SELECTED_REPOSITORY_IS_NULL      = "Selected repository is null";
        public static string SELECTED_OBJECT_IS_NOT_PACKAGE   = "Selected object is not package";
        public static string ERROR_IN_READING_XML_FILE        = "Error in reading XML file";
        public static string SELECT_FILE_TO_IMPORT            = "Select a file to import";      
        
        /* Custom Exception String Macros */
        public static string CUSTOM_ERROR_CODE_1              = "LIN-0001";
        public static string CUSTOM_ERROR_CODE_2              = "LIN-0002";
        public static string CUSTOM_ERROR_CODE_3              = "LIN-0003";
        public static string CUSTOM_ERROR_CODE_4              = "LIN-0004"; 
        public static string CUSTOM_ERROR_CODE_5              = "LIN-0005";
        public static string CUSTOM_ERROR_CODE_6              = "LIN-0006";

        /* Log File Exception String Macros */
        public static string LOG_SELECT_RELEVANT_FILE         = "Select a relevant file";
        public static string LOG_RELEVANT_FILE_NOT_SELECTED   = "Relevant file is not selected"; 
       
        /* Custom Error constants of string */
        public static string CUSTOM_ERROR_STRING_1            = "$relevant"; 
    }
}





