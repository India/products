﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EADiagObjConnect.cs

   Description: This File is specific to Diagram Connectors.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
12-Jan-18     Megha Singh         Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains functions which connects two diagram objects.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.eahelpers
{
    class EADiagObjConnect
    {
        /* This function connects two diagramobjects and adds label on the connector. The argument 'strLabelText' contains the label to be added on connector. */
        public void connectDiagramObj(EA.Element elemSource, EA.Element elemTarget, string strLabelText, string strConnectorType)
        {
            try
            {
                /* Adding connector on source element. */
                EA.Connector objConnect = elemSource.Connectors.AddNew(strLabelText, strConnectorType);

                objConnect.SupplierID   = elemTarget.ElementID;

                objConnect.Update();

                elemSource.Connectors.Refresh();
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}














