﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EANotes.cs

   Description: This File is specific to Notes .

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - The functions in this class generate notes specific to a WFTemplate task element by traversing all the elements of list (ltTaskHandlers).
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.containers;
using EAProject.constants;
using EAProject.utilities;

namespace EAProject.eahelpers
{
    class EANotes
    {
        StringUtilities utilString  = new StringUtilities();
        EAActionNotes   notesAction = new EAActionNotes();

        /* This function creates a new list variable and copies elements of list variable(ltTaskHandlers) to another list variable 'ltCopyTemHandler'. Copying is done to avoid the changes done in list
         * from affecting the actual info stored in the member variable of class WFTemplateInfo. It also adds required handlers info to Notes
         * represented by string variable(strTskDtl). It also checks all the 8 actiontypes and extracts the Handlers info corresponding to the actiontype found in the list by calling the function
         * NotesActionType and also checks if the same actiontype further exists in the list and concatenates all the Handlers info by catagorizing them according to their actiontypes in string 
         * variable(strTskDtl). 
         **/
        public void addNotes(WFTemplateInfo infoWFTemplate, EA.Element element)
        {
            try
            {
                List<string> ltCopyTemHandler = new List<string>();
                
                /* Adding the manadatory strings which remain constant for every task to the top of Notes(strTskDtl). */
                string strTskInstr      = Constants.BACKSPACE_END + Constants.TASK_INSTRUCTIONS + Constants.BACKSPACE_END;
                string strTskInstrNotes = String.Concat(strTskInstr, infoWFTemplate.strTaskInstruction + Constants.NEWLINE + Constants.NEWLINE);
                string strTskDtl        = String.Concat(strTskInstrNotes, (Constants.BACKSPACE_END + Constants.TASK_DETAILS + Constants.BACKSPACE_END + Constants.NEWLINE));

                if (infoWFTemplate.ltTaskHandlers != null)
                {
                    /* Copying the elements of list variable(ltTaskHandlers) of class WFTemplateInfo to list variable(ltCopyTemHandler). Copying of elements is done to avoid the original data to be
                     * affected by changes(deletion of elements) done in copied list(ltCopyTemHandler). */
                    ltCopyTemHandler.AddRange(infoWFTemplate.ltTaskHandlers);

                    int iTskHandlersCount = ltCopyTemHandler.Count;

                    if (iTskHandlersCount != 0)
                    {
                        /* Loop for traversing 8 actiontypes . */
                        for (int iDx = 0; iDx <= 8; iDx++)
                        {                            
                            if (ltCopyTemHandler.Count != 0)
                            {
                                /* Calling function to get WFAction Types info. The function takes the argument - index of Action types(iDx), list Handlers info(ltCopyTemHandler) and refrence of
                                 * taskDetail string variable to add required handlers info to notes on WFTemplate elements. */
                                getActionType(iDx, ltCopyTemHandler, ref strTskDtl);                                
                            }                            
                        }
                    }
                }

                element.Notes = strTskDtl; /* Adding notes present in string variable(strTskDtl) to a WFTemplate element. */

                element.Update();
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function checks if the WFActionType(strActionType) passed as argument is present in list 'ltTaskHandlers' which contains handlers info. If this WFAction Type is again found in list than its
         * index is evaluated and all the Handlers and their required arguments info are added to (strTskDtl) variable. This is done to bring together all the handlers and their arguments belonging to same 
         * WFAction type present on a WFTemplate node. And if the WFAction type 'strActionType' is not found than the function returns to its calling function. 
         **/
        public void checkActionTypReAppears(ref List<string> ltTaskHandlers, string strActionType, ref string strTskDtl)
        {
            try
            {
                EAHandlerNotes notesHandlers = new EAHandlerNotes();
                                
                if (ltTaskHandlers.Count != 0)
                {
                    /* Checks if list variable(ltTaskHandlers) contains WFActiontype (strActionType). */
                    if (ltTaskHandlers.Contains(strActionType) == true)
                    {                        
                        int iStartIndex = ltTaskHandlers.IndexOf(strActionType);

                        /* Function infoActionType is called and references of list variable(ltTaskHandlers), index of actiontype in list(iStartIndex), string containing task details to be shown as Notes in EA
                         * are passed. Also strActionType variable is also passed as argument.*/
                        notesHandlers.getHandlerNotes(ref ltTaskHandlers, ref iStartIndex, ref strTskDtl, strActionType);
                                                
                        if (ltTaskHandlers.Contains(strActionType) == true)
                        {
                            /* If the actiontype is found in list than this function checkActionTypReAppears is called recursively. */
                            checkActionTypReAppears(ref ltTaskHandlers, strActionType, ref strTskDtl);
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        /* Function returns if the list doesnot contains the actiontype represented by (strActionType). */
                        return;
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }                   

        /* This function gets the ActionType info present on WFTemplate. Argument - 'iIndex' states the ActionType to be checked, ltCopyHandler contains the Handlers info present on a WFTemplate node.
         * and reference argument 'strTskDtl' contains the handlers required info to be represented on notes of WFTemplte element. */
        public void getActionType(int iIndex, List<string> ltCopyTemHandler, ref string strTskDtl)
        {
            try
            {
                if (iIndex == 0)
                {
                    /* Checking if list contains element 'Assign' actiontype represented by integer value 1 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_ASSIGN) == true) 
                    {
                        /* Passing reference of list containing Handlers info and string variable strTskdtl to store Handlers Info. Also actionType is passed to function NotesActionType. */
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_ASSIGN, ref strTskDtl);

                        /* Calling the function to check if the WFAction type 'Assign' is present again in list containing handlers info(ltCopyTemHandlers). */
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_ASSIGN, ref strTskDtl);
                    }
                }
                else if (iIndex == 1)
                {
                    /* Checking if list contains element 'Start' actiontype.It is represented by integer value 2 in TC.  */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_START) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_START, ref strTskDtl);
                                                
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_START, ref strTskDtl);
                    }
                }
                else if (iIndex == 2)
                {
                    /* Checking if list contains element 'Complete' actiontype' represented by integer value 4 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_COMPLETE) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_COMPLETE, ref strTskDtl);
                                            
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_COMPLETE, ref strTskDtl);
                    }
                }
                else if (iIndex == 3)
                {
                    /* Checking if list contains element 'Skip' actiontype reprsesented by integer value 5 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_SKIP) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_SKIP, ref strTskDtl);
                                               
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_SKIP, ref strTskDtl);
                    }
                }
                else if (iIndex == 4)
                {
                    /* Checking if list contains 'Suspend' actiontype represented by integer value 6 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_SUSPEND) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_SUSPEND, ref strTskDtl);
                                                
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_SUSPEND, ref strTskDtl);
                    }
                }
                else if (iIndex == 5)
                {
                    /* Checking if list contains 'Resume' actiontype represented by integer value 7 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_RESUME) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_RESUME, ref strTskDtl);
                                                
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_RESUME, ref strTskDtl);
                    }
                }
                else if (iIndex == 6)
                {
                    /* Checking if list contains 'Undo' actiontype represented by integer value 8 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_UNDO) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_UNDO, ref strTskDtl);
                                                
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_UNDO, ref strTskDtl);
                    }
                }
                else if (iIndex == 7)
                {
                    /* Checking if list contains 'Abort' actiontype represented by integer value 9 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_ABORT) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_ABORT, ref strTskDtl);
                                                
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_ABORT, ref strTskDtl);
                    }
                }
                else if (iIndex == 8)
                {
                    /* Checking if list contains 'Abort' actiontype represented as integer value 100 in TC. */
                    if (ltCopyTemHandler.Contains(Constants.CONTAINS_PERFORM) == true)
                    {
                        notesAction.getActionNotes(ref ltCopyTemHandler, Constants.CONTAINS_PERFORM, ref strTskDtl);

                        /* Calling the function to check if the WFAction type 'Perform' is present again in list containing handlers info(ltCopyTemHandlers). */
                        checkActionTypReAppears(ref ltCopyTemHandler, Constants.CONTAINS_PERFORM, ref strTskDtl);
                    }
                }
            }
            catch(Exception exException)
            {
                throw exException;
            }
        }
    }
}





































      


















