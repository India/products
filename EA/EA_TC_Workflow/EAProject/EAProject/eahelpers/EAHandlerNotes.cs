﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAHandlerNotes.cs

   Description: This File is specific to handler notes.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
13-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which generates notes for elements from information present on handlers of a WFTaskTemplate node.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.constants;
using EAProject.utilities;

namespace EAProject.eahelpers
{
    class EAHandlerNotes
    {
        /* The function traverses all the elements which belong to a particular Actiontype(strActionType) and stores the required Handlers info in string(strTaskDtl). Arguments of function are - 
         * Reference variables- 'ltTaskHandlers' contains Handlers info to be traversed, 'iStartIndex' contains index of element , 'strTaskDtl' contains details to be added as notes on Element of
         * handlers present on task Template. Also string varaible 'strActionType' is passed as argument.
         **/       
        public void getHandlerNotes(ref List<string> ltTaskHandlers, ref int iStartIndex, ref string strTskDtl, string strActionType)
        {
            try
            {
                StringUtilities utilString = new StringUtilities();

                if (ltTaskHandlers.Count != 0)
                {
                    /* Checking if list(ltTaskHandlers) is not null and also the value of iStartIndex(containing specific actiontype Handlers info) and list length(list count) is not equal.
                     * Also through second condition checking if the element at index represented by iStartIndex is not last element of list. */
                    while (ltTaskHandlers.Count > 0 && ltTaskHandlers.Count != iStartIndex)
                    {
                        if (String.Compare(ltTaskHandlers[iStartIndex], Constants.SEPARATOR_SPL_CHARACTER) != 0) /* Checking if 'iStartIndex' contains special character(§§§). Any two handlers in list(ltTaskHandlers) are separated by special characters. */
                        {
                            if (ltTaskHandlers[iStartIndex].StartsWith(Constants.STARTS_WITH_QUORUM))  /* Checking if element at index represented by iStartIndex starts with string(QuorumValue: ) */
                            {
                                /* Tokenizing the element in list starting with string(Quorum). Colon ':' is passed as delimiter to tokenize the string.*/
                                string[] strSplit = utilString.stringTokenize(ltTaskHandlers.ElementAt(iStartIndex), Constants.SEPARATOR_COLON);

                                /* Concatenating the second token containing quorum value to string(strTaskDtl). */
                                strTskDtl = String.Concat(strTskDtl, (Constants.BACKSPACE_START + Constants.RULE_QUORUM + Constants.BACKSPACE_END + strSplit[1] + Constants.NEWLINE));
                            }
                            else if (ltTaskHandlers[iStartIndex].StartsWith(Constants.STARTS_WITH_HANDLER))  /* Checking if element at 'iStartIndex' starts with prefix string(Handler). In list(ltTaskHandlers) names of all Handlers start with prefix string 'HandlerName:' */
                            {
                                /* Tokenizing the element with ':' as delimiter. */
                                string[] strSplit = utilString.stringTokenize(ltTaskHandlers.ElementAt(iStartIndex), Constants.SEPARATOR_COLON);

                                /* Concatenating the second token to string(strTaskDtl). */
                                strTskDtl = String.Concat(strTskDtl, (Constants.BULLET_POINT + strSplit[1] + Constants.NEWLINE));
                            }
                            else if (ltTaskHandlers[iStartIndex].StartsWith(Constants.DASH))  /* Checking if element at 'iStartIndex' starts with character(-). All arguments of Handlers in List have (-) as prefix attached to them. */
                            {
                                /* Concatenating Handlers argument to string(strTaskDtl).  */
                                strTskDtl = String.Concat(strTskDtl, (Constants.HORIZONTAL_TAB + ltTaskHandlers.ElementAt(iStartIndex) + Constants.NEWLINE));
                            }
                            else if (ltTaskHandlers[iStartIndex].StartsWith(Constants.STARTS_WITH_ACT))  /* Checking if element at 'iStartIndex' starts with prefix string(Act). All the actionTypes in list are prefixed by string 'ActionType: '.*/                                                           
                            {
                                string[] strSplit = utilString.stringTokenize(ltTaskHandlers.ElementAt(iStartIndex), Constants.SEPARATOR_COLON);

                                if (String.Compare(strSplit[1], strActionType) == 0) /* Checking if the element contains the same actiontype as represented by argument(strActionType) passed to this function. */
                                {
                                    /* Removing the element at index(iStartIndex)  to avoid traversing it again */
                                    ltTaskHandlers.Remove(ltTaskHandlers.ElementAt(iStartIndex));

                                    /* Recursively calling the function infoActionType to get handlers info present below the same actiontype as passed as argument to this function initially from 
                                     * notesActionTypes function. */
                                    getHandlerNotes(ref ltTaskHandlers, ref iStartIndex, ref strTskDtl, strActionType);

                                    return;
                                }                               
                                else
                                {
                                    /* Function returns when element starts with prefix string (Action) but has a different value as compared to (strActionType) passed as argument */
                                    return;
                                }
                            }
                        }

                        ltTaskHandlers.Remove(ltTaskHandlers.ElementAt(iStartIndex));  /* Removing the traversed element present at index(iStartIndex) to avoid traversing it again in list. */
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}



























