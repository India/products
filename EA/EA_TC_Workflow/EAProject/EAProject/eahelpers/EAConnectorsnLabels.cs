﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAConnectorsnLabels.cs

   Description: This File is specific to Diagram Connectors and labels.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains functions which connect two diagram objects with the help of Connectors and adds label to connector based on type of DiagramObjects 
 * connected.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.containers;
using EAProject.constants;
using EAProject.appdelegate;
using EAProject.utilities;

namespace EAProject.eahelpers
{
    class EAConnectorsnLabels
    {
        EADiagObjConnect connectDiagObj = new EADiagObjConnect();
        StringUtilities  utilString     = new StringUtilities();
        EALabels         findLabels     = new EALabels();
        /* This function gets the source and destination element. It also gets the label to be added on connector and connects the two elements with the 
         * help of Connector and adds a label on Connector.
         **/
        public void connectDiagramObj(Dictionary<string, EA.Element> dictElement, Dictionary<string, WFTemplateInfo> dictTemplateInfo)
        {
            try
            {                                             
                if (dictTemplateInfo.Count != 0)
                {
                    for (int iDx = 1; iDx < dictElement.Count; iDx++)
                    {
                        EA.Element elemSource      = AppDelegate.statSelectedPackage.Element;

                        EA.Element elemDestination = AppDelegate.statSelectedPackage.Element;

                        string strDestKey = dictElement.Keys.ElementAt(iDx);

                        dictElement.TryGetValue(strDestKey, out elemDestination);

                        List<string>     ltIDStartDepend   = new List<string>();

                        List<string>     ltIDFailDepend    = new List<string>();
                        
                        List<EA.Element> ltElemStartDepend = new List<EA.Element>();
                        
                        List<EA.Element> ltElemFailDepend  = new List<EA.Element>();
                        
                        /* Checks if the object dictTemplateInfo contains the key(strDestKey) */
                        if (dictTemplateInfo.ContainsKey(strDestKey) == true && String.Compare(strDestKey, Constants.FINAL) != 0)
                        {                     
                            /* Calling function to get the IDs of Fail and Start Dependency task present on a WFTemplate node. */
                            getDependencyTaskIDs(strDestKey, dictElement, dictTemplateInfo, ref ltIDStartDepend, ref ltIDFailDepend);

                            /* Calling function to get the element equivalent of all the WFTemplate ids present in ltIDStartDepend and ltIDFailDepend. */
                            getDependencyTaskElems(dictElement, ltIDStartDepend, ltIDFailDepend, ref ltElemFailDepend, ref ltElemStartDepend);
                            
                            /* This condition particularly arises in case of Review task especially when it is the second element to be created in diagram. Therefore value of 'iDx' is checked to be '1'.  */
                            if (ltElemStartDepend.Count == 0 && ltElemFailDepend.Count == 0)
                            {
                                if (iDx == 1) 
                                {
                                    /* Here sourcekey is the element present at 0th index in dictElement dictionary. */
                                    String strSrcKey = dictElement.Keys.ElementAt(iDx - 1);

                                    /* Getting EA.Element(value) corresponding to key(strSrcKey) from dictionary 'dictElement'. */
                                    dictElement.TryGetValue(strSrcKey, out elemSource);

                                    /* Getting EA.Element(value) corresponding to key(strDestKey) from dictionary 'dictElement'. */
                                    dictElement.TryGetValue(strDestKey, out elemDestination);

                                    /* Calling the function to connect both the source and destination elements. Since both the list (ltElemStartDepend and ltElemFailDepend) containing connector type info is null, therefore
                                     * the default connector type 'ContolFlow' is passed. */
                                    connectDiagObj.connectDiagramObj(elemSource, elemDestination, null, Constants.CONTROL_FLOW);
                                }                                 
                            }
                            else
                            {                                
                                connectTasks(elemDestination, dictElement, ltElemStartDepend, ltElemFailDepend, dictTemplateInfo);    
                            }
                        }                        
                        else if (String.Compare(strDestKey, Constants.FINAL) == 0) /* Check for 'Finish Task'. */
                        {
                            /* Getting element at secondlast position in 'dictElement' dictionary. */
                            String strSrcKey  = dictElement.Keys.ElementAt(dictElement.Count - 2);

                            /* Getting element at last position in 'dictElement' dictionary. */
                            strDestKey = dictElement.Keys.ElementAt(dictElement.Count - 1);
                           
                            /* Getting EA.Element values for(source and destination) corresponding to keys(strSrcKey and strDestKey) which contains ids of WFTemplate. */
                            dictElement.TryGetValue(strSrcKey, out elemSource);

                            dictElement.TryGetValue(strDestKey, out elemDestination);
                            
                            /* Calling function to connect source and dectination element objects in Diagram. */
                            connectDiagObj.connectDiagramObj(elemSource, elemDestination, null, Constants.CONTROL_FLOW);                           
                        }                        
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }              
        
        /* This function gets the list of StartDependency and FailDependency IDs in the reference list arguments - StartDepend and FailDepend  WFTemplate node. */
        public void getDependencyTaskIDs(string strDestKey, Dictionary<string, EA.Element> dictElement, Dictionary<string, WFTemplateInfo> dictTemplateInfo, ref List<string> ltStartDepend, ref List<string> ltFailDepend)
        {
            try
            {
                if (dictTemplateInfo.ContainsKey(strDestKey) == true && String.Compare(strDestKey, Constants.FINAL) != 0)
                {
                    WFTemplateInfo infoDestWFTpl = new WFTemplateInfo();

                    dictTemplateInfo.TryGetValue(strDestKey, out infoDestWFTpl);

                    if (infoDestWFTpl.dictTaskDependTaskRef.Count != 0)
                    {
                        int iTaskDepCount = infoDestWFTpl.dictTaskDependTaskRef.Count;

                        for (int iFx = 0; iFx < iTaskDepCount; iFx++)
                        {
                            /* Calling function to get list of FailDepend and StartDepend ids on WFTemplate node. */
                            getDependIds(infoDestWFTpl, iFx, dictElement, dictTemplateInfo, ref ltFailDepend, ref ltStartDepend);                            
                        }
                    }
                }                
            }
            catch(Exception exException)
            {
                throw exException;
            }
        }
        
        /* The function gets the lists(ltIDFailDepend and ltIDStartDepend) from the object of class WFTemplateInfo. It also calls the function to check if this id and its equivalent element is present
         * in dictionaries (dictElement and dictTemplateInfo). And if not found the elemnt is removed from list. */
        public void getDependIds(WFTemplateInfo infoDestWFTpl, int iIndex, Dictionary<string, EA.Element> dictElement, Dictionary<string, WFTemplateInfo> dictTemplateInfo, ref List<string> ltIDFailDepend, ref List<string> ltIDStartDepend)
        {
            try
            {
                foreach (List<string> value in infoDestWFTpl.dictTaskDependTaskRef.Values)
                {
                    if (value.Count != 0)
                    {
                        if (String.Compare(infoDestWFTpl.dictTaskDependTaskRef.Keys.ElementAt(iIndex), Constants.FAIL_DEPENDENCY_TASK_REF) == 0)
                        {
                            infoDestWFTpl.dictTaskDependTaskRef.TryGetValue(Constants.FAIL_DEPENDENCY_TASK_REF, out ltIDFailDepend);                                        

                            if (ltIDFailDepend.Count != 0)
                            {
                                checkDependIDs(dictTemplateInfo, dictElement, ref ltIDFailDepend);
                            }
                        }
                        else
                        {
                            infoDestWFTpl.dictTaskDependTaskRef.TryGetValue(infoDestWFTpl.dictTaskDependTaskRef.Keys.ElementAt(iIndex), out ltIDStartDepend);

                            if (ltIDStartDepend.Count != 0)
                            {
                                checkDependIDs(dictTemplateInfo, dictElement, ref ltIDStartDepend);                               
                            }
                        }
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function removes the ids from list 'ltIDDestDepend' which are not present as keys in dictionaries 'dictElement' and 'dictTemplateInfo'. */
        public void checkDependIDs(Dictionary<string, WFTemplateInfo> dictTemplateInfo, Dictionary<string, EA.Element> dictElement, ref List<string> ltIDDestDepend)
        {
            try
            {
                for (int iDx = 0; iDx < ltIDDestDepend.Count; iDx++)
                {
                    if (dictElement.ContainsKey(ltIDDestDepend[iDx]) == false && dictTemplateInfo.ContainsKey(ltIDDestDepend[iDx]) == false)
                    {
                        ltIDDestDepend.RemoveAt(iDx);
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function calls functions to get list of elements against all the ids present in lists(ltIDStartDepend and ltIDFailDepend). */
        public void getDependencyTaskElems(Dictionary<string, EA.Element> dictElement, List<string> ltIDStartDepend, List<string> ltIDFailDepend, ref List<EA.Element> ltElemFailDepend, ref List<EA.Element> ltElemStartDepend)
        {
            try
            {
                if (ltIDStartDepend.Count != 0)
                {
                    /* Getting list of elements(leElemStartDepend) for all the ids present in list(ltIDStartDepend). */
                    getAllDependencyTasks(ltIDStartDepend, dictElement, ref ltElemStartDepend);                    
                }
              
                if (ltIDFailDepend.Count != 0)
                {
                    /* Getting list of elements(leElemFailDepend) for all the ids present in list(ltIElemDepend). */
                    getAllDependencyTasks(ltIDFailDepend, dictElement, ref ltElemFailDepend);                    
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function traverses all Ids present in 'ltIDDest' and gets its element equivalent from dictionary 'dictElement' and adds the element to the list 'ltElemDest'. */
        public void getAllDependencyTasks(List<string> ltIDDest, Dictionary<string, EA.Element> dictElement, ref List<EA.Element> ltElemDest)
        {
            try
            {
                for (int iDx = 0; iDx < ltIDDest.Count; iDx++)
                {
                    EA.Element elemDepend = AppDelegate.statSelectedPackage.Element;

                    if (dictElement.ContainsKey(ltIDDest[iDx]) == true)
                    {
                        dictElement.TryGetValue(ltIDDest[iDx], out elemDepend);

                        ltElemDest.Add(elemDepend);
                    }
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
        
        /* This function calls the functions to get Labels for destination element 'elemDest' by getting the WFTemplate class object from dictionary(dictTemplateInfo). Every element present in Lists
         * (ltElemStartDepend and ltElemStartDepend) is traversed. */
        public void connectTasks(EA.Element elemDest, Dictionary<string, EA.Element> dictElement, List<EA.Element> ltElemStartDepend, List<EA.Element> ltElemFailDepend, Dictionary<string, WFTemplateInfo> dictTemplateInfo)
        {
            try
            {
                EADiagObjConnect conDiagObj         = new EADiagObjConnect();
                EALabels         findLabels         = new EALabels();
                WFTemplateInfo   infoDestinationTpl = new WFTemplateInfo();

                String strDest = dictElement.FirstOrDefault(x => x.Value == elemDest).Key;
                dictTemplateInfo.TryGetValue(strDest, out infoDestinationTpl);

                if (ltElemStartDepend.Count != 0)
                {
                    connectTasksGetLabel(ltElemStartDepend, elemDest, infoDestinationTpl, dictElement, dictTemplateInfo, Constants.CONTROL_FLOW);
                }
                if (ltElemFailDepend.Count != 0)
                {
                    connectTasksGetLabel(ltElemFailDepend, elemDest, infoDestinationTpl, dictElement, dictTemplateInfo, Constants.EXTEND);  
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* The function calls the functions to gets object of source class WFTemplateInfo, gets label for connectors and connects both source and destination diagram objects. */
        public void connectTasksGetLabel(List<EA.Element> ltElemDestObj, EA.Element elemDest, WFTemplateInfo infoDestinationTpl, Dictionary<string, EA.Element> dictElement, Dictionary<string, WFTemplateInfo> dictTemplateInfo, string strConnectType)
        {
            try
            {
                for (int iDx = 0; iDx < ltElemDestObj.Count; iDx++)
                {
                    string         strLabelTxt   = null;
                    WFTemplateInfo infoSourceTpl = new WFTemplateInfo();
                    String         strSrc        = dictElement.FirstOrDefault(x => x.Value == ltElemDestObj[iDx]).Key;

                    /* Getting object of source WFTemplate class. */
                    dictTemplateInfo.TryGetValue(strSrc, out infoSourceTpl);

                    /* Getting label for connector */
                    findLabels.getLabel(infoDestinationTpl, infoSourceTpl, ref strLabelTxt);

                    /* Connecting the source and destination diagram object elements. */
                    connectDiagObj.connectDiagramObj(ltElemDestObj[iDx], elemDest, strLabelTxt, strConnectType);
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}


































































