﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAActionNotes.cs

   Description: This File is specific to ActionType notes.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains functions which get notes related to actiontype(Start, Complete,...) of a WFTaskTemplate. 
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.constants;
using EAProject.containers;
using EAProject.utilities;

namespace EAProject.eahelpers
{
    class EAActionNotes
    {
        /* This function finds the element starting with prefix(ActionType:)in list(ltTaskHandlers) which contains handlers info. The element found is tokenized and the second token containing 
         * actiontype info is added to string variable(strTaskDtl). Also the function 'getHandlerNotes' is called to add details of handlers to the string variable(strTaskDtl) containing notes. 
         * Arguments of this function are - References of list(string) variable(ltTaskHandlers) and string variable(strTaskDtl). Also string variable representing actiontype(strActionType) are
         * passed.
         **/
        public void getActionNotes(ref List<string> ltTaskHandlers, string strActionType, ref string strTskDtl)
        {
            try
            {
                EAHandlerNotes  notesHandlers = new EAHandlerNotes();
                StringUtilities utilString    = new StringUtilities();

                /* Finds first occurence of element starting with actiontype(strActionType) in list(ltTaskHandlers). */
                var vFirstOccurence = ltTaskHandlers.FirstOrDefault(s => s.StartsWith(Constants.PREFIX_ACTION_TYPE));
                                
                if (vFirstOccurence != null)
                {
                    /* Finds index of actiontype(strActionType) inlist(ltTaskHandlers). */
                    int iStartIndex = ltTaskHandlers.IndexOf(strActionType);

                    /* Tokenizes the element starting with prefix (ActionType: ) with delimiter (:). */
                    string[] strActionArgSplit = utilString.stringTokenize(strActionType, Constants.SEPARATOR_COLON);

                    getActionNote(strActionArgSplit[1], ref strTskDtl);

                    /* Removing th element which starts with prefix(ActionType: ) from list to avoid reading it again.*/
                    ltTaskHandlers.Remove(ltTaskHandlers.ElementAt(iStartIndex));

                    /* Calling infoActionType function to add Handlers information present on this actiontype(strActionArgSplit[1]). References of list(ltTaskHandlers), index of actiontype(iStartIndex),
                     * string containing taskDetails(strTaskDtl) are passed as arguments. Also varaible actiontype(strActionArgSplit[1]) is passed as argument*/
                    notesHandlers.getHandlerNotes(ref ltTaskHandlers, ref iStartIndex, ref  strTskDtl, strActionArgSplit[1]);
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
        
        /* This function adds the actiontype information of a WFTemplate node to 'strTskDlt' variable. A reference of this variable is passed since it contains notes to be added to each element. */
        public void getActionNote(string strActionArg, ref string strTskDtl)
        {
            try
            {
                if (String.Compare(strActionArg, Constants.ONE) == 0)  /* Checks if the value of inputargument 'strActionArg' is '1' as it represents 'Assign' actiontype. */
                {
                    /* Concatenating actiontype value to string(strTaskDtl) containing tasks detail.  */
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.ASSIGN + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.TWO) == 0) /* Checks if the value of inputargument 'strActionArg' is '2' as it represents 'Start' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.START + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.FOUR) == 0)  /* Checks if the value of inputargument 'strActionArg; is '4' as it represents 'Complete' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.COMPLETE + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.FIVE) == 0)  /* Checks if the value of inputargument 'strActionArg' is '5' as it represents 'Skip' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.SKIP + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.SIX) == 0)  /* Checks if the value of inputargument 'strActionArg' is '6' as it represents 'Suspend' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.SUSPEND + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.SEVEN) == 0) /* Checks if the value of inputargument 'strActionArg' is '7' as it represents 'Resume' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.RESUME + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.EIGHT) == 0) /* Checks if the value of inputargument 'strActionArg' is '8' as it represents 'Undo' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.UNDO + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.NINE) == 0)  /* Checks if the value of inputargument 'strActionArg' is '9' as it represents 'Abort' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.ABORT + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
                else if (String.Compare(strActionArg, Constants.HUNDRED) == 0) /* Checks if the value of inputargument 'strActionArg' is '100' as it represents 'Perform' actiontype. */
                {
                    strTskDtl = String.Concat(strTskDtl, (Constants.NEWLINE + Constants.BACKSPACE_START + Constants.PERFORM + Constants.BACKSPACE_END + Constants.NEWLINE));
                }
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}

























