﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EADiagram.cs

   Description: This File is specific to EA diagrams.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which adds diagram to package.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.appdelegate;
using EAProject.constants;

namespace EAProject.eahelpers
{
    class EADiagram
    {
        /* This function adds diagram to the stored selected package at app delegate. This function is not safe for duplicay to diagrm with same name
        * inside package. */
        public EA.Diagram addDiagram()
        {
            try
            {
                EA.Diagram eaDiagram = (AppDelegate.statSelectedPackage.Diagrams).AddNew(Constants.DIAGRAM_NAME, Constants.DIAGRAM_TYPE);

                eaDiagram.Update();

                return eaDiagram;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}







































