﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EADiagramObject.cs

   Description: This File is specific to Elements.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes functions which create diagramObjects and add diagramobjects to diagram.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.containers;
using EAProject.constants;
using EAProject.utilities;
using EAProject.appdelegate;

namespace EAProject.eahelpers
{
    class EADiagramObject
    {
        /* This function adds diagramObjects to diagram based on the objectType of WFTemplate, connects them and adds label to connectors. */
        public void insertDiagramObjects(string strAttrId, WFTemplateInfo infoWFTemplate, ref int iDx, ref Dictionary<string, EA.Element> dictElement, Dictionary<string, WFTemplateInfo> dictTemplateInfo)
        {
            try
            {
                EAConnectorsnLabels lablesNConnectors = new EAConnectorsnLabels();

                /* Checking if WFTemplate is not 'Perform Signoff task' or ' Select Signoff task' template. Since these tasks form sub workflows. */
                if (String.Compare(infoWFTemplate.strTaskObjectType, Constants.EPM_PERFORM_SIGNOFF_TASK_TEMPLATE) != 0 && String.Compare(infoWFTemplate.strTaskObjectType, Constants.EPM_SELECT_SIGNOFF_TASK_TEMPLATE) != 0)
                {
                    if (iDx == 0) /* Checking for Start Task which is also first task template. */
                    {
                        /* Adding 'Start' diagobj to diagram */
                        createDiagram(infoWFTemplate, infoWFTemplate.strTaskName, Constants.STATE_NODE, Constants.START_SUB_TYPE, Constants.START, ref dictElement, strAttrId);
                    }
                    else if (String.Compare(infoWFTemplate.strTaskObjectType, Constants.EPM_CONDITION_TASK_TEMPLATE) == 0) /* Checking if template is Condition Task Template */
                    {
                        /* Adding 'Decision' diagramobj to diagram */
                        createDiagram(infoWFTemplate, infoWFTemplate.strTaskName, Constants.DECISION, Constants.TASK_SUB_TYPE, Constants.START, ref dictElement, strAttrId);
                    }
                    else
                    {
                        /* Add 'Action' activity to diagram */
                        createDiagram(infoWFTemplate, infoWFTemplate.strTaskName, Constants.ACTION, Constants.TASK_SUB_TYPE, Constants.START, ref dictElement, strAttrId);
                    }

                    /* Checking if the value of iDx incremented by 1 and count of elements in dictionary becomes equal. 'iDx' has been incremented by one since the initial value of 'iDx' is 0 at which
                     * Start task is added. Since after all tasks of WF are added, Final task must be added so the WFtemplate count is incremented by one to obtain the index of Final task. */                      
                    if ((iDx + 1) == dictTemplateInfo.Count)
                    {                        
                        createDiagram(infoWFTemplate, Constants.FINISH, Constants.STATE_NODE, Constants.FINAL_SUB_TYPE, Constants.FINISH, ref dictElement, Constants.FINAL);
                        
                        /* Calling the function to connect all diagramobjects. Arguments - Dictinary<string, EA.Element> containing 'WFTemplate id' as key and 'Element id' corresponding to every WFTemplate
                         * node as 'Value'. And Dictionary<string  WFTemplate infoWFTemplate> has 'WFTemplate id' as key and value as the 'object of class WFTemplate'. */
                        lablesNConnectors.connectDiagramObj(dictElement, dictTemplateInfo);
                    }
                }

                iDx++;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }

        /* This function adds element to selected package and further adds notes to the element created. */
        public void createDiagram(WFTemplateInfo infoWFTemplate, string strTemplateName, string strElemTaskType, int iSubType, string strDiagObjTaskType, ref Dictionary<string, EA.Element> dictElement, string strAttrId)
        {
            try
            {
                EANotes notes = new EANotes();

                /* Element is added to the selected package with the element name represented by the variable 'strTemplateName' and element type represented by 'strElemTaskType'. */
                EA.Element element = AppDelegate.statSelectedPackage.Elements.AddNew(strTemplateName, strElemTaskType);
               
                if (iSubType != 0)
                {
                    element.Subtype = iSubType;
                }
                                
                if (String.Compare(strTemplateName, Constants.FINISH) != 0)
                {
                    /* Notes are added to all elements except for Finish tasktemplate element. */
                    notes.addNotes(infoWFTemplate, element);
                }
                
                AppDelegate.statSelectedPackage.Elements.Refresh();

                /* Adding DiagramObjects to Diagram with name represented by the varaible 'strDiagObjTaskType' which contains the type of task. */
                EA.DiagramObject objDiagram = AppDelegate.statSelectedDiagram.DiagramObjects.AddNew(strDiagObjTaskType, Constants.BLANK);

                objDiagram.ElementID = element.ElementID;

                objDiagram.DiagramID.ToString();

                objDiagram.Update();

                /* Adding the created task element to the dictionary. The structure of Dictionary is (<string, EA.Element>) string is the key which is represented by strAttrId(id of source task)
                 * EA.Element is the value which is represented by element(element of source task). */ 
                 dictElement.Add(strAttrId, element);
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }             
    }
}

















































































    

