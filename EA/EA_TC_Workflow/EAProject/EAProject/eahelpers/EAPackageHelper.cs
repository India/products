﻿/*======================================================================================================================================================================
                                       Copyright 2017  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EAPackageHelper.cs

   Description: This File is specific to Packages.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class contains function which returns a List containing all child Packages present below the input 'inpPackage'. 
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAProject.eahelpers
{
    class EAPackageHelper
    {
        public List<EA.Package> getChildPackage(EA.Package inpPackage)
        {
            try
            {
                List<EA.Package> ltChildPackage = new List<EA.Package>();
                
                if (inpPackage != null && inpPackage.Packages.Count != 0)
                {
                    for (int iDx = 0; iDx < inpPackage.Packages.Count; iDx++)
                    {
                        /* Getting Package at index(iDx) */
                        EA.Package tempPackage = inpPackage.Packages.GetAt((short)iDx);

                        /* Adding Package to list */
                        ltChildPackage.Add(tempPackage);
                    }
                }
                return ltChildPackage;
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }
    }
}






