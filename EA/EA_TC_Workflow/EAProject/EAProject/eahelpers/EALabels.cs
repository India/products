﻿/*======================================================================================================================================================================
                                       Copyright 2018  LMtec
                                   Unpublished - All rights reserved
========================================================================================================================================================================

File Description:

   Filename: EALabels.cs

   Description: This File is specific to Connector Labels.

========================================================================================================================================================================

Date          Name                Description of Change
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
11-Jan-18     Megha Singh          Initial Release
========================================================================================================================================================================*/

/* Class Description - This class includes function which gets the label for connector connecting two diagram objects.
 * 
 **/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAProject.constants;
using EAProject.containers;
using EAProject.utilities;

namespace EAProject.eahelpers
{
    class EALabels
    {
        StringUtilities utilString = new StringUtilities();
        /* This function takes two objects of class 'WorkflowTemplateInfo'. 'infoDestinationTpl' is destination WFTemplate object for connector. It contains label('strArgElem') info passed as
         * reference to the function) to be attached on connector. While 'infoSourceTpl' is the source WFTemplate object for connector. Generally labels are present on connectors where the source
         * task is a Condition task. 
         * For every Condition Task the value of element starting with '-decision' is get from list member variable(ltTskHandlers) of class(WFTemplateInfo), tokenized and second element from array
         * containing tokens is returned as label to be added on connector.             
         **/
        public void getLabel(WFTemplateInfo infoDestinationTpl, WFTemplateInfo infoSourceTpl, ref string strArgElem)
        {
            try
            {
                strArgElem = null;
                                
                if (String.Compare(infoSourceTpl.strTaskObjectType, Constants.EPM_CONDITION_TASK_TEMPLATE) == 0 && infoDestinationTpl.ltTaskHandlers != null)
                {
                    for (int iFx = 0; iFx < infoDestinationTpl.ltTaskHandlers.Count; iFx++)
                    {
                        /* Checking if element in list(lttakHandlers) at index(iFx) starts with '-decision'. As in a WorkflowTemplate the value of this argument('-decision') present in destination
                         * template serves as label for connector connecting the two templates. */
                        if (infoDestinationTpl.ltTaskHandlers[iFx].StartsWith(Constants.ARG_DECISION))
                        {
                            string[] szUnTokenizedArg = infoDestinationTpl.ltTaskHandlers[iFx].Split(Constants.ARG_TOKEN_CONSTANT);

                            strArgElem = szUnTokenizedArg[1];                                                       
                        }
                    }                      
                }                              
            }
            catch (Exception exException)
            {
                throw exException;
            }
        }           
    }
}





















