@echo off
set UPG=-u=infodba -p=infodba -g=dba

REM dont modify beyond this point
set UGII_TEMPLATE_DIR=%UGII_BASE_DIR%\UGII\templates
if not defined ugii_tmp_dir set ugii_tmp_dir=%temp%
if defined TC_TMP_DIR set ugii_tmp_dir=%TC_TMP_DIR%
set thisdrive=%~d0
set thispath=%~p0
set thisdir=%thisdrive%%thispath%
cd /d "%thisdir%"

set ugii_keep_system_log=true
set iman_keep_system_log=true

Set BYPASS_RULES=ON
Set NR_BYPASS=ON
Set BMF_BYPASS_ALL_EXTENSION_RULES=ON
Set TC_BYPASS_CANNED_METHODS=ALL
Set IMAN_BYPASS_CANNED_METHODS=ALL
Set AM_BYPASS=ON

echo STEP 1: copy PAX and image files...
copy /y %thisdir%*.jpg %UGII_TEMPLATE_DIR%
copy /y %thisdir%*.pax %UGII_TEMPLATE_DIR%
echo.

echo STEP 2: seedpart import (1 of 3)...
echo %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%assembly-mm-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo.
call %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%assembly-mm-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo STEP 2: seedpart import (2 of 3)...
echo %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%model-plain-1-mm-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo.
call %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%model-plain-1-mm-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo.
echo STEP 3: seedpart import (3 of 3)...
echo %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%Drawing-A0-Size-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo.
call %UGII_BASE_DIR%\UGMANAGER\ug_import %UPG% -part=%thisdir%Drawing-A0-Size-template.prt -mapping=%thisdir%seedpart_mapping.txt
echo.

echo Process finished!