# --------------------------
# General arguments
#---------------------------

# Set the import Mode if available
set Import_Mode:

# Set a prefix for imported files
# If not set no prefix will be written
#set Prefix_After_Clone: IMP_

# Set the clone type: ug_clone (default) or ug_import
set Clone_Type: ug_import

# Default action if an item revision was found
# in Teamcenter. Valid Values are:
# OVERWRITE
# NEW_REVISION
  set Default_Action_For_Existing_Rev: NEW_REVISION

# --------------------------
# Cloning related arguments
#---------------------------
set Operation_Type: IMPORT_OPERATION
set Default_Cloning_Action: OVERWRITE
set Default_Naming_Technique: AUTO_GENERATE 
set Default_Container: ":Newstuff:Import"
set Default_Part_Type: R4_Article
set Default_Part_Type_List: A4_Design_CAD;A4_Des_Tool_NX;A4_DesToolRootNX
set Default_Part_Dwg: A4_Drawing_CAD
set Default_Part_Name: ""
set Default_Part_Description: ""
set Default_Associated_Files_Directory: ""
set Default_Validation_Mode: OFF
set Default_NXAttributes: DB_PART_NO
set Default_TC_Group:
set Default_TC_User:
set Default_DB_Owner:
set Default_CLONE_FILE: C:\temp\

# --------------------------
# Internal arguments
#---------------------------

# Setting the following arguement indicates if the program
# runs NX internally
set Run_Internal: true
set Mixed_Assemblies_Support: true

# --------------------------
# Multi Key-Value Atributes
#---------------------------
set MKV_Separator_TBK_MasterData: :
set MKV_Attribute_TBK_MasterData: ChangeNumber
set MKV_Attribute_TBK_MasterData: Nomenclature_2
set MKV_Attribute_TBK_MasterData: j0ChangeDescription
set MKV_Attribute_TBK_MasterData: Weight

# Set Logging Level. Allowed Values are:
#   DEBUG
#   INFO
#   ERROR
set Logging_Level: DEBUG

# Workaround if option "Enable Multiple Revisions" is set
set Force_Load: ON