﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;
using System.Data;
using System.Text.RegularExpressions;

namespace NXImportAsync.XMLHandling.Roeders
{
    class MetaData : XMLDocument
    {

        //Constructor
        public MetaData(string metadataFile) : base(metadataFile)
        {
        }
        //here we do all the excel validation. As of now check the part names and file names provided
        public bool ValidateExcel()
        {
            int numColumns = 0;

            bool retValue = false;
            bool fileNameFound = false;
            bool fileNameCorrectColumn = false;

            foreach (DataColumn c in DataTbl.Columns)
            {
                if (c.ColumnName.Equals("FILENAME"))
                {
                    fileNameFound = true;
                    if (numColumns == 0)
                    { 
                        fileNameCorrectColumn = true;
                    }

                }
                numColumns++;
            }

            if (fileNameFound && fileNameCorrectColumn)
                retValue = true;
            else
            {
                string msg = "";
                string add = "";

                if (!fileNameFound)
                    add = "Column 'FILENAME' not found.\n";

                if (!fileNameCorrectColumn)
                    add = add + "The first Column ('A') has to be 'FILENAME'\n";

                if (add.Length > 0)
                {
                    msg = "The following errors are found in the Excel Sheet " + XMLDoc + ":\n\n" + add;
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
            }
            return retValue;
        }

        //Do we need this? This is HIG specific!
        public string AskCustomerRev(string itemId)
        {
            string custRev = "";
            DataRow[] foundRows = DataTbl.Select("[FILENAME] = '" + itemId + "'");

            if (foundRows.Length > 0)
            {
                foreach (DataRow r in foundRows)
                {
                    foreach (DataColumn c in DataTbl.Columns)
                    {
                        if (c.ColumnName.Equals("CUSTOMER_REV"))
                        {
                            custRev = r[c].ToString();
                        }
                    }
                }
            }

            return custRev;
        }
    }
}