﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;
using System.Data;
using System.Text.RegularExpressions;

namespace NXImportAsync.XMLHandling
{
    class XMLDocument
    {
        private String _xmlDoc;
        private DataTable _dt;
        private static List<char> Letters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

        //Constructor
        public XMLDocument(String xmlDoc)
        {
            try
            {
                XMLDoc = xmlDoc;
                _dt = new DataTable();
            }
            catch (NXOpen.NXException ex)
            {
                // ---- Enter your exception handling code here -----
                MessageBox.Show(ex.Message);
            }
        }

        public DataTable ExcelDataTable
        {
            get
            {
                return _dt;
            }
        }

        public string XMLDoc
        {
            get
            {
                return _xmlDoc;
            }

            set
            {
                _xmlDoc = value;
            }
        }

        public DataTable DataTbl
        {
            get
            {
                return _dt;
            }

            set
            {
                _dt = value;
            }
        }

        public string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);

            return match.Value;
        }
        public int? GetColumnIndexFromName(string columnName)
        {
            int? columnIndex = null;
            string[] colLetters = null;

            //string[] colLetters = Regex.Split(columnName, "([A-Z]+)");
            if (columnName.Length == 1)
            {
                colLetters = new string[1];
                colLetters[0] = columnName;
            }
            else if (columnName.Length == 2)
            {
                colLetters = new string[2];
                colLetters[0] = columnName.Substring(0, 1);
                colLetters[1] = columnName.Substring(1);
            }

            //colLetters = colLetters.Where(s => !string.IsNullOrEmpty(s)).ToArray();

            if (colLetters != null)
            {
                int index = 0;
                foreach (string col in colLetters)
                {
                    List<char> col1 = colLetters.ElementAt(index).ToCharArray().ToList();
                    int? indexValue = Letters.IndexOf(col1.ElementAt(0));

                    if (indexValue != -1)
                    {
                        // The first letter of a two digit column needs some extra calculations
                        if (index == 0 && colLetters.Count() == 2)
                        {
                            columnIndex = columnIndex == null ? (indexValue + 1) * 26 : columnIndex + ((indexValue + 1) * 26);
                        }
                        else
                        {
                            columnIndex = columnIndex == null ? indexValue : columnIndex + indexValue;
                        }
                    }

                    index++;
                }
            }

            return columnIndex;
        }

        public string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = "";

            try
            {
                value = cell.CellValue.InnerXml;
            }
            catch (Exception ex)
            {
                //myLog.Log(ex.Message);
                value = "";
            }

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public bool openXMLReadExcel(int startAtRow)
        {
            bool success = true;

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(XMLDoc, false))//replace with incoming xls file
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                int numColumns = 0;
                int columnIndex = 0;
                int cellColumnIndex = 0;

                foreach (Cell cell in rows.ElementAt(startAtRow))
                {
                    cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));

                    if (numColumns < cellColumnIndex)
                    {
                        MessageBox.Show("The Excel sheet " + XMLDoc + "\ncontains empty cells in the header row!",
                            "Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1);

                        success = false;
                        break;
                    }
                    else
                    {
                        string value = GetCellValue(spreadSheetDocument, cell);
                        DataTbl.Columns.Add(value);
                    }

                    numColumns++;
                }

                if (success)
                {
                    int currentRow = -1;
                    //bool firstRow = true;
                    foreach (Row row in rows) //this will also include your header row...
                    {
                        currentRow++;
                        // The first row contains useless data. skip it
                        if (currentRow < startAtRow)
                            continue;

                        DataRow tempRow = DataTbl.NewRow();
                        columnIndex = 0;

                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            try
                            {
                                Cell cell = row.Descendants<Cell>().ElementAt(i);

                                cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));

                                if (columnIndex < cellColumnIndex)
                                {
                                    if (columnIndex < cellColumnIndex)
                                    {
                                        do
                                        {
                                            tempRow[columnIndex] = "";
                                            columnIndex++;
                                        }
                                        while (columnIndex < cellColumnIndex);
                                    }
                                }

                                string value = GetCellValue(spreadSheetDocument, cell);
                                tempRow[columnIndex] = value;

                                columnIndex++;
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.ToString() + ex.Message);
                                success = false;
                            }
                        }

                        //add last cells if empty
                        if (columnIndex < numColumns)
                        {
                            do
                            {
                                tempRow[columnIndex] = "";
                                columnIndex++;
                            }
                            while (columnIndex < numColumns);
                        }

                        DataTbl.Rows.Add(tempRow);
                    }
                }
            }
            return success;
        }
    }
}
