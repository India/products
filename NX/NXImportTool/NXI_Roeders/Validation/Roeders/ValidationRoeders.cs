﻿using System;
using System.Collections.Generic;
using System.Text;
using NXOpen;
using NXOpen.UF;
using NXOpen.Assemblies;
using NXOpen.Utilities;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.IO.Packaging;
using System.Linq;
using System.Data.OleDb;
using System.Data;
using NXImportAsync.XMLHandling.Roeders;
using NXImportAsync.Base.Utilities.Log;
using NXImportAsync.Base.GUI;
using NXImportAsync.Base.Data;
using NXImportAsync.Base.Utilities.Teamcenter;
using NXImportAsync.Base.Utilities.NX;
using NXImportAsync.Base.Validation;
using NXImportAsync.Base.Prep;

namespace NXImportAsync.Validation.Roeders
{
    class ValidationRoeders : ValidationCore
    {
        private String _attNameForQry = "";
        private String _attValueForQry = "";
        private String _attQueryForQry = "";

        //Constructor
        public ValidationRoeders(String metaDataFile, String inputFileName)
            : base(metaDataFile, inputFileName, false)
        {
            //CLONEFileName = PartFilesDirectory + "\\clone_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".clone";
            //ReportFileName = PartFilesDirectory + "\\report_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".txt";
            CLONEFileName = MetadataDirectory + "\\clone_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".clone";
            ReportFileName = MetadataDirectory + "\\report_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".txt";
            _report = new Report(ReportFileName);
        }
        public ValidationRoeders(String metaDataFile, String inputFileName, String attName, String attValue, String attQuery)
            : base(metaDataFile, inputFileName, false)
        {
            CLONEFileName = MetadataDirectory + "\\clone_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".clone";
            ReportFileName = MetadataDirectory + "\\report_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".txt";

            _attNameForQry = attName;
            _attValueForQry = attValue;
            _attQueryForQry = attQuery;

            _report = new Report(ReportFileName);
        }
        public override bool CreateImportClone(bool searchInTC, bool handleMixedAssies)
        {
            bool retcode = false;
            string partName = PartFileNames;

            bool modified = false;
            NXGeneralUtils utils = NXGeneralUtils.Instance;

            if (handleMixedAssies)
            {
                MixedAssemblies mxA = MixedAssemblies.Instance;
                modified = mxA.CreateWrappingParts(partName);
            }

            if (modified)
                utils.setLoadOptionsMixedAssemblies();
            else
                utils.setLoadOptionsFull();

            NXPart[] nxParts = ProcessParts(partName, searchInTC);

            Logger.Instance.LogInfo("The following parts have been validated:");
            foreach (var pair in resultsDictionary)
                Logger.Instance.LogInfo("\t" + pair.Key);

            if (modified)
                foreach (NXPart p in nxParts)
                    p.SaveJTSubAssies();

            TheSession.Parts.CloseAll(BasePart.CloseModified.CloseModified, null);

            return retcode;
        }
        public override NXPart[] ProcessParts(string partName, bool searchInTC)
        {
            List<NXPart> retParts = new List<NXPart>();

            try
            {
                string[] partNameSplit = partName.Split(';');

                //Report start of setting attributes
                _report.ReportStartSettingAttrs();

                foreach (string s in partNameSplit)
                {
                    List<Results> resultIDList = new List<Results>();

                    NXPart nxPart = new NXPart(s);
                    retParts.Add(nxPart);
                    nxPart.OpenPartQuiet();

                    if (processedItems.ContainsKey(nxPart.GetFullPath()) == false)
                    {
                        string itemType = AskItemType(nxPart.AskFileName());
                        string[] MFKsearchResults = null;

                        if (searchInTC)
                            MFKsearchResults = SearchObjsInTC(nxPart.GetBasePart(), nxPart.AskFileName());

                        string revision_id = "";
                        string part_rev = "";
                        string latest_rev = "";
                        string cust_rev = "";
                        bool attributesSet = true;

                        if (searchInTC && MFKsearchResults != null && MFKsearchResults.Length > 0)
                            AskTCAttributes(MFKsearchResults[0], ref itemType, ref part_rev, ref revision_id);

                        //do we need this...but this where excel works
                        //SetAttributes(nxPart.AskFileName(), nxPart.GetBasePart(), ((MFKsearchResults != null) && (MFKsearchResults.Length != 0)) ? MFKsearchResults[0] : "", out attributesSet, out latest_rev, out cust_rev);

                        //ask object description
                        string objectDesc = AskObjectDescription(nxPart);

                        String partNameinLogFile = "";
                        Results res = null;

                        if (searchInTC && MFKsearchResults != null && MFKsearchResults.Length > 0)
                        {
                            if (attributesSet)
                            {
                                res = AddEntryNewRevision(nxPart, "TOP_ASSEMBLY", itemType, nxPart.AskFileName(), nxPart.AskFileName(), objectDesc, MFKsearchResults[0], part_rev, false);
                                //res.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                                if ((_attNameForQry.Length > 0) && (_attValueForQry.Length > 0))
                                    res.SetTCAttribute(_attNameForQry, _attValueForQry, NXAttribute.Target.R);

                                partNameinLogFile = "@DB/" + MFKsearchResults[0] + "/" + part_rev;
                            }
                            else
                            {
                                res = AddEntryUseExisting(nxPart, "TOP_ASSEMBLY", itemType, nxPart.AskFileName(), nxPart.AskFileName(), objectDesc, MFKsearchResults[0], latest_rev);
                                partNameinLogFile = "@DB/" + MFKsearchResults[0] + "/" + latest_rev;
                            }
                        }
                        else
                        {
                            res = AddEntryOverwrite(nxPart, "TOP_ASSEMBLY", itemType, nxPart.AskFileName(), objectDesc, nxPart.AskFileName());
                            //res.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                            if ((_attNameForQry.Length > 0) && (_attValueForQry.Length > 0))
                                res.SetTCAttribute(_attNameForQry, _attValueForQry, NXAttribute.Target.R);

                            SetAttributes(res, nxPart.AskFileName(), nxPart.GetBasePart(), ((MFKsearchResults != null) && (MFKsearchResults.Length != 0)) ? MFKsearchResults[0] : "", out attributesSet, out latest_rev, out cust_rev);

                            partNameinLogFile = "@DB/000000/01";
                        }

                        // Set OccNotes
                        /*if (nxPart.GetRootComponent() != null)
                            res.SetBOMAttributesFromInput(nxPart.GetRootComponent(), BOMAttributes);*/

                        // set Customer Rev
                        //res.CustomerRevision = cust_rev;

                        resultIDList.Add(res);
                        resultsDictionary.Add(nxPart.GetFullPath(), resultIDList);

                        if (searchInTC && MFKsearchResults != null && MFKsearchResults.Length > 1)
                        {
                            for (int i = 1; i < MFKsearchResults.Length; i++)
                            {
                                Results resOther = AddEntryNewRevision(nxPart, "TOP_ASSEMBLY", itemType, nxPart.AskFileName(), nxPart.AskFileName(), objectDesc, MFKsearchResults[i], null, true);
                                //resOther.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                                if ((_attNameForQry.Length > 0) && (_attValueForQry.Length > 0))
                                    res.SetTCAttribute(_attNameForQry, _attValueForQry, NXAttribute.Target.R);

                                resOther.CustomerRevision = cust_rev;

                                resultsDictionary[nxPart.GetFullPath()].Add(resOther);
                            }
                        }

                        processedItems.Add(nxPart.GetFullPath(), partNameinLogFile);
                    }

                    if (nxPart.GetRootComponent() != null)
                    {
                        ProcessChildren(nxPart.GetRootComponent(), nxPart.AskFileName(), searchInTC);
                    }
                }

                //Report end of setting attributes
                _report.ReportEndSettingAttrs();
            }
            catch (Exception loadException)
            {
                MessageBox.Show(loadException.StackTrace.ToString());
                Logger.Instance.Log(loadException.Message + loadException.StackTrace, Logger.LogLevel.Debug);
            }
            return retParts.ToArray();
        }
        public void ProcessChildren(NXOpen.Assemblies.Component component, string iRootName, bool searchInTC)
        {
            try
            {
                NXOpen.Assemblies.Component[] childComponents = component.GetChildren();

                for (int i = 0; i < childComponents.Length; i++)
                {
                    List<Results> resultIDList = new List<Results>();
                    BasePart owner = childComponents[i].Prototype.OwningPart;

                    bool isSuppressed = childComponents[i].IsSuppressed;

                    if (isSuppressed)
                    {
                        childComponents[i].Unsuppress();
                        owner = (BasePart)childComponents[i].Prototype.OwningPart;
                    }

                    if (owner == null)
                    {
                        NXUtils.openComponent(childComponents[i]);
                        owner = (BasePart)childComponents[i].Prototype.OwningPart;
                    }

                    if ((owner != null) && (processedItems.ContainsKey(owner.FullPath) == false))
                    {
                        NXPart nxPart = new NXPart((Part)owner);
                        String part_rev = "";

                        string fNameAsPartName = Path.GetFileNameWithoutExtension(owner.FullPath);
                        string itemType = AskItemType(fNameAsPartName);

                        string[] MFKsearchResults = null;
                        if (searchInTC)
                            MFKsearchResults = SearchObjsInTC(owner, fNameAsPartName);

                        if (MFKsearchResults != null && MFKsearchResults.Length > 0)
                        {
                            bool modifiable = false;
                            int oCode = 0;

                            TCInterface.TCAskItemType(MFKsearchResults[0], out oCode, ref itemType);

                            TheUfSession.Ugmgr.AssignPartRev(MFKsearchResults[0], itemType, out part_rev, out modifiable);
                        }

                        bool attributesSet = true;
                        string latestRev = "";
                        string customerRev = "";

                        //SetAttributes(fNameAsPartName, owner, ((MFKsearchResults != null) && (MFKsearchResults.Length != 0)) ? MFKsearchResults[0] : "", out attributesSet, out latestRev, out customerRev);

                        //ask object description
                        string objectDesc = AskObjectDescription(nxPart);

                        String partNameinLogFile = "";
                        Results res = null;

                        if (searchInTC && MFKsearchResults != null && MFKsearchResults.Length > 0)
                        {
                            if (attributesSet)
                            {
                                res = AddEntryNewRevision(nxPart, "PART", itemType, nxPart.AskFileName(), iRootName, objectDesc, MFKsearchResults[0], part_rev, false);
                                //res.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                                if ((_attNameForQry.Length > 0) && (_attValueForQry.Length > 0))
                                    res.SetTCAttribute(_attNameForQry, _attValueForQry, NXAttribute.Target.R);

                                partNameinLogFile = "@DB/" + MFKsearchResults[0] + "/" + part_rev;
                            }
                            else
                            {
                                res = AddEntryUseExisting(nxPart, "PART", itemType, nxPart.AskFileName(), iRootName, objectDesc, MFKsearchResults[0], latestRev);
                                partNameinLogFile = "@DB/" + MFKsearchResults[0] + "/" + latestRev;
                            }
                        }
                        else
                        {
                            res = AddEntryOverwrite(nxPart, "PART", itemType, nxPart.AskFileName(), objectDesc, iRootName);
                            //res.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                            if ((_attNameForQry.Length > 0) && (_attValueForQry.Length > 0))
                                res.SetTCAttribute(_attNameForQry, _attValueForQry, NXAttribute.Target.R);

                            partNameinLogFile = "@DB/000000/01";
                        }

                        // Set OccNotes
                        if (nxPart.GetRootComponent() != null)
                            res.SetBOMAttributesFromInput(nxPart.GetRootComponent(), BOMAttributes);

                        // set Customer Rev
                        res.CustomerRevision = customerRev;

                        resultIDList.Add(res);
                        resultsDictionary.Add(nxPart.GetFullPath(), resultIDList);

                        if (searchInTC && MFKsearchResults != null && MFKsearchResults.Length > 1)
                        {
                            for (int inx = 1; inx < MFKsearchResults.Length; inx++)
                            {
                                Results resOther = AddEntryNewRevision(nxPart, "PART", itemType, nxPart.AskFileName(), iRootName, objectDesc, MFKsearchResults[inx], null, true);
                                //resOther.SetTCAttribute("h4_imported", "true", NXAttribute.Target.R);
                                resOther.CustomerRevision = customerRev;

                                resultsDictionary[nxPart.GetFullPath()].Add(resOther);
                            }
                        }

                        processedItems.Add(owner.FullPath, partNameinLogFile);
                    }

                    if (isSuppressed)
                        childComponents[i].Suppress();

                    ProcessChildren(childComponents[i], iRootName, searchInTC);
                }
            }
            catch (NXException ex)
            {
                Logger.Instance.Log(ex.Message + ex.StackTrace);
            }
        }
    }
}
