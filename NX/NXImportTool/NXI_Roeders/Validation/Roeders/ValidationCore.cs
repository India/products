﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using NXOpen;
using NXOpen.UF;
using NXImportAsync.XMLHandling.Roeders;
using NXImportAsync.Base.Validation;
using NXImportAsync.Base.Data;
using NXImportAsync.Base.Utilities.Teamcenter;
using NXImportAsync.Base.Utilities.Log;

namespace NXImportAsync.Validation.Roeders
{
    class ValidationCore : ValidationBase
    {   
        public static MetaData _metaData;
        //Constructor
        public ValidationCore(String metaDataFile, String inputFileName, bool useWorkaround)
            : base(metaDataFile, inputFileName)
        {
            _metaData = new MetaData(metaDataFile);

            //CLONEFileName = PartFilesDirectory + "\\clone_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".clone";
            //ReportFileName = PartFilesDirectory + "\\report_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".txt";

            //_report = new Report(ReportFileName);
        }
        public ValidationCore(String metaDataFile, String inputFileName, Session iSession, UFSession iUfSession)
            : base(metaDataFile, inputFileName, iSession, iUfSession)
        {
            _metaData = new MetaData(metaDataFile);
            //CLONEFileName = PartFilesDirectory + "\\clone_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".clone";
            //ReportFileName = PartFilesDirectory + "\\report_" + DateTime.Now.ToString("yyyy-MM-dd_hhmmss") + ".txt";

            //_report = new Report(ReportFileName);
        }
        protected string PartFilesDirectory
        {
            get
            {
                //if there are more part files form the correct directory
                if (PartFileNames != null)
                    return PartFileNames.Substring(0, PartFileNames.LastIndexOf("\\"));
                else
                    return "";
            }
        }

        protected string MetadataDirectory
        {
            get
            {
                if (_metaData.XMLDoc != null)
                    return _metaData.XMLDoc.Substring(0, _metaData.XMLDoc.LastIndexOf("\\"));
                else
                    return "";
            }
        }
        public bool InitMetaData()
        {
            bool retValue = false;
            bool successOpen = false;
            bool successValidation = false;

            //check if metadata file is locked by another process
            FileInfo fl = new FileInfo(_metaData.XMLDoc);
            if (IsFileLocked(fl))
            {
                MessageBox.Show("The Excel sheet " + _metaData.XMLDoc + "\nis locked by another process!",
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error,
                    MessageBoxDefaultButton.Button1);
            }
            else
            {
                successOpen = _metaData.openXMLReadExcel(1);
                successValidation = _metaData.ValidateExcel();

                if (successOpen && successValidation)
                    retValue = true;
            }

            return retValue;
        }
        //here we collect Roeders specific excel data
        public void SetAttributes(Results res, string partName, NXOpen.BasePart bPart, string itemId, out bool attributesSet, out string latestRev, out string custRev)
        {
            DataTable dTable = _metaData.ExcelDataTable;
            NXObject.AttributeInformation info = new NXObject.AttributeInformation();

            attributesSet = true;

            latestRev = "";
            custRev = "";

            try
            {
                //MessageBox.Show(partName);
                DataRow[] foundRows = dTable.Select("[FILENAME] = '" + partName + "'");

                if (foundRows.Length > 0)
                {
                    foreach (DataRow r in foundRows)
                    {
                        //workaround start: set attribues allways --> but don't touch the flag (attributesSet)
                        if (attributesSet)
                        {
                            foreach (DataColumn c in dTable.Columns)
                            {
                                int length = r[c].ToString().Length;
                                if ((c.ColumnName != "FILENAME") && (length > 0))
                                {
                                    if (c.ColumnName == "DESCRIPTION" && length > 128)
                                    {
                                        _report.ReportSingleMsg("Fehler bei Part " + bPart.FullPath + ": Attributwert fuer 'DESCRIPTION' is laenger als 128 Zeichen! " +
                                            "Mapping nach Teamcenter nicht moeglich.");
                                    }
                                    else if (c.ColumnName == "DESCRIPTION_1" && length > 32)
                                    {
                                        _report.ReportSingleMsg("Fehler bei Part " + bPart.FullPath + ": Attributwert fuer 'DESCRIPTION_1' is laenger als 32 Zeichen! " +
                                            "Mapping nach Teamcenter nicht moeglich.");
                                    }
                                    else if (c.ColumnName == "DESCRIPTION_2" && length > 64)
                                    {
                                        _report.ReportSingleMsg("Fehler bei Part " + bPart.FullPath + ": Attributwert fuer 'DESCRIPTION_2' is laenger als 64 Zeichen! " +
                                            "Mapping nach Teamcenter nicht moeglich.");
                                    }
                                    else if (c.ColumnName == "DESCRIPTION_eng" && length > 32)
                                    {
                                        _report.ReportSingleMsg("Fehler bei Part " + bPart.FullPath + ": Attributwert fuer 'DESCRIPTION_eng' is laenger als 32 Zeichen! " +
                                            "Mapping nach Teamcenter nicht moeglich.");
                                    }

                                    info.Type = NXObject.AttributeType.String;
                                    info.Title = c.ColumnName;
                                    info.StringValue = r[c].ToString();
                                    Logger.Instance.Log(info.Title + ":" + info.StringValue);
                                    //MessageBox.Show(info.Title + " " + info.StringValue);
                                    res.SetTCAttribute(info.Title, info.StringValue, NXAttribute.Target.R);
                                    try
                                    {
                                        bPart.SetUserAttribute(info, Update.Option.Now);
                                    }
                                    catch (NXException nxex)
                                    {
                                        Logger.Instance.Log("This is NX Exception for: " + info.Title + "-" + info.StringValue + nxex.Message + nxex.StackTrace);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //get the latest rev
                    int oCode = 0;

                    TCInterface.TCAskLatestRevID(itemId,
                        out oCode,
                        out latestRev
                        );

                    // if no info about the cusomer rev and item is available
                    // in teamcenter don't set any attributes --> don't upload to new revision
                    /*if (itemId.Length > 0)
                        attributesSet = false;*/
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                Logger.Instance.Log("This is .NET Exception: " + ex.Message + ex.StackTrace);
            }
        }
        protected string AskObjectDescription(NXPart nxPart)
        {
            string objDesc = "";

            if (nxPart.GetPart().HasUserAttribute("Aenderungstext", NXObject.AttributeType.String, -1))
                objDesc = nxPart.GetPart().GetUserAttributeAsString("Aenderungstext", NXObject.AttributeType.String, -1);
            else if (nxPart.GetPart().HasUserAttribute("AENDERUNGSTEXT", NXObject.AttributeType.String, -1))
                objDesc = nxPart.GetPart().GetUserAttributeAsString("AENDERUNGSTEXT", NXObject.AttributeType.String, -1);
            else if (nxPart.GetPart().HasUserAttribute("DB_PART_DESC", NXObject.AttributeType.String, -1))
                objDesc = nxPart.GetPart().GetUserAttributeAsString("DB_PART_DESC", NXObject.AttributeType.String, -1);

            return objDesc;
        }
    }
}
