﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using NXImportAsync.Base.Validation;
using NXImportAsync.Validation.Roeders;
using NXImportAsync.Base.Data;
using NXImportAsync.Base.Utilities.NX;
using NXImportAsync.Base.Utilities.Log;
using NXImportAsync.Base.Utilities.Teamcenter;
using NXImportAsync.Base.GUI;
using NXOpen.UF;

namespace NXImportAsync.General.GUI
{
    public partial class Roeders : MainWindow
    {
        [DllImport("kernel32.dll")]
        static extern bool AttachConsole(int dwProcessId);
        private const int ATTACH_PARENT_PROCESS = -1;

        string subItemText = "";
        int selectedSubItem = 0;
        private int X = 0;
        private int Y = 0;
        private NXGeneralUtils _generalUtils;
        ListViewItem itm;

        public Roeders()
        {
            InitializeComponent();
            InitializeCustomComponents();
        }

        private void GeneralMainForm_Load(object sender, EventArgs e)
        {
            AttachConsole(ATTACH_PARENT_PROCESS);
            rbFileNameChars.Checked = true;
            tbWildCard.Text = "ALL";
            cbPartFileTypes.SelectedItem = cbPartFileTypes.Items[0];
            cbAttributeTypes.SelectedItem = cbAttributeTypes.Items[0];
            OneStatusMessage = true;
            _generalUtils = NXGeneralUtils.Instance;

            //Show current version on window border
            Version version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            Config.Instance.NXIAVersion = version.ToString();
            this.Text = "NXImport, LMtec GmbH (V." + version.ToString() + ")";

            //Disable Choose Clone File button and the text box associated to it
            btnAdditionalFile.Enabled = false;
            tbAdditionalFile.Enabled = false;

            //MessageBox.Show(Directory.GetCurrentDirectory().ToString());
            string executablePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string path = executablePath.Substring(0, executablePath.LastIndexOf("\\"));

            if (File.Exists(path + "\\Preferences.txt"))
            {
                ReadPreferencesFile(path + "\\Preferences.txt");

                // set up item types combo box
                if (Config.Instance.ItemTypes != null)
                    this.comboBoxItemTypes.Items.AddRange(Config.Instance.ItemTypes);

                if (!_generalUtils.IsManagedMode())
                {
                    NXOpen.Session theSession = NXOpen.Session.GetSession();
                    MessageBox.Show("This program has to be executed in managed mode.");

                    this.Close();
                }
                else
                {
                    NXOpen.Session theSession = NXOpen.Session.GetSession();

                    // AllowForceLoad will open subassemblies independend if the option
                    // "Enable Multiple Revisions" is set
                    if (!Config.Instance.AllowForceLoad)
                    {
                        // check if option "Enable Multiple Revisions" is disabled
                        bool multiRevSet = theSession.OptionsManager.GetLogicalValue("UGMGR_EnableMultipleRevisions", NXOpen.Options.LevelType.User);

                        if (multiRevSet)
                        {
                            MessageBox.Show("You need to uncheck the Option \"Enable Multiple Revisions\"\nin the Customer Defaults to run the NXImport Tool",
                                "ERROR",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);

                            SetButtonsEnabled(false);
                            chkBoxDryRun.Enabled = false;
                            chkSeachInTC.Enabled = false;
                        }

                    }
                    // check availability of PDM functions
                    else if (TCInterface.TCCheckFunctions())
                    {
                        MessageBox.Show("Error checking PDM-functions.\nPlease check correct installaton of LMImportLib.dll.",
                            "ERROR",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                        SetButtonsEnabled(false);
                        chkBoxDryRun.Enabled = false;
                        chkSeachInTC.Enabled = false;
                    }

                    else if (!Config.Instance.HasDefaultUser)
                    {
                        int oCode = 0;
                        string oString = "";

                        NXImportAsync.Base.Utilities.Teamcenter.TCInterface.TCAskUserAndGroup(out oCode, out oString);

                        lblDefaultOwnerValue.Text = oString;
                        string[] splits = oString.Split(' ');
                        if (splits.Length == 2)
                        {
                            Config.Instance.DefOwner = splits[0];
                            Config.Instance.DefGroup = splits[1];

                            Logger.Instance.LogRaw("   User Information:");
                            Logger.Instance.LogRaw("      User Name : " + splits[0]);
                            Logger.Instance.LogRaw("      User Group: " + splits[1]);
                            Logger.Instance.LogRaw("===============================================");
                        }
                    }
                }
            }
        }

        private void ItemTypesKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
            {
                comboBoxItemTypes.Hide();
            }
        }

        private void ItemTypesSelected(object sender, System.EventArgs e)
        {
            int i = comboBoxItemTypes.SelectedIndex;
            if (i >= 0)
            {
                string str = comboBoxItemTypes.Items[i].ToString();
                itm.SubItems[selectedSubItem].Text = str;
            }

            comboBoxItemTypes.Hide();
        }

        private void ItemTypesFocusExit(object sender, System.EventArgs e)
        {
            comboBoxItemTypes.Hide();
        }

        private void NamingRulesKeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13 || e.KeyChar == 27)
            {
                comboBoxNamingRules.Hide();
            }
        }

        private void NamingRulesSelected(object sender, System.EventArgs e)
        {
            int i = comboBoxNamingRules.SelectedIndex;
            if (i >= 0)
            {
                string str = comboBoxNamingRules.Items[i].ToString();

                if (itm.SubItems[selectedSubItem].Text != str)
                {
                    UFSession theUfSession = UFSession.GetUFSession();

                    bool modifiable = false;
                    string partRev = "";
                    string type = itm.SubItems[PosItemType].Text;
                    string iId = itm.SubItems[PosTCID].Text.Substring(itm.SubItems[PosTCID].Text.IndexOf('/') + 1);
                    iId = iId.Substring(0, iId.IndexOf('/'));

                    if (str.Equals("NEW_REVISION"))
                    {
                        if (iId.Equals("000000"))
                            iId = ObjImport.AskItemIdOfPart(itm.SubItems[PosFullPath].Text);

                        if (iId != null)
                        {
                            theUfSession.Ugmgr.AssignPartRev(iId, type, out partRev, out modifiable);
                            ObjImport.AddMappedAttributes(itm.SubItems[PosFullPath].Text);
                            itm.SubItems[PosTCID].Text = "@DB/" + iId + "/" + partRev;

                            itm.SubItems[selectedSubItem].Text = str;

                            CheckPrivilegesForRevise(ref itm, iId);
                            ObjImport.ChangeNamingRule(itm.SubItems[PosFullPath].Text, str);
                        }
                    }
                    else if (str.Equals("NEW_ITEM"))
                    {
                        itm.SubItems[PosTCID].Text = "@DB/000000/01";
                        UpdateLVIten(ref itm, PosImportable, "true", itm.SubItems[PosTCID].ForeColor, Color.White);

                        itm.SubItems[selectedSubItem].Text = str;
                        ObjImport.ChangeNamingRule(itm.SubItems[PosFullPath].Text, str);
                    }
                    else
                    {
                        if (iId.Equals("000000"))
                            iId = ObjImport.AskItemIdOfPart(itm.SubItems[PosFullPath].Text);

                        if (iId != null)
                        {
                            int oCode = 0;
                            TCInterface.TCAskLatestRevID(iId,
                                out oCode,
                                out partRev
                                );

                            itm.SubItems[PosTCID].Text = "@DB/" + iId + "/" + partRev;
                            itm.SubItems[selectedSubItem].Text = str;

                            if (str.Equals("OVERRIDE"))
                                CheckPrivilegesForOverride(ref itm, iId);

                            ObjImport.ChangeNamingRule(itm.SubItems[PosFullPath].Text, str);
                        }
                    }
                }
            }

            comboBoxNamingRules.Hide();
        }

        private void NamingRulesFocusExit(object sender, System.EventArgs e)
        {
            comboBoxNamingRules.Hide();
        }

        private void LvSearchResultsDoubleClick(object sender, System.EventArgs e)
        {
            // Check whether the subitem was clicked
            int start = X;
            int position = this.lvSearchResults.Bounds.Left;
            int end = this.lvSearchResults.Columns[0].Width + this.lvSearchResults.Bounds.Left;
            for (int i = 1; i < this.lvSearchResults.Columns.Count; i++)
            {
                if (start > position && start < end)
                {
                    selectedSubItem = i - 1;
                    break;
                }

                position = end;
                end += this.lvSearchResults.Columns[i].Width;
            }

            subItemText = itm.SubItems[selectedSubItem].Text;

            if (selectedSubItem == PosImportRule)
            {
                int width = this.lvSearchResults.Columns[selectedSubItem].Width;

                Rectangle r = new Rectangle(position, itm.Bounds.Top, width, itm.Bounds.Bottom);
                this.comboBoxNamingRules.Size = new System.Drawing.Size(width, itm.Bounds.Bottom - itm.Bounds.Top);
                this.comboBoxNamingRules.Location = new System.Drawing.Point(position + 3, itm.Bounds.Y + lvSearchResults.Bounds.Top);
                this.comboBoxNamingRules.Show();
                this.comboBoxNamingRules.BringToFront();
                this.comboBoxNamingRules.Text = subItemText;
                this.comboBoxNamingRules.SelectAll();
                this.comboBoxNamingRules.Focus();
            }
            else if (selectedSubItem == PosItemType && comboBoxItemTypes.Items.Count > 0)
            {
                int width = this.lvSearchResults.Columns[selectedSubItem].Width;

                Rectangle r = new Rectangle(position, itm.Bounds.Top, width, itm.Bounds.Bottom);
                this.comboBoxItemTypes.Size = new System.Drawing.Size(width, itm.Bounds.Bottom - itm.Bounds.Top);
                this.comboBoxItemTypes.Location = new System.Drawing.Point(position + 3, itm.Bounds.Y + lvSearchResults.Bounds.Top);
                this.comboBoxItemTypes.Show();
                this.comboBoxItemTypes.BringToFront();
                this.comboBoxItemTypes.Text = subItemText;
                this.comboBoxItemTypes.SelectAll();
                this.comboBoxItemTypes.Focus();
            }
            if (selectedSubItem == PosImportable)
            {
                int oCode = 0;
                string oValue = "";
                string itemId = "";

                if (itm.SubItems[PosImportRule].Text.Equals("OVERWRITE"))
                {
                    itemId = itm.SubItems[PosTCID].Text.Split('/')[1];
                    TCInterface.TCCheckPrivilegeForOverwrite(itemId, out oCode, out oValue);

                    if (oCode == 0)
                    {
                        UpdateLVIten(ref itm, PosImportable, "true", itm.SubItems[PosTCID].ForeColor, Color.White);
                        ObjImport.UpdatePrivilege(itemId, false);
                    }
                    else
                    {
                        UpdateLVIten(ref itm, PosImportable, "false", Color.WhiteSmoke, Color.Red);
                        ObjImport.UpdatePrivilege(itemId, true);
                        MessageBox.Show("No write permission on Item Revision or dependend object(s)",
                        "ERROR",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    }

                }
                else if (itm.SubItems[PosImportRule].Text.Equals("NEW_REVISION"))
                {
                    itemId = itm.SubItems[PosTCID].Text.Split('/')[1];
                    TCInterface.TCCheckPrivilegeForRevise(itemId, out oCode, out oValue);

                    if (oCode == 0)
                    {
                        ObjImport.UpdatePrivilege(itemId, false);

                        TCInterface.TCCheckLatestStatus(itemId, out oCode, out oValue);

                        if (oCode == 0)
                        {
                            UpdateLVIten(ref itm, PosImportable, "true", itm.SubItems[PosTCID].ForeColor, Color.White);
                            ObjImport.UpdateLastRevStatus(itemId, false);
                        }
                        else
                        {
                            UpdateLVIten(ref itm, PosImportable, "false", Color.WhiteSmoke, Color.Red);
                            ObjImport.UpdateLastRevStatus(itemId, true);
                            MessageBox.Show("Latest Revision without status",
                            "ERROR",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        UpdateLVIten(ref itm, PosImportable, "false", Color.WhiteSmoke, Color.Red);
                        ObjImport.UpdatePrivilege(itemId, true);
                        MessageBox.Show("No write permission on Item or dependend object(s)",
                        "ERROR",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void LvSearchResultsMouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            itm = this.lvSearchResults.GetItemAt(e.X, e.Y);
            X = e.X + this.lvSearchResults.Bounds.Left;
            Y = e.Y + this.lvSearchResults.Bounds.Top;
        }

        private void LvSearchResultsColumnClick(object sender, System.Windows.Forms.ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvSearchResultsSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvSearchResultsSorter.Order == SortOrder.Ascending)
                {
                    lvSearchResultsSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvSearchResultsSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvSearchResultsSorter.SortColumn = e.Column;
                lvSearchResultsSorter.Order = SortOrder.Ascending;
            }

            // Perform the sort with these new sort options.
            this.lvSearchResults.Sort();
        }

        private void btnMetaDataXSLX_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "XLSX|*.xlsx";
            //openFileDialog1.Multiselect = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                tbMetaDataXLSX.Text = openFileDialog1.FileName;
                //tbAdditionalFile.Text = "";
                /*foreach (string s in openFileDialog1.FileNames)
                    if (tbFileNames.Text == "")
                        tbFileNames.Text = s;
                    else
                        tbFileNames.Text = tbFileNames.Text + ";" + s;*/
            }
        }
        //BTN Start Click - Override to do excel validation.
        public override void BTNStart_Click(object sender, EventArgs e)
        {
            SetButtonsEnabled(false);
            string msg = "";

            if (tbFileNames.Text.Contains(" "))
                msg = "The path to the NX-Part file contains spaces.\n\nPlease use another directory!";
            else if (tbMetaDataXLSX.Text.Contains(" "))
                msg = "The path to the Excel file contains spaces.\n\nPlease use another directory!";

            if (msg.Length > 0)
            {
                MessageBox.Show(msg, "Info", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                SetButtonsEnabled(true);
            }
            else
            {
                startValidationRoeders();
            }
        }
        private void startValidationRoeders()
        {
            try
            {
                ValidationRoeders impRoeders = null;
                impRoeders = new ValidationRoeders(tbMetaDataXLSX.Text, tbFileNames.Text);
                //MessageBox.Show(tbMetaDataXLSX.Text);
                //MessageBox.Show(tbFileNames.Text);
                ObjImport = (ValidationBase)impRoeders;

                if (impRoeders.InitMetaData())
                {
                    lbStatusListBox.Items.Add("Report File generated: " + ObjImport.ReportFileName);
                    lbStatusListBox.Refresh();

                    InitObjImport();

                    ObjImport.ReportBasicInfos();

                    impRoeders.CreateImportClone(chkSeachInTC.Checked, Config.Instance.CreateWrappingParts);

                    FinishJob();
                    btnImport.Enabled = true;
                }
                else
                {
                    SetButtonsEnabled(true);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show(ex.Message + ex.StackTrace);
            }
        }
    }
}
