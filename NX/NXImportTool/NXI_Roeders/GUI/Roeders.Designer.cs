﻿using NXImportAsync.Base.GUI;

namespace NXImportAsync.General.GUI
{
    partial class Roeders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        new private void InitializeComponent()
        {
            this.btnMetaDataXSLX = new System.Windows.Forms.Button();
            this.tbMetaDataXLSX = new System.Windows.Forms.TextBox();
            this.gbSearchCriteria.SuspendLayout();
            this.gbDefaults.SuspendLayout();
            this.gbFileSelection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(495, 221);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(587, 221);
            // 
            // gbDefaults
            // 
            this.gbDefaults.Controls.Add(this.btnDefaultsFile);
            // 
            // gbFileSelection
            // 
            this.gbFileSelection.Controls.Add(this.tbMetaDataXLSX);
            this.gbFileSelection.Controls.Add(this.btnMetaDataXSLX);
            this.gbFileSelection.Size = new System.Drawing.Size(649, 101);
            this.gbFileSelection.Controls.SetChildIndex(this.btnNativeNXPartFiles, 0);
            this.gbFileSelection.Controls.SetChildIndex(this.tbFileNames, 0);
            this.gbFileSelection.Controls.SetChildIndex(this.btnAdditionalFile, 0);
            this.gbFileSelection.Controls.SetChildIndex(this.tbAdditionalFile, 0);
            this.gbFileSelection.Controls.SetChildIndex(this.btnMetaDataXSLX, 0);
            this.gbFileSelection.Controls.SetChildIndex(this.tbMetaDataXLSX, 0);
            // 
            // lvSearchResults
            // 
            this.lvSearchResults.Location = new System.Drawing.Point(13, 253);
            this.lvSearchResults.Size = new System.Drawing.Size(1014, 174);
            this.lvSearchResults.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.LvSearchResultsColumnClick);
            this.lvSearchResults.DoubleClick += new System.EventHandler(this.LvSearchResultsDoubleClick);
            this.lvSearchResults.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LvSearchResultsMouseDown);
            // 
            // lbReplacementItemIds
            // 
            this.lbReplacementItemIds.Location = new System.Drawing.Point(12, 466);
            // 
            // btnReplace
            // 
            this.btnReplace.Location = new System.Drawing.Point(329, 433);
            // 
            // btnImport
            // 
            this.btnImport.Location = new System.Drawing.Point(913, 564);
            // 
            // llbStatus
            // 
            this.llbStatus.Location = new System.Drawing.Point(679, 450);
            // 
            // lblDefaultItemTypeValue
            // 
            this.lblDefaultItemTypeValue.Size = new System.Drawing.Size(0, 13);
            this.lblDefaultItemTypeValue.Text = "";
            // 
            // lblDefaultContainerValue
            // 
            this.lblDefaultContainerValue.Size = new System.Drawing.Size(0, 13);
            this.lblDefaultContainerValue.Text = "";
            // 
            // lblDefaultOwnerValue
            // 
            this.lblDefaultOwnerValue.Size = new System.Drawing.Size(0, 13);
            this.lblDefaultOwnerValue.Text = "";
            // 
            // lblDefaultItemTypeDWGValue
            // 
            this.lblDefaultItemTypeDWGValue.Size = new System.Drawing.Size(0, 13);
            this.lblDefaultItemTypeDWGValue.Text = "";
            // 
            // lblSearchResults
            // 
            this.lblSearchResults.Location = new System.Drawing.Point(20, 450);
            // 
            // llbPartsList
            // 
            this.llbPartsList.Location = new System.Drawing.Point(20, 232);
            // 
            // lbStatusListBox
            // 
            this.lbStatusListBox.Location = new System.Drawing.Point(673, 466);
            this.lbStatusListBox.Size = new System.Drawing.Size(354, 56);
            // 
            // comboBoxItemTypes
            // 
            this.comboBoxItemTypes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxItemTypes.Size = new System.Drawing.Size(1, 21);
            this.comboBoxItemTypes.SelectedIndexChanged += new System.EventHandler(this.ItemTypesSelected);
            this.comboBoxItemTypes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ItemTypesKeyPress);
            this.comboBoxItemTypes.LostFocus += new System.EventHandler(this.ItemTypesFocusExit);
            // 
            // comboBoxNamingRules
            // 
            this.comboBoxNamingRules.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNamingRules.Items.AddRange(new object[] {
            "NEW_ITEM",
            "OVERWRITE",
            "USE_EXISTING",
            "NEW_REVISION"});
            this.comboBoxNamingRules.Size = new System.Drawing.Size(1, 21);
            this.comboBoxNamingRules.SelectedIndexChanged += new System.EventHandler(this.NamingRulesSelected);
            this.comboBoxNamingRules.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NamingRulesKeyPress);
            this.comboBoxNamingRules.LostFocus += new System.EventHandler(this.NamingRulesFocusExit);
            // 
            // btnGenClone
            // 
            this.btnGenClone.Location = new System.Drawing.Point(793, 564);
            // 
            // btnMetaDataXSLX
            // 
            this.btnMetaDataXSLX.Location = new System.Drawing.Point(10, 78);
            this.btnMetaDataXSLX.Name = "btnMetaDataXSLX";
            this.btnMetaDataXSLX.Size = new System.Drawing.Size(162, 23);
            this.btnMetaDataXSLX.TabIndex = 6;
            this.btnMetaDataXSLX.Text = "Choose Metadeta XLSX";
            this.btnMetaDataXSLX.UseVisualStyleBackColor = true;
            this.btnMetaDataXSLX.Click += new System.EventHandler(this.btnMetaDataXSLX_Click);
            // 
            // tbMetaDataXLSX
            // 
            this.tbMetaDataXLSX.Location = new System.Drawing.Point(179, 78);
            this.tbMetaDataXLSX.Name = "tbMetaDataXLSX";
            this.tbMetaDataXLSX.Size = new System.Drawing.Size(454, 20);
            this.tbMetaDataXLSX.TabIndex = 7;
            // 
            // GeneralMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1039, 599);
            this.Controls.Add(this.chkBoxDryRun);
            this.MinimumSize = new System.Drawing.Size(1055, 638);
            this.Name = "GeneralMainForm";
            this.Load += new System.EventHandler(this.GeneralMainForm_Load);
            this.gbSearchCriteria.ResumeLayout(false);
            this.gbSearchCriteria.PerformLayout();
            this.gbDefaults.ResumeLayout(false);
            this.gbDefaults.PerformLayout();
            this.gbFileSelection.ResumeLayout(false);
            this.gbFileSelection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private void InitializeCustomComponents()
        {
            this.lvSearchResultsSorter = new ListViewColumnSorter();
            this.lvSearchResults.ListViewItemSorter = lvSearchResultsSorter;
        }

        private System.Windows.Forms.TextBox tbMetaDataXLSX;
        private System.Windows.Forms.Button btnMetaDataXSLX;
    }
}
