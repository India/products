package com.atlassian.oauth.client.example;


import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.stream.Collectors;

public class PropertiesClient {
    public static final String CONSUMER_KEY = "consumer_key";
    public static final String PRIVATE_KEY = "private_key";
    public static final String REQUEST_TOKEN = "request_token";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String SECRET = "secret";
    public static final String JIRA_HOME = "jira_home";


    private final static Map<String, String> DEFAULT_PROPERTY_VALUES = ImmutableMap.<String, String>builder()
            .put(JIRA_HOME, "http://asingh.atlassian.net")
            .put(CONSUMER_KEY, "OauthKey")
            .put(PRIVATE_KEY, "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAOiYhYxSwSpJWEIP 3OyTU86HrBsB1T14Q0B2mtrBCgyHDFNc35D3OlT47FRWLovB8iqa/NAFtZZ7mbmu nKBJIprXz/ceINLA+HRElGXDbefhYJ8sABUqjOXmI/XmGOGjrT8aDNf7ZBdINBbZ Tv0HDfWH8dp/JDEdgjpMRoLLq0w9AgMBAAECgYEAnlamIz56rRS89kviM1Kf7KrD aKc4yN3DunCDPoPHGXwd0eoZb5A9r3a9OzRn/1AKUSKtE9evJEAECDlr+lmPKdl7 Ej6WHrRNU7m+Qsd54CTtyIP1hMJWcWi976tFvChnw9Cs+nWWPIyba7SMzwzsSZhX 6Z9VD9v0Yjb7/sdakgECQQD7ad4BAm2gQLAxEQNAj8G4bnOVFei0lyhXb/hc1Pa+ 0XXEUbtGYCWz/GF/4jHhZxpTsGrlAC8YZSIGOddoxZdBAkEA7NbF76LqPPALjjGu v81/zrtaADV3C6kI7Srnk0O22Tjbeewe3IM20larYGwqJhSIERhlrfTGwy5vNakF 2HmR/QJAMVhbEXmniJ+JtWewariMfiV3nBhbvy435cl9RsMp1tQHuEuOZUazv7rW bCI6RhJVcX2bzWyW0cPvxKcPp2m4wQJBANOHGy/lXLniVx+/xeiuvJjCgiavmcyD Q2K3sC66CsRF4NgrCIFAfCUunU+4K4jpS4GhPac5eJB4l4DgnkTyh20CQHXUhE8b k+kH21O5QGOF5wFUVVdFl0kMnPhtyMvVAfX4onNv/DSFj330nNPmTS6RRjV7a1RA sJUWG73iHNQu9Wo=")
			.build();

    private final String fileUrl;
    private final String propFileName = "config.properties";

    public PropertiesClient() throws Exception {
        fileUrl = "./" + propFileName;
    }

    public Map<String, String> getPropertiesOrDefaults() {
        try {
            Map<String, String> map = toMap(tryGetProperties());
            map.putAll(Maps.difference(map, DEFAULT_PROPERTY_VALUES).entriesOnlyOnRight());
            return map;
        } catch (FileNotFoundException e) {
            tryCreateDefaultFile();
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        } catch (IOException e) {
            return new HashMap<>(DEFAULT_PROPERTY_VALUES);
        }
    }

    private Map<String, String> toMap(Properties properties) {
        return properties.entrySet().stream()
                .filter(entry -> entry.getValue() != null)
                .collect(Collectors.toMap(o -> o.getKey().toString(), t -> t.getValue().toString()));
    }

    private Properties toProperties(Map<String, String> propertiesMap) {
        Properties properties = new Properties();
        propertiesMap.entrySet()
                .stream()
                .forEach(entry -> properties.put(entry.getKey(), entry.getValue()));
        return properties;
    }

    private Properties tryGetProperties() throws IOException {
        InputStream inputStream = new FileInputStream(new File(fileUrl));
        Properties prop = new Properties();
        prop.load(inputStream);
        return prop;
    }

    public void savePropertiesToFile(Map<String, String> properties) {
        OutputStream outputStream = null;
        File file = new File(fileUrl);

        try {
            outputStream = new FileOutputStream(file);
            Properties p = toProperties(properties);
            p.store(outputStream, null);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            closeQuietly(outputStream);
        }
    }

    public void tryCreateDefaultFile() {
        System.out.println("Creating default properties file: " + propFileName);
        tryCreateFile().ifPresent(file -> savePropertiesToFile(DEFAULT_PROPERTY_VALUES));
    }

    private Optional<File> tryCreateFile() {
        try {
            File file = new File(fileUrl);
            file.createNewFile();
            return Optional.of(file);
        } catch (IOException e) {
            return Optional.empty();
        }
    }

    private void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        } catch (IOException e) {
            // ignored
        }
    }
}
