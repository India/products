License Version: 2017-01-07
##########################################################################
#                                                                        #
#               CAD`N ORG Engineering and Consulting GmbH                #
#                              License File                              #
#                           license@cadnorg.de                           #
#                                                                        #
# Customer Name:  SMA Solar Technology AG                                #
# Contact Person: Christoph Setzkorn                                     #
# E-mail Address: Christoph.Setzkorn@SMA.DE                              #
# Created:        07.07.2017                                             #
##########################################################################
Module: cno_validation,cno_validation_batch
TC DB Host: svr-de-plmpt-t1
DB Name: plmpt1
TC Version: 10
TC Named User: 1200
Expire: 2017-08-31
Serial number: 595f9f948cb0e
License ID: 1707-10323-40a00

vswwEXRbuapFF2u1E2C5jQ==
FSk+hxZpD9xPgH5FxYzIQ9goHwaxKl7vR+9siYmMkDU=
qVxLYTmx5xTHkL5IaIdLIA==
RlNrK2h4WnBEOXhQZ0g1RnhZeklROWdvSHdheEtsN3ZSKzlzaVltTWtEVT0KIAtZrKZG9Lhj
ZV3nEY3xj5wVfZlkSBJgvd30Q4WEqoTpxEtd+IbgdWtJIaVZfBOIWbCXhJpZpN8GeZiFv+dx
YqnCOKhjNt1Vgl4Hzmsfc9SgFvo9SFK8R7Z8xPJDsTN9BbuQRsSLjMXPbX+I51TeEnyStmNr
8BnfVetnFVKJ91s=

